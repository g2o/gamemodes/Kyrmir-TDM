
// Ranged distance multipler element / bigger = easier
const RANGED_CHANCE_MAXDIST = 5000.0;
const RANGED_CHANCE_MINDIST = 1200.0;

// REWARD SYS
const LEVEL_CAP = 10;

const LP_PER_LEVEL = 20;
const HP_PER_LEVEL = 10;

// SWAP LEVEL BETWEEN TEAMS
const MAX_SWAP_LEVEL = 3;