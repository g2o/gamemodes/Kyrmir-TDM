
class VoiceCall 
{
//:public
    static function client_scream(playerId, packet)
    {
        if(isPlayerLogged(playerId))
        {
            local newPacket = Packet(PacketIDs.VoiceCall, VoiceCall_PacketIDs.Scream);

                newPacket.writeInt16(playerId);
                newPacket.writeInt8(packet.readInt8());

            local maxSlots = getMaxSlots();
            
            local playerVirutal = getPlayerVirtualWorld(playerId), 
                playerWorld = getPlayerWorld(playerId);
                
            for(local id = 0; id < maxSlots; id++)
            {
                if(isPlayerConnected(id) && isPlayerLogged(id) 
                    && playerWorld == getPlayerWorld(id) && playerVirutal == getPlayerVirtualWorld(id))
                {
                    newPacket.send(id, RELIABLE);
                }
            }
        }
    }

    static function packetRead(playerId, packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case VoiceCall_PacketIDs.Scream: client_scream(playerId, packet); break;
        }   
    } 
}