
class VoiceCall
{
    _lockVoiceCall = null;

//:protected 
    static function server_scream(packet)
    {
        local playerId = packet.readInt16(), voiceName = null;
        
        switch(packet.readInt8())
        {
            case 4: voiceName = "SVM_9_ENEMYKILLED.WAV"; break;
            case 5: voiceName = "SVM_9_KILLENEMY.WAV"; break;
            case 6: voiceName = "SVM_13_HELP.WAV"; break;
            case 7: voiceName = "SVM_13_ALARM.WAV"; break;
            case 8: voiceName = "SVM_13_RUNCOWARD.WAV"; break;
            case 9: voiceName = "SVM_13_RUNAWAY.WAV"; break;
        }

        if(voiceName)
        {
            local sound = Sound3d(voiceName);

            sound.setTargetPlayer(playerId);
            sound.play();
        }
    }   

//:public
    static function scream(voiceId)
    {
        if(isLogged())
        {
            if(_lockVoiceCall < getTickCount())
            {
                local packet = Packet(PacketIDs.VoiceCall, VoiceCall_PacketIDs.Scream);
                    packet.writeInt8(voiceId);
                packet.send(RELIABLE);

                _lockVoiceCall <- getTickCount() + 6000;
            }
            else 
                GUI.GameText(GUI.PositionPr(50, 7.5), "POP-UP_MSG_VOICECALL", "FONT_DEFAULT", 2000.0, true, 255, 0, 0);   
        }
    }

    static function key(keyId)
    {
        switch(keyId)
        {
            case KEY_NUMPAD4: scream(4); break;
            case KEY_NUMPAD5: scream(5); break;
            case KEY_NUMPAD6: scream(6); break;
            case KEY_NUMPAD7: scream(7); break;
            case KEY_NUMPAD8: scream(8); break; 
            case KEY_NUMPAD9: scream(9); break;
        }
    }

    static function packetRead(packet)
    {
        local packetType = packet.readInt8();

        switch(packetType)
        {
            case VoiceCall_PacketIDs.Scream: server_scream(packet); break;	
        }
    }
}