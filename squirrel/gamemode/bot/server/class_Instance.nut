
class Bot.Instance
{
    _npc = null;
    _template = null;
        
    _position = null;

    _aiState = false;

    _damageType = -1;
    _damage = 0;

    _protection = null;

    _warnTime = -1;
    _respawnTime = -1;

    _run = false;
    _fight = -1;
    
    _distanceToEnemy = 0;
    _enemyNumber = 0;

    _hardTrigger = false;

    _lastHitTime = 0;
    _routineTime = 0;

    _lastHitList = null;

    constructor(template, world = null, virtualWorld = 0)
    {
        _npc = createNpc(template.getIdentifier(), template.getInstance(), world, virtualWorld);
        _template = template;
        
        _position = { x = 0, y = 0, z = 0 };

        _aiState = false;

        _damageType = -1;
        _damage = 0;

        _protection = {};

        _warnTime = -1;
        _respawnTime = -1;

        _run = false;
        _fight = -1;

        _distanceToEnemy = 0;
        _enemyNumber = 0;

        _hardTrigger = false;

        _lastHitTime = 0;
        _routineTime = 0;

        _lastHitList = {};
    }

//:public
    function spawn()
    {
        if(_npc != null)
        {
            _npc.setPosition(_position.x, _position.y, _position.z);

            _npc.equipArmor(_template.getArmor())
                .equipHelmet(_template.getHelmet())
                .equipMeleeWeapon(_template.getMeleeWeapon())
                .equipRangedWeapon(_template.getRangedWeapon());
            
            _npc.setMaxHealth(_template.getHealth())
                .setHealth(_template.getHealth());

            _npc.setSkillWeapon(WEAPON_1H, _template.getSkillWeapon(WEAPON_1H))
                .setSkillWeapon(WEAPON_2H, _template.getSkillWeapon(WEAPON_2H))
                .setSkillWeapon(WEAPON_BOW, _template.getSkillWeapon(WEAPON_BOW))
                .setSkillWeapon(WEAPON_CBOW, _template.getSkillWeapon(WEAPON_CBOW));

            _npc.setAngle(rand() % 360);
            _npc.spawn();
        }

        return this;
    }

    function destroy()
    {
        if(_npc != null)
        {
            destroyNpc(_npc.getId());
        }
    }

    function getNpc() { return _npc; }
    
    function getId() { return _npc.getId() }

    function getTemplate() { return _template; }

    function getName() { return _npc.getName(); }
    
    function getIdentifier() { return _template.getIdentifier(); }

    function getInstance() { return _template.getInstance(); }
    
    function setGroup(group)
    {
        _npc.setGroup(group);
            return this;
    }

    function getGroup() { return _npc.getGroup(); }

    function setHealth(health)
    {
        _npc.setHealth(health);
    }

    function getHealth() { return _npc.getHealth(); }

    function setMaxHealth(maxHealth)
    {
        _npc.setMaxHealth(maxHealth);
    }

    function getMaxHealth() { return _npc.getMaxHealth(); }

    function isAlive() { return _npc.isAlive(); }

    function getWorld() { return _npc.getWorld(); }

    function getVirtualWorld() { return _npc.getVirtualWorld(); }

    function setPosition(x, y, z)
    {
        _npc.setPosition(x, y, z);
    }

    function getPosition() { return _npc.getPosition(); }

    function setAngle(angle)
    {
        _npc.setAngle(angle);
    }

    function getAngle() { return _npc.getAngle(); }

    function setTarget(targetId)
    {
        _npc.setTarget(targetId);
    }

    function getTarget() { return _npc.getTarget(); }

    function attackFist(attackTime) 
    {
        _npc.attackFist(attackTime);
    }

    function isAttackingWithFist() { return _npc.isAttackingWithFist(); }

    function attackMelee(pattern, attackTime)
	{	
        _npc.attackMelee(pattern, attackTime);
	}

    function isAttackingWithMelee() { return _npc.isAttackingWithMelee(); }

    function attackRanged(attackTime) 
    {
        _npc.attackRanged(attackTime);
    }
    
    function isAttackingWithRanged() { return _npc.isAttackingWithRanged(); }

    function attackMagic(attackTime) 
    {
        _npc.attackMagic(attackTime);
    }

    function isAttackingWithMagic() { return _npc.isAttackingWithMagic(); }

    function isInAttackMode() { return _npc.isInAttackMode(); }

    function stopAttack() 
    {
        _npc.stopAttack();
    }

    function follow(distance)
    {
        _npc.follow(distance);
    }

    function isObstacle() { return _npc.isObstacle(); }

    function isFollowing() { return _npc.isFollowing(); }
    
    function parade(paradeType)
    {
        _npc.parade(paradeType);
    }
    
    function setSkillWeapon(skillType, skill) 
    {
        _npc.setSkillWeapon(skillType, skill);
    }

    function getSkillWeapon(skillType) { return _npc.getSkillWeapon(skillType); }

    function setActiveSpell(spellId)
    {
        _npc.setActiveSpell(spellId);
    }

    function getActiveSpell() { return _npc.getActiveSpell(); }

    function playAni(animation)
    {
        _npc.playAni(animation);
    }

    function stopAni() 
    {
        _npc.stopAni();
    }

    function getLastAni() { return _npc.getLastAni(); }
    
    function setWeaponMode(weaponMode)
    {
        _npc.setWeaponMode(weaponMode);
    }

    function getWeaponMode() { return _npc.getWeaponMode(); }

    function setRespawnPosition(x, y, z)
    {
        _position.x = x;
        _position.y = y;
        _position.z = z;
        
        return this;
    }

    function getRespawnPosition() { return _position; }

    function setDamage(damageType, damage) 
    {
        _damageType = damageType;
        _damage = damage;
            return this;
    }

    function getDamage() { return _damage; }

    function getDamageType() { return _damageType; }

    function setProtection(protectionType, protection)
    {
        _protection[protectionType] <- protection;
            return this;
    }

    function getProtection(protectionType) { return protectionType in _protection ? _protection[protectionType] : 0; }
    
    function setAIState(aiState)
    {
        _aiState = aiState;
            return this;
    }

    function getAIState() { return _aiState; }

    function setWarnTime(warnTime)
    {
        _warnTime = warnTime;
    }

    function getWarnTime() { return _warnTime; }

    function startRespawnTime(respawnTime)
    {
        _respawnTime = respawnTime + _template.getRespawnTime();
    }

    function setRespawnTime(respawnTime)
    {
        _respawnTime = respawnTime;
    }

    function getRespawnTime() { return _respawnTime; }

    function setRun(run)
    {
        _run = run;
    }

    function isRunning() { return _run; }

    function setFight(fight)
    {   
        _fight = (fight == false ? -1 : fight); 
    }

    function isFighting() { return _fight != -1; }

    function getFight() { return _fight; }

    function setDistanceToEnemy(distance)
    {
        _distanceToEnemy = distance;
    }

    function getDistanceToEnemy() { return _distanceToEnemy; }

    function setEnemyNumber(enemyNumber)
    {
        _enemyNumber = enemyNumber;
    }

    function getEnemyNumber() { return _enemyNumber; }

    function isHardTriggered() { return _hardTrigger; }
    
    function setLastHitTime(killerId, lastHitTime)
    {
        _warnTime = 0;

        if(_aiState && _npc.getTarget() == -1)
        {
            _npc.setTarget(killerId);
                _fight = SKIP_WARNING_STATE;

            _hardTrigger = true;
        } 

        if(killerId in _lastHitList) 
        {
            _lastHitList[killerId] = lastHitTime;
        }
        else 
            _lastHitList[killerId] <- lastHitTime;

        _lastHitTime = lastHitTime;
    }

    function removeLastHit(playerId)
    {
        if(playerId in _lastHitList) delete _lastHitList[playerId];
    }

    function getLastHitList(fromTime = 8000) 
    { 
            local tick = getTickCount();
        _lastHitList = _lastHitList.filter(@(playerId, timeValue) timeValue + fromTime >= tick);

        return _lastHitList; 
    }

    function getTimeFromLastHit() { return getTickCount() - _lastHitTime; }

    function setRoutineTime(routineTime)
    {
        _routineTime = routineTime + _template.getRoutineFreq();
    }

    function getRoutineTime() { return _routineTime; }

    function respawn()
    {
        _warnTime = -1;
        _respawnTime = -1;

        _run = false;
        _fight = -1;

        _distanceToEnemy = 0;
        _enemyNumber = 0;

        _hardTrigger = false;

        _lastHitTime = 0;
        _routineTime = 0;

        _npc.setHealth(_npc.getMaxHealth());
        _npc.setPosition(_position.x, _position.y, _position.z);

        _lastHitList.clear();
    }

    function getStreamList() { return _npc.getStreamList(); }
}