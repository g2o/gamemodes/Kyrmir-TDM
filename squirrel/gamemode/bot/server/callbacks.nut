
addEventHandler("onPlayerDisconnect", function(playerId, reason) 
{ 
    if(isPlayerLogged(playerId) == false) return;
    
    getBotManager().playerDisconnect(playerId);
});

addEventHandler("Damage.onNpcDead", Bot.Manager.npcDead.bindenv(Bot.Manager));

setTimer(Bot.Manager.actionLoop.bindenv(Bot.Manager), 500, 0);

