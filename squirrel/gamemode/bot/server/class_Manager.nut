
class Bot.Manager
{
    static _botList = {};
    static _templateList = {};

//:public
    static function registerTemplate(instance, template)
    {
        if(instance in _templateList == false)
        {
            _templateList[instance] <- template;
        }
    }

    static function spawn(template, x, y, z, world, virtualWorld = 0, aiState = true)
    {
        local bot = Bot.Instance(template, world, virtualWorld)
            .setGroup(template.getGroup())
            .setDamage(template.getDamageType(), template.getDamage())
            .setProtection(DAMAGE_BLUNT, template.getProtection(DAMAGE_BLUNT))
            .setProtection(DAMAGE_EDGE, template.getProtection(DAMAGE_EDGE))
            .setProtection(DAMAGE_POINT, template.getProtection(DAMAGE_POINT))
            .setProtection(DAMAGE_FIRE, template.getProtection(DAMAGE_FIRE))
            .setProtection(DAMAGE_MAGIC, template.getProtection(DAMAGE_MAGIC))
            .setRespawnPosition(x, y, z)
            .setAIState(aiState)
            .spawn();
        
        _botList[bot.getId()] <- bot;

        local routineFunction = template.getRoutineFunction();
        if(routineFunction) routineFunction(bot)

        return bot;
    }

    static function unspawn(botId)
    {
        if(botId in _botList) _botList[botId].unspawn();
    }

    static function destroy(botId)
    {
        foreach(index, bot in _botList)
        {
            if(bot.getId() == botId)
            {
                _botList.remove(index);
                    bot.destroy();            

                break;
            }
        }
    }

    static function resetAll()
    {
        foreach(index, bot in _botList)
        {
            if(bot.getRespawnTime() != -1)
            {
                bot.respawn();
            }
        }    
    }

    static function playerDisconnect(playerId)
    {
        foreach(bot in _botList)
        {
            bot.removeLastHit(playerId);
        }
    }

    static function hit(killerId, botId)
    {   
        if(botId in _botList) 
        { 
            _botList[botId].setLastHitTime(killerId, getTickCount());
        }
    }

    static function npcDead(botId, killerId, hitList)
    {
        if(botId in _botList)
        {
                local bot = _botList[botId];
            bot.startRespawnTime(getTickCount());

            callEvent("Bot.onMonsterDead", bot, killerId, hitList);
        }
    }

    static function getNearestTarget(bPosition, detectionDistance, stream, ignoreLevel = -1)
    {
        local minDistance = 7500, closestId = -1, enemyNumber = 0;

        foreach(playerId in stream)
        {
            if(isPlayerDead(playerId) || getPlayerInvisible(playerId)) continue;
            if(ignoreLevel != -1 && getPlayerLevel(playerId) >= ignoreLevel) continue;
         
            local pPosition = getPlayerPosition(playerId);
            local distance = getDistance3d(bPosition.x, bPosition.y, bPosition.z, pPosition.x, pPosition.y, pPosition.z);

            if(distance <= detectionDistance) 
            {
                enemyNumber++;
                
                if(distance < minDistance)
                {
                    minDistance = distance;
                    closestId = playerId;
                }
            }
        }

        return [ closestId, enemyNumber, minDistance ];
    }

    static function detectTarget(bot)
    {
        local targetId = bot.getTarget(), template = bot.getTemplate();
        local bPosition = bot.getPosition();

        if(targetId == -1)
        {
            local nerestData = getNearestTarget(bPosition, template.getDetectionDistance(), bot.getStreamList(), 10);
                
            local nerestId = nerestData[0], nearestNumber = nerestData[1], 
                nearestDistance = nerestData[2];

            if(nerestId != -1)
            {
                local pPosition = getPlayerPosition(nerestId);
                   
                bot.setTarget(targetId = nerestId);

                bot.setDistanceToEnemy(nearestDistance);
                bot.setEnemyNumber(nearestNumber);
            }
        }
        else 
        {
            local state = bot.getFight();

            if(state == WARNING_STATE)
            {
                local nerestData = getNearestTarget(bPosition, template.getDetectionDistance(), bot.getStreamList());
                
                local nerestId = nerestData[0], nearestNumber = nerestData[1], 
                    nearestDistance = nerestData[2];

                if(nerestId != -1)
                {
                    bot.setTarget(targetId = nerestId);

                    bot.setDistanceToEnemy(nearestDistance);
                    bot.setEnemyNumber(nearestNumber);
                } 
                else
                {
                    bot.setTarget(targetId = -1);

                    bot.stopAni();
                    bot.setWeaponMode(WEAPONMODE_NONE);

                    bot.setWarnTime(-1);
                }
            }
            else 
            {
                local nerestData = getNearestTarget(bPosition, bot.isHardTriggered() ? 5000 : template.getChaseDistance(), bot.getStreamList());
                
                local nerestId = nerestData[0], nearestNumber = nerestData[1], 
                    nearestDistance = nerestData[2];

                if(nerestId != -1)
                {
                    if(nerestId != targetId) 
                    {
                        bot.setDistanceToEnemy(nearestDistance);
                        bot.setEnemyNumber(nearestNumber);

                        bot.setTarget(targetId = nerestId);
                    }
                    else 
                    {
                        if(isPlayerDead(targetId))
                        {
                            bot.setTarget(targetId = -1);

                            bot.stopAni();
                            bot.setWeaponMode(WEAPONMODE_NONE);
                            
                            bot.respawn();   
                        }
                        else 
                        {
                            bot.setDistanceToEnemy(nearestDistance);
                            bot.setEnemyNumber(nearestNumber);
                        }
                    }
                }
                else 
                {
                    bot.setTarget(targetId = -1);

                    bot.stopAni();
                    bot.setWeaponMode(WEAPONMODE_NONE);
                    
                    bot.respawn();   
                }
            }
        }

        return targetId != -1;
    }

    static function takeInitiative(template, bot)
    {
        local initiativeFunction = template.getInitiativeFunction();

        if(initiativeFunction)
        {
            initiativeFunction(bot);
        }
    }

    static function actionLoop()
    {
        local tickCount = getTickCount();

        foreach(bot in _botList)
        {
            local template = bot.getTemplate();

            if(bot.isAlive() == false)
            {		
                if(bot.getRespawnTime() < tickCount)
                {
                    bot.setRespawnTime(-1);

                    if(template.checkRespawnRequirement())
                    {
                            bot.respawn();
                        callEvent("Bot.onMonsterRespawn", bot);
                    }
                }
            }
            else 
            {
                if(bot.getAIState() == false) continue;

                if(detectTarget(bot)) 
                {
                    takeInitiative(template, bot);
                }
                else 
                {
                    if(bot.getRoutineTime() < tickCount)
                    {
                        local routineFunction = template.getRoutineFunction();

                        if(routineFunction) 
                        {
                            bot.setRoutineTime(tickCount)
                                routineFunction(bot)
                        }
                    }
                }
            }			
        }
    }

    static function getTemplates() { return _templateList; }

    static function getBotById(botId)
    {
        return botId in _botList ? _botList[botId] : null;
    }

    static function getBots() { return _botList; }
}

getBotManager <- @() Bot.Manager;

// Bot
registerMonsterTemplate <- @(instance, template) Bot.Manager.registerTemplate(instance, template);
getMonsterTemplates <- @() Bot.Manager.getTemplates();

spawnBot <- @(template, x, y, z, world, virtualWorld = 0) Bot.Manager.spawn(template, x, y, z, world, virtualWorld);
unspawnBot <- @(botId) Bot.Manager.unspawn(botId); 
destroyBot <- @(botId) Bot.Manager.destroy(botId); 

resetAIState <- @() Bot.Manager.resetAll(); 

getBot <- @(botId) Bot.Manager.getBotById(botId);
getBots <- @() Bot.Manager.getBots();

