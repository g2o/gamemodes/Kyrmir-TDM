
enum MySQLOrder 
{
    ASC = "ASC",
    DESC = "DESC"
}

enum MySQLCondition 
{
    Equal = "=",
    NotEqual = "!=",
    Greater = ">",
    GreaterEqual = ">=",
    Less = "<",
    LessEqual = "<=", 
    AND = "AND",
    OR = "OR"
}