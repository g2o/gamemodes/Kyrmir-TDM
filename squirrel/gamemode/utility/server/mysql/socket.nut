
class MySQL.Socket
{
	_host = null;
	_user = null;
	_password = null;
	_base = null;

    _connection = null;

    constructor(host, user, password, baseName)
    {
        _host = host;
        _user = user;
        _password = password;
        _base = baseName;
    }

//:public 
    function connect()
    {
        local connection = mysql_connect(_host, _user, _password, _base, 3306);

        if(_connection = connection)
        {
                mysql_query(_connection, "SET NAMES utf8;")
            print("+ MySQL.Socket: Server succesfully connected with database!"); 
        }
        else
        {
            print("+ MySQL.Socket: Server cannot connect with database! ");
                exit();
        }

		return connection;
    }

    function checkConnection()
    {
		if(!mysql_ping(_connection))
		{
			local connection = mysql_connect(_host, _user, _password, _base);

			if(_connection = connection)
            {       
                    mysql_query(_connection, "SET NAMES utf8;")
            	print("+ MySQL.Socket: Server succesfully reconnected with database!"); 
            }
            else
            {
				print("+ MySQL.Socket: Server cannot connect with database!");
                    exit();
            }
            
            return _connection != null;
		}

		return true;
    }

    function query(sql) { return mysql_query(_connection, sql); }

    function getLastError()
    {
        return _connection != null ? mysql_error(_connection) : null;
    }

    function getLastInsertedId()
    {
        return _connection != null ? mysql_insert_id(_connection) : null;
    }

    function getConnection() { return _connection; }
}