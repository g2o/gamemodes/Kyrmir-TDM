
class MySQL.QueryUpdate
{
    _result = null;

    _table = null;
    sUpdates = null;

    _where = null;

    _connection = null;

    constructor(connection, table, ...)
    {
        _connection = connection;
        
        local updates = "",
            lastArgument = vargv.len() - 1;

        foreach(i, arg in vargv) 
        {
            if(i == lastArgument)
            {
                if(arg[1] != null)
                    updates += ("`" + arg[0] +"` = '" + arg[1] + "'"); 
                else 
                    updates += ("`" + arg[0] +"` = NULL"); 
            }
            else 
            {
                if(arg[1] != null)
                    updates += ("`" + arg[0] +"` = '" + arg[1] + "', "); 
                else 
                    updates += ("`" + arg[0] +"` = NULL, "); 
            }
        }   

        _table = table;
        sUpdates = updates;

        _where = null;    
    }

//:public
    function generateQuery()
    {
        return "UPDATE `" + _table + "` SET " + sUpdates + (_where != null ? _where : "") + ";"
    }

    function where(...)
    {
        local where = "",
            lastIndex = vargv.len() - 1;

        foreach(i, arg in vargv)
        {
            if(i == lastIndex)
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "'"); 
            else 
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "' " + vargv[i][3] + " "); 
        }

        _where = " WHERE " + where;
            return this;
    }

    function exec()
    {
        _result = _connection.query(generateQuery());
            return this;
    }

    function getQuery() { return generateQuery(); }

    function getResult() { return _result; }
}