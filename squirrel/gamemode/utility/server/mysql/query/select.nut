
class MySQL.QuerySelect
{
    _result = null;

    _table = null;
    _columns = null;

    _where = null;
    sLimit = null;
    sOrder = null;

    _connection = null;
    
    constructor(connection, table, ...)
    {
        _connection = connection;

        local columns = "",
            lastIndex = vargv.len() - 1;

        if(vargv.len() != 1)
        {
            foreach(i, column in vargv) 
            {
                if(typeof(column) != "array")
                {
                    if(i == lastIndex)
                        columns += ("`" + column + "`"); 
                    else 
                        columns += ("`" + column + "`, "); 
                }
                else
                {
                    if(i == lastIndex)
                        columns += ("`" + column[0] + "` AS '" + column[1] + "'"); 
                    else 
                        columns += ("`" + column[0] + "` AS '" + column[1] + "', "); 
                }
            }    
        }
        else 
			columns = (vargv[0] == "*" ? vargv[0] : ("`" + vargv[0] + "`"));

        _table = table;
        _columns = columns;
        
        _where = null;    
    }

//:protected 
    function generateQuery()
    {
        return "SELECT " + _columns + " FROM `" + _table + "`" + (_where != null ? _where : "") + (sOrder != null ? sOrder : "") + (sLimit != null ? sLimit : "") + ";";
    }

//:public
    function limit(start, end = -1)
    {
        if(end == -1)
            sLimit = " LIMIT " + start;
        else 
            sLimit = " LIMIT " + end + " OFFSET " + start;

        return this;       
    }

    function order(column, orderType)
    {
        sOrder = " ORDER BY `" + column + "` " + orderType;
            return this;
    }

    function where(...)
    {
        local where = "",
            lastIndex = vargv.len() - 1;

        foreach(i, arg in vargv)
        {
            if(i == lastIndex)
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "'"); 
            else 
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "' " + vargv[i][3] + " "); 
        }

        _where = " WHERE " + where;
            return this;
    }

    function exec()
    {
        _result = _connection.query(generateQuery());
            return this;
    }

    function assoc()
    {
        return _result != null ? mysql_fetch_assoc(_result) : null;
    }

    function row()
    {
        return _result != null ? mysql_fetch_row(_result) : null;
    }

    function row_num()
    {
        return _result != null ? mysql_num_rows(_result) : null;
    }

    function getQuery() { return generateQuery(); }

    function getResult() { return _result; }

    function free()
    {
        if(_result != null)
            mysql_free_result(_result);

        return this;
    }
}