
local eventName = null;

if(SERVER_SIDE)
	eventName = "onPlayerCommand"
else
	eventName = "onCommand"

local commands = {};

addEventHandler(eventName, function(...)
{
	local command = SERVER_SIDE ? vargv[1] : vargv[0];

    if(command in commands)
    {
        local object = commands[command];

        if(SERVER_SIDE)
        {
            if(object.permission == null || object.permission(vargv[0]))
            {
                object.func(vargv[0], vargv[2]);
            }
            else 
                sendServerMessageToPlayer(vargv[0], 128, 0, 0, "CHAT_NO_PERMISSION");
        } 
        else 
        {
            if(object.permission == null || object.permission())
            {     
                object.func(vargv[1]);
            }
            else 
                serverMessage(ALL_CHATS, 128, 0, 0, "CHAT_NO_PERMISSION");     
        }
    }
});

function addCommand(commmand, func, permission = null)
{
    if(commmand in commands == false)
    {
        commands[commmand] <- { func = func, permission = permission };
    }
    else 
        print("addCommand: You have already registred this command!");
}

function getChatCommands() { return commands; }