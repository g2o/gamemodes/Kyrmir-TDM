
addEventHandler("Player.onPlayerLogin", function(playerId, logged)
{    
    if(logged) getChatManager().playerLogin(playerId);
});

addEventHandler("onExit", Chat.Manager.exit.bindenv(Chat.Manager));

addEventHandler("onPlayerMessage", Chat.Manager.playerMessage.bindenv(Chat.Manager));