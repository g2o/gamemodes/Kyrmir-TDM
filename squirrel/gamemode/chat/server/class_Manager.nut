
class Chat.Manager 
{
    static _chatList = {};
    
//:protected
    static function _listToPacket(playerId)
    {
        local packet = Packet(PacketIDs.Chat, Chat_PacketIDs.List);
        local chatList = getPermitedChats(playerId);
        
        packet.writeInt8(chatList.len());

        foreach(chat in chatList)
        {
            packet.writeInt8(chat.getPrefix());
            packet.writeString(chat.getInstance());
        }

        return packet;
     }

//:public   
    static function exit()
    {
        foreach(chat in _chatList)
        {
            chat.close();
        }
    }

    static function playerLogin(playerId)
    {
        _listToPacket(playerId).send(playerId, RELIABLE);
    }
    
    static function addChat(chat)
    {
        _chatList[chat.getPrefix()] <- chat;
    }

    static function givePermissionToChat(playerId, prefix)
    {
        if(prefix in _chatList)
        {
            local packet = Packet(PacketIDs.Chat, Chat_PacketIDs.Add);
            
            local chat = _chatList[prefix];

                packet.writeInt8(chat.getPrefix());
                packet.writeString(chat.getInstance());

            packet.send(playerId, RELIABLE);
        }
    }
    
    static function removePermissionToChat(playerId, prefix)
    {
        if(prefix in _chatList)
        {
            local packet = Packet(PacketIDs.Chat, Chat_PacketIDs.Remove);
                packet.writeInt8(prefix);
            packet.send(playerId, RELIABLE);
        }
    }

    static function getPermitedChats(playerId)
    {
        local chats = [];

        foreach(chat in _chatList)
        {
            if(chat.checkPermission(playerId) == false)
                continue;
            
            chats.push(chat);
        }

        return chats;
    }

    static function sendServerMessageToPlayer(playerId, r, g, b, message)
    {
        sendMessageToPlayer(playerId, r, g, b, ALL_CHATS + message);
    }

    static function sendServerMessageToAll(r, g, b, message)
    {
        local players = getPlayers();
        
        foreach(player in players)
        {   
            if(player.isLogged())
            {
                sendMessageToPlayer(player.getId(), r, g, b, ALL_CHATS + message);
            }
        }
    }

    static function playerMessage(playerId, message)
    {
        if(isPlayerMuted(playerId))
        {
            sendServerMessageToPlayer(playerId, 128, 0, 0, "CHAT_MUTED");
            return;
        }

        if(message[0] == '/') 
            return;

        local prefix = message[0].tochar().tointeger();

        if(prefix in _chatList)
        {
            local chat = _chatList[prefix];

            if(chat.checkPermission(playerId))
            {
                local distance = chat.getDistance(), color = chat.getColor(), 
                    players = getPlayers();

                chat.writeLog(getPlayerName(playerId), message.slice(2));
                
                if(distance != -1)
                {
                    local senderPosition = getPlayerPosition(playerId);
                    
                    foreach(player in players)
                    {
                        local receiverId = player.getId();

                        if(isPlayerLogged(playerId) && chat.checkReceiver(playerId, receiverId))
                        {
                            local position = getPlayerPosition(receiverId);
                
                            if(getDistance3d(senderPosition.x, senderPosition.y, senderPosition.z, position.x, position.y, position.z) <= distance)
                            {
                                sendPlayerMessageToPlayer(playerId, receiverId, color.r, color.g, color.b, message);
                            }
                        }
                    }
                }
                else 
                {
                    foreach(player in players)
                    {
                        local receiverId = player.getId();
   
                        if(isPlayerLogged(playerId) && chat.checkReceiver(playerId, receiverId))
                        {
                            sendPlayerMessageToPlayer(playerId, player.getId(), color.r, color.g, color.b, message);
                        }
                    }
                }
            }
        }
    }
}

getChatManager <- @() Chat.Manager;

// Chat
sendServerMessageToPlayer <- @(playerId, r, g, b, message) Chat.Manager.sendServerMessageToPlayer(playerId, r, g, b, message);
sendServerMessageToAll <- @(r, g, b, message) Chat.Manager.sendServerMessageToAll(r, g, b, message);

givePermissionToChat <- @(playerId, prefix) Chat.Manager.givePermissionToChat(playerId, prefix);
removePermissionToChat <- @(playerId, prefix) Chat.Manager.removePermissionToChat(playerId, prefix);