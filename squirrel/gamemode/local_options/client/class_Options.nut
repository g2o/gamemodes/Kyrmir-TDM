
class LocalOptions 
{
    static optionList = {};

    static function loadOptions()
    {
        // ARMORS/ASC
        setLODStrengthOverride(0);
        
        optionList["USERNAME"] <- null;
        optionList["PASSWORD"] <- null;

        optionList["MUSIC_SYSTEM"] <- isMusicSystemDisabled();
        optionList["SOUND_VOLUME"] <- getSoundVolume();
        optionList["MUSIC_VOLUME"] <- getMusicVolume();

        optionList["WEAPON_TRAIL"] <- false;
        
        optionList["CHAT_LINES"] <- 15;
        optionList["SAVE_LOGIN"] <- false;
        
        local music = LocalStorage.getItem("MUSIC_SYSTEM");
        if(music) disableMusicSystem(optionList["MUSIC_SYSTEM"] <- music); 

        local soundVolume = LocalStorage.getItem("SOUND_VOLUME");
        if(soundVolume) setSoundVolume(optionList["SOUND_VOLUME"] <- soundVolume); 

        local musicVolume = LocalStorage.getItem("MUSIC_VOLUME");
        if(musicVolume) setMusicVolume(optionList["MUSIC_VOLUME"] <- musicVolume); 

        local weaponTrail = LocalStorage.getItem("WEAPON_TRAIL");
        if(weaponTrail) enable_WeaponTrail(optionList["WEAPON_TRAIL"] <- weaponTrail); 

        local chatLines = LocalStorage.getItem("CHAT_LINES");
        if(chatLines) ch_setMaxLines(optionList["CHAT_LINES"] <- chatLines); 

        local saveLogin = LocalStorage.getItem("SAVE_LOGIN");
        if(saveLogin) optionList["SAVE_LOGIN"] <- saveLogin;    

        local username = LocalStorage.getItem("USERNAME");
        if(username) optionList["USERNAME"] <- username;    

        local password = LocalStorage.getItem("PASSWORD");
        if(password) optionList["PASSWORD"] <- password;    
    }

    function music(system)
    {
            disableMusicSystem(optionList["MUSIC_SYSTEM"] <- system)
        LocalStorage.setItem("MUSIC_SYSTEM", system);
    }

    function isMusicDisabled() { return optionList["MUSIC_SYSTEM"]; }
    
    function soundVolume(volume)
    {
            setSoundVolume(optionList["SOUND_VOLUME"] = volume); 
        LocalStorage.setItem("SOUND_VOLUME", volume);
    }

    function getSound() { return optionList["SOUND_VOLUME"]; }

    function musicVolume(volume)
    {
            setMusicVolume(optionList["MUSIC_VOLUME"] = volume); 
        LocalStorage.setItem("MUSIC_VOLUME", volume);
    }

    function getMusic() { return optionList["MUSIC_VOLUME"]; }

    function weaponTrail(trail)
    {
            enable_WeaponTrail(optionList["WEAPON_TRAIL"] <- trail);
        LocalStorage.setItem("WEAPON_TRAIL", trail);
    }

    function isWeaponTrailEnabled() { return optionList["WEAPON_TRAIL"]; }

    function chatLines(lines)
    {
            ch_setMaxLines(optionList["CHAT_LINES"] <- lines);
        LocalStorage.setItem("CHAT_LINES", lines);
    }

    function getChatLines() { return optionList["CHAT_LINES"]; }

    function saveLogin(save)
    {
            optionList["SAVE_LOGIN"] <- save;
        LocalStorage.setItem("SAVE_LOGIN", save);

        if(save == false)
        {
            LocalStorage.removeItem("USERNAME");
            LocalStorage.removeItem("PASSWORD");            
        }
    }

    function isLoginSaved() { return optionList["SAVE_LOGIN"]; }

    function saveLoginData(username, password)
    {
        LocalStorage.setItem("USERNAME", optionList["USERNAME"] <- username);
        LocalStorage.setItem("PASSWORD", optionList["PASSWORD"] <- password);
    }

    function getUsername() { return optionList["USERNAME"]; }

    function getPassword() { return optionList["PASSWORD"]; }
}

getLocalOptions <- @() LocalOptions;