
setTimer(GroundDrop.Manager.checkItem.bindenv(GroundDrop.Manager), 1000, 0);

addEventHandler("onPlayerTakeItem", function(playerId, itemGround)
{   
    if(getGroundDropManager().playerTakeItem(playerId, itemGround) == false) 
    {
        cancelEvent();
    }
});

addEventHandler("onPlayerDropItem", function(playerId, itemGround)
{   
    cancelEvent();
});

 