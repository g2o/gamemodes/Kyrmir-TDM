
local scene = getSceneManager();

addEventHandler("onInit", clearMultiplayerMessages);

addEventHandler("onMouseClick", function(button)
{
	scene.mouseButton(button, MouseButton.Press);
});

addEventHandler("onMouseRelease", function(button)
{
	scene.mouseButton(button, MouseButton.Release);
});

addEventHandler("onMouseWheel", scene.mouseWheel.bindenv(scene));

addEventHandler("onKey", scene.keyBoard.bindenv(scene));

addEventHandler("onRender", scene.render.bindenv(scene));