
GUI <- {};

function GUI::isCursorIn(x, y, width, height, cursor_x, cursor_y)
{
	if(x <= cursor_x && x + width >= cursor_x
		&& y <= cursor_y && y + height >= cursor_y)
			return true;

	return false;
}