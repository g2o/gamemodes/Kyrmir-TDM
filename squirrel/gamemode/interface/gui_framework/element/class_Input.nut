
class GUI.Input extends GUI.Button
{
	_inputText = "";
	
	_hash = false;
	_placeHolder = true;	

	_type = 0;
	_margin = 0;
		
	_active = false;	

    constructor(text, position, size, hash = false)
    {
		_inputText = "";
	
		_hash = hash;
		_placeHolder = true;

		_type = 0;
		_margin = 150;

		_active = false;	

        base.constructor(text, position, size);
    }

//:public
    function destroy()
    {		
		setVisible(false);
        	base.destroy();
    }

	function setHash(hash)
	{
		_hash = hash;
			return this;
	}

	function isHash()
	{
		return _hash;
	}

	function setType(typeId)
	{
		if(!_visible)
			_type = typeId;

		return this;
	}

	function setInputText(text)
	{
		if(_visible)
		{
			_placeHolder = false;

			_text =
				_inputText = text;
				
			_textAdjust();
		}

		return this;
	}

	function getInputValue()
	{
		if(_inputText == "") 
			return null;

		return _type == GUIInputType.Number ? _inputText.tointeger() : _inputText;
	}

	function setMargin(margin)
	{
		_margin = margin;

		if(_visible)
			_textAdjust();

		return this;
	}

	function setVisible(visible)
	{
		if(_visible != visible)
		{
			base.setVisible(visible);

			if(visible == false)
			{		
				_inputText = "";

				_placeHolder = true;
				_active = false;
			}
			else 
			{
				_inputText = "";
				_textAdjust();
			}
		}
		
		return this;
	}

	function isDefaultText() { return _placeHolder; }

	function setActive(active)
	{
		if(_visible)
		{					
			if(active)
			{
				if(_placeHolder) 
				{
					_draw.text = "|";
					_placeHolder = false;
				}
				else 
					_draw.text = _draw.text + "|";
			}
			else
			{
					local text = _draw.text;
				_draw.text = text.slice(0, text.len() - 1);
			}

			if(active != _active)
				callEvent(GUIEventInterface.Active, _active = active);

			_textAdjust();
		}
	}

	function isActive() { return _active; }

	function keyBoard(key)
	{
		if(_active)
		{
			if(_addCharacter(key) == false)			
			{
				switch(key)
				{
					case KEY_RETURN: setActive(false); break;
					case KEY_BACK: _removeCharacter(); break;
					case KEY_DELETE: _clearInput(); break;
				}
			}

			_textAdjust();
		}
	}

	function mouse(cursorPosition, button, event)
	{
		local position = _texture ? _texture.getPosition() : _draw.getPosition();
		local size = _texture ? _texture.getSize() : _draw;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
				setActive(!_active);
			return callEvent(GUIEventInterface.Click);
		}
		else 
			if(_active) setActive(false);

		return false;
	}
	
//:private
	function _textAdjust()
	{		
		textSetFont(getLanguageFont(_fontFile));

		local margin = _size.getWidth() - _margin * 2;
			_draw.text = "";

		if(_placeHolder)
		{
			local translation = getTranslation(_text);
			local textLoop = translation.len() - 1;
			
			for(local i = textLoop; i >= 0; i--)
			{
				_draw.text = translation[i].tochar() + _draw.text; 
				if(margin <= _draw.width) break;
			}
		}
		else 
		{
			local textLoop = _inputText.len() - 1;

			if(_hash)
			{
				for(local i = textLoop; i >= 0; i--)
				{
					_draw.text = "#" + _draw.text; 
					if(margin <= _draw.width) break;
				}
			}
			else 
			{
				for(local i = textLoop; i >= 0; i--)
				{
					_draw.text = _inputText[i].tochar() + _draw.text; 
					if(margin <= _draw.width) break;
				}
			}

			if(_active) _draw.text += "|";
		}

		base._textAdjust();
	}

	function _addCharacter(key)
	{
		local character = getKeyLetter(key);

		if(character)
		{
			if(GUIInputType.Number == _type)
			{
				local charCode = character[0];

				if(!(charCode >= 48 && 57 >= charCode)) 
					return;

				callEvent(GUIEventInterface.Input, (_inputText += character).tointeger())
			}
			else
				callEvent(GUIEventInterface.Input, _inputText += character)
			
			return true;
		}		

		return false;
	}

	function _removeCharacter()
	{
		local inputLength = _inputText.len();
		
		if(inputLength != 0)
		{
			_inputText = _inputText.slice(0, inputLength - 1);

			if(_inputText != "") 
				callEvent(GUIEventInterface.Input, GUIInputType.Number == _type ? _inputText.tointeger() : _inputText);
			
			return true;
		}

		return false;
	}
	
	function _clearInput()
	{
		callEvent(GUIEventInterface.Input, _draw.text = (_inputText = "") + "|");
	}
}