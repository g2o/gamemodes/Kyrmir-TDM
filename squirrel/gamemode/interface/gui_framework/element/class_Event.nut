
class GUI.Event
{
	_eventList = null;
	
	constructor()
	{
		_eventList = [];
	}

//:public
	function destroy()
	{
		_eventList = null;
	}

	function addEvent(action, func, param = null)
	{
		_eventList.push({
			object = this,
			action = action,
			func = func,
			param = param
		});

		return this;
	}

	function removeEvent(func)
	{
		foreach(i, table in _eventList)
		{
			if(table.func == func)
			{
				_eventList.remove(i);
				break;
			}
		}

		return this;
	}
	
	function callEvent(action, var = null)
	{	
		local lockEvent = false;

		foreach(event in _eventList)
		{
			local object = event.object;

			if(object == this && event.action == action)
			{			
				local param = event.param;

				if(param != null)
				{
					if(typeof(param) == "array")
					{ 
						local table = [ object ];
						if(var) table.push(var);

						table.extend(param);

						event.func.acall(table);
					}
					else
					{
						if(var != null)  
							event.func.call(object, var, param);
						else
							event.func.call(object, param);
					}
				}
				else if(var != null) 
					event.func.call(object, var);
				else 
					event.func.call(object);
			
				lockEvent = true;
			}
		}

		return lockEvent;
	}
	
	function getEvents() { return _eventList; }		
}