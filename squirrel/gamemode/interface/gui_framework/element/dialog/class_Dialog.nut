
class GUI.Dialog extends GUI.Event
{
    _drawList = null;

    _draw = null;
    _texture = null;

    _text = null;
    _speakerId = -1;

    _soundFile = null;
    _textureFile = null;
   
    _timestamp = null;

    _visible = false;

    _animationInterval = 0;
    _animationBeginTime = -1;

    constructor(text, speakerId, soundFile = null, timestamp = 5000)
    {
        base.constructor();

        _drawList = [];

        _draw =
            _texture = null;

        _text = text;
        _speakerId = speakerId;

        _soundFile = soundFile;
        _textureFile = "DLG_CONVERSATION.TGA";

        _timestamp = timestamp;

        _visible = false;

        _animationInterval = 500.0;
        _animationBeginTime = -1;        
    }

//:public
	function destroy()
	{
        setVisible(false);

        _drawList = null;
		    base.destroy();
	}

    function getSpeaker() { return _speakerId; }

    function getSound() { return _soundFile; }

    function setTexture(texture)
    {
		_textureFile = texture;

		if(_texture)
			_texture.file = texture;

		return this;
    }

    function getTexture() { return _textureFile; }

    function getTimestamp() { return _timestamp; }

    function setVisible(visible)
    {
        if(_visible != visible)
        {
            callEvent(GUIEventInterface.Visible, visible);
  
            if(visible)
            {
                _texture = Texture(1632, 608, 4912, 700, _textureFile);
                
                local position = _texture.getPosition(), size = _texture.getSize();

                playGesticulation(_speakerId);
                playFaceAni(_speakerId, "VISEME");

                local name = getPlayerName(_speakerId);

                _draw = Draw(position.x + size.width / 2 - textWidth(name) / 2, 708, name);
                
                _draw.setColor(255, 255, 255);
                _draw.font = getLanguageFont("FONT_DEFAULT");

                _draw.visible =        
                    _texture.visible = true;

                local lines = textSlice(_text, 70);
                
                foreach(index, line in lines)
                {
                    local draw = Draw(position.x + size.width / 2 - textWidth(line) / 2, 1008 + index * 200, line);

                    draw.font = getLanguageFont("FONT_OLD_10_WHITE_HI");
                    draw.visible = true;

                    _drawList.push(draw);
                }
                
                _texture.setSize(4912, 700 + lines.len() * 200);

                _texture.alpha = 0;
                _draw.alpha = 0;

                foreach(draw in _drawList)
                    draw.alpha = 0;

                _animationBeginTime = getTickCount();

				getSceneManager().pushObject(this);				
            }
            else 
            {      
                stopFaceAni(_speakerId);

                _draw.visible =
                    _texture.visible = false;

                _draw =
                    _texture = null;

                foreach(draw in _drawList)
                    draw.visible = false;
                
                _drawList.clear();  

				getSceneManager().removeObject(this);				
            }

            _visible = visible;
        }

        return this;
    }

    function isVisible() { return _visible; }

    function render()
    {
        if(_animationInterval <= 0) return;

        if(_animationBeginTime != -1)
        {
            local deltaTime = (getTickCount() - _animationBeginTime) / _animationInterval;

            if(deltaTime < 1.0)
            {
                local alpha = 255 * deltaTime;

                _texture.alpha = alpha;
                _draw.alpha = alpha;
                
                foreach(draw in _drawList)
                    draw.alpha = alpha;
            }
            else
            {
                _texture.alpha = 255;
                _draw.alpha = 255;
                
                foreach(draw in _drawList)
                    draw.alpha = 255;

                _animationBeginTime = -1;
            }
        }
    }
}