
class GUI.Texture extends GUI.Event
{
	_textureFile = null;
	_focusTextureFile = null;

	_alpha = -1;
	_focusAlpha = -1;

	_texture = null;

	_position = null;
	_size = null;

	_visible = false;
	_focused = false;

	constructor(texture, position, size)
	{
        base.constructor();

		_textureFile = texture;
		_focusTextureFile = null;

        _alpha = 255;
		_focusAlpha = 255;

		_texture = null;

        _position = position;
		_size = size;

        _visible = false;
		_focused = false;
	}

//:public
    function destroy()
    {
		setVisible(false);
        	base.destroy();
    }

	function setTexture(texture)
	{
		_textureFile = texture;

		if(_visible)
			_texture.file = texture;

		return this;
	}

	function getTexture() { return _textureFile; }

	function setFocusTexture(texture)
	{
		_focusTextureFile = texture;

		if(_visible && _focused)
			_texture.file = texture;

		return this;	
	}

	function getFocusTexture() { return _focusTextureFile; }

	function setAlpha(alpha)
	{
		_alpha = alpha;

		if(_visible && !_focused)
			_texture.alpha = alpha;

		return this;
	}

	function getAlpha() { return _alpha; }

	function setFocusAlpha(alpha)
	{
		_focusAlpha = alpha;

		if(_visible && _focused)
			_texture.alpha = alpha;

		return this;
	}

	function getFocusAlpha() { return _focusAlpha; }

	function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible)
			_texture.setPosition(position.x, position.y);

		return this;
	}

	function getPosition() { return _position; }

	function setSize(size)
	{
		local size = (_size = size).getSize();

		if(_visible)
			_texture.setSize(size.width, size.height);

		return this;
	}

	function getSize() { return _size; }

	function setVisible(visible)
	{
		if(_visible != visible)
		{			
			if(visible)
			{
				local position = _position.getPosition();
				local size = _size.getSize();

				_texture = Texture(position.x, position.y, size.width, size.height, _textureFile);
				
				if(_alpha != -1)
					_texture.alpha = _alpha;
		
				_texture.visible = true;
				
				getSceneManager().pushObject(this);			
			}
			else 
			{			
				_focused = false;
				
				_texture.visible = false;
				_texture = null;

				getSceneManager().removeObject(this);
			}

			callEvent(GUIEventInterface.Visible, _visible = visible);
		}
		return this;
	}

	function isVisible() { return _visible; }

//:private
	function setFocus_Graphical(focus)
	{
		if(focus)
		{	
			if(_focusTextureFile)
				_texture.file = _focusTextureFile;
			if(_focusAlpha != -1)
				_texture.alpha = _focusAlpha;
		}
		else 
		{
			if(_textureFile)
				_texture.file = _textureFile;
			if(_alpha != -1)
				_texture.alpha = _alpha;
		}
	}

//:public
	function setFocus(focus)
	{		
		if(_visible)
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
			
			setFocus_Graphical(focus);
		}		
	}

	function isFocused() { return _focused; }

    function cursor(cursorPosition)
    {
		local position = _position.getPosition();
		local size = _size.getSize();

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
    }
}