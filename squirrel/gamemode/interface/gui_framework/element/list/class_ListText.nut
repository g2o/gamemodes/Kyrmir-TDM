
class GUI.ListText extends GUI.Button
{
	_list = null;

	constructor(text)
	{
		_list = null;
		base.constructor(text);
	}

//:public
	function destroy()
	{
		_list = null;
			base.destroy();
	}

	function setList(list)
	{
		_list = list;
	}

	function getList() { return _list; }

	function clickFromList()
	{
		return callEvent(GUIEventInterface.Click);
	}

	function setFocus(focus)
	{
		if(focus != _focused)
		{
			base.setFocus(focus);
			
			if(focus)
				_list.setCurrentButton(this);
		}
	}

	function cursor(cursorPosition)
	{
		local position = _texture ? _texture.getPosition() : _draw.getPosition();
		local size = _texture ? _texture.getSize() : _draw;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else
		{
			if(_list.getButtons()[_list.getCurrentButton()] != this)
			{
				setFocus(false);
			}
		}
	}

	function mouse(cursorPosition, button, event)
	{
		local position = _texture ? _texture.getPosition() : _draw.getPosition();
		local size = _texture ? _texture.getSize() : _draw;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			local value = callEvent(GUIEventInterface.Click);

			if(_list.mouseClick() == false)
			{
				return value;
			}

			return true;
		}

		return false;
	}
}
