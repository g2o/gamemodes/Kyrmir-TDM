
class GUI.GridRender extends GUI.Render
{
    _grid = null;
    _item = null;

	constructor(itemInstance, cellSize = null)
	{        
		_grid = null; 
		_item = null;
  
 		base.constructor(itemInstance, null, cellSize);            
    }

//:public
	function setGrid(grid)
	{
		_grid = grid;
			return this;
	}

	function getGrid() { return _grid; }

    function setItem(item)
    {
        _item = item;
            return this;
    }

    function getItem() { return _item; }
	
	function setFocus(focus)
	{	
		if(focus != _focused)
		{
			base.setFocus(focus);
				_grid.setFocusedRender(this);
		}
	}

	function cursor(cursorPosition)
	{
		local position = _position.getPosition();
		local size = _size.getSize();

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else 
		{
			setFocus((_grid.getRenders()[_grid.getFocusedRender()] == this));
		}
	}    

	function mouse(cursorPosition, button, event)
	{
		local position = _texture ? _texture.getPosition() : _render.getPosition();
		local size = _texture ? _texture.getSize() : _render;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			if(_grid.mouseClick() == false)
			{
				return callEvent(GUIEventInterface.Click);
			}

			return true;
		}
	}
}
