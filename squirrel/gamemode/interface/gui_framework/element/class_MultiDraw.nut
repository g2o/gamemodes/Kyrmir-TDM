
class GUI.MultiDraw extends GUI.Event
{
	_position = -1;

	_lineHeight = 0;
	_lineLimit = 0;

	_location = 0;

	_scrollable = false;
	_centerAtPosition = false;

	_visible = false;
	_focused = false;

	_drawList = null;

	constructor(position, lineLimit = 0)
	{
		base.constructor();

		_position = position;

		_lineHeight = 200;
		_lineLimit = lineLimit;

		_scrollable = false;
		_centerAtPosition = false;
		
		_visible = false;
		_focused = false;

		_location = 0;

		_drawList = [];
	}

//:public
    function destroy()
    {
		clearDraws();
		
		_drawList = null;
        	base.destroy();
    }

	function setScrollable(scroll) 
	{
		_scrollable = scroll;
			return this;
	}

	function isScrollable() { return _scrollable; }

	function setCenterAtPosition(centerAtPosition)
	{
		_centerAtPosition = centerAtPosition;
		
		foreach(draw in _drawList)
			draw.setCenterAtPosition(_centerAtPosition);

		return this;
	}

	function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible) 	
		{
            foreach(i, draw in _drawList)
			{
     		    if(draw.isVisible())
                	draw.setPosition(GUI.Position(position.x, position.y + i * _lineHeight));
			}
		}

		return this;
	}

	function getPosition() { return _position; }

    function setLineHeight(height)
    {
		_lineHeight = height;
			return this;
    }

    function getLineHeight() { return _lineHeight; }

	function setVisible(visible)
	{
		if(visible != _visible) 
		{
			if(visible)
			{
				local lineLimit = _lineLimit ? (_lineLimit < _drawList.len() ? _lineLimit : _drawList.len()) : _drawList.len(), 
					position = _position.getPosition();

				for(local i = 0; i < lineLimit; i++)
				{   
					_drawList[i]
					.setPosition(GUI.Position(position.x, position.y + i * _lineHeight))
					.setCenterAtPosition(_centerAtPosition)
					.setVisible(true);
				}

				getSceneManager().pushObject(this);				
			}
			else 
			{
				_location = 0;

				foreach(draw in _drawList)
					draw.setVisible(false);

				_focused = false;

				getSceneManager().removeObject(this);
			}
			
			callEvent(GUIEventInterface.Visible, _visible = visible)
		}

		return this; 
	}

	function isVisible() { return _visible; }

	function pushDraw(text, color = null, position = -1, font = null, pure = false, width = 1.0, height = 1.0)
	{
		if(position == -1) position = _drawList.len();

		local draw = GUI.Draw(text)
			.setColor(color)
			.setPureText(pure)
			.setTextScale(width, height);

		if(font != null) draw.setFont(font);

		draw.setCenterAtPosition(_centerAtPosition);

		_drawList.insert(position, draw);		

		if(_visible)
		{
			local lastLine = _location + _lineLimit;

			if(position <= lastLine)
			{		
				local position = _position.getPosition();

				foreach(index, draw in _drawList)
				{
					if(index >= _location && index < lastLine)
					{
						draw.setPosition(GUI.Position(position.x, position.y + (index - _location) * _lineHeight)).setVisible(true);
					}
					else draw.setVisible(false);
				}	
			}
		}
		
		return this;
	}

	function removeDraw(draw)
	{
		if(typeof(draw) == "integer")
		{
			if(draw < _drawList.len())
			{
				draw = _drawList[draw];
			}
		}			

		local id = _drawList.find(draw);

		if(id != null)
		{
			_drawList.remove(id);

			if(draw.isVisible())
				draw.setVisible(false);
			
			if(_visible) updateMultiDraw(0);
		}
			
		return this;
	}

	function clearDraws()
	{
		_location = 0;

		foreach(draw in _drawList)
			draw.destroy();

		_drawList.clear();
	}

	function getDraws() { return _drawList; }

	function getSize() { return _drawList.len(); }

//:private
	function up()
	{
		updateMultiDraw(_location - 1);
	}

	function down()
	{
		updateMultiDraw(_location + 1);
	}

	function top()
	{
		updateMultiDraw(0);
	}

	function bottom()
	{
		updateMultiDraw(_drawList.len() - _lineLimit);
	}

	function updateMultiDraw(newPosition)
	{
		if(_visible == false) return;
		
		if(_lineLimit != 0)
		{			
			if(newPosition >= 0 && newPosition <= (_drawList.len() - _lineLimit))
			{
				local lastLine = newPosition + _lineLimit;
				local position = _position.getPosition();

				foreach(i, draw in _drawList)
				{
					if(i >= newPosition && i < lastLine)
					{
						draw.setPosition(GUI.Position(position.x, position.y + (i - newPosition) * _lineHeight)).setVisible(true);
					}
					else draw.setVisible(false);
				}

				_location = newPosition;
			}
		}
	}

//:public
	function mouseWheel(direction)
	{
		if(_scrollable && _focused)
		{
			switch(direction)
			{
				case 1: up(); 
				break;
				case -1: down();
				break;
			}
		}
	}

	function keyBoard(key)
	{
		if(!_focused || !_scrollable) return;

		switch(key)
		{
			case KEY_UP: 
			case KEY_W:
				up(); 
			break;
			case KEY_DOWN: 
			case KEY_S: 
				down(); 
			break;
		}	
	}

	function setFocus(focus)
	{
		if(_visible)
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
		}
	}

	function isFocused() { return _focused; }

	function cursor(cursorPosition)
	{
		local position = _position.getPosition();
		
		if(GUI.isCursorIn(position.x, position.y, 2000, (_lineLimit == 0 ? _drawList.len() : _lineLimit) * _lineHeight, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}	
}
