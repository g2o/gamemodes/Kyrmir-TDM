
class GUI.Button extends GUI.Draw 
{
	_textureFile = null;
	_focusTextureFile = null;

	_format = 0;

	_texture = null;
	_size = null;

    constructor(text, position = null, size = null)
    {
        _textureFile = null;
		_focusTextureFile = null;

        _format = 0;

        _texture = null;
		_size = size;

        base.constructor(text, position);
    }

//:public
	function destroy()
	{
		setVisible(false);
			base.destroy();
	}

	function setText(text)
	{
		base.setText(text);
			_textAdjust();
			
		return this;
	}

	function refreshText()
	{
		base.refreshText();
			_textAdjust();
	}

    function setTexture(texture)
    {
		_textureFile = texture;

		if(_texture && !_focused)
			_texture.file = texture;

		return this;
    }

    function getTexture() { return _textureFile; }

    function setFocusTexture(texture)
    {
		_focusTextureFile = texture;

		if(_texture && _focused)
			_texture.file = texture;
		
		return this;
    }

    function getFocusTexture() { return _focusTextureFile; }

	function setAlpha(alpha)
	{		
		_alpha = alpha;

		if(_visible && !_focused)
		{
			_draw.alpha = alpha;

			if(_texture)		
				_texture.alpha = alpha;
		}

		return this;
	}

	function setFocusAlpha(alpha)
	{
		_focusAlpha = alpha;

		if(_visible && _focused)
		{
			_draw.alpha = alpha;

			if(_texture)
				_texture.alpha = alpha;
		}

		return this;
	}

	function setFormat(format)
	{
		_format = format;
			_textAdjust();

		return this;
	}

	function getFormat() { return _format; }

	function setPosition(position)
	{
		base.setPosition(position);
		
		if(_visible) 	
		{
			local position = position.getPosition();
			
			if(_texture)
				_texture.setPosition(position.x, position.y);

			_textAdjust();
		}

		return this;
	}

    function setSize(size) 
    {
		_size = size;

		if(size)
		{
			local size = size.getSize();

			if(_texture)
				_texture.setSize(size.width, size.height);
		}
		else 
		{
			local size = _draw;

			if(_texture)
				_texture.setSize(size.width, size.height);		
		}

		return this;
    }

    function getSize() { return _size; }

	function setVisible(visible)
	{
		if(visible != _visible)
		{
			base.setVisible(visible);

			if(visible)
			{
				if(_textureFile)
				{
					local position = _position.getPosition();
					local size = _draw;

					if(_size) size = _size.getSize();	

					_texture = Texture(position.x, position.y, size.width size.height, _textureFile);
					_texture.visible = true;
					
					if(_alpha != -1)
						_texture.alpha = _alpha;

					_draw.top();
				}		

				_textAdjust();
			}
			else 
			{
				if(_texture)
				{
					_texture.visible = false;
					_texture = null;
				}
			}
		}
		
		return this;
	}

//:private
	function setFocus_Graphical(focus)
	{
		if(focus)
		{
			if(_focusColor)
			{
				local color = _focusColor.getColor();
					_draw.setColor(color.r, color.g, color.b);
			}
			else _draw.setColor(255, 255, 255);

			if(_texture)
			{
				if(_focusTextureFile)
					_texture.file = _focusTextureFile;
				if(_focusAlpha != -1)
					_texture.alpha  = _focusAlpha;
			}

			if(_focusAlpha != -1)
				_draw.alpha = _focusAlpha;			
		}
		else 
		{ 
			if(_color)
			{
				local color = _color.getColor();
					_draw.setColor(color.r, color.g, color.b);
			}
			else _draw.setColor(200, 184, 152);

			_draw.alpha = 255;		

			if(_texture)
			{
				if(_textureFile)
					_texture.file = _textureFile;

				_texture.alpha = _alpha;
			}
		}	
	}

//:public
	function setFocus(focus)
	{
		if(_visible) 
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
			
			setFocus_Graphical(focus);
		}
	}

	function cursor(cursorPosition)
	{
		if(_disableFocus) return;

		local position = _texture ? _texture.getPosition() : _draw.getPosition();
		local size = _texture ? _texture.getSize() : _draw;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}

	function mouse(cursorPosition, button, event)
	{
		local position = _texture ? _texture.getPosition() : _draw.getPosition();
		local size = _texture ? _texture.getSize() : _draw;

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			return callEvent(GUIEventInterface.Click);
		}

		return false;
	}

	function getTexture() { return _texture; }

//:private	
	function _textAdjust()
	{
		if(_visible == false) return;

		textSetFont(getLanguageFont(_fontFile));
		
		if(_size)
		{
			local position = _position.getPosition(),
				size = _size.getSize();
			
			local vertical = position.y + size.height / 2 - _draw.height / 2;
			
			switch(_format)
			{
				case GUITextFormat.Center: _draw.setPosition(position.x + size.width / 2 - (textWidth(_draw.text) * _textScale.width) / 2, vertical); break;
				case GUITextFormat.Left: _draw.setPosition(position.x + 100, vertical); break;
				case GUITextFormat.Right: _draw.setPosition(position.x + size.width - (textWidth(_draw.text) * _textScale.width) - 100, vertical); break;
			}
		}

        textSetFont(getLanguageFont("FONT_DEFAULT"));
	}
}