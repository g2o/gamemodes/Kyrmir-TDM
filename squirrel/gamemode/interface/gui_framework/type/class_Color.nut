
class GUI.Color
{
    _color = null;
   
	constructor(r, g, b)
	{
        _color = { r = r, g = g, b = b };
	}

//:public
    function setColor(r, g, b)
    {
        _color.r = r;
        _color.g = g;
        _color.b = b;
    }

    function getColor() { return { r = _color.r, g = _color.g, b = _color.b } }

    function setR(r)
    {
        _color.r = r;
    }

    function getR() { return _color.r; }

    function setG(g)
    {
        _color.g = g;
    }

    function getG() { return _color.g; }

    function setB(b)
    {
        _color.b = b;
    }

    function getB() { return _color.b; }
}