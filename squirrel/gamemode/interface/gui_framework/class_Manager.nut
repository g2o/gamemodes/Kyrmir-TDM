
class GUI.Manager
{
	static _ticks = { wheel = null, render = null };
	static _objectList = [];
	
//:public
	function keyBoard(key)
	{
		if(!isConsoleOpen() && _ticks.wheel == 0)
		{					
			foreach(object in _objectList)
			{		
				switch(object.getclass())
				{
					case GUI.Input:
					case GUI.List:
					case GUI.MultiDraw:
					case GUI.DialogSequence:
					{
						if(object.keyBoard(key))
						{
							return; 
						}
					}
					break;
				}
			}
		}
	}

	function mouseButton(button, event)
	{
		if(!isCursorVisible() || event == 1)
			return false;
		
		foreach(object in _objectList)
		{			
			switch(object.getclass())
			{
				case GUI.Button:
				case GUI.ListText:
				case GUI.ListRow:
				case GUI.Input:
				case GUI.Render:
				case GUI.GridRender:
				{
					if(object.mouse(getCursorPosition(), button, event))
					{
						return; 
					}
				}
				break;
			}
		}
	}
	
	function mouseWheel(direction)
	{
		if(!isCursorVisible())
			return false;
		
		_ticks.wheel = getTickCount() + 400;

		foreach(object in _objectList)
		{
			switch(object.getclass())
			{
				case GUI.List:
				case GUI.Grid:
				case GUI.MultiDraw:
					object.mouseWheel(direction);			
				break;
			}
		}
	}
	
	function render()
	{		
		if(isCursorVisible())
		{
			if(_ticks.wheel != 0)
			{
				if(_ticks.wheel < getTickCount())
					_ticks.wheel = 0;
			}

			if(_ticks.wheel == 0) 
			{
				foreach(object in _objectList)
				{
					switch(object.getclass())
					{
						case GUI.Draw:
						case GUI.MultiDraw:
						case GUI.Button:
						case GUI.Input:
						case GUI.Render:
						case GUI.List:
						case GUI.ListText:
						case GUI.ListRow:
						case GUI.Grid:
						case GUI.GridRender:
						case GUI.Texture:
							object.cursor(getCursorPosition());
						break;
					}
				}	
			}
		}

		if(_ticks.render < getTickCount())
		{
			foreach(object in _objectList)
			{
				switch(object.getclass())
				{
					case GUI.Render: object.rotate(); break;
					case GUI.GridRender: object.rotate(); break;
					case GUI.Dialog: object.render(); break;
					case GUI.DialogSequence: object.refresh(); break;
					case GUI.GameText: object.render(); break;
				}
			}
		
			_ticks.render = getTickCount() + 40;
		}
	}

	function refreshText()
	{
		foreach(object in _objectList)
		{
			switch(object.getclass())
			{
				case GUI.Draw:
				case GUI.Button:
				case GUI.Input:
				case GUI.Render:
				case GUI.ListText:
					object.refreshText();
				break;
			}
		}		
	}

	function pushObject(object)
	{
		_objectList.push(object);
	}

	function removeObject(element)
	{
		local id = _objectList.find(element);
		
		if(id != null) 
			_objectList.remove(id);
	}
	
	function getObjects()
	{
		return _objectList;
	}
}	

getSceneManager <- @() GUI.Manager;

// GUI

refreshGUIText <- @() GUI.Manager.refreshText();