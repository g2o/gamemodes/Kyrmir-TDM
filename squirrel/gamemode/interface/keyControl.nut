
// Interface overwrite
disableLogicalKey(GAME_END, true);
disableLogicalKey(GAME_INVENTORY, true);
disableLogicalKey(GAME_SCREEN_STATUS, true);
disableLogicalKey(GAME_SCREEN_LOG, true);

// Control overwrite
disableLogicalKey(GAME_WEAPON, true);
disableLogicalKey(GAME_LAME_POTION, true);
disableLogicalKey(GAME_LAME_HEAL, true);
disableLogicalKey(GAME_SCREEN_MAP, true);

// Quickslot
disableKey(KEY_1, true);
disableKey(KEY_2, true);
disableKey(KEY_3, true);
disableKey(KEY_4, true);
disableKey(KEY_5, true);
disableKey(KEY_6, true);
disableKey(KEY_7, true);
disableKey(KEY_8, true);
disableKey(KEY_9, true);
disableKey(KEY_0, true);

// F1, F2, F3, F4, CTRL + ALT + F8
local lastActivity = getTickCount(),
    lockRenderSettings = lastActivity, 
    lockDebugCombination = lastActivity;

// Events
addEventHandler("onKey", function(key)
{
    lastActivity = getTickCount();

	switch(key)
	{
		case KEY_F1: 
        case KEY_F2: 
		case KEY_F3: 
		case KEY_F4: 
        {
            if(lockRenderSettings > lastActivity)
            { 
                GUI.GameText(GUI.PositionPr(50, 7.5), "POP-UP_MSG_RENDER", "FONT_DEFAULT", 2000.0, true, 255, 0, 0);               
                    cancelEvent(); 
            }
            else
            {            
                lockRenderSettings = lastActivity + 15000;
            }
        } 
        break;  
        case KEY_Z:
        {
            if(isLogged() == true && chatInputIsOpen() == false)
            {
                getHero().reverseTransform();
            }
        }
    }

    if(isKeyPressed(KEY_LCONTROL) && isKeyPressed(KEY_LMENU) && isKeyPressed(KEY_F8))
    {
        if(lockDebugCombination > lastActivity)
        { 
            GUI.GameText(GUI.PositionPr(50, 7.5), "POP-UP_MSG_DEBUG_COMBINATION", "FONT_DEFAULT", 2000.0, true, 255, 0, 0);               
                cancelEvent(); 
        }
        else
        {            
            getHero().getEquipmentModule().debug();
                lockDebugCombination = lastActivity + 8000;
        }
    }  
});

addEventHandler("onMouseClick", function(button)
{
    lastActivity = getTickCount();
});

addEventHandler("onMouseRelease", function(button)
{
    lastActivity = getTickCount();
});

addEventHandler("onMouseWheel", function(direction)
{
    lastActivity = getTickCount();
});

addEventHandler("onMouseMove", function(x, y)
{
    lastActivity = getTickCount();
});

setTimer(function()
{
    callEvent("onInactive", getTickCount() - lastActivity)
}
, 500, 0);
