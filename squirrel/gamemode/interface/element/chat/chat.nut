
local emoji_View = GUI.View("EMOJI_VIEW");

const CHAT_TIME_LIMIT = 3000;

local chatElementList = {};

local list_Position = GUI.Position(100, 100), emojis_Position = GUI.Position(1600, 4800);
local list_Gap = 300;

local currentElement = null,
    currentPrefix = -1;

local interfaceVisible = false;
local lastActivity = null, sendLimit = null;

local function setActiveElement(prefix)
{
    foreach(elementPrefix, element in chatElementList)
    {
        if(elementPrefix != prefix)
            continue;

        local container = element.container, button = element.button;

        if(interfaceVisible)
        {
            if(currentElement != null)
            {
                currentElement.container.setVisible(false, false);     
                currentElement.button.setColor(GUI.Color(200, 184, 152));     
            }

            container.setVisible(true);
        }
        
        button.setColor(GUI.Color(255, 255, 255));
        
        currentElement = element;
        currentPrefix = elementPrefix;

        break;
    }
}

local function findDiffrenPrefix(prefix)
{
    lastActivity = getTickCount();

    foreach(elementPrefix, button in chatElementList)
    {
        if(prefix < elementPrefix)
        {
            setActiveElement(elementPrefix);
            break;
        }
    }

    if(currentElement == null)
        return;
    
    if(prefix == -1)
    {
        foreach(elementPrefix, element in chatElementList)
        {
            setActiveElement(elementPrefix);
            break;
        }
    }
    else 
    {
        if(currentPrefix != prefix) return;

        foreach(elementPrefix, element in chatElementList)
        {
            if(element == chatElementList[prefix])
                continue;

            setActiveElement(elementPrefix);
            break;
        }
    }
}

local function updateButtonList()
{
    local elementListSize = chatElementList.len(),
        eIndex = 0;

    for(local index = 0; index <= elementListSize; index++)
    {
        if(index in chatElementList) 
        { 
            chatElementList[index].button.setPosition(GUI.PositionPr(list_Position.getXPr() + 10.5 * eIndex++, list_Position.getYPr()));
        }
    }
}

local function chatInput_Send()
{
    lastActivity = getTickCount();

    local message = chatInputGetText();
    if(message.len() == 0) return false;

    if(message.slice(0, 1) == "/")
    {
        chatInputSetText(message);
    }
    else 
    {
        local now = getTickCount();

        if(now < sendLimit) 
            return false;

        chatInputSetText(format("%d%d%s", currentPrefix, getLanguage(), message));

        sendLimit = now + CHAT_TIME_LIMIT;
    }

    chatInputSend();

    return true;
}

local function chatInput_setVisible(visible)
{
    lastActivity = getTickCount();

    if(visible)
    {
        chatInputSetFont(getLanguageFont("FONT_OLD_10_WHITE_HI"));
        chatInputOpen();

        if(getLanguage() == LANG.RU) setKeyLayout(KEY_LAYOUT_RU); 
    }
    else 
    {
        chatInputClose();
    }
    
    emoji_View.setVisible(visible);

    disableControls(visible);
    setCursorVisible(visible);

    Camera.movementEnabled = !visible; 
}

// GLOBAL
function ch_setVisibility(visible)
{
    lastActivity = getTickCount();

    if(currentElement == null) 
        findDiffrenPrefix(-1);

    if(currentElement == null)
        return;

    interfaceVisible = visible;

    foreach(element in chatElementList) 
    {
        element.button.setVisible(visible);
    }

    currentElement.container.setVisible(visible);
}

function ch_setMaxLines(lines)
{
    foreach(element in chatElementList)
    {
        element.container.setMaxLines(lines);
    }
}

function serverMessage(chatType, r, g, b, message)
{    
    local font = getLanguageFont("FONT_NEW_20_EMOTICONS_HI");

    if(chatType == ALL_CHATS)
    {
        foreach(element in chatElementList)
        {
            element.container.printMessage(ChatInterface.Message(getTranslation("SERVER"), r, g, b, getTranslation(message), r, g, b, font));
        }
    }
    else 
        chatElementList[chatType].container.printMessage(ChatInterface.Message(getTranslation("SERVER"), r, g, b, getTranslation(message), r, g, b, font));

    lastActivity = getTickCount();
}

// EVENTS
emoji_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible)
    {
        local positionY = emojis_Position.getYPr(), numberX = 0;
        local emojiList = getEmojis(getLanguage());

        foreach(index, emoji in emojiList)
        {
            if(numberX != 0 && numberX % 15 == 0)
            { 
                numberX = 0;
                positionY += 2.75;
            }

            local button = GUI.Button(emoji.tochar(), GUI.PositionPr(emojis_Position.getXPr() + (numberX++ * 2.4), positionY), GUI.SizePr(2.4, 2.7))
                .setFont("FONT_NEW_20_EMOTICONS_HI")
                .setTexture("BACKGROUND_LINE_256X64.TGA")
                .setTextScale(0.4, 0.4)
                .setPureText(true)
                .setVisible(true);

            button.addEvent(GUIEventInterface.Click, function(word) {
                chatInputSetText(chatInputGetText() + " " + word);
            }, index); 

            emoji_View.pushObject(button);
        }      
    }
    else 
        emoji_View.clearObjects();
});

addEventHandler("Chat.onChatListUpdate", function(prefix, name)
{
    chatElementList[prefix] <- {};

    chatElementList[prefix].container <- ChatInterface.Container(GUI.Position(list_Position.getX(), list_Position.getY() + list_Gap));
    chatElementList[prefix].button <- GUI.Button(
        name, 
        null, 
        GUI.SizePr(9.5, 2.5)
    )
    .setTexture("BUTTON_BROWN.TGA")
    .setColor(GUI.Color(200, 184, 152))
    .setVisible(interfaceVisible)
    .addEvent(GUIEventInterface.Click, setActiveElement, prefix); 

    chatElementList[prefix].container.setMaxLines(getLocalOptions().getChatLines());

    updateButtonList();
});

addEventHandler("View.onViewVisible", function(visible, viewId)
{        
    if(chatInputIsOpen())
    {        
        chatInput_setVisible(false);
    }

    ch_setVisibility(!visible);
});

addEventHandler("onPlayerMessage", function(playerId, r, g, b, message)
{
    local prefix_chat = message[0].tochar();
    prefix_chat = prefix_chat.tointeger();

    if(playerId != -1)
    {
        local prefix_lang = message[1].tochar().tointeger();
        if(prefix_lang == LANG.DEF) prefix_lang = getLanguage();

        message = translateToEmojis(prefix_lang, message.slice(2, message.len()));
        local font = getLanguageFont("FONT_NEW_20_EMOTICONS_HI", prefix_lang);
        
        local lines = textSlice(message, 55);

        if(prefix_chat == ALL_CHATS)
        {
            local color = getPlayerColor(playerId), 
                name = getPlayerName(playerId);

            foreach(element in chatElementList)
            {
                foreach(message in lines) element.container.printMessage(ChatInterface.Message(name, color.r, color.g, color.b, message, r, g, b, font));
            }
        }
        else
        {
            if(prefix_chat in chatElementList)
            {       
                local color = getPlayerColor(playerId), 
                    name = getPlayerName(playerId);

                foreach(message in lines)
                {
                    chatElementList[prefix_chat].container.printMessage(ChatInterface.Message(name, color.r, color.g, color.b, message, r, g, b, font));
                }
                
                if(prefix_chat != currentPrefix)
                {
                    chatElementList[prefix_chat].button.setColor(GUI.Color(255, 0, 0));
                }
            }
        }
    }
    else 
    {
        message = message.slice(1, message.len());
        local lines = textSlice(message, 55);

        foreach(message in lines) 
            serverMessage(prefix_chat, r, g, b, message); 
    }

    lastActivity = getTickCount();
});

addEventHandler("onRender", function()
{
    if(interfaceVisible == false || currentElement == null) return;

    local container = currentElement.container;

    container.render();

    if(lastActivity + 15000 < getTickCount())
    {
        if(chatInputIsOpen() == false)
        {
            container.setVisible(false);
        }
        else 
        {
            container.setVisible(true);
        }
    }
    else 
    {
        container.setVisible(true);
    }
});

addEventHandler("onKey", function(key)
{
    if(interfaceVisible == false) return;

    if(isKeyPressed(KEY_LCONTROL) && isKeyPressed(KEY_X))
    {
        if(currentElement != null) 
        {
            findDiffrenPrefix(currentPrefix);
        }
    }  

	if(chatInputIsOpen())
	{		
        local chat = currentElement.container;

		switch(key)
		{
		    case KEY_UP: chat.move(chat.getLocation() - 1); break;
            case KEY_DOWN: chat.move(chat.getLocation() + 1); break;
            case KEY_PRIOR: chat.move(chat.getTopLocation()); break;
            case KEY_NEXT: chat.move(0); break; 
            case KEY_RETURN:
            {
                chatInput_setVisible(false);

                if(chatInput_Send() == false)
                {
                    serverMessage(currentPrefix, 190, 15, 15, "CHAT_ERROR");
                }

                currentElement.container.move(0);
            } 
            break;
            case KEY_ESCAPE: chatInput_setVisible(false); break;
            default: 
                playGesticulation(heroId); 
            break;
		}
	}
	else
	{
        if(key == KEY_T)
        {
            if(!isConsoleOpen() && !isAnyViewVisible() && interfaceVisible)
            {
                chatInput_setVisible(true);  
            }
		}
	}
});