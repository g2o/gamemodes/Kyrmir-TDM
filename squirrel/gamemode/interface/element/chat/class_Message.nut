
class ChatInterface.Message
{
    _nameDraw = null;
	_textDraw = null;

	_name = null;
	_text = null;

	_fontFile = "FONT_NEW_20_EMOTICONS_HI";

	_colorX = null;
	_colorY = null;

	_fadeBeginTime = -1;

    _visible = false;

	constructor(name, r_x, g_x, b_x, text, r_y, g_y, b_y, font = "FONT_NEW_20_EMOTICONS_HI")
	{
		_nameDraw = null;
		_textDraw = null;
		
		_name = name;
		_text = text;

		_fontFile = font;

		_colorX = { r = r_x, g = g_x, b = b_x };
		_colorY = { r = r_y, g = g_y, b = b_y };

		_fadeBeginTime = -1;

		_visible = false;
	}

//:public
	function setVisible(visible)
	{
        _visible = visible;
		
		if(visible)
		{
			_nameDraw = Draw(0, 0, _name + ": ");
			_textDraw = Draw(0, 0, _text);

			_nameDraw.font = _fontFile;
			_textDraw.font = _fontFile;
						
			_nameDraw.setColor(_colorX.r, _colorX.g, _colorX.b);
			_textDraw.setColor(_colorY.r, _colorY.g, _colorY.b);

            _nameDraw.alpha = 255;
            _textDraw.alpha = 255;
			
			_nameDraw.setScale(0.5, 0.5);
			_textDraw.setScale(0.5, 0.5);

			_nameDraw.visible = visible;
            _textDraw.visible = visible;
		}
		else 
		{
			_nameDraw = null;
			_textDraw = null;
		}
	}

	function isVisible() { return _visible; }

	function setPosition(x, y)
	{
        _nameDraw.setPosition(x, y);
        _textDraw.setPosition(_nameDraw.width + x, y);
	}
	
	function setAlpha(alpha)
	{
        _nameDraw.alpha = alpha;
        _textDraw.alpha = alpha;
	}

	function getLineHeight() { return _textDraw.height; }

	function setFadeBeginTime(beginTime)
	{
		_fadeBeginTime = beginTime;
	}

	function getFadeBeginTime() { return _fadeBeginTime; }
}
