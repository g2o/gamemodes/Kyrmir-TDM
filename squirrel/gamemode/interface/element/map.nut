
local map_View = GUI.View("VIEW_MAP");

local map_Size = {
    buttonsBosses = GUI.Size(200, 200),
    textureMap = GUI.Size(6692, 6692),
};

local map_Position = {
    textureMap = GUI.Position(750, 750),
};

local map_Elements = {
    buttonsBosses = {},
    drawsPlayers = {},
    textureMap = GUI.Texture("MAP_WORLD_ORC.TGA", map_Position.textureMap, map_Size.textureMap),
};

local map_Timer = -1;

// Coordinates for calc function
local world_Coords = {
    "COLONY.ZEN" : { 
        vecWorldPositionMin = Vec2(-88838, -66924), 
        vecWorldPositionMax = Vec2(88071, 64729)
    },
    "ADDONWORLD.ZEN" : { 
        vecWorldPositionMin = Vec2(-47783, -32300), 
        vecWorldPositionMax = Vec2(43949, 36300) 
    }
};

local map_Coords = {};

local function calcMapData()
{
    local texturePosition = map_Position.textureMap.getPositionPx();
    local textureSize = map_Size.textureMap.getSizePx();

    map_Coords.vecMapMin <- Vec2(texturePosition.x, texturePosition.y),
    map_Coords.vecMapMax <- Vec2(textureSize.width, textureSize.height)

    map_Coords.vecMapMax += map_Coords.vecMapMin;
}

calcMapData();

local function calc2DPosition(x, z)
{
    local vecMapDir	= map_Coords.vecMapMax - map_Coords.vecMapMin;
    local world_Coords = world_Coords[getWorld()];

    local vecWorldDir = world_Coords.vecWorldPositionMax - world_Coords.vecWorldPositionMin;
        vecMapDir[0] = vecMapDir[0] / vecWorldDir[0];
        vecMapDir[1] = vecMapDir[1] / vecWorldDir[1];

    local vecWorldPosition = Vec2(x, z);
    local vecMapPosition = vecWorldPosition - world_Coords.vecWorldPositionMin;
        vecMapPosition[0] = vecMapPosition[0] * vecMapDir[0]; 
        vecMapPosition[1] = vecMapPosition[1] * vecMapDir[1]; 

    vecMapPosition[1] = (map_Coords.vecMapMax - map_Coords.vecMapMin)[1] - vecMapPosition[1];
    vecMapPosition += map_Coords.vecMapMin;

    return vecMapPosition;
}

local function updateDraws()
{
    local bossList = getBossData();
    local buttonsBosses = map_Elements.buttonsBosses;

    foreach(bossId, data in bossList)
    {        
        if(bossId in buttonsBosses)
        {           
            local timeDiff = data.status - getTickCount();

            if(data.status == -1)
            {
                buttonsBosses[bossId]
                .setPureText(true)
                .setText("");
            }
            else if(data.status == 0 || timeDiff < 0)
            {
                buttonsBosses[bossId]
                .setPureText(false)
                .setText("MAP_VIEW_RESPAWN");
            }
            else 
            {
                if(timeDiff > 0)
                {
                    local timeStampSeconds = timeDiff / 1000.0, 
                        timeStampMinutes = timeStampSeconds / 60.0;

                    buttonsBosses[bossId].setPureText(true).setText(data.status != 0 ? format("%d.%02dm", timeStampMinutes, timeStampSeconds % 60) : "");
                }
            }
        }
        else if(data.world == getWorld()) // just let me write mmo sram
        {
            local npc = getNpc(bossId);

            if(npc)
            {
                local elementPosition = calc2DPosition(data.respawn_x, data.respawn_z);
                local elementPositionPx = GUI.PositionPx(elementPosition.x - (map_Size.buttonsBosses.getWidthPx() / 2), 
                    elementPosition.y - (map_Size.buttonsBosses.getHeightPx() / 2));  
                
                local textureName = npc.getName() + ".TGA";

                buttonsBosses[bossId] <- GUI.Button("", elementPositionPx, map_Size.buttonsBosses)
                    .setTexture(textureName)
                    .setColor(GUI.Color(255, 255, 255))
                    .setPureText(true)
                    .setVisible(true);

                map_View.pushObject(buttonsBosses[bossId]);      
            }
            else 
            {
                print("Map_ERROR: " + bossId + " ");
            }
        }
    }

    local guild = getPlayerGuild(heroId);
    local drawsPlayers = map_Elements.drawsPlayers;

    for(local playerId = 0; playerId < getMaxSlots(); playerId++)
    {
        if(isPlayerCreated(playerId))
        {
            if(getPlayerGuild(playerId) == guild)
            {
                local position = getPlayerPosition(playerId);
                local drawPosition = calc2DPosition(position.x, position.z), drawPositionPx = GUI.PositionPx(drawPosition.x, drawPosition.y);

                local draw = null;

                if(playerId in drawsPlayers)
                {
                    draw = drawsPlayers[playerId];
                    draw.setPosition(drawPositionPx);
                }
                else 
                {
                    drawsPlayers[playerId] <- GUI.Draw("." + getPlayerName(playerId), drawPositionPx)
                        .setColor(GUI.Color(255, 255, 255))
                        .setPureText(true)
                        .setVisible(true);
                        
                    draw = drawsPlayers[playerId];
 
                    map_View.pushObject(drawsPlayers[playerId]);
                }

                if(playerId == heroId)
                {
                    draw.setColor(GUI.Color(0, 255, 0));
                }
                else 
                {
                    draw.setColor(GUI.Color(255, 255, 255)); 
                }
            }
            else 
            {
                if(playerId in drawsPlayers)
                {     
                    local draw = drawsPlayers[playerId];
                    
                        map_View.removeObject(draw); 
                        draw.destroy();   

                    delete drawsPlayers[playerId];
                }     
            }
        }
    }
}

map_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible)
    {
        switch(getWorld())
        {
            case "COLONY.ZEN":
                map_Elements.textureMap.setTexture("MAP_WORLD_ORC.TGA");
            break;
            case "ADDONWORLD.ZEN":
                map_Elements.textureMap.setTexture("MAP_ADDONWORLD.TGA");
            break;
            default: return;
        }
     
        map_Timer = setTimer(updateDraws, 100, 0);
    }
    else 
    {
        killTimer(map_Timer);
            map_Timer = -1;

        foreach(button in map_Elements.buttonsBosses)
        {
            map_View.removeObject(button); 
                button.destroy();
        }

        map_Elements.buttonsBosses.clear();

        foreach(draw in map_Elements.drawsPlayers)
        {
            map_View.removeObject(draw); 
                draw.destroy();         
        }

        map_Elements.drawsPlayers.clear();
    }
});

addEventHandler("onPlayerDestroy", function(playerId)
{
    if(map_View.isVisible() == false) return;
 
    if(playerId in map_Elements.drawsPlayers)
    {     
        local draw = map_Elements.drawsPlayers[playerId];
            
        map_View.removeObject(draw); 
        draw.destroy();   

        delete map_Elements.drawsPlayers[playerId];
    }   
});

addEventHandler("onChangeResolution", calcMapData);

// REGISTER ELEMENTS
map_View
.pushObject(map_Elements.textureMap);

registerInterfaceView(map_View, KEY_M, true, false);