
local lockBuy = null;
local selectedTrade = null;

// INTERFACE
local dialog_View = GUI.View("VIEW_DIALOG"),
    dialogList_View = GUI.View("VIEW_DIALOG_LIST"),
    dialogTrade_View = GUI.View("VIEW_DIALOG_TRADE");

// INTERFACE - LIST
local dialogList_Size = {
    textureBackground = GUI.SizePr(50, 21.5),
    line = GUI.SizePr(49, 2.5)
};

local dialogList_Position = {
    listOptions = GUI.PositionPr(25, 75),
    textureBackground = GUI.PositionPr(25.5, 74.25),
};

local dialogList_Elements = {
    textureBackground = GUI.Texture("DLG_CONVERSATION.TGA", dialogList_Position.textureBackground, dialogList_Size.textureBackground),
    listOptions = GUI.List(dialogList_Position.listOptions, 8)
        .setLineSize(dialogList_Size.line)	
        .setScrollable(true)
};

// INTERFACE - TRADE
local widthMultipler = getResolution().x.tofloat() / getResolution().y.tofloat();
local renderItem_Height = 15 * widthMultipler;

local dialogTrade_Size = {
    textureBackground = GUI.SizePr(60, 72),
    gridItems_Cell = GUI.SizePr(7.5, 7.5 * widthMultipler),
    gridItems_Gap = anx(1 / 100.0 * getResolution().x),
    textureDescription = GUI.SizePr(32.5, renderItem_Height),
    renderItem = GUI.SizePr(15, renderItem_Height),
    buttonPrice = GUI.SizePr(10, 5),
    inputAmount = GUI.SizePr(10, 5),
    buttonBuy = GUI.SizePr(10, 5),
    buttonEnd = GUI.SizePr(12.5, 5),
};

local dialogTrade_Position = {
    textureBackground = GUI.PositionPr(20, 10),
    gridItems = GUI.PositionPr(25, 17),
    drawCurrency = GUI.PositionPr(25, 11.5),
    textureDescription = GUI.PositionPr(42.5, 47),
    drawDescription = GUI.PositionPr(44.5, 49),
    renderItem = GUI.PositionPr(25, 47),
    buttonPrice = GUI.PositionPr(48, 84),
    inputAmount = GUI.PositionPr(59, 84),
    buttonBuy = GUI.PositionPr(70, 84), 
    buttonEnd = GUI.PositionPr(20, 84),
    gameText = GUI.PositionPr(50, 40)
};

local dialogTrade_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_2048X2048.TGA", dialogTrade_Position.textureBackground, dialogTrade_Size.textureBackground),
    gridItems = GUI.Grid(dialogTrade_Position.gridItems, 6, 2)
        .setBorderGap(dialogTrade_Size.gridItems_Gap)
        .setCellSize(dialogTrade_Size.gridItems_Cell)
        .setScrollable(true),                
    drawCurrency = GUI.Draw("DIALOG_VIEW_AVAILABLE;: 0", dialogTrade_Position.drawCurrency)
        .setFont("FONT_OLD_20_WHITE_HI"),
    textureDescription = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", dialogTrade_Position.textureDescription, dialogTrade_Size.textureDescription),
    drawDescription = GUI.MultiDraw(dialogTrade_Position.drawDescription, 9)
        .setScrollable(true) 
        .setCenterAtPosition(false), 
    renderItem = GUI.Render(null, dialogTrade_Position.renderItem, dialogTrade_Size.renderItem)    
        .setRotate(true)
        .setTexture("BACKGROUND_BORDER_512x512.TGA"),
    buttonPrice = GUI.Button("DIALOG_VIEW_COST_NO_DATA", dialogTrade_Position.buttonPrice, dialogTrade_Size.buttonPrice)
        .setFocusColor(GUI.Color(255, 255, 255)),
    inputAmount = GUI.Input("DIALOG_VIEW_PLACEHOLDER", dialogTrade_Position.inputAmount, dialogTrade_Size.inputAmount)
        .setType(GUIInputType.Number)
        .setTexture("INPUT_UNI.TGA"),
    buttonBuy = GUI.Button("DIALOG_VIEW_BUY", dialogTrade_Position.buttonBuy, dialogTrade_Size.buttonBuy)
        .setTexture("BUTTON_BROWN.TGA"),
    buttonEnd = GUI.Button("DIALOG_VIEW_END", dialogTrade_Position.buttonEnd, dialogTrade_Size.buttonEnd)
        .setTexture("BUTTON_RED.TGA")
};

dialogTrade_Elements.drawCurrency
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible) 
    {
        local currencyId = getCurrentDialogData().currencyId, item = hasItem(currencyId);

        if(item)
        {
            setText("DIALOG_VIEW_AVAILABLE;(;" + item.getTemplate().getInstance() + ";): " + item.getAmount());
        }
        else 
            setText("DIALOG_VIEW_AVAILABLE;(;" + getItemTemplateById(currencyId).getInstance() + ";): 0");
    }
});

dialogTrade_Elements.buttonPrice
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible == false) setText("DIALOG_VIEW_COST_NO_DATA");
});

dialogTrade_Elements.inputAmount
.addEvent(GUIEventInterface.Input, function(number) 
{
    local button = dialogTrade_Elements.buttonPrice;

    if(number != "")
        button.setText(number * selectedTrade.price + " ;DIALOG_VIEW_COST");
    else 
        button.setText("DIALOG_VIEW_COST_NO_DATA");
});

dialogTrade_Elements.buttonBuy
.addEvent(GUIEventInterface.Click, function() 
{
    if(lockBuy > getTickCount())
    {
        GUI.GameText(dialogTrade_Position.gameText, "DIALOG_VIEW_BUY_FAULT", "FONT_OLD_20_WHITE_HI", 2500.0, true, 255, 0, 0);
    }
    else 
    {
        local currencyItem = hasItem(getCurrentDialogData().currencyId);
   
        if(currencyItem == null) 
        {
            GUI.GameText(dialogTrade_Position.gameText, "DIALOG_VIEW_CANNOT_BUY", "FONT_OLD_20_WHITE_HI", 2500.0, true, 255, 0, 0)
        }
        else 
        {
            local inputAmount_Value = dialogTrade_Elements.inputAmount.getInputValue();

            if(inputAmount_Value == null) inputAmount_Value = 1;

            if(inputAmount_Value <= 0 || inputAmount_Value > 10000)
            {
                GUI.GameText(dialogTrade_Position.gameText, "DIALOG_VIEW_BUY_FAULT_AMOUNT", "FONT_OLD_20_WHITE_HI", 2500.0, true, 255, 0, 0);               
            }
            else 
            {
                local price = inputAmount_Value * selectedTrade.price;
                
                if(currencyItem.getAmount() < price)
                {
                    GUI.GameText(dialogTrade_Position.gameText, "DIALOG_VIEW_CANNOT_BUY", "FONT_OLD_20_WHITE_HI", 2500.0, true, 255, 0, 0);               
                }
                else 
                {
                    GUI.GameText(dialogTrade_Position.gameText, "DIALOG_VIEW_BUY_SUCCES", "FONT_OLD_20_WHITE_HI", 2500.0, true, 0, 255, 0); 
                        buyItem(selectedTrade.itemId, inputAmount_Value);

                    dialogTrade_Elements.drawCurrency
                        .setText("DIALOG_VIEW_AVAILABLE;(;" + currencyItem.getTemplate().getInstance() + ";): " + (currencyItem.getAmount() - price));  

                    lockBuy = getTickCount() + 5000; 
                }
            }
        }
    }
});

dialogTrade_Elements.buttonEnd
.addEvent(GUIEventInterface.Click, function() 
{
    setInterfaceVisible(false, "VIEW_DIALOG"); 
});

dialogList_Elements.listOptions
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible == false) clearButtons();
});

dialogTrade_Elements.gridItems
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible == false) clearRenders();
});

local function selectItem(selectId)
{
    local draw = dialogTrade_Elements.drawDescription,
        render = dialogTrade_Elements.renderItem;

    draw.clearDraws();
    
    selectedTrade = getCurrentDialogData().list[selectId];

    local inputValue = dialogTrade_Elements.inputAmount.getInputValue();

    if(inputValue != null)
    {
        dialogTrade_Elements.buttonPrice
            .setText(inputValue * selectedTrade.price + " ;DIALOG_VIEW_COST");
    }

    local template = dialogTrade_Elements.gridItems.getRenders()[selectId].getItem();

    render.setItemInstance(template.getVisual());
    draw.pushDraw(format("%s; - %d ;DIALOG_VIEW_ITEM_PRICE", template.getInstance(), selectedTrade.price));

    if(template instanceof Item.Gear)
    {    
        local requirementList = template.getRequirements();

        if(requirementList.len() > 0)
        {
            draw.pushDraw("");
            draw.pushDraw("DIALOG_VIEW_REQUIREMENT_TITLE;:");
            
            foreach(index, requirement in requirementList)
            {
                if(requirement > 0)
                    draw.pushDraw("REQUIREMENT_TYPE_" + index + "; - " + requirement);     
            }
        }

        if(template instanceof Item.Armor)
        {
            draw.pushDraw("");
            draw.pushDraw("DIALOG_VIEW_PROTECTION_TITLE;:");     

            local protectionList = template.getProtections();
            local text = "";

            foreach(index, protection in protectionList)
            {
                if(protection > 0)
                {
                    draw.pushDraw("DAMAGE_TYPE_" + index + "; - " + protection);     
                }
            }
        }
        else if(template instanceof Item.Weapon)
        {
            local damageType = template.getDamageType();
            
            draw.pushDraw("");

            if(damageType != -1) draw.pushDraw("DAMAGE_TYPE_" + template.getDamageType() + ";: " + template.getDamage());      
            if(template instanceof Item.Spell) draw.pushDraw("DIALOG_VIEW_MANA_USAGE;: " + template.getManaUsage());          
        }
    }
    else if(template instanceof Item.Usable)
    {
        draw.pushDraw("");
        draw.pushDraw("DIALOG_VIEW_EFFECT_TITLE;:");

        local effectList = template.getEffects();

        foreach(index, effect in effectList)
        {
            if(typeof(effect) == "integer")
            {
                if(effect > 0)
                {
                    draw.pushDraw("EFFECT_TYPE_" + index + "; - " + effect);     
                }
            }
            else 
                draw.pushDraw(effect);     
        }
    }

    local translationId = template.getInstance() + "_DESC";
    local description = getTranslation(translationId);

    if(translationId != description)
    {
        draw.pushDraw("");
        draw.pushDraw("DIALOG_VIEW_DESCRIPTION_TITLE;:");     

        local lines = textSlice(description, 35);

        foreach(line in lines)
            draw.pushDraw(line);
    }
};

local function gridCreate(data)
{
    foreach(item in data.list)
    {
        local template = getItemTemplateById(item.itemId);

        local render = GUI.GridRender(template.getVisual())
            .setItem(template)
            .setTexture("SLOT_TRADE.TGA")
            .setFocusTexture("SLOT_TRADE_HIGHLIGHTED.TGA");
    
        dialogTrade_Elements.gridItems.pushRender(render);
    }

    selectItem(0);
};

dialogTrade_Elements.gridItems
.addEvent(GUIEventInterface.Click, selectItem);

local function selectOption(instance, typeId)
{   
    local transcription = getDialog(instance);
    if(transcription == null) return;

    dialogList_View.setVisible(false);

    local dialogSequence = GUI.DialogSequence();

    foreach(dialog in transcription.getDialogs())
    {
        local who = dialog.who ? heroId : getDialogPartnerId();

        if(who != -1) 
            dialogSequence.addText(GUI.Dialog(getTranslation(dialog.translation), who, dialog.file, dialog.timestamp));
    }

    dialogSequence.addEvent(GUIEventDialog.Start, function() 
    {
        lockInterface(true); 
    })
    .addEvent(GUIEventDialog.Stop, function() 
    {
        lockInterface(false); 

            dialogSequence.destroy();
        selectDialogOption(instance);
    })
    .start(); 
};

local function listCreate(data)
{
    local list = dialogList_Elements.listOptions;

    foreach(option in data)
    {
        local dialog = getDialog(option.instance);
        
        if(dialog)
        {
            list.pushListButton(
                GUI.ListText(getTranslation(dialog.getTitle()))
                .setFormat(GUITextFormat.Left)
                .setTexture(null)  
            );
        }
        else 
        {
            list.pushListButton(
                GUI.ListText(option.instance)
                .setFormat(GUITextFormat.Left)
                .setTexture(null)
            );
        }
    }
};

dialogList_Elements.listOptions
.addEvent(GUIEventInterface.Click, function(id) 
{            
        local data = getCurrentDialogData()[id];
    selectOption(data.instance, data.typeId); 
});

dialogList_View.addEvent(GUIEventInterface.Visible, function(visible) 
{            
    if(visible == false) dialog_View.removeObject(dialogList_View); 
});

dialogTrade_View.addEvent(GUIEventInterface.Visible, function(visible) 
{            
    if(visible == false) dialog_View.removeObject(dialogTrade_View);
});

addEventHandler("Dialog.onInitialData", function(instance, typeId, data)
{
    setInterfaceVisible(true, "VIEW_DIALOG"); 

    local transcription = getDialog(instance);
    if(transcription == null) return;
    
    local dialogSequence = GUI.DialogSequence();

    foreach(dialog in transcription.getDialogs())
    {
            local who = dialog.who ? heroId : getDialogPartnerId();
        dialogSequence.addText(GUI.Dialog(getTranslation(dialog.translation), who, dialog.file, dialog.timestamp));
    }

    dialogSequence.addEvent(GUIEventDialog.Start, function() 
    {
        lockInterface(true); 
    })
    .addEvent(GUIEventDialog.Stop, function() 
    {
        dialogSequence.destroy();

        switch(typeId)
        {           
            case DialogType.Buy: 
            { 
                    gridCreate(data);
                dialog_View.pushObject(dialogTrade_View);
                dialogTrade_View.setVisible(true);
            } 
            break;
            case DialogType.List: 
            { 
                    listCreate(data);
                dialog_View.pushObject(dialogList_View); 
                dialogList_View.setVisible(true);
            } 
            break;
            case DialogType.None: 
            case DialogType.End: 
                setInterfaceVisible(false, "VIEW_DIALOG"); 
            break;
        }  
        
        lockInterface(false); 
    });

    dialogSequence.start(); 
});

addEventHandler("Dialog.onUpdateData", function(typeId, data)
{
    setInterfaceVisible(true, "VIEW_DIALOG");

    switch(typeId)
    {
        case DialogType.None:
        case DialogType.List: 
        {
            if(data != null)
            {
                    listCreate(data);       
                dialog_View.pushObject(dialogList_View); 
                dialogList_View.setVisible(true); 
            }
        } 
        break;
        case DialogType.Buy: 
        { 
                gridCreate(data);
            dialog_View.pushObject(dialogTrade_View);
            dialogTrade_View.setVisible(true); 
        } 
        break;
        case DialogType.End: 
            setInterfaceVisible(false, "VIEW_DIALOG"); 
        break;
    }
});

addEventHandler("View.onViewVisible", function(visible, viewId)
{
    if(viewId != "VIEW_DIALOG") return;
    
    if(visible == false)
    {
        selectedTrade = null;
        
        local partnerId = getDialogPartnerId(), partner = getNpcByLocalId(partnerId);

        if(partner) 
        {
            setPlayerAngle(partnerId, partner.getAngle());
        }
        
        local module = getHero().getDialogModule();
            module.finishConversation();
    }
    else 
    {
        local partnerId = getDialogPartnerId();

        local partnerPosition = getPlayerPosition(partnerId), 
            playerPosition = getPlayerPosition(heroId);

        stopAni(partnerId);

        setPlayerAngle(partnerId, getVectorAngle(partnerPosition.x, partnerPosition.z, playerPosition.x, playerPosition.z));
        setPlayerAngle(heroId, getVectorAngle(playerPosition.x, playerPosition.z, partnerPosition.x, partnerPosition.z));
    }

    qs_setVisibility(!visible);
});

// REGISTER ELEMENTS
dialogList_View
.pushObject(dialogList_Elements.textureBackground)
.pushObject(dialogList_Elements.listOptions);

dialogTrade_View
.pushObject(dialogTrade_Elements.textureBackground)
.pushObject(dialogTrade_Elements.gridItems)
.pushObject(dialogTrade_Elements.drawCurrency)
.pushObject(dialogTrade_Elements.textureDescription)
.pushObject(dialogTrade_Elements.drawDescription)
.pushObject(dialogTrade_Elements.renderItem)
.pushObject(dialogTrade_Elements.buttonPrice)
.pushObject(dialogTrade_Elements.inputAmount)
.pushObject(dialogTrade_Elements.buttonBuy)
.pushObject(dialogTrade_Elements.buttonEnd);

registerInterfaceView(dialog_View);