
local stats_View = GUI.View("VIEW_PLAYER_STATS");

local stats_Size = {
    texture = GUI.SizePr(52, 61.5),
}

local stats_Position = {
    texture = GUI.PositionPr(24, 20),
    draw_title = GUI.PositionPr(50, 23),
    draw_stats_0 = GUI.PositionPr(37.5, 29),
    draw_stats_1 = GUI.PositionPr(62.5, 29)
}

local stats_Elements = {
    texture = GUI.Texture("BACKGROUND_BORDER_512x512.TGA", stats_Position.texture, stats_Size.texture),
    draw_title = GUI.Draw("STATS_VIEW_TITLE; - " + getPlayerName(heroId), stats_Position.draw_title)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setColor(GUI.Color(255, 255, 255))
        .setCenterAtPosition(true)
        .addEvent(GUIEventInterface.Visible, function(visible)
        {
            if(visible)
            {     
                setText("STATS_VIEW_TITLE; - " + getPlayerName(heroId));
            }
        }
    ),
    draw_stats_0 = GUI.MultiDraw(stats_Position.draw_stats_0, 20)
        .addEvent(GUIEventInterface.Visible, function(visible)
        {
            if(visible)
            {
                clearDraws();

                pushDraw("STATS_VIEW_CHARACTER", GUI.Color(255, 255, 255), -1, "FONT_OLD_20_WHITE_HI");
                pushDraw("");
                pushDraw("LEVEL;: " + getLevel());
                pushDraw("EXPERIENCE;: " + getExp());
                pushDraw("EXPERIENCE_NEXT_LEVEL;: " + getNextLevelExp());
                pushDraw("LEARN_POINTS;: " + getLearnPoints());
                pushDraw("");      
                pushDraw("STATS_VIEW_ATRIBUTES", GUI.Color(255, 255, 255), -1, "FONT_OLD_20_WHITE_HI");
                pushDraw("");
                pushDraw("STRENGTH;: " + getPlayerStrength(heroId));
                pushDraw("DEXTERITY;: " + getPlayerDexterity(heroId));
                pushDraw("MANA;: " + getPlayerMana(heroId) + "/" + getPlayerMaxMana(heroId));
                pushDraw("HEALTH;: " + getPlayerHealth(heroId) + "/" + getPlayerMaxHealth(heroId));
                pushDraw("");
                pushDraw("STATS_VIEW_PROTECTION", GUI.Color(255, 255, 255), -1, "FONT_OLD_20_WHITE_HI");
                pushDraw("");
                pushDraw("DAMAGE_TYPE_" + DAMAGE_BLUNT + ";: " + getHeroProtection(DAMAGE_BLUNT));
                pushDraw("DAMAGE_TYPE_" + DAMAGE_EDGE + ";: " + getHeroProtection(DAMAGE_EDGE));
                pushDraw("DAMAGE_TYPE_" + DAMAGE_POINT + ";: " + getHeroProtection(DAMAGE_POINT));
                pushDraw("DAMAGE_TYPE_" + DAMAGE_MAGIC + ";: " + getHeroProtection(DAMAGE_MAGIC));
                setCenterAtPosition(true);
            }
        }
    ),
    draw_stats_1 = GUI.MultiDraw(stats_Position.draw_stats_1, 20)
        .addEvent(GUIEventInterface.Visible, function(visible)
        {
            if(visible)
            {
                clearDraws();

                pushDraw("STATS_VIEW_SKILLS", GUI.Color(255, 255, 255), -1, "FONT_OLD_20_WHITE_HI");
                pushDraw("");
                pushDraw("MAGIC_LEVEL;: " + getPlayerMagicLevel(heroId));
                pushDraw("");
                pushDraw("WEAPON_SKILL_" + WEAPON_1H + ";: " + getPlayerSkillWeapon(heroId, WEAPON_1H));
                pushDraw("WEAPON_SKILL_" + WEAPON_2H + ";: " + getPlayerSkillWeapon(heroId, WEAPON_2H));
                pushDraw("WEAPON_SKILL_" + WEAPON_BOW + ";: " + getPlayerSkillWeapon(heroId, WEAPON_BOW));
                pushDraw("WEAPON_SKILL_" + WEAPON_CBOW + ";: " + getPlayerSkillWeapon(heroId, WEAPON_CBOW));
                pushDraw("");      
                pushDraw("STATS_VIEW_GUILD", GUI.Color(255, 255, 255), -1, "FONT_OLD_20_WHITE_HI");
                pushDraw("");     
                pushDraw("GUILD_NAME;: ;" + getPlayerGuild(heroId));
                setCenterAtPosition(true);    
            }
        }
    )
}

// REGISTER
stats_View
.pushObject(stats_Elements.texture)
.pushObject(stats_Elements.draw_title)
.pushObject(stats_Elements.draw_stats_0)
.pushObject(stats_Elements.draw_stats_1);

registerInterfaceView(stats_View, KEY_B, true);