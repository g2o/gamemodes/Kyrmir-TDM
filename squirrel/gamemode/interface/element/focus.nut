
enable_NicknameId(false);

local barPosition = getBarPosition(HUD_FOCUS_BAR), barSize = getBarSize(HUD_FOCUS_BAR);

local focus_Elements = {
    barParade = GUI.Bar(GUI.Position(barPosition.x, barPosition.y + 150), GUI.Size(barSize.width, barSize.height), GUI.Size(40, 45), null, "BAR_MISC_GRAY.TGA"),
    drawName = GUI.Draw("NICKNAME", GUI.Position(0, 0))
        .setCenterAtPosition(true),
    drawInformation = GUI.MultiDraw(GUI.Position(0, 0))
        .pushDraw("INTERACTION_KEY_0")
        .pushDraw("CTRL")
        .setCenterAtPosition(true),
    drawsPlayers = {}
};

local lastFocusId = -1;

// CALLBACKS
addEventHandler("onRender", function() 
{     
    local maxPlayers = getMaxSlots();

    for(local id = 0; id < maxPlayers; id++)
    {
        if(id == heroId || isPlayerCreated(id) == false) continue;

        local position = getPlayerPosition(id), projection = Camera.project(position.x, position.y + 100, position.z);

        if(projection && isPlayerStreamed(id))
        {
            if(id == lastFocusId)
            {
                if(id in focus_Elements.drawsPlayers)
                {
                    focus_Elements.drawsPlayers[id].setVisible(false);
                        delete focus_Elements.drawsPlayers[id];
                }
            }
            else 
            {
                local text = format("%s (%d/%d) (%d/%d)", getPlayerName(id), getPlayerHealth(id), getPlayerMaxHealth(id), getPlayerParades(id), MAX_PARADE);
                
                local heroPosition = getPlayerPosition(heroId);
                local scale = 1.0 - (getDistance3d(position.x, position.y, position.z, heroPosition.x, heroPosition.y, heroPosition.z) / 5000.0);
                
                if(scale > 1.0) scale = 1.0;
                else if(scale < 0.5) scale = 0.5;

                if(getPlayerGuild(id) == getPlayerGuild(heroId))
                {
                    if(id in focus_Elements.drawsPlayers)
                    {
                        focus_Elements.drawsPlayers[id]
                            .setText(text)
                            .setPosition(GUI.PositionPx(projection.x, projection.y))
                            .setTextScale(scale, scale);
                    }   
                    else 
                    {
                        focus_Elements.drawsPlayers[id] <- GUI.Draw(text, GUI.PositionPx(projection.x, projection.y))
                            .setColor(GUI.Color(0, 255, 0))
                            .setCenterAtPosition(true)
                            .setVisible(true)
                            .setTextScale(scale, scale);
                    }
                }
                else 
                {
                    if(id in focus_Elements.drawsPlayers)
                    {
                        focus_Elements.drawsPlayers[id].setVisible(false);
                            delete focus_Elements.drawsPlayers[id];
                    }      
                }
            }
        }
        else 
        {
            if(id in focus_Elements.drawsPlayers)
            {
                focus_Elements.drawsPlayers[id].setVisible(false);
                    delete focus_Elements.drawsPlayers[id];
            }      
        }
    }
});

addEventHandler("onRenderFocus", function(vobType, focusId, x, y, name) 
{ 
    if(focusId != -1) 
    {
        local color = getPlayerColor(focusId), weaponMode = getPlayerWeaponMode(heroId);

        if(focusId >= getMaxSlots())
        {
            local npc = getNpcByLocalId(focusId);

            if(isNpcTalkative(npc.getId()))
            {
                focus_Elements.drawInformation
                    .setPosition(GUI.Position(x, y - 500))
                    .setVisible(true);
            }

            local drawText = format("%s; (%d)", npc.getName(), npc.getId());

            if((weaponMode == WEAPONMODE_BOW || weaponMode == WEAPONMODE_CBOW) 
                && isNpcTalkative(npc.getId()) == false)
            {
                local hitChance = getPlayerSkillWeapon(heroId, weaponMode == WEAPONMODE_BOW ? WEAPON_BOW : WEAPON_CBOW);
        
                if(hitChance < 100)
                {
                    local position = getPlayerPosition(heroId), fPosition = getPlayerPosition(focusId);
                    local distance = getDistance3d(position.x, position.y, position.z, fPosition.x, fPosition.y, fPosition.z);

                    if(distance <= RANGED_CHANCE_MINDIST)
                    {
                        hitChance = 100.0;
                    }
                    else 
                    {
                        hitChance = hitChance + (distance / RANGED_CHANCE_MAXDIST) * (0.0 - hitChance);
                    }
                }

                drawText += format("\nHit chance: %.2f", hitChance);
            }

            focus_Elements.drawName
                .setPureText(false)
                .setText(drawText)
                .setColor(GUI.Color(color.r, color.g, color.b))
                .setPosition(GUI.Position(x, y))
                .setVisible(true);
        }
        else 
        {
            focus_Elements.drawName
                .setPureText(true)
                .setText(format("%s; (%d)", name, focusId))
                .setColor(GUI.Color(color.r, color.g, color.b))
                .setPosition(GUI.Position(x, y))
                .setVisible(true);

            focus_Elements.barParade.setProgress(getPlayerParades(focusId) / MAX_PARADE.tofloat());
            focus_Elements.barParade.setVisible(true);     
        }
        
        lastFocusId = focusId;
    }
    else 
    {
        focus_Elements.drawName
            .setPureText(false)
            .setText("INTERACTION_KEY_0")
            .setColor(GUI.Color(255, 255, 255))
            .setPosition(GUI.Position(x, y))
            .setVisible(true);    
    }

    cancelEvent();
});

addEventHandler("onLostFocus", function(vobType, focusId, name) 
{
    foreach(element in focus_Elements)
    {
        if(typeof(element) != "table")
        {
            element.setVisible(false);
        }
    }

    lastFocusId = -1;
});

addEventHandler("onWorldChange", function(world)
{
    foreach(element in focus_Elements)
    {
        if(typeof(element) != "table")
        {
            element.setVisible(false);
        }
    }

    lastFocusId = -1;
});

addEventHandler("onPlayerDestroy", function(playerId)
{
    if(playerId in focus_Elements.drawsPlayers)
    {
        focus_Elements.drawsPlayers[playerId].setVisible(false);
            delete focus_Elements.drawsPlayers[playerId];
    }  
});

addEventHandler("onPlayerDead", function(playerId)
{
    if(playerId == lastFocusId) focus_Elements.barParade.setVisible(false);     
});
 
addEventHandler("Damage.onParadeUpdate", function(playerId, parade)
{
    if(playerId != lastFocusId) 
        return;

    focus_Elements.barParade.setProgress(getPlayerParades(playerId) / MAX_PARADE.tofloat());
});

addEventHandler("onKey", function(key) 
{
    if(chatInputIsOpen() || key != KEY_LCONTROL) return;

    local focusId = getFocusNpc();
    
    if(focusId != -1)
    {
        local pPosition = getPlayerPosition(heroId), fPosition = getPlayerPosition(focusId);

        if(getDistance2d(pPosition.x, pPosition.z, fPosition.x, fPosition.z) < 400)
        {
            local npc = getNpcByLocalId(focusId);

            if(npc != null && isNpcTalkative(npc.getId()))
            {
                beginConversation(focusId);
            }
        }
    }
});

addEventHandler("onFocusCollect", function(vobList) 
{
    if(getPlayerWeaponMode(heroId) == WEAPONMODE_NONE) 
        return;

    local weapon = getHeroCurrentWeapon();

    if(weapon != null)
    {
        local template = weapon.getTemplate();
        
        if(template instanceof Item.Spell && template.isInstant())
            return;
    }

    local removeList = [];

    foreach(index, vob in vobList)
    {
        local playerId = getPlayerIdByPtr(vob);
        
        if(playerId == -1) 
            continue;

        if(getPlayerGuild(playerId) == getPlayerGuild(heroId)) 
            removeList.push(vob);
    }   

    foreach(vob in removeList)
    {   
        local index = vobList.find(vob);
        
        if(index == -1) 
            continue;

        vobList.remove(index);
    }
});