
local spawnInstance = null;

// INTERFACE
local spawner_View = GUI.View("VIEW_BOT_SPAWNER");

local spawner_Size = {
    textureBackground = GUI.SizePr(22.5, 42),
    line = GUI.SizePr(21, 2.5),
    buttonSpawn = GUI.SizePr(10, 3),
    buttonDelete = GUI.SizePr(10, 3),
}

local spawner_Position = {
    textureBackground = GUI.PositionPr(71.25, 15),
    listMonsters = GUI.PositionPr(72, 16.5),
    buttonSpawn = GUI.PositionPr(71.66, 58),
    buttonDelete = GUI.PositionPr(83.32, 58),
}

local spawner_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_1024x2048.TGA", spawner_Position.textureBackground, spawner_Size.textureBackground),
    listMonsters = GUI.List(spawner_Position.listMonsters, 15)
        .setLineSize(spawner_Size.line)	
        .setScrollable(true)
        .setLineGap(15),
    buttonSpawn = GUI.Button("BOT_VIEW_SPAWN", spawner_Position.buttonSpawn, spawner_Size.buttonSpawn)
        .setTexture("MENU_INGAME.TGA"),
    buttonDelete = GUI.Button("BOT_VIEW_DELETE", spawner_Position.buttonDelete, spawner_Size.buttonDelete)
        .setTexture("MENU_INGAME.TGA"),
};

spawner_Elements.buttonSpawn
.addEvent(GUIEventInterface.Click, function() 
{
    if(spawnInstance != null) getAdmin().spawn(spawnInstance);
});

spawner_Elements.buttonDelete
.addEvent(GUIEventInterface.Click, function() 
{
    getAdmin().unspawn();
});

spawner_Elements.listMonsters
.addEvent(GUIEventInterface.Click, function(id) 
{
    spawnInstance = getMonsterTemplates()[id];
})
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible == false)
    {
        clearButtons();
    }
    else 
        foreach(instance in getMonsterTemplates()) 
            pushListButton(GUI.ListText(instance).setTexture("MENU_INGAME.TGA"));
});

// REGISTER ELEMENTS
spawner_View 
.pushObject(spawner_Elements.textureBackground)
.pushObject(spawner_Elements.listMonsters)
.pushObject(spawner_Elements.buttonSpawn)
.pushObject(spawner_Elements.buttonDelete);

registerInterfaceView(spawner_View, KEY_F7, true, true, PermissionLevel.Administrator);