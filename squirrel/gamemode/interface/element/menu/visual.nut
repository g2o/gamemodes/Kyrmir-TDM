
const lastFaceId = 164;
const lastTorsoId = 12;

local headModels = [ 
	"Hum_Head_Pony", 
	"Hum_Head_Fighter", 
	"Hum_Head_FatBald", 
	"Hum_Head_Bald", 
	"Hum_Head_Thief", 
	"Hum_Head_Psionic",
	"Hum_Head_Babe",
	"Hum_Head_Babe1",
	"Hum_Head_Babe2",
	"Hum_Head_Babe3",
	"Hum_Head_Babe4",
	"Hum_Head_Babe5",
	"Hum_Head_Babe6",
	"Hum_Head_Babe7",
	"Hum_Head_Babe8",
	"Hum_Head_BabeHair",
    "Orc_HeadShaman",
    "Orc_HeadWarrior",
    "Ske_Head",
    "Zom_Head"
];

local bodyModels = [
    "Hum_Body_Naked0",
    "Hum_Body_Babe0"
];

const CAMERA_DISTANCE = 300;

local sideCamera = false;

// INTERFACE
local visual_View = GUI.View("VIEW_VISUAL");

local firstVisual_Size = {
    textureBackgroundTop = GUI.SizePr(30, 69),
    textureBackgroundBottom = GUI.SizePr(30, 10),
    facesListLine = GUI.SizePr(27, 5),
    textureIcons = GUI.SizePr(2.5, 2.5),
    buttonSave = GUI.SizePr(12, 6),
    buttonReset = GUI.SizePr(12, 6),
    buttonCamera = GUI.SizePr(12, 6)
};

local firstVisual_Position = {
    textureBackgroundTop = GUI.PositionPr(67.5, 6),
    textureBackgroundBottom = GUI.PositionPr(67.5, 77),
    drawTitle = GUI.PositionPr(82.5, 8),
    facesList = GUI.PositionPr(69, 14),
    drawFacesList = GUI.PositionPr(82.5, 49.5),
    drawHead = GUI.PositionPr(82.5, 59),
    buttonHeadLeft = GUI.PositionPr(70, 59),
    buttonHeadRight = GUI.PositionPr(92.5, 59),
    drawTorso = GUI.PositionPr(82.5, 63),
    buttonTorsoLeft = GUI.PositionPr(70, 63),
    buttonTorsoRight = GUI.PositionPr(92.5, 63),
    drawSex = GUI.PositionPr(82.5, 67),
    buttonSexLeft = GUI.PositionPr(70, 67),
    buttonSexRight = GUI.PositionPr(92.5, 67),
    buttonSave = GUI.PositionPr(69.5, 79),
    buttonReset = GUI.PositionPr(83.5, 79),
    buttonCamera = GUI.PositionPr(2.5, 6)
};

local firstVisual_Elements = {
    textureBackgroundTop = GUI.Texture("DLG_CONVERSATION.TGA", firstVisual_Position.textureBackgroundTop, firstVisual_Size.textureBackgroundTop),
    textureBackgroundBottom = GUI.Texture("DLG_CONVERSATION.TGA", firstVisual_Position.textureBackgroundBottom, firstVisual_Size.textureBackgroundBottom),
    drawTitle = GUI.Draw("VISUAL_VIEW_TITLE", firstVisual_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
    facesList = GUI.List(firstVisual_Position.facesList, 6)
        .setLineSize(firstVisual_Size.facesListLine)
        .setLineGap(50)	
        .setScrollable(true),
    drawFacesList = GUI.Draw("VISUAL_VIEW_FACES_DESCRIPTION", firstVisual_Position.drawFacesList)
        .setCenterAtPosition(true),
    drawHead = GUI.Draw("VISUAL_VIEW_HEAD", firstVisual_Position.drawHead)
        .setCenterAtPosition(true),
    buttonHeadLeft = GUI.Button("", firstVisual_Position.buttonHeadLeft, firstVisual_Size.textureIcons)
        .setTexture("L.TGA")
        .setFocusAlpha(200),
    buttonHeadRight = GUI.Button("", firstVisual_Position.buttonHeadRight, firstVisual_Size.textureIcons)
        .setTexture("R.TGA")
        .setFocusAlpha(200),
    drawTorso = GUI.Draw("VISUAL_VIEW_TORSO", firstVisual_Position.drawTorso)
        .setCenterAtPosition(true),
    buttonTorsoLeft = GUI.Button("", firstVisual_Position.buttonTorsoLeft, firstVisual_Size.textureIcons)
        .setTexture("L.TGA")
        .setFocusAlpha(200),
    buttonTorsoRight = GUI.Button("", firstVisual_Position.buttonTorsoRight, firstVisual_Size.textureIcons)
        .setTexture("R.TGA")
        .setFocusAlpha(200),
    drawSex = GUI.Draw("VISUAL_VIEW_SEX", firstVisual_Position.drawSex)
        .setCenterAtPosition(true),
    buttonSexLeft = GUI.Button("", firstVisual_Position.buttonSexLeft, firstVisual_Size.textureIcons)
        .setTexture("L.TGA")
        .setFocusAlpha(200),
    buttonSexRight = GUI.Button("", firstVisual_Position.buttonSexRight, firstVisual_Size.textureIcons)
        .setTexture("R.TGA")
        .setFocusAlpha(200),
    buttonSave = GUI.Button("VISUAL_VIEW_SAVE_VISUAL", firstVisual_Position.buttonSave, firstVisual_Size.buttonSave)
        .setTexture("BUTTON_BROWN.TGA"),
    buttonReset = GUI.Button("VISUAL_VIEW_RESET_VISUAL", firstVisual_Position.buttonReset, firstVisual_Size.buttonReset)
        .setTexture("BUTTON_RED.TGA"),
    buttonCamera = GUI.Button("VISUAL_VIEW_CAMERA", firstVisual_Position.buttonCamera, firstVisual_Size.buttonCamera)
        .setTexture("BUTTON_BROWN.TGA"),
};

local faceId = 0;

while(faceId < lastFaceId)
{    
    local row = GUI.ListRow();

    for(local id = 0; id < 3; id++)
    {
        if(faceId < lastFaceId)
        {
            local buttonFaceId = faceId;

            row.pushButton(GUI.Button("")
                .setSize(GUI.SizePr(9, 5))
                .setTexture("HUM_HEAD_V" + (faceId++) + "_C0.TGA")
                .setFocusAlpha(180)
                .addEvent(GUIEventInterface.Click, function() 
                {
                        local visual = getPlayerVisual(heroId);	
                    setPlayerVisual(heroId, visual.bodyModel, visual.bodyTxt, visual.headModel, buttonFaceId);	
                }) 
            );
        }
    }

    firstVisual_Elements.facesList.pushListButton(row);
}

visual_View.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible)
    {  
        local position = getPlayerPosition(heroId),
            angle = getPlayerAngle(heroId) / 180 * PI;
        
        Camera.setPosition(position.x + sin(angle) * CAMERA_DISTANCE, position.y + 80, position.z + cos(angle) * CAMERA_DISTANCE);
        Camera.setRotation(0, getPlayerAngle(heroId) + 180, 0);
    }
});

firstVisual_Elements.buttonHeadLeft
.addEvent(GUIEventInterface.Click, function() 
{
        local visual = getPlayerVisual(heroId), diff = headModels.find(visual.headModel) - 1;
    setPlayerVisual(heroId, visual.bodyModel, visual.bodyTxt, diff < 0 ? headModels[headModels.len() - 1] : headModels[diff], visual.headTxt);	
});

firstVisual_Elements.buttonHeadRight
.addEvent(GUIEventInterface.Click, function() 
{ 
        local visual = getPlayerVisual(heroId), suma = headModels.find(visual.headModel) + 1;
    setPlayerVisual(heroId, visual.bodyModel, visual.bodyTxt, suma > headModels.len() - 1 ? headModels[0] : headModels[suma], visual.headTxt);	
});

firstVisual_Elements.buttonTorsoLeft
.addEvent(GUIEventInterface.Click, function() 
{
        local visual = getPlayerVisual(heroId), diff = visual.bodyTxt - 1;
    setPlayerVisual(heroId, visual.bodyModel, diff < 0 ? lastTorsoId : diff, visual.headModel, visual.headTxt);	
});

firstVisual_Elements.buttonTorsoRight
.addEvent(GUIEventInterface.Click, function() 
{ 
     local visual = getPlayerVisual(heroId), suma = visual.bodyTxt + 1;
    setPlayerVisual(heroId, visual.bodyModel, suma > lastTorsoId ? 0 : suma, visual.headModel, visual.headTxt);	       	
});  

firstVisual_Elements.buttonSexLeft
.addEvent(GUIEventInterface.Click, function() 
{      
        local visual = getPlayerVisual(heroId), diff = bodyModels.find(visual.bodyModel) - 1;
    setPlayerVisual(heroId, diff < 0 ? bodyModels[bodyModels.len() - 1] : bodyModels[diff], visual.bodyTxt, visual.headModel, visual.headTxt);	 
});

firstVisual_Elements.buttonSexRight
.addEvent(GUIEventInterface.Click, function() 
{           
        local visual = getPlayerVisual(heroId), suma = bodyModels.find(visual.bodyModel) + 1; 
    setPlayerVisual(heroId, suma > bodyModels.len() - 1 ? bodyModels[0] : bodyModels[suma], visual.bodyTxt, visual.headModel, visual.headTxt);           	
});

firstVisual_Elements.buttonSave
.addEvent(GUIEventInterface.Click, function() 
{
        local visual = getPlayerVisual(heroId);
    getHero().visual(visual.bodyModel, visual.bodyTxt, visual.headModel, visual.headTxt);

    lockInterface(false);

    if(getPlayerGuild(heroId) == null)
    {
	    setInterfaceVisible(true, "VIEW_GUILD");
            lockInterface(true);
    }
    else 
	    setInterfaceVisible(false, "VIEW_VISUAL");
});

firstVisual_Elements.buttonReset
.addEvent(GUIEventInterface.Click, function() 
{
    setPlayerVisual(heroId, "Hum_Body_Naked0", 9, "Hum_Head_Pony", 18);           	
});

firstVisual_Elements.buttonCamera
.addEvent(GUIEventInterface.Click, function() 
{           
    local position = getPlayerPosition(heroId), angle = 0; 

    if(sideCamera = !sideCamera)
        angle = (getPlayerAngle(heroId) + 90) / 180 * PI;   
    else 
        angle = getPlayerAngle(heroId) / 180 * PI;

    Camera.setPosition(position.x + sin(angle) * CAMERA_DISTANCE, position.y + 80, position.z + cos(angle) * CAMERA_DISTANCE);
    Camera.setRotation(0, getPlayerAngle(heroId) + (sideCamera ? 270 : 180), 0);
});

visual_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible && getPlayerGuild(heroId) == null) 
        lockInterface(true);
    else 
        if(getPlayerInstance(heroId) != "PC_HERO")
        {
            setInterfaceVisible(false, "VIEW_VISUAL");
        }
});

// REGISTER ELEMENTS
visual_View.pushObject(firstVisual_Elements.textureBackgroundTop)
.pushObject(firstVisual_Elements.textureBackgroundBottom)
.pushObject(firstVisual_Elements.drawTitle)
.pushObject(firstVisual_Elements.facesList)
.pushObject(firstVisual_Elements.drawFacesList)
.pushObject(firstVisual_Elements.drawHead)
.pushObject(firstVisual_Elements.buttonHeadLeft)
.pushObject(firstVisual_Elements.buttonHeadRight)
.pushObject(firstVisual_Elements.drawTorso)
.pushObject(firstVisual_Elements.buttonTorsoLeft)
.pushObject(firstVisual_Elements.buttonTorsoRight)
.pushObject(firstVisual_Elements.drawSex)
.pushObject(firstVisual_Elements.buttonSexLeft)
.pushObject(firstVisual_Elements.buttonSexRight)
.pushObject(firstVisual_Elements.buttonSave)
.pushObject(firstVisual_Elements.buttonReset)
.pushObject(firstVisual_Elements.buttonCamera);

registerInterfaceView(visual_View);
