
// INTERFACE
local guild_View = GUI.View("VIEW_GUILD");

local guild_Size = {
    buttonCamp = GUI.SizePr(16, 56.8),
};

local guild_Position = {
    drawTitle = GUI.PositionPr(50, 10),
};

local guild_Elements = {
    buttonsCamps = {},
    drawTitle = GUI.Draw("GUILD_VIEW_TITLE", guild_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
}

guild_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible) 
    {    
        local guildList = getGuilds().filter(function(index, value) {
            return value.isLocked() == false;
        });

        if(guild_Elements.buttonsCamps.len() != 0) 
        {
            foreach(guild in guildList)
                guild_Elements.buttonsCamps[guild.getId()].draw.setText("GUILD_VIEW_BTN_CAMP;: " + guild.getMembers());

            return;
        }

        foreach(guild in guildList)
        {
            local guildId = guild.getId();

            guild_Elements.buttonsCamps[guildId] <- {};

            guild_Elements.buttonsCamps[guildId].button <- GUI.Button("", GUI.Position(0, 0), guild_Size.buttonCamp)
                .setTexture("CARD_" + guild.getId() + ".TGA")
                .addEvent(GUIEventInterface.Click, function() 
                {            
                    if(guild.isLocked() == false)            
                        getGuildManager().changeGuild(guildId);
                    else 
                        GUI.GameText(GUI.PositionPr(50, 50), "LOCKED", "FONT_OLD_20_WHITE_HI", 2500.0, true, 255, 0, 0);
                });

            guild_Elements.buttonsCamps[guildId].draw <- GUI.Draw("GUILD_VIEW_BTN_CAMP;: " + guild.getMembers(), GUI.Position(0, 0))
                .setFont("FONT_OLD_20_WHITE_HI")
                .setCenterAtPosition(true);
        }

        local guildsSize = guildList.len();
        local buttonWidthPx = guild_Size.buttonCamp.getWidthPx();

        local width = (guildsSize * buttonWidthPx) + ((guildsSize - 1) * 60);
        
        local startPositionX = (getResolution().x / 2) - width / 2, startPositionY = 130;
        local multipler = 0;

        foreach(element in guild_Elements.buttonsCamps)
        {
            guild_View.pushObject(element.button
                .setPosition(GUI.PositionPx(startPositionX + (buttonWidthPx + 60) * multipler, multipler % 2 ? startPositionY + 25 : startPositionY))
                .setVisible(true)
            );
            
            local buttonPosition = element.button.getPosition(), buttonSize = element.button.getSize();

            guild_View.pushObject(element.draw
                .setPosition(GUI.Position(buttonPosition.getX() + buttonSize.getWidth() / 2, buttonPosition.getY() + buttonSize.getHeight() + (multipler % 2 ? 250 : 50)))
                .setVisible(true)
            );

            multipler++;
        }
    }
    else 
    {
        guild_View.clearObjects();
        guild_Elements.buttonsCamps.clear();
    }
});

addEventHandler("Guild.onMembersUpdate", function(guildId, members)
{
    if(guild_View.isVisible() == false) 
        return;
        
    if(guildId in guild_Elements.buttonsCamps)
    {
        guild_Elements.buttonsCamps[guildId].draw.setText("GUILD_VIEW_BTN_CAMP;: " + members);
    }
});

addEventHandler("Guild.onReset", function(guildId)
{
    if(guild_View.isVisible() == false) 
        return;

    if(guildId in guild_Elements.buttonsCamps)
    {
        guild_Elements.buttonsCamps[guildId].draw.setText("GUILD_VIEW_BTN_CAMP;: 0");
    }
});

addEventHandler("Player.onPlayerChangeGuild", function(playerId, oldGuild, newGuild)
{
    if(playerId != heroId) return;

    if(newGuild != null && guild_View.isVisible())
    {
        lockInterface(false);
        setInterfaceVisible(false, "VIEW_GUILD");
    }
});

addEventHandler("Round.onRoundStart", function() 
{
    if(isLogged() == false) return;
    
    if(chatInputIsOpen()) 
    {
        chatInputClose();
    }

	setInterfaceVisible(true, "VIEW_GUILD");
    lockInterface(true);
});

// REGISTER ELEMENTS
guild_View
.pushObject(guild_Elements.drawTitle);

registerInterfaceView(guild_View);
