
// INTERFACE
local register_View = GUI.View("VIEW_REGISTER");

local register_Size = {
    textureBackground = GUI.SizePr(45, 41),
    inputs = GUI.SizePr(28, 4.25),
    textureIcons = null,
    buttonsRegister = GUI.SizePr(13.5, 6),
    buttonExit = null
};

// COPY HEIGHT FROM INPUTS FOR ICON TEXTURES AND EXIT BUTTON
local inputHeight = register_Size.inputs.getHeightPx();

register_Size.textureIcons = GUI.SizePx(inputHeight, inputHeight);
register_Size.buttonExit = GUI.SizePx(inputHeight, inputHeight);

local register_Position = {
    textureBackground = GUI.PositionPr(27.5, 20),
    gameText = GUI.PositionPr(50, 15),
    drawTitle = GUI.PositionPr(50, 21.5),
    drawSubtitle = GUI.PositionPr(50, 26),
    inputUsername = GUI.PositionPr(0, 32),
    inputPassword = GUI.PositionPr(0, 38),
    inputRepeatPassword = GUI.PositionPr(0, 44),
    textureUsername = GUI.PositionPr(0, 32),
    texturePassword = GUI.PositionPr(0, 38),
    textureRepeatPassword = GUI.PositionPr(0, 44),
    buttonRegister = GUI.PositionPr(0, 51),
    buttonBack = GUI.PositionPr(0, 51),
    buttonExit = GUI.PositionPr(0, 20)
};

// CENTER INPUTS = TEXTURE.WIDTH + INPUT.WIDTH
local iconsWidth = register_Size.textureIcons.getWidthPx();
local centerX = getResolution().x / 2 - (iconsWidth + register_Size.inputs.getWidthPx()) / 2;

register_Position.textureUsername.setXPx(centerX);
register_Position.texturePassword.setXPx(centerX);
register_Position.textureRepeatPassword.setXPx(centerX);

register_Position.inputUsername.setXPx(iconsWidth + centerX);
register_Position.inputPassword.setXPx(iconsWidth + centerX);
register_Position.inputRepeatPassword.setXPx(iconsWidth + centerX);

// CENTER REGISTER BUTTONS 
register_Position.buttonRegister.setXPr(register_Position.textureUsername.getXPr() + 1);
register_Position.buttonBack.setXPr(register_Position.inputUsername.getXPr() + register_Size.inputs.getWidthPr() - register_Size.buttonsRegister.getWidthPr() - 1);

// SIDE EXIT BUTTON TO RIGHT
register_Position.buttonExit.setXPr(register_Position.textureBackground.getXPr() + register_Size.textureBackground.getWidthPr() - register_Size.buttonExit.getWidthPr());

local register_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", register_Position.textureBackground, register_Size.textureBackground),
    drawTitle = GUI.Draw("REGISTER_VIEW_TITLE", register_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
    drawSubtitle = GUI.Draw("REGISTER_VIEW_SUBTITLE", register_Position.drawSubtitle)
        .setFont("FONT_OLD_10_WHITE_HI")
        .setCenterAtPosition(true),
    drawInformation = GUI.MultiDraw(GUI.PositionPr(50, 70))
        .setCenterAtPosition(true)
        .pushDraw("REGISTER_VIEW_INFO_0")
        .pushDraw("REGISTER_VIEW_INFO_1")
        .pushDraw("")
        .pushDraw("REGISTER_VIEW_INFO_2"),
    inputUsername = GUI.Input("REGISTER_VIEW_USERNAME", register_Position.inputUsername, register_Size.inputs)
        .setTexture("INPUT_512X64.TGA"),
    inputPassword = GUI.Input("REGISTER_VIEW_PASSWORD", register_Position.inputPassword, register_Size.inputs, true)
        .setTexture("INPUT_512X64.TGA"),
    inputRepeatPassword = GUI.Input("REGISTER_VIEW_PASSWORD_REPEAT", register_Position.inputRepeatPassword, register_Size.inputs, true)
        .setTexture("INPUT_512X64.TGA"),
    textureUsername = GUI.Texture("ICON_USERNAME_64X64.TGA", register_Position.textureUsername, register_Size.textureIcons),
    texturePassword = GUI.Texture("ICON_PASSWORD_64X64.TGA", register_Position.texturePassword, register_Size.textureIcons),
    textureRepeatPassword = GUI.Texture("ICON_PASSWORD_64X64.TGA", register_Position.textureRepeatPassword, register_Size.textureIcons),
    buttonRegister = GUI.Button("REGISTER_VIEW_BTN_REGISTER", register_Position.buttonRegister, register_Size.buttonsRegister)
        .setTexture("BUTTON_YELLOW_256X64.TGA"),
    buttonBack = GUI.Button("REGISTER_VIEW_BTN_BACK", register_Position.buttonBack, register_Size.buttonsRegister)
        .setTexture("BUTTON_RED_256X64.TGA"),
    buttonExit = GUI.Button("X", register_Position.buttonExit, register_Size.buttonExit)
};

register_Elements.buttonRegister
.addEvent(GUIEventInterface.Click, function() 
{
    local username = register_Elements.inputUsername.getInputValue();
    local password = register_Elements.inputPassword.getInputValue();
    local repeatPassword = register_Elements.inputPassword.getInputValue();

    if(username != null && password != null && repeatPassword != null)
    {
        if(validate_username(username))
        {  
            if(validate_password(password))
            {
                if(password == repeatPassword)
                {
                    getHero().register(username, password);
                }
                else
                    GUI.GameText(GUI.PositionPr(50, 7.5), "REGISTER_VIEW_MSG_PASSWORDS_MATCH", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
            }
            else 
                GUI.GameText(GUI.PositionPr(50, 7.5), "REGISTER_VIEW_MSG_PASSWORD", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
        }
        else
            GUI.GameText(GUI.PositionPr(50, 7.5), "REGISTER_VIEW_MSG_USERNAME", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
    }
    else 
        GUI.GameText(GUI.PositionPr(50, 7.5), "REGISTER_VIEW_MSG_EMPTY_FIELD", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
});

register_Elements.buttonBack
.addEvent(GUIEventInterface.Click, function() 
{
    lockInterface(false);
	setInterfaceVisible(true, "VIEW_LOGIN");
});

register_Elements.buttonExit
.addEvent(GUIEventInterface.Click, exitGame);

register_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible) lockInterface(true);
});

// REGISTER ELEMENTS
register_View
.pushObject(register_Elements.textureBackground)
.pushObject(register_Elements.drawTitle)
.pushObject(register_Elements.drawSubtitle)
.pushObject(register_Elements.drawInformation)
.pushObject(register_Elements.inputUsername)
.pushObject(register_Elements.inputPassword)
.pushObject(register_Elements.inputRepeatPassword)
.pushObject(register_Elements.textureUsername)
.pushObject(register_Elements.texturePassword)
.pushObject(register_Elements.textureRepeatPassword)
.pushObject(register_Elements.buttonRegister)
.pushObject(register_Elements.buttonBack)
.pushObject(register_Elements.buttonExit);

foreach(element in register_Elements)
{
    register_View.pushObject(element);
}

registerInterfaceView(register_View);

// CALLBACKS
addEventHandler("Player.onHeroRegister", function(responseCode)
{
    switch(responseCode)
    {
        case RegisterCode.Succes:
        {
            lockInterface(false);
            setInterfaceVisible(true, "VIEW_LOGIN");

            GUI.GameText(register_Position.gameText, "REGISTER_VIEW_MSG_SUCCES", "FONT_DEFAULT", 5000.0, true, 34, 139, 34);  
        } 
        break;
        case RegisterCode.Fault:
            GUI.GameText(register_Position.gameText, "REGISTER_VIEW_MSG_FAULT", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);            
        break;
        case RegisterCode.DataUsed:
            GUI.GameText(register_Position.gameText, "REGISTER_VIEW_MSG_DATA_USED", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);            
        break;       
    }
});