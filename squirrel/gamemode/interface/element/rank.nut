
local rankList_View = GUI.View("VIEW_RANK_LIST");

local rankList_Size = {
    textureBackground = GUI.SizePr(57, 62),
    line = GUI.SizePr(52, 2.5)
}

local rankList_Position = {
    drawTitle = GUI.PositionPr(50, 11),
    textureBackground = GUI.PositionPr(21.5, 9),
    listPlayers = GUI.PositionPr(24, 16),
}

local rankList_Elements = {
    drawTitle = GUI.Draw("RANK_LIST_VIEW_TITLE", rankList_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", rankList_Position.textureBackground, rankList_Size.textureBackground),
    listPlayers = GUI.List(rankList_Position.listPlayers, 21)
        .setLineSize(rankList_Size.line)	
        .setScrollable(true),
};

// TITLE ROW
rankList_Elements.listPlayers
.pushListButton(GUI.ListRow()
    .pushButton(GUI.Button("RANK_LIST_VIEW_NUMBER")
        .setSize(GUI.SizePr(4, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_NICKNAME")
        .setSize(GUI.SizePr(15, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_KILLS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_DEATHS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_ASSISTS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_KDA")
        .setSize(GUI.SizePr(6, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("RANK_LIST_VIEW_DMG")
        .setSize(GUI.SizePr(6, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
);

for(local playerId = 0; playerId < getMaxSlots(); playerId++)
{
    rankList_Elements.listPlayers
    .pushListButton(GUI.ListRow()
        .pushButton(GUI.Button(playerId.tostring())
            .setSize(GUI.SizePr(4, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("SLOT")
            .setSize(GUI.SizePr(15, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(6, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(6, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )  
    );
}

// CALLBACKS
//getLeaderboard
addEventHandler("Round.onUpdateLeaderboard", function(leaderboard) 
{
    local buttons = rankList_Elements.listPlayers.getButtons();

    foreach(index, data in leaderboard)
    {
        local button = buttons[index + 1];

        button.getColumn(0).setText((index + 1).tostring());
        button.getColumn(1).setText(data.username);
        button.getColumn(2).setText(data.kills.tostring());
        button.getColumn(3).setText(data.deaths.tostring());
        button.getColumn(4).setText(data.assists.tostring());
        button.getColumn(5).setText(data.kda.tostring());
        button.getColumn(6).setText(data.dealt_damage.tostring());
    }
});

// REGISTER ELEMENTS
rankList_View
.pushObject(rankList_Elements.textureBackground)
.pushObject(rankList_Elements.drawTitle)
.pushObject(rankList_Elements.listPlayers);

registerInterfaceView(rankList_View, KEY_F10, true);