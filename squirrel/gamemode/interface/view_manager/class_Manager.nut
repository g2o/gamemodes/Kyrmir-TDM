
class View.Manager
{
	static _viewList = [];
	static _interfaceLocked = false;

//:public
	static function lock(lock)
	{
		_interfaceLocked <- lock;
	}

	static function isLocked() { return _interfaceLocked; }

	static function setVisible(visible, viewId, hideBars = true, freeze = true)
	{			
		if(_interfaceLocked || chatInputIsOpen()) return;
		
		local view = getViewById(viewId);
		if(view == null) return;

		if(visible)
		{
			if(view.visible == true) return;

			foreach(view in _viewList)
			{
				local object = view.object;
				
				if(view.visible)
					callEvent("View.onViewVisible", false, object.getId());

				object.setVisible(view.visible = false);
			}

			view.object.setVisible(view.visible = true);   
			if(hideBars) setHudMode(HUD_ALL, HUD_MODE_HIDDEN);
		}
		else 
		{
			if(view.visible == false) return;

			view.object.setVisible(view.visible = false);  
			setHudMode(HUD_ALL, HUD_MODE_DEFAULT);
		}	

		callEvent("View.onViewVisible", visible, viewId);

		if(freeze) 
		{
			Camera.movementEnabled =
				Camera.modeChangeEnabled = !visible;

			setCursorVisible(visible);	
			setFreeze(visible);	
		}
		else 
		{
			if(visible == false)
			{
				Camera.movementEnabled =
					Camera.modeChangeEnabled = !visible;

				setCursorVisible(visible);	
				setFreeze(visible);				
			}
		}
	}

	static function isViewVisible(viewId)
	{
		local view = getViewById(viewId);

		if(view && view.visible) 
			return true;

		return false;
	}

    static function isAnyViewVisible()
    {
        foreach(view in _viewList)
        {
            if(view.visible) return true;
        }

        return false;
    }

	static function getViewById(viewId)
	{
		foreach(view in _viewList)
		{
			local object = view.object;

			if(object.getId() == viewId)
				return view;
		}

		return null;
	}

	static function registerView(view, keyId = -1, hideBars = false, freeze = true, permission = 1)
	{
		local id = _viewList.find(view);
		
		if(id == null)
		{
            _viewList.push( { object = view, key = keyId, hideBars = hideBars, freeze = freeze, permission = permission, visible = false } );	
		}

		return this;
	}

	static function removeAllViews()
	{
		foreach(view in _viewList)
		{
			view.object.setVisible(false);
		}

		_viewList.clear();

		return this;
	}	

	static function getViews() { return _viewList; }	

	static function key(keyId)
	{
		if(isKeyPressed(KEY_LCONTROL)) return;
		
		foreach(view in _viewList)
		{
			if(view.key == keyId && view.permission <= getHeroPermission())
			{
					local object = view.object;
				setVisible(!object.isVisible(), object.getId(), view.hideBars, view.freeze)
			}
		}
	}
}

getViewManager <- @() View.Manager; 

// View
registerInterfaceView <- @(view, keyId = -1, hideBars = false, freeze = true, permission = -1) View.Manager.registerView(view, keyId, hideBars, freeze, permission);

setInterfaceVisible <- @(visible, viewId, hideBars = true) View.Manager.setVisible(visible, viewId, hideBars);
lockInterface <- @(lock) View.Manager.lock(lock);

isViewVisible <- @(viewId) View.Manager.isViewVisible(viewId);
isAnyViewVisible <- @() View.Manager.isAnyViewVisible();