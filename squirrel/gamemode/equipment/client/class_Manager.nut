
class Equipment.Manager
{
//:protected 
    static function server_giveItem(packet)
    {
        getHero().getEquipmentModule().server_giveItem(packet.readInt16(), packet.readInt32());
    }

    static function server_giveItemMultiple(packet)
    {
        local packetSize = packet.readInt16();

        for(local i = 0; i < packetSize; i++)
        {
            try 
            {
                getHero().getEquipmentModule().server_giveItem(packet.readInt16(), packet.readInt32());
            }
            catch(error)
            {
                print("Equipment.Player(server_giveItemMultiple) - " + error)
            }
        }
    }

    static function server_removeItem(packet)
    {
        getHero().getEquipmentModule().server_removeItem(packet.readInt16(), packet.readInt32());
    }

    static function server_setInventory(packet)
    {
        local packetSize = packet.readInt16(), itemList = [];

        for(local i = 0; i < packetSize; i++)
        {
            try 
            {
                itemList.push( { itemId = packet.readInt16(), amount = packet.readInt32(), slotId = packet.readInt8() } );
            }
            catch(error)
            {
                print("Equipment.Player(server_setInventory) - " + error)
            }
        }

        getHero().getEquipmentModule().server_setInventory(itemList);
    }

    static function server_clear(packet)
    {
        getHero().getEquipmentModule().server_clear();
    }

    static function server_equipItem(packet)
    {
        getHero().getEquipmentModule().server_equipItem(packet.readInt16(), packet.readInt8());
    }

    static function server_unequipItem(packet)
    {
        getHero().getEquipmentModule().server_unequipItem(packet.readInt16());
    }

    static function server_useItem(packet)
    {
        getHero().getEquipmentModule().server_useItem(packet.readInt16());
    }

//:public
    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Equipment_PacketIDs.Give: server_giveItem(packet); break;
            case Equipment_PacketIDs.Give_Multiple: server_giveItemMultiple(packet); break;
            case Equipment_PacketIDs.Remove: server_removeItem(packet); break;
            case Equipment_PacketIDs.SetInventory: server_setInventory(packet); break;
            case Equipment_PacketIDs.Clear: server_clear(packet); break;
            case Equipment_PacketIDs.Equip: server_equipItem(packet); break;
            case Equipment_PacketIDs.Unequip: server_unequipItem(packet); break;
            case Equipment_PacketIDs.Use: server_useItem(packet); break;
        }   
    } 
}

getEquipmentManager <- @() Equipment.Manager;

// Equipment
dropItem <- @(itemId, amount) getHero().getEquipmentModule().dropItem(itemId, amount);

isItemEquipped <- @(itemId) getHero().getEquipmentModule().isItemEquipped(itemId);
getEq <- @() getHero().getEquipmentModule().getEq();

getHeroCurrentWeapon <- @() getHero().getEquipmentModule().getCurrentWeapon();