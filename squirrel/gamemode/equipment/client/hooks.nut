
_equipItem <- equipItem;
_unequipItem <- unequipItem;

_drawWeapon <- drawWeapon;
_removeWeapon <- removeWeapon;

_readySpell <- readySpell;
_unreadySpell <- unreadySpell;

_useItem <- useItem;
_hasItem <- hasItem;

function equipItem(playerId, itemId, slotId = -1)
{
    return playerId == heroId ? getHero().getEquipmentModule().equipItem(itemId, slotId) : _equipItem(playerId, itemId);
}

function unequipItem(playerId, itemId)
{
    return playerId == heroId ? getHero().getEquipmentModule().unequipItem(itemId) : _unequipItem(playerId, itemId);
}

function drawWeapon(playerId, value)
{
    return playerId == heroId ? getHero().getEquipmentModule().drawWeapon(value) : _drawWeapon(playerId, value);
}

function removeWeapon(playerId)
{
    return playerId == heroId ? getHero().getEquipmentModule().removeWeapon() : _removeWeapon(playerId);
}

function readySpell(playerId, value)
{
    return playerId == heroId ? getHero().getEquipmentModule().readySpell(value) : _readySpell(playerId, value, 0);
}

function unreadySpell(playerId)
{
    return playerId == heroId ? getHero().getEquipmentModule().unreadySpell() : _removeWeapon(playerId); 
}

function useItem(playerId, itemId)
{
    return playerId == heroId ? getHero().getEquipmentModule().useItem(itemId) : _useItem(playerId, itemId);
}

function hasItem(itemId)
{
    return getHero().getEquipmentModule().hasItem(itemId);
}