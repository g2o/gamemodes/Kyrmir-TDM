
class Equipment.Item
{
    _template = null;
    _amount = 0;

    constructor(template, amount)
    {
        _template = template;
        _amount = amount;
    }

//:public
    function getTemplate() { return _template; }

    function setAmount(amount) 
    {
        _amount = amount;
    }

    function getAmount() { return _amount; }
}