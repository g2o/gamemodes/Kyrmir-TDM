
// Register packet callback
PacketReceive.add(PacketIDs.Equipment, getEquipmentManager());

addEventHandler("onPlayerRespawn", Equipment.Manager.playerRespawn.bindenv(Equipment.Manager));

addEventHandler("onPlayerShoot", function(playerId)
{       
    if(getEquipmentManager().playerShoot(playerId) == false)
    {
        sendServerMessageToAll(255, 80, 0, format("%s got kicked because of - MUNITION_DESYNC!", getPlayerName(playerId)));
 
        kick(playerId, "MUNITION_DESYNC");
            cancelEvent();
    }
});

addEventHandler("onPlayerSpellCast", function(playerId, itemId)
{   
    if(getEquipmentManager().playerSpellCast(playerId) == false)
    {
        sendServerMessageToAll(255, 80, 0, format("%s got kicked because of - MANA_DESYNC!", getPlayerName(playerId)));

        kick(playerId, "MANA_DESYNC");
            cancelEvent();
    }
});

addEventHandler("GroundDrop.onPlayerTakeItem", Equipment.Manager.playerTakeItem.bindenv(Equipment.Manager));
