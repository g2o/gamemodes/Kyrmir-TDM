
_giveItem <- giveItem;
_removeItem <- removeItem;

_equipItem <- equipItem;
_unequipItem <- unequipItem;
_useItem <- useItem;

function giveItem(playerId, itemId, amount)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().giveItem(itemId, amount);
}

function removeItem(playerId, itemId, amount)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().removeItem(itemId, amount);
}

function equipItem(playerId, itemId, slotId = -1)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().equipItem(itemId, slotId);
}

function unequipItem(playerId, itemId)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().unequipItem(itemId);
}

function useItem(playerId, itemId, state = 0)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().useItem(itemId);
}