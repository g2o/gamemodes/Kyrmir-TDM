
class Equipment.Manager 
{
//:protected
    static function client_equipItem(playerId, packet)
    {
        getPlayer(playerId).getEquipmentModule().client_equipItem(packet.readInt16(), packet.readInt8());
    }

    static function client_unequipItem(playerId, packet)
    {
        getPlayer(playerId).getEquipmentModule().client_unequipItem(packet.readInt16());
    }

    static function client_useItem(playerId, packet)
    {
        getPlayer(playerId).getEquipmentModule().client_useItem(packet.readInt16());
    }

    static function client_dropItem(playerId, packet)
    {
        getPlayer(playerId).getEquipmentModule().client_dropItem(packet.readInt16(), packet.readInt32());
    }

//:public
    static function playerRespawn(playerId)
    {
        getPlayer(playerId).getEquipmentModule().playerRespawn();   
    }

    static function playerSpellCast(playerId)
    {
        return getPlayer(playerId).getEquipmentModule().playerSpellCast();   
    }

    static function playerShoot(playerId)
    {
        return getPlayer(playerId).getEquipmentModule().playerShoot();   
    }

    static function playerSpellCast(playerId)
    {
        return getPlayer(playerId).getEquipmentModule().playerSpellCast();   
    }

    static function playerTakeItem(playerId, itemGround)
    {
        getPlayer(playerId).getEquipmentModule().playerTakeItem(itemGround);   
    }

    static function packetRead(playerId, packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Equipment_PacketIDs.Equip: client_equipItem(playerId, packet); break;
            case Equipment_PacketIDs.Unequip: client_unequipItem(playerId, packet); break;
            case Equipment_PacketIDs.Use: client_useItem(playerId, packet); break;
            case Equipment_PacketIDs.Drop: client_dropItem(playerId, packet); break;
        }   
    } 
}

getEquipmentManager <- @() Equipment.Manager;

// Equipment
function giveItemMultiple(playerId, itemList)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().giveItemMultiple(itemList);
}

function setPlayerStats(playerId, statsTable)
{
    local player = getPlayer(playerId);
    if(player) player.setStats(statsTable);
}

function setPlayerInventory(playerId, itemList)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().setInventory(itemList);
}

function dropItem(playerId, itemId, amount, dissTime = 300)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().dropItem(itemId, amount, dissTime);
}

function clearEq(playerId)
{
    local player = getPlayer(playerId);
    if(player) player.getEquipmentModule().clearEq();
}

function getEquippedByType(playerId, typeId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getEquipmentModule().getEquippedByType(typeId);

    return null;
}

function isItemEquipped(playerId, itemId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getEquipmentModule().isItemEquipped(itemId);

    return false;
}

function hasItem(playerId, itemId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getEquipmentModule().hasItem(itemId);

    return null;
}

function getEq(playerId) 
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getEquipmentModule().getEq();

    return null;
}