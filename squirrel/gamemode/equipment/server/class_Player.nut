
class Equipment.Player 
{
    _playerId = -1;

    _quickSlot = null;

    _equipped = null;
    _itemList = null;

    constructor(playerId)
    {
        _playerId = playerId;

        _quickSlot = Equipment.QuickSlot();

        _equipped = {};
        _itemList = [];
    }

//:private
    function _canEquipPhysicaly(template)
    {
        return template.isSpell() == false && template.isUsable() == false;
    }

    function _applyUseEffect(effectList)
    {
        foreach(index, effect in effectList)
        {
            switch(index)
            {
                case ItemEffect.HealthRecovery:
                    setPlayerHealth(_playerId, getPlayerHealth(_playerId) + effect);
                break;
                case ItemEffect.HealthPercentRecovery: 
                    setPlayerHealth(_playerId, getPlayerHealth(_playerId) + fabs(getPlayerMaxHealth(_playerId) * (effect / 100.0)));
                break;
                case ItemEffect.ManaRecovery:
                    setPlayerMana(_playerId, getPlayerMana(_playerId) + effect);
                break;
                case ItemEffect.ManaPercentRecovery:
                    setPlayerMana(_playerId, getPlayerMana(_playerId) + fabs(getPlayerMaxMana(_playerId) * (effect / 100.0)));
                break;
                case ItemEffect.Custom:
                    playerAddEffect(_playerId, effect);
                break;
            }
        }
    }

    function shared_removeOverlay(item)
    {
        local template = item.getTemplate();

        if(template instanceof Item.Gear)
        {
            local overlayId = template.getOverlay();

            if(overlayId != -1)
            {
                removePlayerOverlay(_playerId, overlayId);
            }
        }
    }

    function shared_applyOverlay(item)
    {
        local template = item.getTemplate();
     
        if(template instanceof Item.Gear)
        {
            local overlayId = template.getOverlay();

            if(overlayId != -1)
            {
                applyPlayerOverlay(_playerId, overlayId);
            }
        }
    }

    function shared_equipItem(itemId, slotId = -1)
    {
        local item = hasItem(itemId);
        if(item == null) return false;  

        local template = item.getTemplate(), isEquippable = template.isEquippable();

        if(isEquippable == false && template.isQuickSlotPossible() == false)
            return false;
    
        if(isEquippable && template.canBeEquippedByPlayer(_playerId) == false)
            return false;

        local typeId = template.getType(), prevItem = getEquippedByType(typeId);

        if(prevItem)
        { 
                delete _equipped[typeId];
            _quickSlot.freeSlotByItem(prevItem);

            shared_removeOverlay(prevItem);

            callEvent("Equipment.onPlayerUnequipItem", _playerId, prevItem.getTemplate().getInstance());
        }

        if(template.isQuickSlotPossible() == false)
        {            
                _equipped[typeId] <- item;
            _equipItem(_playerId, template.getVisualId());  

            callEvent("Equipment.onPlayerEquipItem", _playerId, template.getInstance(), slotId = -1);
        }
        else 
        {
            if(isItemEquipped(template.getId()))
            {
                    _quickSlot.freeSlotByItem(item);
                callEvent("Equipment.onPlayerUnequipItem", _playerId, template.getInstance());
            }

            if(slotId == -1) slotId = _quickSlot.getFreeSlot()
            prevItem = _quickSlot.getItemBySlot(slotId);

            if(prevItem)
            {
                local prevItem_template = prevItem.getTemplate();
               
                _quickSlot.freeSlotByItem(prevItem);   

                if(_canEquipPhysicaly(prevItem_template))
                {
                        delete _equipped[prevItem_template.getType()];
                    _unequipItem(_playerId, prevItem_template.getVisualId());
                }

                shared_removeOverlay(prevItem);

                callEvent("Equipment.onPlayerUnequipItem", _playerId, prevItem_template.getInstance());
            }

            if(_canEquipPhysicaly(template))
            {
                    _equipped[typeId] <- item;
                _equipItem(_playerId, template.getVisualId());                                  
            }
                
                _quickSlot.assignItemToSlot(slotId, item);
            callEvent("Equipment.onPlayerEquipItem", _playerId, template.getInstance(), slotId);
        }
        
        shared_applyOverlay(item);

        return true;
    }   

//:public
    function client_equipItem(itemId, slotId)
    {
        shared_equipItem(itemId, slotId);
    }

    function client_unequipItem(itemId)
    {        
        if(isItemEquipped(itemId) == false)
            return;

        local item = hasItem(itemId), template = item.getTemplate();

        if(template.isQuickSlotPossible()) _quickSlot.freeSlotByItem(item);

        if(_canEquipPhysicaly(template))
        {
            delete _equipped[template.getType()];
        }
        
        shared_removeOverlay(item);

        callEvent("Equipment.onPlayerUnequipItem", _playerId, template.getInstance());
    }

    function client_useItem(itemId)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            local template = item.getTemplate();

            if(template.isUsable())
            {
                local oldAmount = item.getAmount(), 
                    newAmount = oldAmount - 1;

                if(newAmount > 0)
                    item.setAmount(newAmount);
                else 
                {                    
                    if(template.isQuickSlotPossible())
                    {
                        _quickSlot.freeSlotByItem(item);
                    }

                    _itemList.remove(_itemList.find(item));
                }

                _applyUseEffect(template.getEffects());

                callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);
            }
        }
    }

    function client_dropItem(itemId, amount)
    {
        if(isItemEquipped(itemId) == false && amount > 0)
        {    
            local item = hasItem(itemId);
            if(item == null) return;
            
            local template = item.getTemplate();
            if(template.isBounded()) return;

            local oldAmount = item.getAmount();
            if(oldAmount < amount) return;

            local newAmount = oldAmount - amount;
            
            if(newAmount > 0)
                item.setAmount(newAmount);
            else 
                _itemList.remove(_itemList.find(item));

            _removeItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? amount + 1 : amount);

            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);

                local position = getPlayerPosition(_playerId);
            createGroundItem(template, amount, position.x, position.y, position.z, getPlayerWorld(_playerId), getPlayerVirtualWorld(_playerId), 300);    
        }
    }

    function giveItem(itemId, amount)
    {
        if(amount <= 0) return;
        
        local template = getItemTemplateById(itemId);

        if(template != null)
        {
            local item = hasItem(itemId), oldAmount = 0;

            if(item != null)
            {
                    oldAmount = item.getAmount();
                item.setAmount(oldAmount + amount);
            } 
            else 
                _itemList.push(item = Equipment.Item(template, amount));
            
            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, item.getAmount());

            _giveItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? amount + 1 : amount);

            local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Give);
            
                packet.writeInt16(itemId);
                packet.writeInt32(amount);
                
            packet.send(_playerId, RELIABLE);
        }
    }

    function giveItemMultiple(itemList)
    {
        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Give_Multiple);
            packet.writeInt16(itemList.len());

        foreach(itemData in itemList)
        {
            local amount = itemData[1], itemId = itemData[0];
            if(amount <= 0) continue;

            local template = getItemTemplateById(itemId);

            if(template != null)
            {
                local item = hasItem(itemId), oldAmount = 0;
                
                if(item != null)
                {
                        oldAmount = item.getAmount();
                    item.setAmount(oldAmount + amount);
                }
                else 
                    _itemList.push(item = Equipment.Item(template, amount));
               
                callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, item.getAmount());

                _giveItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? amount + 1 : amount);

                packet.writeInt16(itemId);
                packet.writeInt32(amount); 
            }
        }

        packet.send(_playerId, RELIABLE);
    }

    function removeItem(itemId, amount)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            local oldAmount = item.getAmount(), diff = oldAmount - amount;
            local template = item.getTemplate();

            if(diff > 0)
                item.setAmount(diff);
            else 
            {
                if(template.isQuickSlotPossible())
                {
                    _quickSlot.freeSlotByItem(item);
                }

                _itemList.remove(_itemList.find(item));
            }

            _removeItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? amount + 1 : amount);

            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, diff);

            local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Remove);

                packet.writeInt16(itemId);
                packet.writeInt32(amount);
                
            packet.send(_playerId, RELIABLE);
        }
    }
    
    function setInventory(itemList)
    {
        if(itemList == null)
        {
            print("Equipment: Player " + getPlayerName(_playerId) + " inventory is empty!");
            return;
        }

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.SetInventory);
            packet.writeInt16(itemList.len());

        foreach(itemData in itemList)
        {
            local itemId = itemData[0], amount = itemData[1], slotId = itemData[2];
            local template = getItemTemplateById(itemId);

            if(template == null) continue;

            local item = hasItem(itemId);
            
            if(item != null)
            {
                item.setAmount(item.getAmount() + amount);
            }
            else 
                _itemList.push(item = Equipment.Item(template, amount));

            local typeId = template.getType();

            _giveItem(_playerId, template.getVisualId(), typeId == ItemType.Scroll ? amount + 1 : amount);
        
            if(slotId == -1)
            {
                shared_applyOverlay(_equipped[typeId] <- item);
            }
            else if(slotId >= 0)
            {
                _equipped[typeId] <- item;

                _quickSlot.assignItemToSlot(slotId, item);
                    shared_applyOverlay(item);
            }

            packet.writeInt16(itemId);
            packet.writeInt32(amount); 
            packet.writeInt8(slotId); 
        }

        packet.send(_playerId, RELIABLE);    
    }

    function dropItem(itemId, amount, dissTime)
    {
        if(amount <= 0) 
            return;
            
        local item = hasItem(itemId);

        if(item != null)
        {
            local template = item.getTemplate();
            if(template.isBounded()) return false;

            local oldAmount = item.getAmount();
            if(oldAmount < amount) return false;

            local newAmount = oldAmount - amount;

            if(newAmount > 0)
                item.setAmount(newAmount);
            else 
                _itemList.remove(_itemList.find(item));

            _removeItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? amount + 1 : amount);
            
            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);
            
            local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Remove);

                packet.writeInt16(itemId);
                packet.writeInt32(amount);
                
            packet.send(_playerId, RELIABLE);

                local position = getPlayerPosition(_playerId);
            createGroundItem(item.getTemplate(), amount, position.x, position.y, position.z, getPlayerWorld(_playerId), getPlayerVirtualWorld(_playerId), dissTime);    
        }
    }

    function clearEq()
    {
        _quickSlot.clear();
        _equipped.clear();

        foreach(item in _itemList)
        {
                local template = item.getTemplate();
            _removeItem(_playerId, template.getVisualId(), template.getType() == ItemType.Scroll ? item.getAmount() + 1 : item.getAmount());
        }
        
            _itemList.clear();
        callEvent("Equipment.onPlayerClear", _playerId);

        Packet(PacketIDs.Equipment, Equipment_PacketIDs.Clear).send(_playerId, RELIABLE);
    }

    function equipItem(itemId, slotId = -1)
    {
        if(shared_equipItem(itemId, slotId) == false)
            return false;

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Equip);

            packet.writeInt16(itemId);
            packet.writeInt8(slotId);

        packet.send(_playerId, RELIABLE_ORDERED);
        
        return true;
    }

    function unequipItem(itemId)
    {
        if(isItemEquipped(itemId) == false)
            return false;

        local item = hasItem(itemId), template = item.getTemplate();

        if(template.isQuickSlotPossible()) _quickSlot.freeSlotByItem(item);
        
        if(_canEquipPhysicaly(template))
        {
                delete _equipped[template.getType()];
            _unequipItem(_playerId, template.getVisualId());
        }
        
        shared_removeOverlay(item);

        callEvent("Equipment.onPlayerUnequipItem", _playerId, template.getInstance());

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Unequip);
            packet.writeInt16(itemId);
        packet.send(_playerId, RELIABLE_ORDERED);         

        return true;   
    }

    function useItem(itemId)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            local template = item.getTemplate();
            
            if(template.isUsable())
            {
                local oldAmount = item.getAmount(),
                    newAmount = oldAmount - 1;

                if(newAmount > 0)
                    item.setAmount(newAmount);
                else 
                {                
                    if(template.isQuickSlotPossible())
                    {
                        _quickSlot.freeSlotByItem(item);
                    }

                    _itemList.remove(_itemList.find(item));
                }

                _applyUseEffect(template.getEffects());

                callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);

                local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Use);
                    packet.writeInt16(itemId);
                packet.send(RELIABLE);                        
            }
        }
    }
    
    function playerShoot()
    {
        local context = ItemType.WeaponRanged in _equipped ? _equipped[ItemType.WeaponRanged] : -1;
        if(context == -1) return false;

        local template = context.getTemplate().getMunition();
        local item = hasItem(template.getId());

        if(item)
        {
            local oldAmount = item.getAmount(),
                newAmount = oldAmount - 1;  

            if(newAmount > 0)
                item.setAmount(newAmount);
            else 
                _itemList.remove(_itemList.find(item));
            
            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);

            return true;
        }

        return false;
    }
    
    function playerSpellCast()
    {
        local context = getPlayerContext(_playerId, EQUIPMENT_CTX);
        
        if(context == -1) 
        {
            print("ERROR EQ_CTX");
            return false;
        }

        local template = getItemTemplateById(context);
        
        local health = getPlayerHealth(_playerId) - template.getHealthUsage();
        local mana = getPlayerMana(_playerId) - template.getManaUsage();

        if(health >= 0 && mana >= 0)
        {
            setPlayerHealth(_playerId, health);
            setPlayerMana(_playerId, mana); 
            
            callEvent("Equipment.onPlayerSpellCast", _playerId, template);
            
            if(template.getType() == ItemType.Scroll)
            {
                local item = hasItem(context);
                if(item == null) return false;
            
                local oldAmount = item.getAmount(), newAmount = oldAmount - 1;

                if(newAmount > 0)
                    item.setAmount(newAmount);
                else 
                {
                    if(template.isQuickSlotPossible())
                    {
                        _quickSlot.freeSlotByItem(item);
                    }
                    
                    _itemList.remove(_itemList.find(item));
                }

                callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, newAmount);
            }

            return true;
        }

        return false;
    }

    function playerRespawn()
    {
        foreach(item in _itemList) 
        {
            local template = item.getTemplate(), visualId = template.getVisualId();

            _giveItem(_playerId, visualId, template.getType() == ItemType.Scroll ? item.getAmount() + 1 : item.getAmount());

            if(isItemEquipped(template.getId()) && _canEquipPhysicaly(template))
            {
                _equipItem(_playerId, visualId);
                    shared_applyOverlay(item);
            }
        }
    }

    function playerTakeItem(itemGround)
    {
        local template = itemGround.getTemplate();
          
        if(template != null)
        {
            local itemId = template.getId(), item = hasItem(itemId);
            local amount = itemGround.getAmount(), oldAmount = 0;

            if(item != null)
            {
                    oldAmount = item.getAmount();
                item.setAmount(oldAmount + amount);
            } 
            else 
                _itemList.push(item = Equipment.Item(template, amount));
            
            callEvent("Equipment.onPlayerItemUpdate", _playerId, template.getInstance(), oldAmount, item.getAmount());

            local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Give);
            
                packet.writeInt16(itemId);
                packet.writeInt32(amount);
                
            packet.send(_playerId, RELIABLE);
        }
    }

    function getMeleeWeapon() { return ItemType.WeaponMelee in _equipped ? _equipped[ItemType.WeaponMelee] : null; }
    
    function getRangedWeapon() { return ItemType.WeaponRanged in _equipped ? _equipped[ItemType.WeaponRanged] : null; }

    function getHelmet() { return ItemType.Helmet in _equipped ? _equipped[ItemType.Helmet] : null; }
    
    function getBelt() { return ItemType.Belt in _equipped ? _equipped[ItemType.Belt] : null; }
    
    function getArmor() { return ItemType.Armor in _equipped ? _equipped[ItemType.Armor] : null; }
    
    function getRing() { return ItemType.Ring in _equipped ? _equipped[ItemType.Ring] : null; }
    
    function getAmulet() { return ItemType.Amulet in _equipped ? _equipped[ItemType.Amulet] : null; }

    function getEquippedByType(typeId) { return typeId in _equipped ? _equipped[typeId] : null; }

    function isItemEquipped(itemId)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            if(item.getTemplate().isQuickSlotPossible())
            {
                return _quickSlot.isItemOnSlotList(item);
            }
            else 
            {
                foreach(item in _equipped)
                {
                    if(item.getTemplate().getId() == itemId)
                        return true;
                }
            }
        }
        
        return false;
    }

    function hasItem(itemId)
    {
        foreach(item in _itemList)
        {
            if(item.getTemplate().getId() == itemId)
                return item;
        }

        return null;
    }
    
    function getEq() { return _itemList; }

    function getProtection(damageType)
    {
        local protection = 0;

        for(local index = ItemType.Helmet; index <= ItemType.Amulet; index++)
        {
            if(index in _equipped) protection += _equipped[index].getTemplate().getProtection(damageType);
        }

        return protection;
    }
}
