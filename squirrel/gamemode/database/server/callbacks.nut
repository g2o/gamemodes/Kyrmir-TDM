
addEventHandler("onInit", Data.Manager.init_OnlineStatus.bindenv(Data.Manager));

addEventHandler("Dialog.onPlayerDialogEffect", Data.Manager.updatePlayer.bindenv(Data.Manager));

addEventHandler("Player.onPlayerPreDisconnect", Data.Manager.updatePlayerDisconnect.bindenv(Data.Manager));

//addEventHandler("Player.onPlayerReset", Data.Player.resetPlayerData.bindenv(Data.Player));

addEventHandler("Equipment.onPlayerItemUpdate", Data.Manager.playerUpdateItem.bindenv(Data.Manager));

addEventHandler("Equipment.onPlayerEquipItem", Data.Manager.playerEquipItem.bindenv(Data.Manager));

addEventHandler("Equipment.onPlayerUnequipItem", Data.Manager.playerUnequipItem.bindenv(Data.Manager));

addEventHandler("Equipment.onPlayerClear", Data.Manager.playerClear.bindenv(Data.Manager));

addEventHandler("Player.onPlayerKills", Data.Manager.updatePlayerStats.bindenv(Data.Manager));

addEventHandler("Player.onPlayerDeaths", Data.Manager.updatePlayerStats.bindenv(Data.Manager));

addEventHandler("Player.onPlayerAssists", Data.Manager.updatePlayerStats.bindenv(Data.Manager));

addEventHandler("Player.onPlayerChangeVisual", Data.Manager.playerChangeVisual.bindenv(Data.Manager));

addEventHandler("Player.onPlayerChangeGuild", function(playerId, oldGuild, newGuild)
{
    getDataBase().updatePlayer(playerId);
});

addEventHandler("Player.onPlayerChangeLevel", function(playerId, oldLevel, newLevel)
{
    getDataBase().updatePlayer(playerId);
});

addEventHandler("onPlayerEnterWorld", function(playerId, worldFilePath)
{
    getDataBase().updatePlayer(playerId);
});