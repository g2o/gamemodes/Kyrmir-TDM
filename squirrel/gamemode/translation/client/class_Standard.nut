
class Translation.Standard 
{
    _text = null;

    constructor()
    {
        _text = {};
    }

//:public
    function setText(lang, text)
    {
        _text[lang] <- text;
            return this;
    }

    function getText(lang) { return lang in _text ? _text[lang] : null; }
}