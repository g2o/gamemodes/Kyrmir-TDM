
class Damage.Player 
{
    _playerId = -1;

    _parade = MAX_PARADE;
    _regenParade = 0;

    constructor(playerId)
    {
        _playerId = playerId;
        
        _parade = MAX_PARADE;
        _regenParade = 0;
    }

//:public 
    function client_parade(killerId)
    {
            _parade -= 1;
        if(_parade < 0) _parade = 0;
        
        local packet = Packet(PacketIDs.Damage, Damage_PacketIDs.Parade)
            
            packet.writeInt16(_playerId);
            packet.writeInt8(_parade);

        local playerList = getPlayers();

        foreach(id, player in playerList)
        {
            if(id != killerId)
            {
                packet.send(id, UNRELIABLE_SEQUENCED);
            }
        }

        _regenParade = getTickCount() + 1000;
    }

    function regenerate(tick)
    {
        if(_regenParade <= tick && _parade < MAX_PARADE)
        {
                _parade += 1;
            _regenParade = tick + 1000;

            local packet = Packet(PacketIDs.Damage, Damage_PacketIDs.Parade)
                
                packet.writeInt16(_playerId);
                packet.writeInt8(_parade);

            local playerList = getPlayers();

            foreach(id, player in playerList)
            {
                packet.send(id, UNRELIABLE_SEQUENCED);
            }
        }
    }

    function getParades() { return _parade; }
}