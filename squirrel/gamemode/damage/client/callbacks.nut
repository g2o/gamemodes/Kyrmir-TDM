
// Register packet callback
PacketReceive.add(PacketIDs.Damage, getDamageManager());

addEventHandler("onPlayerParade", function(playerId, attackerId) 
{
    if(getDamageManager().playerParade(playerId))
    {
        cancelEvent();
    }
});

addEventHandler("onDamage", function(desc)
{
    if(isInFlyMode() == false && desc.flags == DAMAGE_FALL) 
    {
        getDamageManager().playerDamageFall(desc.damage);
    }

    cancelEvent();
});