
class Damage.Player 
{
    _playerId = -1;
    _parade = MAX_PARADE;

    constructor(playerId)
    {
        _playerId = playerId;
        _parade = MAX_PARADE;
    }
    
    function parade()
    {
            _parade -= 1;
        if(_parade < 0) _parade = 0;
        
        local packet = Packet(PacketIDs.Damage, Damage_PacketIDs.Parade);
            packet.writeInt16(_playerId);
        packet.send(UNRELIABLE);

        callEvent("Damage.onParadeUpdate", _playerId, _parade);
    }

    function server_parade(parade)
    {
        callEvent("Damage.onParadeUpdate", _playerId, _parade = parade);
    }

    function canParade() { return _parade > 0; }

    function getParades() { return _parade; }

    function damageFall(damage)
    {
        local packet = Packet(PacketIDs.Damage, Damage_PacketIDs.Damage_Fall);
            packet.writeInt16(damage);
        packet.send(RELIABLE);
    }
}