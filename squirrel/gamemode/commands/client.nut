
local PERMISSION_FUNC = function()
{
    return getHeroPermission() >= PermissionLevel.Administrator;
}

addCommand("fly_mode", function(params) 
{
        setFlyMode(!isInFlyMode());
    serverMessage(ALL_CHATS, 128, 0, 0, "Fly mode " + (isInFlyMode() ? "enabled" : "disabled") + "!");     
}
, PERMISSION_FUNC);

addCommand("help", function(params) 
{
    local commands = getChatCommands();
    local text = "";

    foreach(index, command in commands)
    {
        if(command.permission == null || command.permission())
        {
            text += ("/" + index + " ");
        }
    }

    serverMessage(ALL_CHATS, 128, 0, 0, "Commands possible to use by you:");
    serverMessage(ALL_CHATS, 128, 0, 0, text);
}
, PERMISSION_FUNC);