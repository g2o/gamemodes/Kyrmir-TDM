
// ADMIN COMMANDS

local PERMISSION_FUNC_ADMIN = function(playerId)
{
    return getPlayerPersmission(playerId) >= PermissionLevel.Administrator;
}

local PERMISSION_FUNC_MOD = function(playerId)
{
    return getPlayerPersmission(playerId) >= PermissionLevel.Moderator;
}

addCommand("mute", function(playerId, params) 
{
    local args = sscanf("dds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /mute <id> <muted or not(0/1)> <reason>");
        return;
    }

    local id = args[0], mute = args[1], reason = args[2];

    if(!isPlayerConnected(id))
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    setPlayerMute(id, mute);

    sendServerMessageToAll(255, 80, 0, format("Player %s new mute status is %d, received from admin %s!", getPlayerName(id), mute, getPlayerName(playerId)));
    sendServerMessageToAll(255, 80, 0, format("Reason: %s", reason));
}
, PERMISSION_FUNC_MOD);

addCommand("kick", function(playerId, params) 
{
    local args = sscanf("ds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /kick <id> <reason>");
        return;
    }

    local id = args[0], reason = args[1];

    if(!isPlayerConnected(id))
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    kick(id, reason);

    sendServerMessageToAll(255, 80, 0, format("%s got kicked by %s", getPlayerName(id), getPlayerName(playerId)));
    sendServerMessageToAll(255, 80, 0, format("Reason: %s", reason));
}
, PERMISSION_FUNC_MOD);

addCommand("ban", function(playerId, params) 
{
    local args = sscanf("dds", params);
    
    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /ban <id> <minutes> <reason>");
        return;
    }

    local id = args[0], minutes = args[1], reason = args[2];

    if(!isPlayerConnected(id))
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    ban(id, minutes, reason);

    sendServerMessageToAll(255, 80, 0, format("%s get banned by %s", getPlayerName(id), getPlayerName(playerId)));
    sendServerMessageToAll(255, 80, 0, format("Reason: %s Ban time: %dh", reason, minutes / 60.0));
}
, PERMISSION_FUNC_MOD);

addCommand("kill", function(playerId, params) 
{
    local args = sscanf("d", params);
    
    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /kill <id>");
        return;
    }

    local id = args[0];

    if(!isPlayerConnected(id))
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    setPlayerHealth(id, 0);

    sendServerMessageToPlayer(playerId, 255, 80, 0, format("You have killed player %s!", getPlayerName(id)));
    sendServerMessageToPlayer(id, 255, 80, 0, format("You have been killed by player %s", getPlayerName(playerId)));
}
, PERMISSION_FUNC_MOD);

addCommand("world", function(playerId, params) 
{
    local args = sscanf("ds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /world <id> <world>");
        return;
    }

    local id = args[0], world = args[1];

    if(!isPlayerConnected(id))
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    setPlayerWorld(id, world);
    setPlayerPosition(id, 0, 100, 0);
    
    sendServerMessageToAll(255, 80, 0, format("%s got teleported to world %s by %s", getPlayerName(id), world, getPlayerName(playerId)));
}
, PERMISSION_FUNC_MOD);

addCommand("invisible", function(playerId, params) 
{
        setPlayerInvisible(playerId, !getPlayerInvisible(playerId));
    sendServerMessageToPlayer(playerId, 128, 0, 0, "Invisible mode " + (getPlayerInvisible(playerId) ? "enabled" : "disabled") + "!");   
}
, PERMISSION_FUNC_ADMIN);

addCommand("god_mode", function(playerId, params) 
{
        setPlayerImmortal(playerId, !isPlayerImmortal(playerId));
    sendServerMessageToPlayer(playerId, 128, 0, 0, "Godmode mode " + (isPlayerImmortal(playerId) ? "enabled" : "disabled") + "!");   
}
, PERMISSION_FUNC_ADMIN);

addCommand("heal", function(playerId, params) 
{
    local args = sscanf("s", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /heal <id_0> <id_1> (...) or /heal all");
        return;
    }
    
    local players = null;

    if(args[0] != "all")
    {
        players = split(args[0], " ");

        foreach(targetId in players) 
        {
            try 
            {
                targetId = targetId.tointeger();

                setPlayerHealth(targetId, getPlayerMaxHealth(targetId));
                sendServerMessageToPlayer(targetId, 255, 80, 0, format("You have been healed by player %s", getPlayerName(playerId)));
            }
            catch(error)
            {
                print("+ health command: " + error);
            }
        }
    }
    else 
    {
        players = getPlayers();

        foreach(targetId, object in players) 
        {
            setPlayerHealth(targetId, getPlayerMaxHealth(targetId));
            sendServerMessageToPlayer(targetId, 255, 80, 0, format("You have been healed by player %s", getPlayerName(playerId)));
        }
    }

    sendServerMessageToPlayer(playerId, 128, 0, 0, "You have healed " + players.len() + " players!");
}
, PERMISSION_FUNC_ADMIN);

addCommand("bring", function(playerId, params) 
{
    local args = sscanf("s", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /bring <id_0> <id_1> (...)");
        return;
    }

    local position = getPlayerPosition(playerId);
    local players = split(args[0], " ");

    foreach(targetId in players) 
    {
        try 
        {
            targetId = targetId.tointeger();

            setPlayerPosition(targetId, position.x + rand() % 100, position.y, position.z - rand() % 100);
            sendServerMessageToPlayer(targetId, 255, 80, 0, format("You have been teleported to player %s", getPlayerName(playerId)));
        }
        catch(error)
        {
            print("+ bring command: " + error);
        }
    }

    sendServerMessageToPlayer(playerId, 128, 0, 0, "You have teleported " + players.len() + " players to you!");
}
, PERMISSION_FUNC_MOD);

addCommand("goto", function(playerId, params) 
{
    local args = sscanf("d", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /goto <id>");
        return;
    }

    local id = args[0];

    if(!isPlayerConnected(id))  
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

        local position = getPlayerPosition(id);
    setPlayerPosition(playerId, position.x, position.y, position.z);
    
    sendServerMessageToPlayer(playerId, 128, 0, 0, "You have teleported yourself to player " + getPlayerName(id) + "!");
}
, PERMISSION_FUNC_MOD);

addCommand("say", function(playerId, params) 
{
    local args = sscanf("s", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /say <text>");
        return;
    }

    sendServerMessageToAll(255, 0, 0, args[0]);
}
, PERMISSION_FUNC_ADMIN);

addCommand("guild", function(playerId, params) 
{
    local args = sscanf("ds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /guild <player_id> <guild_id>");
        return;
    }

    local id = args[0];

    if(!isPlayerConnected(id))  
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    local guildId = args[1];

    if(forceChangeGuild(id, guildId) == false)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Cannot change player guild!");
        return;
    }

    sendServerMessageToAll(255, 80, 0, format("Admin %s set new guild value for player %s to %s!", getPlayerName(playerId), getPlayerName(id), guildId));
}
, PERMISSION_FUNC_MOD);

addCommand("giveitem", function(playerId, params) 
{
    local args = sscanf("dds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /giveitem <player_id> <amount> <item_instance>");
        return;
    }

    local id = args[0];

    if(!isPlayerConnected(id))  
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    local instance = args[2], 
        amount = args[1];
        
    local itemId = getItemTemplateIdByInstance(instance);

    if(itemId == -1 || amount <= 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "You are trying to give not existing item!");
        return;
    }

        giveItem(id, itemId, amount);
    sendServerMessageToAll(255, 80, 0, format("Admin %s give item %s to %s!", getPlayerName(playerId), instance, getPlayerName(id)));
}
, PERMISSION_FUNC_ADMIN);

addCommand("level", function(playerId, params) 
{
    local args = sscanf("dd", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /level <player_id> <number>");
        return;
    }

    local id = args[0];

    if(!isPlayerConnected(id))  
    {
        sendServerMessageToPlayer(playerId, 255, 0, 0, "There is no player with given ID!");
        return;
    }

    local number = args[1];
        
    if(number < 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Bad number!");
        return;
    }

        setPlayerLevel(id, number);
    sendServerMessageToAll(255, 80, 0, format("Admin %s give level %d to %s!", getPlayerName(playerId), number, getPlayerName(id)));
}
, PERMISSION_FUNC_ADMIN);

addCommand("points", function(playerId, params) 
{
    local args = sscanf("sd", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /points <guild_id> <kills>");
        return;
    }

    local guildId = args[0];
    local guild = getGuildById(guildId);

    if(guild == null) 
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Given guild ID is not existing!");
        return;
    }

    local kills = args[1];
        
    if(kills < 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Bad number!");
        return;
    }

    guild.setPoints(kills);

    sendServerMessageToAll(255, 80, 0, format("Admin %s set new kills value for team %s to %d", getPlayerName(playerId), guildId, kills));
}
, PERMISSION_FUNC_ADMIN);

addCommand("round_start", function(playerId, params) 
{
    local args = sscanf("dd", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /round_start <target_points> <time_in_seconds>");
        return;
    }

    if(getRound() != null) 
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Round already created!");
        return;
    }

    local points = args[0], timeInSeconds = args[1];

    if(points <= 0 && timeInSeconds <= 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Target points/time in seconds value must be positive!");
        return;
    }

    startRound(points, timeInSeconds);

    sendServerMessageToAll(255, 80, 0, format("Round will start in %f minutes!", timeInSeconds / 60.0));
}
, PERMISSION_FUNC_ADMIN);

addCommand("round_stop", function(playerId, params) 
{
    if(getRound() == null) 
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "There is no round!");
        return;
    }

    stopRound();

    sendServerMessageToAll(255, 80, 0, format("Admin %s terminated active round!", getPlayerName(playerId)));
}
, PERMISSION_FUNC_ADMIN);

addCommand("round_target", function(playerId, params) 
{
    local args = sscanf("d", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /round_target <target_points>");
        return;
    }

    local points = args[0];

    if(points < 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Target points value must be positive!");
        return;
    }

    if(changeRoundTarget(points))
    {
        sendServerMessageToAll(255, 80, 0, format("Round target changed to %d points!", points));
    }
    else 
        sendServerMessageToPlayer(playerId, 128, 0, 0, "You cannot change target value right now!");
}
, PERMISSION_FUNC_ADMIN);

addCommand("guild_lock", function(playerId, params) 
{
    local args = sscanf("s", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /guild_lock <guild_id>");
        return;
    }

    local guildId = args[0];
    local guild = getGuildById(guildId);

    if(guild == null) 
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Given guild ID is not existing!");
        return;
    }

    local lock = !guild.isLocked();
        guild.setLock(lock);

    sendServerMessageToAll(255, 80, 0, format("Admin %s %s %s!", getPlayerName(playerId), lock ? "locked" : "unlocked", guildId));
}
, PERMISSION_FUNC_ADMIN);

addCommand("effects", function(playerId, params) 
{
    local text = "";
    local effectList = getEffectTemplates();

    foreach(index, effect in effectList)
    {
        text += (index + " ");
    }

    sendServerMessageToPlayer(playerId, 128, 0, 0, "Effects possible to apply/modify by you:");
    sendServerMessageToPlayer(playerId, 128, 0, 0, text); 
}
, PERMISSION_FUNC_ADMIN);

addCommand("effect_time_set", function(playerId, params) 
{
    local args = sscanf("sd", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /effect_time_set <effect_name> <value_in_seconds>");
        return;
    }

    local name = args[0], value = args[1];
    local effect = getEffectTemplateById(name);

    if(effect == null)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Invalid effect name!");
        return;
    }

    if(value < -1)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Invalid effect value!");
        return;
    }

        effect.setTime(value);
    sendServerMessageToAll(255, 80, 0, format("Admin %s set new value for %s, new value is %d!", getPlayerName(playerId), name, value));
}
, PERMISSION_FUNC_ADMIN);

addCommand("effect_time_get", function(playerId, params) 
{
    local args = sscanf("s", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /effect_time_get <effect_name>");
        return;
    }

    local effect = getEffectTemplateById(args[0]);

    if(effect == null)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Invalid effect name!");
        return;
    }

    sendServerMessageToPlayer(playerId, 255, 80, 0, format("Effect time value is %d!", effect.getTime() / 1000));
}
, PERMISSION_FUNC_ADMIN);

addCommand("time", function(playerId, params) 
{
    local args = sscanf("dd", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /time <hour> <minute>");
        return;
    }

    local hour = args[0], minute = args[1];

    if(hour > 23 || hour < 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Invalid hour value!");
        return;
    }
    
    if(minute > 59 || minute < 0)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Invalid minute value!");
        return;
    }

    setTime(hour, minute);

    sendServerMessageToAll(255, 80, 0, format("Admin %s set new hour - %02d:%02d!", getPlayerName(playerId), hour, minute));
}
, PERMISSION_FUNC_MOD);

addCommand("rain", function(playerId, params) 
{
    if(Sky.dontRain)
    {
        sendServerMessageToAll(255, 80, 0, format("Admin %s enabled rain!", getPlayerName(playerId)));
            Sky.dontRain = false;
    }
    else 
    {
        sendServerMessageToAll(255, 80, 0, format("Admin %s disabled rain!", getPlayerName(playerId)));
            Sky.dontRain = true;
    }
}
, PERMISSION_FUNC_MOD);

// PLAYER COMMANDS
addCommand("help", function(playerId, params) 
{
    local commands = getChatCommands();
    local text = "";

    foreach(index, command in commands)
    {
        if(command.permission == null || command.permission(playerId))
        {
            text += ("/" + index + " ");
        }
    }

    sendServerMessageToPlayer(playerId, 128, 0, 0, "Commands possible to use by you:");
    sendServerMessageToPlayer(playerId, 128, 0, 0, text);
});

local privateMessage = function(playerId, params)
{
    local args = sscanf("ds", params);

    if(!args)
    {
        sendServerMessageToPlayer(playerId, 128, 0, 0, "Valid command structure - /pm or /pw <target_id> <message>");
        return;
    }

    local targetId = args[0], message = args[1];

    if(isPlayerLogged(targetId) && playerId != targetId)
    {
        local message = format("%d%d%s", ALL_CHATS, 0, message);

        sendPlayerMessageToPlayer(playerId, playerId, 255, 255, 255, message);
        sendPlayerMessageToPlayer(playerId, targetId, 255, 255, 255, message);
    }
    else 
        sendServerMessageToPlayer(playerId, 128, 0, 0, "You cannot send message to this ID!");
}

addCommand("pm", privateMessage);
addCommand("pw", privateMessage);