
class Area.Player
{
    _areaList = null;

    constructor()
    {
        _areaList = [];
    }

//:public
    function server_enterArea(instance)
    {
        _areaList.push(instance);
    }

    function server_leaveArea(instance)
    {
        local id = _areaList.find(instance);
        
        if(id != null)
        {
            _areaList.remove(id);
        }
    }
    
    function server_clear(instance)
    {
        _areaList.clear();
    }
        
    function isInArea(area)
    {
        foreach(listArea in _areaList)
        {
            if(area == listArea)
                return true;
        }
    
        return false;
    }
}