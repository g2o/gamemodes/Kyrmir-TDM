
class Area.Manager
{
//:public
    static function server_enterArea(packet)
    {
            local instance = packet.readString();
        getHero().getAreaModule().server_enterArea(instance);

        callEvent("Area.onHeroEnterArea", instance);
    }

    static function server_leaveArea(packet)
    {
            local instance = packet.readString();
        getHero().getAreaModule().server_leaveArea(instance);

        callEvent("Area.onHeroLeaveArea", instance);
    }

    static function server_clear(packet)
    {
        getHero().getAreaModule().server_clear();
    }

    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Area_PacketIDs.Enter: server_enterArea(packet); break;
            case Area_PacketIDs.Leave: server_leaveArea(packet); break;
            case Area_PacketIDs.Leave: server_clear(packet); break;
        }   
    } 
}

getAreaManager <- @() Area.Manager;

// Area
isHeroInArea <- @(area) getHero().getAreaModule().isInArea(area);