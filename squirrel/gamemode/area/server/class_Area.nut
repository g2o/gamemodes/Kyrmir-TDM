
class Area.Area
{
    _instance = null;
    _world = null;

    _pointList = null;

    constructor(instance, world)
    {
        _instance = instance;
        _world = world;
        
        _pointList = [];
    }

//:public
    function getInstance() { return _instance; }

    function addPoint(x, y)
    {
        _pointList.push( { x = x, y = y } );
            return this;
    }

    function checkPoint(world, x, y)
    {
        local oddNodes = false;

        if(world == _world)
        {
            local arraySize = _pointList.len(), j = arraySize - 1;

            for(local i = 0; i < arraySize; i++)
            {
                if((_pointList[i].y < y && _pointList[j].y >= y
                    || _pointList[j].y < y && _pointList[i].y >= y)
                    && (_pointList[i].x <= x || _pointList[j].x <= x))
                {
                    if(_pointList[i].x + (y - _pointList[i].y) / (_pointList[j].y - _pointList[i].y) * (_pointList[j].x - _pointList[i].x) < x)
                    {
                        oddNodes = !oddNodes;
                    }
                }

                j = i;
            }
        }

        return oddNodes;
    }

    function getPoints() { return _pointList; }
}