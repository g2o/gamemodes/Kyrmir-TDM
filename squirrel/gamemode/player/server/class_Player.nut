
class Player.Player 
{
    _playerId = -1;
    _dataBaseId = -1;

    _eqModule = null;
    _dialogModule = null;
    _areaModule = null;
    _effectModule = null;
    _rewardModule = null;
    _damageModule = null;
    
    _logged = false;

    _onlineTime = 0;
    _sessionStartTime = 0;
    
    _mute = false;
    
    _permission = 0;
    _immortal = false;

    _guild = null;

    _instance = "PC_HERO";
    _visual = null;

    _level = 0;
    _learnPoints = 0;

    _kills = 0;
    _deaths = 0;
    _assists = 0;
        
    _dealtDamage = 0;

    _lastHitList = null;
    _killerId = -1;

    _damageType = -1;

    _experience = 0;
    _experienceNextLevel = 0;

    _health = 0;
    _maxHealth = 0;
    _maxHealth_Bonus = 0;

    _mana = 0;
    _maxMana = 0;
    _maxMana_Bonus = 0;
    
    _magicLevel = 0;

    _strength = 0;
    _strength_Bonus = 0;

    _dexterity = 0;
    _dexterity_Bonus = 0;

    _weaponSkill = null; 

    constructor(playerId)
    {
        _playerId = playerId;
        _dataBaseId = -1;

        _eqModule = Equipment.Player(playerId);
        _dialogModule = Dialog.Player(playerId);
        _areaModule = Area.Player(playerId);
        _effectModule = Effect.Player(playerId);
        _rewardModule = Reward.Player(playerId);
        _damageModule = Damage.Player(playerId);

        _logged = false;
        
        _onlineTime = 0;
        _sessionStartTime = 0;

        _mute = false;

        _permission = 0;
        _immortal = false;

        _guild = null;

        _instance = "PC_HERO";
        _visual = { bodyModel = "Hum_Body_Naked0", bodyTxt = 9, headModel = "Hum_Head_Pony", headTxt = 18 };

        _level = 0;
        _learnPoints = 0;
        
        _experience = 0;
        _experienceNextLevel = 0;
        
        _kills = 0;
        _deaths = 0;
        _assists = 0;

        _dealtDamage = 0;

        _lastHitList = {};
        _killerId = -1;

        _damageType = DAMAGE_BLUNT;

        _health = 0;
        _maxHealth = 0;
        _mana = 0;
        _maxMana = 0;

        _magicLevel = 0;

        _strength = 0;
        _dexterity = 0;

        _weaponSkill = {};
    }

//:public
    function getId() { return _playerId; }  

    function setDataBaseId(dabaBaseId)
    {
        _dataBaseId = dabaBaseId;
    }

    function getDataBaseId() { return _dataBaseId; }

    function setLogged(logged)
    {
        if(_logged != logged)
        {
            callEvent("Player.onPlayerLogin", _playerId, _logged = logged);
        }
    }

    function isLogged() { return _logged }

    function setOnlineTime(onlineTime)
    {
        _onlineTime = onlineTime;
    }

    function getOnlineTime() { return _onlineTime; }

    function setSessionStartTime(sessionStartTime)
    {
        _sessionStartTime = sessionStartTime;
    }

    function getSessionStartTime() { return _sessionStartTime; }

    function setMute(mute)
    {
        _mute = mute;
    }

    function isMuted() { return _mute; }

    function setPermission(permission)
    {
        if(_permission != permission)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Permission);
                packet.writeInt8(_permission = permission);    
            packet.send(_playerId, RELIABLE_ORDERED);    
        }
    }

    function getPermission() { return _permission; }

    function setImmortal(immortal)
    {
        _immortal = immortal;
    }

    function isImmortal() { return _immortal; }

    function setGuild(guild)
    {
        if(_guild != guild)
        {
            callEvent("Player.onPlayerChangeGuild", _playerId, _guild, _guild = guild);
 
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Guild);

                packet.writeInt16(_playerId);    
                packet.writeString(guild == null ? "null" : guild);    

            packet.sendToAll(RELIABLE_ORDERED);   
        }
    }

    function getGuild() { return _guild; }

    // Modules
    function getEquipmentModule() { return _eqModule; }

    function getDialogModule() { return _dialogModule; } 

    function getAreaModule() { return _areaModule; }

    function getEffectModule() { return _effectModule; }

    function getRewardModule() { return _rewardModule; }
    
    function getDamageModule() { return _damageModule; }

    // Player.Player
    function setData(dataTable)
    {
        local guildId = dataTable["guild"];

        _mute = dataTable["mute"];
        _onlineTime = dataTable["online_time"];
        _permission = dataTable["permission"];

        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Permission);
            packet.writeInt8(_permission);    
        packet.send(_playerId, RELIABLE_ORDERED);    

        _guild = guildId != "null" ? guildId : null;

        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Guild);
            packet.writeInt16(_playerId);    
            packet.writeString(_guild == null ? "null" : _guild);    
        packet.sendToAll(RELIABLE_ORDERED); 

        setPlayerName(_playerId, dataTable["username"]);
        _setPlayerVisual(_playerId, _visual.bodyModel = dataTable["body_model"], _visual.bodyTxt = dataTable["body_texture"], _visual.headModel = dataTable["head_model"], _visual.headTxt = dataTable["head_texture"]);
        
        setPlayerWorld(_playerId, dataTable["world"]);
        setPlayerPosition(_playerId, dataTable["pos_x"], dataTable["pos_y"], dataTable["pos_z"]);
        setPlayerAngle(_playerId, dataTable["angle"]);

        _level = dataTable["level"];
        
        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Level);
            packet.writeInt16(_level);    
        packet.send(_playerId, RELIABLE);  

        setLearnPoints(dataTable["learn_points"]);
        setExperienceNextLevel(dataTable["experience_next"]);
        setExperience(dataTable["experience"]);
        setMagicLevel(dataTable["magic_level"]);

        setMaxHealth(_maxHealth = dataTable["max_health"]);
        setHealth(dataTable["health"]);
        setMaxMana(dataTable["max_mana"]);
        setMana(dataTable["mana"]);

        setStrength(dataTable["strength"]);
        setDexterity(dataTable["dexterity"]);

        setSkillWeapon(WEAPON_1H, dataTable["skill_1h"]);
        setSkillWeapon(WEAPON_2H, dataTable["skill_2h"]);
        setSkillWeapon(WEAPON_BOW, dataTable["skill_bow"]);
        setSkillWeapon(WEAPON_CBOW, dataTable["skill_cbow"]); 
    }

    function setStats(statsTable)
    {
        _rewardModule.setStats(statsTable);
                
        _kills = statsTable["kills"];
        _deaths = statsTable["deaths"];
        _assists = statsTable["assists"];       

        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Kills);
            packet.writeInt16(_playerId);    
            packet.writeInt16(_kills);    
        packet.sendToAll(UNRELIABLE); 

        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Deaths);
            packet.writeInt16(_playerId);    
            packet.writeInt16(_deaths);  
        packet.sendToAll(UNRELIABLE);    
        
        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Assists);
            packet.writeInt16(_playerId);    
            packet.writeInt16(_assists);    
        packet.sendToAll(UNRELIABLE);    

        _dealtDamage = statsTable["dealt_damage"];
    }

    function setInstance(instance)
    {
        if(_instance != instance)
        {
            // RESET CONTEXT...
            setPlayerWeaponMode(_playerId, WEAPONMODE_NONE);

            _setPlayerInstance(_playerId, _instance = instance);

            _setPlayerMagicLevel(_playerId, _magicLevel);
        
            _setPlayerMaxHealth(_playerId, _maxHealth);
            _setPlayerHealth(_playerId, _health = _maxHealth);

            _setPlayerMaxMana(_playerId, _maxMana);
            _setPlayerMana(_playerId, _mana = _maxMana);

            _setPlayerStrength(_playerId, _strength);
            _setPlayerDexterity(_playerId, _dexterity);
        
            foreach(index, skill in _weaponSkill) 
            {
                _setPlayerSkillWeapon(_playerId, index, skill);
            }
        }   
    }

    function reverseTransform()
    {
        if(_instance != "PC_HERO") 
        {
            _setPlayerInstance(_playerId, _instance = "PC_HERO");
       
            _damageType = DAMAGE_BLUNT;

            _maxHealth_Bonus = 0;
            _maxMana_Bonus = 0;

            _strength_Bonus = 0;
            _dexterity_Bonus = 0;

            _setPlayerVisual(_playerId, _visual.bodyModel, _visual.bodyTxt, _visual.headModel, _visual.headTxt);
            _setPlayerMagicLevel(_playerId, _magicLevel);
        
            _setPlayerMaxHealth(_playerId, _maxHealth);
            _setPlayerHealth(_playerId, _health = _maxHealth);

            _setPlayerMaxMana(_playerId, _maxMana);
            _setPlayerMana(_playerId, _mana = _maxMana);

            _setPlayerStrength(_playerId, _strength);
            _setPlayerDexterity(_playerId, _dexterity);

            foreach(index, skill in _weaponSkill) 
            {
                _setPlayerSkillWeapon(_playerId, index, skill);
            }

            print("reverseTransform - recovering items and stats!");
            
            // Packet order
            _eqModule.playerRespawn();
        }
    }

    function getInstance() { return _instance; }

    function setVisual(bodyModel, bodyTxt, headModel, headTxt)
    {
        _setPlayerVisual(_playerId, _visual.bodyModel = bodyModel, _visual.bodyTxt = bodyTxt, _visual.headModel = headModel, _visual.headTxt = headTxt);
            callEvent("Player.onPlayerChangeVisual", _playerId, bodyModel, bodyTxt, headModel, headTxt);
    }

    function getVisual() { return _visual; }

    function setLevel(level)
    {
        if(_level != level)
        {   
            callEvent("Player.onPlayerChangeLevel", _playerId, _level, _level = level);

            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Level);
                packet.writeInt16(level);    
            packet.send(_playerId, RELIABLE);    
        }
    }

    function getLevel() { return _level; }

    function setLearnPoints(learnPoints)
    {
        if(_learnPoints != learnPoints)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.LearnPoints);
                packet.writeInt16(_learnPoints = learnPoints);    
            packet.send(_playerId, RELIABLE);    
        }
    }

    function getLearnPoints() { return _learnPoints; }
    
    function setDamageType(damageType)
    {
        _damageType = damageType;
    }

    function getDamageType() { return _damageType; }

    function setExperience(experience)
    {
        if(experience != _experience)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Experience);
                packet.writeInt32(_experience = experience);    
            packet.send(_playerId, RELIABLE);    
        }
    }

    function getExperience() { return _experience; }

    function setExperienceNextLevel(experienceNextLevel)
    {
        if(experienceNextLevel != _experienceNextLevel)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.ExperienceNextLevel);
                packet.writeInt32(_experienceNextLevel = experienceNextLevel);    
            packet.send(_playerId, RELIABLE);    
        }
    }

    function getExperienceNextLevel() { return _experienceNextLevel; }
    
    function setKills(kills)
    {
        if(kills != _kills)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Kills);

                packet.writeInt16(_playerId);    
                packet.writeInt16(_kills = kills);    

            callEvent("Player.onPlayerKills", _playerId, kills);

            packet.sendToAll(UNRELIABLE);    
        }
    }

    function getKills() { return _kills; }

    function setDeaths(deaths)
    {
        if(deaths != _deaths)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Deaths);

                packet.writeInt16(_playerId);    
                packet.writeInt16(_deaths = deaths);  

            callEvent("Player.onPlayerDeaths", _playerId, deaths);

            packet.sendToAll(UNRELIABLE);    
        }
    }

    function getDeaths() { return _deaths; }

    function setAssists(assists)
    {
        if(assists != _assists)
        {
            local packet = Packet(PacketIDs.Player, Player_PacketIDs.Assists);

                packet.writeInt16(_playerId);    
                packet.writeInt16(_assists = assists);    
            
            callEvent("Player.onPlayerAssists", _playerId, assists);

            packet.sendToAll(UNRELIABLE);    
        }
    }        

    function getAssists() { return _assists; }
    
    function setDealtDamage(damage)
    {
        if(damage != _dealtDamage)
        { 
            callEvent("Player.onPlayerDamage", _playerId, _dealtDamage = damage);
        }
    }

    function getDealtDamage() { return _dealtDamage; }

    function setLastHitTime(killerId, lastHitTime)
    {
        if(killerId in _lastHitList) 
        {
            _lastHitList[killerId] = lastHitTime;
        }
        else 
            _lastHitList[killerId] <- lastHitTime;
    }

    function removeLastHit(playerId)
    {
        if(playerId in _lastHitList)
        {
            delete _lastHitList[playerId];
        }
    }

    function getLastHitList(fromTime = 8000) 
    { 
            local tick = getTickCount();
        _lastHitList = _lastHitList.filter(@(playerId, timeValue) timeValue + fromTime >= tick);

        return _lastHitList; 
    }

    function setKillerId(killerId)
    {
        _killerId = killerId;
    }

    function getKillerId() { return _killerId; }

    function setMagicLevel(magicLevel)
    {
        _setPlayerMagicLevel(_playerId, _magicLevel = magicLevel);
    }

    function getMagicLevel() { return _magicLevel; }

    function setHealth(health)
    {
        local maxHealth = _maxHealth + _maxHealth_Bonus;

        if(health > maxHealth) 
        {   
            _setPlayerHealth(_playerId, _health = maxHealth);
        }
        else 
            _setPlayerHealth(_playerId, _health = health);
    }

    function getHealth() { return _health; }

    function receiveDamage(damage)
    {
        local newHealth = _health - damage;
     
        if(newHealth <= 0)
            _health = 0;
        else 
            _health = newHealth;
    }
    
    function setMaxHealth(maxHealth)
    {
        local suma = _maxHealth_Bonus + maxHealth;

        _setPlayerMaxHealth(_playerId, suma);
            _maxHealth = maxHealth

        if(_health > suma) _setPlayerHealth(_playerId, _health = suma);
    }

    function getMaxHealth() { return _maxHealth + _maxHealth_Bonus; }

    function setMaxHealth_Bonus(maxHealth)
    {
        local suma = _maxHealth + maxHealth;

        _setPlayerMaxHealth(_playerId, suma);
            _maxHealth_Bonus = maxHealth;

        if(_health > suma) _setPlayerHealth(_playerId, _health = suma);
    }

    function getMaxHealth_Bonus() { return _maxHealth_Bonus; }

    function getMaxHealth_Core() { return _maxHealth; }

    function setMana(mana)
    {
        local maxMana = _maxMana + _maxMana_Bonus;

        if(mana > maxMana) 
        {   
            _setPlayerMana(_playerId, _mana = maxMana);
        }
        else 
            if(mana <= 0)
                _setPlayerMana(_playerId, 0);
            else 
                _setPlayerMana(_playerId, _mana = mana);
    }

    function getMana() { return _mana; }

    function setMaxMana(maxMana) 
    {
        local suma = _maxMana_Bonus + maxMana;

        _setPlayerMaxMana(_playerId, suma);
            _maxMana = maxMana;  

        if(_mana > suma) _setPlayerMana(_playerId, _mana = _maxMana);
    }

    function getMaxMana() { return _maxMana + _maxMana_Bonus; }
    
    function setMaxMana_Bonus(maxMana)
    {
        local suma = _maxMana + maxMana;
 
        _setPlayerMaxMana(_playerId, suma);
            _maxMana_Bonus = maxMana;  

        if(_mana > suma) _setPlayerMana(_playerId, _mana = suma);
    }

    function getMaxMana_Bonus() { return _maxMana_Bonus; }

    function getMaxMana_Core() { return _maxMana; }

    function setStrength(strength) 
    {
        _setPlayerStrength(_playerId, _strength_Bonus + strength);
            _strength = strength;
    }

    function getStrength() { return _strength + _strength_Bonus; }

    function setStrength_Bonus(strength)
    {
        _setPlayerStrength(_playerId, _strength + strength);
            _strength_Bonus = strength;
    }

    function getStrength_Bonus() { return _strength_Bonus; }
    
    function getStrength_Core() { return _strength; }

    function setDexterity(dexterity) 
    {
        _setPlayerDexterity(_playerId, _dexterity_Bonus + dexterity);
            _dexterity = dexterity;
    }

    function getDexterity() { return _dexterity + _dexterity_Bonus; }

    function setDexterity_Bonus(dexterity)
    {
        _setPlayerDexterity(_playerId, _dexterity + dexterity);
            _dexterity_Bonus = dexterity;
    }

    function getDexterity_Bonus() { return _dexterity_Bonus; }
    
    function getDexterity_Core() { return _dexterity; }

    function setSkillWeapon(skillType, skill) 
    {
        _setPlayerSkillWeapon(_playerId, skillType, _weaponSkill[skillType] <- skill);
    }

    function getSkillWeapon(skillType) { return skillType in _weaponSkill ? _weaponSkill[skillType] : 0; }

    function playerDisconnect()
    {
        callEvent("Player.onPlayerPreDisconnect", _playerId);
    }

    function respawn()
    {        
        if(_instance != "PC_HERO") 
        {
            _setPlayerInstance(_playerId, _instance = "PC_HERO");
        }

        _damageType = DAMAGE_BLUNT;

        _maxHealth_Bonus = 0;
        _maxMana_Bonus = 0;

        _strength_Bonus = 0;
        _dexterity_Bonus = 0;

        _setPlayerVisual(_playerId, _visual.bodyModel, _visual.bodyTxt, _visual.headModel, _visual.headTxt);
        _setPlayerMagicLevel(_playerId, _magicLevel);
    
        _setPlayerMaxHealth(_playerId, _maxHealth);
        _setPlayerHealth(_playerId, _health = _maxHealth);

        _setPlayerMaxMana(_playerId, _maxMana);
        _setPlayerMana(_playerId, _mana = _maxMana);

        _setPlayerStrength(_playerId, _strength);
        _setPlayerDexterity(_playerId, _dexterity);

        foreach(index, skill in _weaponSkill) 
        {
            _setPlayerSkillWeapon(_playerId, index, skill);
        }
        
        print("playerRespawn - recovering items and stats!");
            _killerId = -1;

        _lastHitList.clear();
    }

    function reset()
    {
        if(_logged == false) 
            return;

        if(_instance != "PC_HERO") 
        {
            _setPlayerInstance(_playerId, _instance = "PC_HERO");
        }
        
        _guild = null;

        _level = 0;
        _learnPoints = 0;
        _experience = 0;
        _experienceNextLevel = getNextLevelExperience(0);

        setMagicLevel(0);

        setMaxHealth(DEFAULT_STATS.health);
        setHealth(DEFAULT_STATS.health);   
        
        setMaxMana(DEFAULT_STATS.mana);		
        setMana(DEFAULT_STATS.mana);

        _maxHealth_Bonus = 0;
        _maxMana_Bonus = 0;

        setStrength(DEFAULT_STATS.strength);
        setDexterity(DEFAULT_STATS.dexterity);

        _strength_Bonus = 0;
        _dexterity_Bonus = 0;

        setSkillWeapon(WEAPON_1H, 0);
        setSkillWeapon(WEAPON_2H, 0);
        setSkillWeapon(WEAPON_BOW, 0);
        setSkillWeapon(WEAPON_CBOW, 0);
        
        _kills = 0;
        _deaths = 0;
        _assists = 0;

        _dealtDamage = 0;

        _rewardModule.setMonsterStreak(0);
        _rewardModule.setPlayerStreak(0);
        
        setPlayerWeaponMode(_playerId, WEAPONMODE_NONE);

        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Reset);

            packet.writeInt16(_playerId);
            packet.writeInt16(_learnPoints);
            packet.writeInt32(_experienceNextLevel);

        packet.sendToAll(RELIABLE_ORDERED);    

        _eqModule.clearEq();

        foreach(item in DEFAULT_STATS.defaultEq)
        {
            _eqModule.giveItem(getItemTemplateIdByInstance(item[0]), item[1]);
        }
        
        // Overlay reload
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_1HST1.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_1HST2.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_2HST1.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_2HST2.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_BOWT1.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_BOWT2.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_CBOWT1.MDS"));
        _removePlayerOverlay(_playerId, Mds.id("HUMANS_CBOWT2.MDS"));

        callEvent("Player.onPlayerReset", _playerId);
    }

    // Player.Equipment
    function getProtection(damageType)
    {
        return _instance == "PC_HERO" ? _eqModule.getProtection(damageType) : 0;
    }
}