
class Player.Manager 
{    
    static _playerList = {};

//:protected 
    static function _listToPacket()
    {
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.List);

            packet.writeInt16(_playerList.len());

        foreach(id, player in _playerList)
        {
            local guild = player.getGuild();

            packet.writeInt16(id);
            packet.writeString(guild == null ? "null" : guild);         
            packet.writeInt16(player.getKills());         
            packet.writeInt16(player.getDeaths());         
            packet.writeInt16(player.getAssists());         
        }

        return packet;
    }

//:public 
    static function client_login(playerId, packet)
    {
        local result = getDataBase().login(packet.readString(), packet.readString()),
            packet = Packet(PacketIDs.Player, Player_PacketIDs.Login);

        if(typeof(result) != "integer")
        {
            if(result["online"] == 0)
            {
                local playerBaseId = result["id"];

                setPlayerBaseId(playerId, playerBaseId);
                setPlayerData(playerId, result);

                local resultTable = getDataBase().loadStats(playerBaseId);
                if(resultTable) setPlayerStats(playerId, resultTable);

                local resultTable = getDataBase().loadEquipment(playerBaseId);
                if(resultTable) setPlayerInventory(playerId, resultTable);
                
                setPlayerSessionStartTime(playerId, time());
                setPlayerLogged(playerId, true);

                getDataBase().updateOnlineStatus(playerBaseId, 1);

                packet.writeInt8(result["online_time"] == 0 ? LoginCode.FirstLogin : LoginCode.Succes);
            }
            else 
                packet.writeInt8(LoginCode.Fault);
        }
        else 
        {
            switch(result)
            {
                case DataPlayer.BadPassword: 
                    packet.writeInt8(LoginCode.Fault);
                break;
                case DataPlayer.NotExist: 
                    packet.writeInt8(LoginCode.NotExist);
                break;
            }
        }

        packet.send(playerId, RELIABLE); 
    }

    static function client_register(playerId, packet)
    {
        local data = getDataBase().register(packet.readString(), packet.readString()),
            packet = Packet(PacketIDs.Player, Player_PacketIDs.Register);

        switch(data)
        {
            case DataPlayer.Succes: 
                packet.writeInt8(RegisterCode.Succes);
            break;            
            case DataPlayer.Exists: 
                packet.writeInt8(RegisterCode.DataUsed);
            break;
            case DataPlayer.Fault: 
                packet.writeInt8(RegisterCode.Fault);
            break;
        }

        packet.send(playerId, RELIABLE);  
    }

    static function client_visual(playerId, packet)
    {
        getPlayer(playerId).setVisual(packet.readString(), packet.readInt8(), packet.readString(), packet.readInt16());
    }

    static function client_reverseTransform(playerId)
    {
        getPlayer(playerId).reverseTransform();
    }

    static function playerJoin(playerId)
    {
        _playerList[playerId] <- Player.Player(playerId);
            _listToPacket().send(playerId, RELIABLE);
    }

    static function playerDisconnect(playerId)
    {
        _playerList[playerId].playerDisconnect();
        
        foreach(player in _playerList)
        {
            player.removeLastHit(playerId);
        }

        delete _playerList[playerId];
    }
    
    static function playerHit(playerId, killerId)
    {   
        _playerList[playerId].setLastHitTime(killerId, getTickCount());
    }

    static function playerDead(playerId, killerId, hitList)
    {           
        if(isRoundStarted() == false || getPlayerLevel(playerId) < MAX_SWAP_LEVEL) 
        {
            if(killerId != -1 && killerId < getMaxSlots())
            {
                setPlayerKillerId(playerId, killerId); 
            }
            
            return;
        }

        if(hitList.len() == 0) return;

        if(killerId == -1 || killerId >= getMaxSlots())
        {
            local killerId = -1, minValue = 0;

            foreach(lastId, lastHitTime in hitList)
            {
                if(killerId == -1)
                {
                    minValue = lastHitTime;
                    killerId = lastId;       
                }
                else 
                {
                    if(lastHitTime < minValue)
                    {
                        setPlayerAssists(killerId, getPlayerAssists(killerId) + 1); 

                        minValue = lastHitTime;
                        killerId = lastId;
                    }
                    else setPlayerAssists(lastId, getPlayerAssists(lastId) + 1); 
                }
            }

            setPlayerDeaths(playerId, getPlayerDeaths(playerId) + 1);
            setPlayerKills(killerId, getPlayerKills(killerId) + 1);

            setPlayerKillerId(playerId, killerId); 
        }
        else 
        {
            setPlayerDeaths(playerId, getPlayerDeaths(playerId) + 1);

            foreach(lastId, lastHitTime in hitList)
            {
                if(lastId == killerId)
                    setPlayerKills(killerId, getPlayerKills(killerId) + 1);
                else 
                    setPlayerAssists(lastId, getPlayerAssists(lastId) + 1);
            }

            setPlayerKillerId(playerId, killerId); 
        }
    }

    static function playerRespawn(playerId)
    {
        _playerList[playerId].respawn();
    }

    static function getPlayer(playerId)
    {
        return playerId in _playerList ? _playerList[playerId] : null;
    }

    static function getPlayers() { return _playerList; }

    static function packetRead(playerId, packet)
    {
        local packetType = packet.readInt8();

        if(isPlayerLogged(playerId) == false)
        {        
            switch(packetType)
            {
                case Player_PacketIDs.Login: client_login(playerId, packet); break;
                case Player_PacketIDs.Register: client_register(playerId, packet); break;
            }
        }   
        else 
        {
            switch(packetType)
            {
                case Player_PacketIDs.Visual: client_visual(playerId, packet); break;
                case Player_PacketIDs.Transformation: client_reverseTransform(playerId); break;
            }
        }
    } 
}

getPlayerManager <- @() Player.Manager;

// Player

function setPlayerBaseId(playerId, baseId)
{
    local player = getPlayer(playerId);
    if(player) player.setDataBaseId(baseId);
}

function getPlayerBaseId(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDataBaseId();

    return -1; 
}

function setPlayerData(playerId, dataTable)
{
    local player = getPlayer(playerId);
    if(player) player.setData(dataTable);
}

function setPlayerStats(playerId, statsTable)
{
    local player = getPlayer(playerId);
    if(player) player.setStats(statsTable);
}

function setPlayerLogged(playerId, logged)
{
    local player = getPlayer(playerId);
    if(player) player.setLogged(logged);
}

function isPlayerLogged(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.isLogged();

    return false; 
}

function setPlayerOnlineTime(playerId, onlineTime)
{
    local player = getPlayer(playerId);
    if(player) player.setOnlineTime(onlineTime);
}

function getPlayerOnlineTime(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getOnlineTime();

    return 0; 
}

function setPlayerSessionStartTime(playerId, sessionStartTime)
{
    local player = getPlayer(playerId);
    if(player) player.setSessionStartTime(sessionStartTime);
}

function getPlayerSessionStartTime(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getSessionStartTime();

    return 0; 
}

function setPlayerMute(playerId, mute)
{
    local player = getPlayer(playerId);
    if(player) player.setMute(mute);
}

function isPlayerMuted(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.isMuted();

    return false; 
}

function setPlayerPermission(playerId, permission)
{
    local player = getPlayer(playerId);
    if(player) player.setPermission(permission);
}

function getPlayerPersmission(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getPermission();

    return 0; 
}

function setPlayerImmortal(playerId, immortal)
{
    local player = getPlayer(playerId);
    if(player) player.setImmortal(immortal);
}

function isPlayerImmortal(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.isImmortal();

    return false; 
}

function setPlayerGuild(playerId, guild)
{
    local player = getPlayer(playerId);
    if(player) player.setGuild(guild);
}

function getPlayerGuild(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getGuild();

    return null; 
}

function setPlayerLevel(playerId, level)
{
    local player = getPlayer(playerId);
    if(player) player.setLevel(level);
}

function getPlayerLevel(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getLevel();

    return 0;
}

function setPlayerLearnPoints(playerId, learnPoints)
{
    local player = getPlayer(playerId);
    if(player) player.setLearnPoints(learnPoints);
}

function getPlayerLearnPoints(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getLearnPoints();

    return 0;
}

function setPlayerDamageType(playerId, damageType)
{
    local player = getPlayer(playerId);
    if(player) player.setDamageType(damageType);
}

function getPlayerDamageType(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDamageType();

    return -1;
}

function setPlayerExperience(playerId, experience)
{
    local player = getPlayer(playerId);
    if(player) player.setExperience(experience);
}

function getPlayerExperience(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getExperience();

    return 0;
}

function setPlayerExperienceNextLevel(playerId, experienceNextLevel)
{
    local player = getPlayer(playerId);
    if(player) player.setExperienceNextLevel(experienceNextLevel);
}

function getPlayerExperienceNextLevel(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getExperienceNextLevel();

    return 0;
}

function setPlayerKills(playerId, kills)
{
    local player = getPlayer(playerId);
    if(player) player.setKills(kills);
}

function getPlayerKills(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getKills();

    return 0;
}

function setPlayerDeaths(playerId, deaths)
{
    local player = getPlayer(playerId);
    if(player) player.setDeaths(deaths);
}

function getPlayerDeaths(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDeaths();

    return 0;
}

function setPlayerAssists(playerId, assists)
{
    local player = getPlayer(playerId);
    if(player) player.setAssists(assists);
}

function getPlayerAssists(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getAssists();

    return 0;
}

function setPlayerDealtDamage(playerId, damage)
{
    local player = getPlayer(playerId);
    if(player) player.setDealtDamage(damage);
}

function getPlayerDealtDamage(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDealtDamage();

    return 0;
}

function getPlayerLastHitList(playerId, fromTime = 8000)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getLastHitList(fromTime);

    return null;
}

function setPlayerKillerId(playerId, killerId)
{
    local player = getPlayer(playerId);
    if(player) player.setKillerId(killerId);
}

function getPlayerKillerId(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getKillerId();

    return -1;
}

function resetPlayer(playerId)
{
    local player = getPlayer(playerId);
    if(player) player.reset();
}

getPlayer <- @(playerId) Player.Manager.getPlayer(playerId);
getPlayers <- @() Player.Manager.getPlayers();