
_setPlayerWorld <- setPlayerWorld;

_setPlayerVisual <- setPlayerVisual;
_setPlayerInstance <- setPlayerInstance;

_setPlayerMagicLevel <- setPlayerMagicLevel;

_setPlayerHealth <- setPlayerHealth;
_setPlayerMaxHealth <- setPlayerMaxHealth;
_setPlayerMana <- setPlayerMana;
_setPlayerMaxMana <- setPlayerMaxMana;

_setPlayerStrength <- setPlayerStrength;
_setPlayerDexterity <- setPlayerDexterity;

_setPlayerSkillWeapon <- setPlayerSkillWeapon;

function setPlayerWorld(playerId, world)
{
    if(world != getPlayerWorld(playerId)) _setPlayerWorld(playerId, world);
}

function setPlayerVisual(playerId, bodyModel, bodyTxt, headModel, headTxt)
{
    local player = getPlayer(playerId);
    if(player) player.setVisual(bodyModel, bodyTxt, headModel, headTxt);
}

function getPlayerVisual(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getVisual();

    return null;
}

function setPlayerInstance(playerId, instance)
{
    local player = getPlayer(playerId);
    if(player) player.setInstance(instance);
}

function getPlayerInstance(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getInstance();

    return null; 
}

function setPlayerMagicLevel(playerId, magicLevel)
{
    local player = getPlayer(playerId);
    if(player) player.setMagicLevel(magicLevel);
}

function getPlayerMagicLevel(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMagicLevel();

    return null;
}

function setPlayerHealth(playerId, health)
{
    local player = getPlayer(playerId);
    if(player) player.setHealth(health);
}

function getPlayerHealth(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getHealth();

    return 0;
}

function setPlayerMaxHealth(playerId, maxHealth)
{
    local player = getPlayer(playerId);
    if(player) player.setMaxHealth(maxHealth);
}

function getPlayerMaxHealth(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxHealth();

    return 0;
}

function setPlayerMaxHealth_Bonus(playerId, maxHealth)
{
    local player = getPlayer(playerId);
    if(player) player.setMaxHealth_Bonus(maxHealth);
}

function getPlayerMaxHealth_Bonus(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxHealth_Bonus();

    return 0;
}

function getPlayerMaxHealth_Core(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxHealth_Core();

    return 0;
}

function setPlayerMana(playerId, mana)
{
    local player = getPlayer(playerId);
    if(player) player.setMana(mana);
}

function getPlayerMana(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMana();

    return 0;
}

function setPlayerMaxMana(playerId, maxMana)
{
    local player = getPlayer(playerId);
    if(player) player.setMaxMana(maxMana);
}

function getPlayerMaxMana(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxMana();

    return 0;
}

function setPlayerMaxMana_Bonus(playerId, maxMana)
{
    local player = getPlayer(playerId);
    if(player) player.setMaxMana_Bonus(maxMana);
}

function getPlayerMaxMana_Bonus(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxMana_Bonus();

    return 0;
}

function getPlayerMaxMana_Core(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getMaxMana_Core();

    return 0;
}

function setPlayerStrength(playerId, strength)
{
    local player = getPlayer(playerId);
    if(player) player.setStrength(strength);
}

function getPlayerStrength(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getStrength();

    return 0;
}

function setPlayerStrength_Bonus(playerId, strength)
{
    local player = getPlayer(playerId);
    if(player) player.setStrength_Bonus(strength);
}

function getPlayerStrength_Bonus(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getStrength_Bonus();

    return 0;
}

function getPlayerStrength_Core(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getStrength_Core();

    return 0;
}

function setPlayerDexterity(playerId, dexterity)
{
    local player = getPlayer(playerId);
    if(player) player.setDexterity(dexterity);
}

function getPlayerDexterity(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDexterity();

    return 0;
}

function setPlayerDexterity_Bonus(playerId, dexterity)
{
    local player = getPlayer(playerId);
    if(player) player.setDexterity_Bonus(dexterity);
}

function getPlayerDexterity_Bonus(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDexterity_Bonus();

    return 0;
}

function getPlayerDexterity_Core(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDexterity_Core();

    return 0;
}

function setPlayerSkillWeapon(playerId, skillType, skill)
{
    local player = getPlayer(playerId);
    if(player) player.setSkillWeapon(skillType, skill);
}

function getPlayerSkillWeapon(playerId, skillType)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getSkillWeapon(skillType);

    return 0;
}