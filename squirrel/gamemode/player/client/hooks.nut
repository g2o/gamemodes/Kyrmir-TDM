
_setLearnPoints <- setLearnPoints;

// OVERRIDE

function getLevel()
{
    local hero = getHero();

    if(hero) 
        return hero.getLevel();

    return 0; 
}

function getLearnPoints()
{
    local hero = getHero();

    if(hero) 
        return hero.getLearnPoints();

    return 0; 
}

function getExp() 
{
    local hero = getHero();

    if(hero) 
        return hero.getExperience();

    return 0;    
}

function getNextLevelExp()
{
    local hero = getHero();

    if(hero) 
        return hero.getExperienceNextLevel();

    return 0; 
}