
// Register packet callback
PacketReceive.add(PacketIDs.Player, getPlayerManager());

addEventHandler("onPlayerCreate", function(playerId)
{
    getPlayerManager().playerCreate(playerId);
});

addEventHandler("onPlayerDestroy", function(playerId)
{
    getPlayerManager().playerDestroy(playerId);
});

addEventHandler("onWorldChange", function(world)
{
    cancelEvent();
});
 