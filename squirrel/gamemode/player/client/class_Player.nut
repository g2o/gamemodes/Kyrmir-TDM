
class Player.Player 
{
    _playerId = -1;
    
    _guild = null;

    _kills = 0;
    _deaths = 0;
    _assists = 0;

    _damageModule = null;

    constructor(playerId)
    {
        _playerId = playerId;

        _guild = null;  

        _kills = 0;
        _deaths = 0;
        _assists = 0;
    
        _damageModule = Damage.Player(playerId);
    }

//:public
    function getId() { return _playerId; }

    function setGuild(guild)
    {
        if(guild == "null") 
            guild = null;
        
        callEvent("Player.onPlayerChangeGuild", _playerId, _guild, _guild = guild);
    }

    function getGuild() { return _guild; } 

    // Modules
    function getDamageModule() { return _damageModule; }

    // Player.Player
    function setKills(kills)
    {
        if(kills != _kills)
        {
            callEvent("Player.onPlayerKills", _playerId, _kills = kills);
        }

        return this;
    }

    function getKills() { return _kills; }

    function setDeaths(deaths)
    {
        if(deaths != _deaths)
        {
            callEvent("Player.onPlayerDeaths", _playerId, _deaths = deaths);
        }

        return this;
    }

    function getDeaths() { return _deaths; }

    function setAssists(assists)
    {
        if(assists != _assists)
        {
            callEvent("Player.onPlayerAssists", _playerId, _assists = assists);
        }

        return this;
    }        

    function getAssists() { return _assists; }

    function reset()
    {
        _guild = null;  

        _kills = 0;
        _deaths = 0;
        _assists = 0;
    }
}

class Player.Hero extends Player.Player 
{
    _logged = false;

    _onlineTime = 0;
    _permission = 0;

    _flyMode = false;

    _eqModule = null;
    _dialogModule = null;
    _areaModule = null;
    _rewardModule = null;

    _level = 0;
    _learnPoints = 0;

    _experience = 0;
    _experienceNextLevel = 0;

    constructor(playerId)
    { 
        base.constructor(playerId);
        
        _logged = false;

        _onlineTime = 0;
        _permission = 0;

        _flyMode = false;

        _eqModule = Equipment.Player();        
        _dialogModule = Dialog.Player();
        _areaModule = Area.Player();
        _rewardModule = Reward.Player();

        _level = 0;
        _learnPoints = 0;

        _experience = 0;
        _experienceNextLevel = 0;
    }

//:public
    function getId() { return _playerId; }
    
    function getEquipmentModule() { return _eqModule; }   

    function getDialogModule() { return _dialogModule; } 
    
    function getAreaModule() { return _areaModule; }

    function getRewardModule() { return _rewardModule; }

    // Player.Data
    function server_login(responseCode)
    {
        switch(responseCode)
        {
            case LoginCode.Succes:
            case LoginCode.FirstLogin:
                _logged = true;
            break;
        }

        callEvent("Player.onHeroLogin", responseCode);
    }

    function server_register(responseCode)
    {
        callEvent("Player.onHeroRegister", responseCode);
    }

    function server_permission(packet)
    {
        getHero().server_permission(packet.readInt8());
    }

    function login(username, password)
    {
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Login);

            packet.writeString(username);
            packet.writeString(password);
            
        packet.send(RELIABLE);       
    }

    function register(username, password)
    {
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Register);

            packet.writeString(username);
            packet.writeString(password);
            
        packet.send(RELIABLE);  
    }

    function visual(bodyModel, bodyTxt, headModel, headTxt)
    {
        local packet = Packet(PacketIDs.Player, Player_PacketIDs.Visual);

            packet.writeString(bodyModel);
            packet.writeInt8(bodyTxt);
            packet.writeString(headModel);
            packet.writeInt16(headTxt);

        packet.send(RELIABLE);  
    }

    function reverseTransform()
    {
        if(getPlayerInstance(heroId) != "PC_HERO") Packet(PacketIDs.Player, Player_PacketIDs.Transformation).send(RELIABLE);  
    }

    // Player.Hero
    function isLogged() { return _logged; }

    function setOnlineTime(onlineTime)
    {
        _onlineTime = onlineTime;
    }

    function getOnlineTime() { return _onlineTime; }

    function setPermission(permission)
    {
        callEvent("Player.onHeroChangePermission", _permission, _permission = permission);
    }

    function getPersmission() { return _permission; }

    function setFlyMode(flyMode)
    {
        _flyMode = flyMode;
    }

    function isInFlyMode() { return _flyMode; }

    function setLevel(level)
    {
        if(_level != level)
        {   
            callEvent("Player.onHeroChangeLevel", _level, _level = level);
        }
    }

    function getLevel() { return _level; }

    function setLearnPoints(learnPoints)
    {
        _learnPoints = learnPoints;
    }

    function getLearnPoints() { return _learnPoints; }
    
    function setExperience(experience)
    {
        if(_experience != experience)
        {   
            callEvent("Player.onHeroChangeExperience", _experience, _experience = experience);
        }
    }

    function getExperience() { return _experience; }

    function setExperienceNextLevel(experienceNextLevel)
    {
        if(_experienceNextLevel != experienceNextLevel)
        {   
            callEvent("Player.onHeroChangeExperienceNextLevel", _experienceNextLevel, _experienceNextLevel = experienceNextLevel);
                setNextLevelExp(experienceNextLevel);
        }
    }

    function getExperienceNextLevel() { return _experienceNextLevel; }

    function reset(packet)
    {
        _level = 0;
        _learnPoints = packet.readInt16();

        _experience = 0;
        _experienceNextLevel = packet.readInt32();

        base.reset();

        callEvent("Player.onPlayerRest", _playerId);
    }
    
    // Player.Equipment
    function getCurrentWeapon() { return _eqModule.getCurrentWeapon(); }

    function getProtection(damageType)
    {
        return getPlayerInstance(heroId) == "PC_HERO" ? _eqModule.getProtection(damageType) : 0;
    }
}
