
class Player.Manager 
{    
    static _playerList = {};

//:protected
    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt16();

        for(local i = 0; i < packetSize; i++)
        {            
            local id = packet.readInt16(),
                player = _playerList[id];

            player.setGuild(packet.readString());

            player.setKills(packet.readInt16());
            player.setDeaths(packet.readInt16());
            player.setAssists(packet.readInt16());
        }
    }

    static function server_login(packet)
    {
        getHero().server_login(packet.readInt8());
    }

    static function server_register(packet)
    {
        getHero().server_register(packet.readInt8());
    }
    
    static function server_permission(packet)
    {
        getHero().setPermission(packet.readInt8());
    }
                 
    static function server_guild(packet)
    {
        local player = getPlayer(packet.readInt16());
        if(player != null) player.setGuild(packet.readString());  
    }

    static function server_level(packet)
    {
        getHero().setLevel(packet.readInt16());
    }

    static function server_learnPoints(packet)
    {
        getHero().setLearnPoints(packet.readInt16());
    }

    static function server_experience(packet)
    {
        getHero().setExperience(packet.readInt32());
    }

    static function server_experienceNextLevel(packet)
    {
        getHero().setExperienceNextLevel(packet.readInt32());
    }

    static function server_kills(packet)
    {
        local player = getPlayer(packet.readInt16());
        if(player != null) player.setKills(packet.readInt16());  
    }

    static function server_deaths(packet)
    {
        local player = getPlayer(packet.readInt16());
        if(player != null) player.setDeaths(packet.readInt16());  
    }

    static function server_assists(packet)
    {
        local player = getPlayer(packet.readInt16());
        if(player != null) player.setAssists(packet.readInt16());  
    }

    static function server_reset(packet)
    {
        local playerId = packet.readInt16();

        if(playerId == heroId)
        {
            getHero().reset(packet);
        }
        else 
        {
            local player = getPlayer(playerId);
            if(player != null) player.reset();  
        }
    }   

//:public
    static function playerCreate(playerId)
    {
        if(playerId == heroId)
        {
            _playerList[playerId] <- Player.Hero(playerId);
        } 
        else 
            _playerList[playerId] <- Player.Player(playerId);
    }

    static function playerDestroy(playerId)
    {
        delete _playerList[playerId];
    }

    static function heroRespawn()
    {
        _playerList[heroId].respawn();
    }

    static function getPlayer(playerId)
    {
        return playerId in _playerList ? _playerList[playerId] : null;
    }

    static function getHero() { return heroId in _playerList ? _playerList[heroId] : null; }

    static function getPlayers() { return _playerList; }

    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Player_PacketIDs.List: server_packetToList(packet); break;	
            case Player_PacketIDs.Login: server_login(packet); break;	
            case Player_PacketIDs.Register: server_register(packet); break;	
            case Player_PacketIDs.Permission: server_permission(packet); break;	
            case Player_PacketIDs.Guild: server_guild(packet); break;	
            case Player_PacketIDs.Level: server_level(packet); break;	
            case Player_PacketIDs.LearnPoints: server_learnPoints(packet); break;	
            case Player_PacketIDs.Experience: server_experience(packet); break;	
            case Player_PacketIDs.ExperienceNextLevel: server_experienceNextLevel(packet); break;	
            case Player_PacketIDs.Kills: server_kills(packet); break;	
            case Player_PacketIDs.Deaths: server_deaths(packet); break;	
            case Player_PacketIDs.Assists: server_assists(packet); break;	
            case Player_PacketIDs.Reset: server_reset(packet); break;	
        }
    }
}

getPlayerManager <- @() Player.Manager;

getPlayer <- @(playerId) Player.Manager.getPlayer(playerId);
getPlayers <- @() Player.Manager.getPlayers();

// Player
function getPlayerGuild(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getGuild();

    return null; 
}

function getPlayerKills(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getKills();

    return 0;
}

function getPlayerDeaths(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getDeaths();

    return 0;
}

function getPlayerAssists(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getAssists();

    return 0;
}

// Hero
isLogged <- @() Player.Manager.getHero().isLogged();

setFlyMode <- @(flyMode) Player.Manager.getHero().setFlyMode(flyMode);
isInFlyMode <- @() Player.Manager.getHero().isInFlyMode();

getHeroPermission <- @() getHero().getPersmission();
getHeroProtection <- @(damageType) getHero().getProtection(damageType);

getHero <- @() Player.Manager.getHero();
