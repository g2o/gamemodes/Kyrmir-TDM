
class Guild.Instance 
{
    _id = null;

    _color = null;

    _pointCounter = 0;
    _memberCounter = 0;
    _locked = false;

    constructor(id, color_r, color_g, color_b)
    {
        _id = id;

        _color = { r = color_r, g = color_g, b = color_b };

        _pointCounter = 0;
        _memberCounter = 0;  
        _locked = false;
    }

//:public
    function getId() { return _id; }

    function getColor() { return _color; }

    function loadData(points, members, locked)
    {
        _pointCounter = points;        
        _memberCounter = members;  
        _locked = locked;
    }

    function setPoints(points)
    {
        if(points != _pointCounter)
        {
            callEvent("Guild.onPointsUpdate", _id, _pointCounter = points);
        }
    }

    function getPoints() { return _pointCounter; }

    function setMembers(members)
    {
        if(members != _memberCounter)
        {
            callEvent("Guild.onMembersUpdate", _id, _memberCounter = members);
        }     
    }

    function getMembers() { return _memberCounter; }

    function setLock(locked)
    {
        _locked = locked;
    }

    function isLocked() { return _locked; }

    function reset()
    {
        _pointCounter = 0;
        _memberCounter = 0;    

        callEvent("Guild.onReset", _id);
    }
}