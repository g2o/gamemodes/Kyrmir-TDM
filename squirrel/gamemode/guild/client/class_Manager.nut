
class Guild.Manager
{
    static _guildList = {};

//:protected
    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt8();

        for(local i = 0; i < packetSize; i++)
        {            
            local id = packet.readString();

            _guildList[id] <- Guild.Instance(id, packet.readInt16(), packet.readInt16(), packet.readInt16());
            
                local guild = _guildList[id];
            guild.loadData(packet.readInt32(), packet.readInt16(), packet.readBool());
        }

        callEvent("Guild.onInitialData");
    }
 
//:public
    static function server_points(packet)
    {       
        local guildId = packet.readString();
        if(guildId in _guildList) _guildList[guildId].setPoints(packet.readInt32());
    }

    static function server_updateMembers(packet)
    {      
        local guildId = packet.readString();
        if(guildId in _guildList) _guildList[guildId].setMembers(packet.readInt8());  
    }

    static function server_lock(packet)
    {      
        local guildId = packet.readString();
        if(guildId in _guildList) _guildList[guildId].setLock(packet.readBool());  
    }

    static function server_reset(packet)
    {
        local guildId = packet.readString();
        if(guildId in _guildList) _guildList[guildId].reset();
    }

    static function changeGuild(guildId)
    {
        local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.Change);
            packet.writeString(guildId);
        packet.send(RELIABLE);
    }

    static function getGuildById(guildId) { return guildId in _guildList ? _guildList[guildId] : null; }

    static function getGuilds() 
    { 
        return _guildList; 
    }

    static function packetRead(packet)
    {
        local packetId = packet.readInt8();
        
        switch(packetId)
        {
            case Guild_PacketIDs.List: server_packetToList(packet); break;
            case Guild_PacketIDs.Points: server_points(packet); break;
            case Guild_PacketIDs.Members: server_updateMembers(packet); break;
            case Guild_PacketIDs.Lock: server_lock(packet); break;
            case Guild_PacketIDs.Reset: server_reset(packet); break;
        }
    } 
}

getGuildManager <- @() Guild.Manager;

// Guild
getGuildById <- @(guildId) Guild.Manager.getGuildById(guildId);
getGuilds <- @() Guild.Manager.getGuilds();
