
class Guild.Manager
{
    static _guildList = {};

//:protected
    static function _listToPacket()
    {
        local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.List);

            packet.writeInt8(_guildList.len());

        foreach(guild in _guildList)
        {
            packet.writeString(guild.getId());
            
            local color = guild.getColor();

            packet.writeInt16(color.r);
            packet.writeInt16(color.g);
            packet.writeInt16(color.b);

            packet.writeInt32(guild.getPoints());
            packet.writeInt16(guild.getMembers());
            packet.writeBool(guild.isLocked());
        }

        return packet;
    }

//:public
    static function swapArmors(playerId, oldGuild, newGuild)
    {
        local armorList = getEq(playerId).filter(function(index, value) {
            return value.getTemplate().getType() == ItemType.Armor;
        });

        foreach(item in armorList)
        {
            local template = item.getTemplate();
            local itemId = template.getId();

            if(isItemEquipped(playerId, itemId))
            {
                unequipItem(playerId, itemId);
            }

            local amount = item.getAmount();

            removeItem(playerId, itemId, amount);
            giveItem(playerId, getItemTemplateIdByInstance(newGuild.getArmor(oldGuild.checkItem(template.getInstance()))), amount);
        }
    }

    static function client_change(playerId, packet)
    {        
        local newGuild = getGuildById(packet.readString());
        if(newGuild == null) return;

        local playerGuild = getGuildById(getPlayerGuild(playerId));
            
        if(playerGuild == null)
        {                
            local homeWorld = newGuild.getHomeWorld();

            if(homeWorld != getPlayerWorld(playerId))
            {
                setPlayerWorld(playerId, homeWorld);
            }

            local respawn = newGuild.getRespawnPosition();

            setPlayerPosition(playerId, respawn.x, respawn.y, respawn.z);
            setPlayerGuild(playerId, newGuild.getId());

            spawnPlayer(playerId);  
        }
        else 
        {
            if(newGuild == playerGuild)
                return;

            if(isRoundStarted() == false || getPlayerLevel(playerId) <= MAX_SWAP_LEVEL)
            {
                setPlayerHealth(playerId, 0); 

                if(isRoundStarted()) 
                    swapArmors(playerId, playerGuild, newGuild);
                
                setPlayerGuild(playerId, newGuild.getId());
            }
            else 
                print("client_change - " + getPlayerName(playerId) + " - failed attempt to change guild while being above swap level!");
        }
    }

    static function forceChange(playerId, guildId)
    {
        local oldGuild = getGuildById(getPlayerGuild(playerId)), newGuild = getGuildById(guildId);

        if(newGuild != null) 
        {
            setPlayerHealth(playerId, 0); 
            
            if(oldGuild != null && isRoundStarted()) 
                swapArmors(playerId, oldGuild, newGuild);

            setPlayerGuild(playerId, guildId);

            return true;
        }

        return false;
    }

    static function playerJoin(playerId)
    {
        _listToPacket().send(playerId, RELIABLE);
    }

    static function playerLogin(playerId)
    {
        local guildId = getPlayerGuild(playerId);

        if(guildId != null && guildId in _guildList) 
        {
            local guild = _guildList[guildId];   

            guild.setMembers(guild.getMembers() + 1);           

                local color = guild.getColor();
            setPlayerColor(playerId, color.r, color.g, color.b);

            spawnPlayer(playerId);
        }
    }

    static function playerPreDisconnect(playerId)
    {
        local guildId = getPlayerGuild(playerId);

        if(guildId != null && guildId in _guildList) 
        {              
                local guild = _guildList[guildId];
            guild.setMembers(guild.getMembers() - 1);
        }      
    }

    static function playerEnterArea(playerId, instance)
    {
        local guildId = getPlayerGuild(playerId);
        if(guildId == null) return;
        
        if(guildId != instance) 
        {
            if(isPlayerImmortal(playerId) == false) 
            {
                setPlayerHealth(playerId, 0);
            }
        }
        else 
            playerAddEffect(playerId, "HOME_GUARD_SPEED_EFFECT", false);
    }

    static function playerLeaveArea(playerId, instance)
    {
        local guildId = getPlayerGuild(playerId);
        if(guildId == null) return;
            
        if(guildId == instance) 
        {
            playerAddEffect(playerId, guildId + "_SPEED_EFFECT", false);
        }

        playerRemoveEffect(playerId, "HOME_GUARD_SPEED_EFFECT");
    }

    static function playerRespawn(playerId)
    {
        local guildId = getPlayerGuild(playerId);

        if(guildId != null && guildId in _guildList) 
        {
            local playerWorld = getPlayerWorld(playerId);

            if(playerWorld == _guildList[guildId].getHomeWorld())
            {
                    local position = _guildList[guildId].getRespawnPosition();
                setPlayerPosition(playerId, position.x, position.y, position.z);
            }
            else 
            {
                    local defaultPosition = DEFAULT_POSITION(playerWorld);
                setPlayerPosition(playerId, defaultPosition.x, defaultPosition.y, defaultPosition.z);
            }

            spawnPlayer(playerId);
        }
    }

    static function playerHitPlayer(playerId, killerId)
    {
        return getPlayerGuild(playerId) != getPlayerGuild(killerId);
    }

    static function playerDead(playerId, killerId, hitList)
    {
        if(isRoundStarted() == false || getPlayerLevel(playerId) < MAX_SWAP_LEVEL)
            return;

        if(hitList.len() == 0) return;

        if(killerId == -1 || killerId >= getMaxSlots())
        {
            local minValue = 0;
            killerId = -1; 

            foreach(lastId, lastHitTime in hitList)
            {
                if(killerId == -1)
                {
                    minValue = lastHitTime;
                    killerId = lastId;       
                }
                else 
                {
                    if(lastHitTime < minValue)
                    {
                        minValue = lastHitTime;
                        killerId = lastId;
                    }
                }
            }
        }

        local killer_guildId = getPlayerGuild(killerId);

        if(getPlayerGuild(playerId) != killer_guildId)
        {
            local killer_guild = _guildList[killer_guildId]; 
            killer_guild.setPoints(killer_guild.getPoints() + 1);
        }
    }

    static function playerChangeGuild(playerId, oldGuild, newGuild)
    {
        if(oldGuild != null && oldGuild in _guildList)
        {
                local guild = _guildList[oldGuild];   
            guild.setMembers(guild.getMembers() - 1);
        }

        if(newGuild != null)
        {
            if(newGuild in _guildList)
            {
                    local guild = _guildList[newGuild];   
                guild.setMembers(guild.getMembers() + 1);           

                    local color = guild.getColor();
                setPlayerColor(playerId, color.r, color.g, color.b);
            }
            /*else 
            {
            
            }*/
        }
    }

    static function addGuild(guild)
    {
        _guildList[guild.getId()] <- guild;
    } 

    static function getGuildById(guildId) { return guildId in _guildList ? _guildList[guildId] : null; }

    static function getGuilds() { return _guildList; }

    static function packetRead(playerId, packet)
    {
        local packetId = packet.readInt8();
        
        switch(packetId)
        {
            case Guild_PacketIDs.Change: client_change(playerId, packet); break;
        }
    } 
}

getGuildManager <- @() Guild.Manager;

// Guild
registerGuild <- @(guild) Guild.Manager.addGuild(guild);

forceChangeGuild <- @(playerId, guildId) Guild.Manager.forceChange(playerId, guildId);
getGuildById <- @(guildId) Guild.Manager.getGuildById(guildId);
getGuilds <- @() Guild.Manager.getGuilds();