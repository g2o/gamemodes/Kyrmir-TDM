
// Register packet callback
PacketReceive.add(PacketIDs.Guild, getGuildManager());

addEventHandler("onPlayerJoin", Guild.Manager.playerJoin.bindenv(Guild.Manager));

addEventHandler("Player.onPlayerPreDisconnect", Guild.Manager.playerPreDisconnect.bindenv(Guild.Manager));

addEventHandler("Player.onPlayerLogin", function(playerId, logged)
{
    if(logged) getGuildManager().playerLogin(playerId);
});

addEventHandler("Player.onPlayerChangeGuild", Guild.Manager.playerChangeGuild.bindenv(Guild.Manager));

addEventHandler("onPlayerRespawn", Guild.Manager.playerRespawn.bindenv(Guild.Manager));

addEventHandler("Damage.onPlayerHitPlayer", function(playerId, killerId)
{
    if(getGuildManager().playerHitPlayer(playerId, killerId) == false) cancelEvent();
});

addEventHandler("Damage.onPlayerDead", Guild.Manager.playerDead.bindenv(Guild.Manager));

addEventHandler("Area.onPlayerEnterArea", Guild.Manager.playerEnterArea.bindenv(Guild.Manager));

addEventHandler("Area.onPlayerLeaveArea", Guild.Manager.playerLeaveArea.bindenv(Guild.Manager));
