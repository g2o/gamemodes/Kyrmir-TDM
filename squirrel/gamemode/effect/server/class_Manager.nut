
class Effect.Manager
{
    static _templateList = {};

//:public
    static function addTemplate(template)
    {
        local templateId = template.getId();

        if(templateId in _templateList == false)
        {
            _templateList[templateId] <- template;
        }
    }

    static function getTemplateById(templateId)
    {
        return templateId in _templateList ? _templateList[templateId] : null;
    }

    static function getTemplates() { return _templateList; }

    static function playerEquipHandItem(playerId, itemId)
    {
        getPlayer(playerId).getEffectModule().playerEquipHandItem(itemId);   
    }

    static function playerDead(playerId)
    {
        getPlayer(playerId).getEffectModule().playerDead();   
    }

    static function checkEffect()
    {
        local players = getPlayers(), tickCount = getTickCount();

        foreach(playerId, player in players)
		{	    
            if(player.isLogged() == false) continue;
                
            local effectModule = player.getEffectModule();
            local overlayList = effectModule.getOverlays(), effectList = effectModule.getEffects();

            foreach(index, overlay in overlayList)
            {
                if(overlay.applyFaulty)
                {
                    overlay.applyFaulty = !_applyPlayerOverlay(playerId, overlay.id);
                }
                else if(overlay.removeFaulty) 
                {
                    overlay.removeFaulty = !_removePlayerOverlay(playerId, overlay.id);

                    if(overlay.removeFaulty == false)
                    {
                        overlayList.remove(index);
                        break;
                    }
                }
            }

            foreach(effect in effectList)
            {
                local effectTemplate = effect.getTemplate(), effectTemplateTime = effectTemplate.getTime();
                
                if(effectTemplateTime != -1)
                {
                    if(effect.getEndTime() == -1)
                    {
                        if(effectModule.isLocked() == false)
                        {
                            effect.setEndTime(tickCount + effectTemplateTime);
                                effectTemplate.callBeginingFunc(playerId);
                        }
                    }
                    else 
                    {
                        if(effect.getEndTime() >= tickCount)
                        {
                            if(effectTemplate.isRepetitive())
                            {
                                local nextRepeat = effect.getNextRepeatTime();

                                if(nextRepeat == -1 || nextRepeat <= tickCount)
                                {
                                    effect.setNextRepeatTime(tickCount + effectTemplate.getRepeatTime());                
                                        effectTemplate.callRepeatFunc(playerId);                     
                                }
                            }
                        }
                        else 
                        {
                            effectModule.removeEffect(effectTemplate.getId());
                                effectTemplate.callFinishFunc(playerId);
                            
                            break;
                        }
                    }
                }
                else 
                {
                    if(effectTemplate.isRepetitive())
                    {
                        local nextRepeat = effect.getNextRepeatTime();

                        if(nextRepeat == -1 || nextRepeat <= tickCount)
                        {
                            effect.setNextRepeatTime(tickCount + effectTemplate.getRepeatTime());                
                                effectTemplate.callRepeatFunc(playerId);                     
                        }
                    }          
                }
            }
        }
    }
}
    
getEffectManager <- @() Effect.Manager;

// Effect
addEffectTemplate <- @(template) Effect.Manager.addTemplate(template);

getEffectTemplateById <- @(effectId) Effect.Manager.getTemplateById(effectId);
getEffectTemplates <- @() Effect.Manager.getTemplates();

function playerAddEffect(playerId, effectId, potion = true)
{
    local player = getPlayer(playerId);
    if(player) return player.getEffectModule().addEffect(effectId, potion);
}

function playerRemoveEffect(playerId, effectId)
{
    local player = getPlayer(playerId);
    if(player) return player.getEffectModule().removeEffect(effectId);
}