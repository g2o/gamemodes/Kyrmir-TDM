
class Effect.Player 
{
    _playerId = -1;
    
    _effectList = null;
    _overlayList = null;

    _lockState = true;

    constructor(playerId)
    {
        _playerId = playerId;

        _effectList = [];
        _overlayList = [];
        
        _lockState = true;
    }

//:public
    function playerEquipHandItem(itemId)
    {
        _lockState = itemId != -1;
    }

    function isLocked() { return _lockState; }
    
    function playerDead()
    {
        _overlayList.clear();
        
        foreach(index, effect in _effectList)
        {
            effect.getTemplate().callFinishFunc(_playerId);
        }

        _effectList.clear();
    }
    
    function addEffect(effectId, potion = true)
    {
        local template = getEffectTemplateById(effectId);

        if(template != null)
        {   
            _lockState = potion;

            foreach(index, effect in _effectList)
            {
                local effectTemplate = effect.getTemplate();

                if(effectTemplate == template)
                {
                    effectTemplate.callFinishFunc(_playerId);
                        _effectList.remove(index);
                        
                    break;
                }       
            }

            local effect = Effect.Instance(template);

            if(template.getTime() == -1) 
            {
                template.callBeginingFunc(_playerId);
            }
            else if(_lockState == false)
            {
                    template.callBeginingFunc(_playerId);
                effect.setEndTime(getTickCount() + template.getTime());
            }

            _effectList.push(effect);
        }
    }

    function removeEffect(effectId)
    {           
        local template = getEffectTemplateById(effectId);
 
        if(template != null)
        {
            foreach(index, effect in _effectList)
            {
                if(effect.getTemplate() == template)
                {
                    _effectList.remove(index);
                    break;
                }       
            }

            if(template.getTime() == -1) template.callFinishFunc(_playerId);
        }
    }

    function clearEffects()
    {
        _effectList.clear();
    }

    function getEffects() { return _effectList; }

    function applyOverlay(overlayId)
    {
        local find = null;

        foreach(index, overlay in _overlayList)
        {
            if(overlay.id == overlayId)
            {
                find = index; 
                break;
            }
        }

        if(find != null)
        {
            _overlayList[find].amount += 1;
           
            _overlayList[find].applyFaulty = !_applyPlayerOverlay(_playerId, overlayId);
            _overlayList[find].removeFaulty = false;
        }
        else 
        {
            _overlayList.push( { id = overlayId, amount = 1, applyFaulty = !_applyPlayerOverlay(_playerId, overlayId), removeFaulty = false } );  
        }
    }

    function removeOverlay(overlayId)
    {
        foreach(index, overlay in _overlayList)
        {
            if(overlay.id == overlayId)
            {
                overlay.amount -= 1;

                if(overlay.amount <= 0)
                {
                    if(_removePlayerOverlay(_playerId, overlayId))
                    {
                        _overlayList.remove(index);
                    }
                    else overlay.removeFaulty = true;
                }

                break;
            }
        } 
    }

    function getOverlays() { return _overlayList; }
}