
class Effect.Instance
{
    _template = null;

    _endTime = -1;
    _nextRepeatTime = -1;

    constructor(template)
    {
        _template = template;

        _endTime = -1;
        _nextRepeatTime = -1;
    }

    function getTemplate() { return _template; }

    function setEndTime(endTime) 
    {
        _endTime = endTime;
    }

    function getEndTime() { return _endTime; }

    function setNextRepeatTime(nextRepeatTime) 
    {
        _nextRepeatTime = nextRepeatTime;
    }

    function getNextRepeatTime() { return _nextRepeatTime; }
}