
addEventHandler("onPlayerJoin", InfoBox.playerJoin.bindenv(InfoBox));

addEventHandler("Bot.onMonsterDead", function(bot, killerId, hitList) 
{
    getInfoBox().monsterDead(bot, killerId);
});

addEventHandler("Bot.onMonsterRespawn", InfoBox.monsterRespawn.bindenv(InfoBox));

addEventHandler("Damage.onPlayerDead", function(playerId, killerId, hitList) 
{ 
    local realKillerId = getPlayerKillerId(playerId);

    if(realKillerId == -1 || getPlayerLevel(playerId) < MAX_SWAP_LEVEL)
        return;
        
    getInfoBox().kill(playerId, realKillerId, getPlayerHeroStreak(realKillerId));
});

addEventHandler("Player.onPlayerChangeLevel", function(playerId, oldLevel, newLevel) 
{ 
    if(isPlayerLogged(playerId) && newLevel == MAX_SWAP_LEVEL)
    {
        getInfoBox().thirdLevel(playerId);
    }
});