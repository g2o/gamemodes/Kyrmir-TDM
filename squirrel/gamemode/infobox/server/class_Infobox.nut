
class InfoBox 
{
    static _bossList = [];

//:protected 
    static function _listToPacket()
    {
        local packet = Packet(PacketIDs.InfoBox, InfoBox_PacketIDs.BossList);

        packet.writeInt8(_bossList.len());

        foreach(bot in _bossList)
        {
            packet.writeInt16(bot.getId());
            packet.writeString(bot.getWorld());
            
            local respawnTime = bot.getRespawnTime();

            if(respawnTime != -1)
            {
                packet.writeInt32(respawnTime - getTickCount());
            }
            else 
                packet.writeInt32(bot.isAlive() ? -1 : 0);

            local position = bot.getRespawnPosition();
            
            packet.writeInt32(position.x);
            packet.writeInt32(position.z);
        }

        return packet;
    }
    
//:public 
    static function playerJoin(playerId)
    {   
        _listToPacket().send(playerId, RELIABLE);
    }

    static function monsterDead(bot, killerId)
    {
        if(_bossList.find(bot) == null) return;
        
        local packet = Packet(PacketIDs.InfoBox, InfoBox_PacketIDs.BossDead);

            packet.writeInt16(bot.getId());
            packet.writeInt8(killerId);    
            packet.writeInt32(bot.getRespawnTime() - getTickCount());

        packet.sendToAll(RELIABLE);   
    }

    static function monsterRespawn(bot)
    {
        if(_bossList.find(bot) == null) return;
        
        local packet = Packet(PacketIDs.InfoBox, InfoBox_PacketIDs.BossRespawn);
            packet.writeInt16(bot.getId());
        packet.sendToAll(RELIABLE);   
    }

    static function kill(playerId, killerId, streak)
    {
        local packet = Packet(PacketIDs.InfoBox, InfoBox_PacketIDs.Kill);

            packet.writeInt8(playerId);    
            packet.writeInt8(killerId);    
            packet.writeInt8(streak);    

        packet.sendToAll(UNRELIABLE);       
    }

    static function thirdLevel(playerId)
    {
        local packet = Packet(PacketIDs.InfoBox, InfoBox_PacketIDs.ThirdLevel);
            packet.writeInt8(playerId);    
        packet.sendToAll(UNRELIABLE);       
    }   

    static function addBoss(bot)
    {
        _bossList.push(bot);
    }
}

getInfoBox <- @() InfoBox;

// InfoBox
registerBoss <- @(bot) InfoBox.addBoss(bot);
