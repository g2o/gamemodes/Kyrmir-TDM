
class InfoBox 
{
    static _bossList = {};

//:protected 
    static function server_kill(packet)
    {
        local playerId = packet.readInt8(), killerId = packet.readInt8(), streak = packet.readInt8();

        callEvent("InfoBox.onKill", playerId, killerId, streak);
    }

    static function server_thirdLevel(packet)
    {
            local playerId = packet.readInt8();
        callEvent("InfoBox.onThirdLevel", playerId);
    }

    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt8();

        for(local i = 0; i < packetSize; i++)
        {            
            local bossId = packet.readInt16();
            local world = packet.readString();
            local respawnTime = packet.readInt32();

            local x = packet.readInt32(), z = packet.readInt32();
            
            if(respawnTime != -1 && respawnTime != 0) 
            {
                respawnTime = getTickCount() + respawnTime;
            }

            _bossList[bossId] <- { world = world, respawn_x = x, respawn_z = z, status = respawnTime };
        }
    }

    static function server_bossDead(packet)
    {
        local bossId = packet.readInt16(), killerId = packet.readInt8(),
            respawnTime = packet.readInt32();

        if(bossId in _bossList)
        {
            _bossList[bossId].status = getTickCount() + respawnTime;
            callEvent("InfoBox.onBossDead", bossId, killerId);
        }
    }

    static function server_bossRespawn(packet)
    {
        local bossId = packet.readInt16();

        if(bossId in _bossList)
        {
            _bossList[bossId].status = -1;
            callEvent("InfoBox.onBossRespawn", bossId);
        }
    }

//:public
    static function getBossList() { return _bossList; }

    static function packetRead(packet)
    {
        local packetId = packet.readInt8();
        
        switch(packetId)
        {
            case InfoBox_PacketIDs.Kill: server_kill(packet); break;
            case InfoBox_PacketIDs.ThirdLevel: server_thirdLevel(packet); break;
            case InfoBox_PacketIDs.BossList: server_packetToList(packet); break;
            case InfoBox_PacketIDs.BossDead: server_bossDead(packet); break;
            case InfoBox_PacketIDs.BossRespawn: server_bossRespawn(packet); break;
        }
    } 
}

getInfoBox <- @() InfoBox;

// InfoBox
getBossData <- @() InfoBox.getBossList();
