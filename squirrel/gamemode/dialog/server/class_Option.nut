
class Dialog.Option
{
    _instance = null;
    _typeId = DialogType.None;

    _requirementFunc = null;
    _effectFunc = null;

    constructor(instance)
    {
        _instance = instance;

        _requirementFunc = null;
        _effectFunc = null;
    }

//:public
    function getInstance() { return _instance; }

    function getType() { return _typeId; }

    function bindRequirementFunc(func)
    {
        _requirementFunc = func;
            return this;
    }

    function bindEffectFunc(func)
    {
        _effectFunc = func;
            return this;
    }

    function checkRequirement(playerId)
    {
        if(_requirementFunc != null)
            return _requirementFunc(playerId);

        return true;
    }

    function startEffect(playerId)
    {
        if(_effectFunc != null)
        {
            if(_requirementFunc != null)
            {
                if(_requirementFunc(playerId))
                    _effectFunc(playerId);
            }
            else 
                _effectFunc(playerId);

            callEvent("Dialog.onPlayerDialogEffect", playerId);
        }
    }    
}

class Dialog.EndOption extends Dialog.Option
{
    constructor(instance)
    {
        base.constructor(instance);
            _typeId = DialogType.End;
    }
}

class Dialog.ListOption extends Dialog.Option
{
    _list = null;

    constructor(instance, list)
    {
            _list = list;
        base.constructor(instance);

        _typeId = DialogType.List;
    }

//:public
    function getList() { return _list; }
}

class Dialog.TradeOption extends Dialog.Option
{
    _itemList = null;
    _currency = null;

    constructor(instance, currencyInstance)
    {
        _currency = getItemTemplateIdByInstance(currencyInstance);

        if(_currency != -1)
        {
                _itemList = [];   
            base.constructor(instance);

            _typeId = DialogType.Buy;
        }
        else 
            print("Dialog.TradeOption(constructor) - Non existing currency " + instance);
    }

//:public 
    function addItem(instance, price)
    {
        local itemId = getItemTemplateIdByInstance(instance);

        if(itemId != -1)
            _itemList.push({ itemId = itemId, price = price });
        else 
            print("Dialog.TradeOption(addItem) - Non existing item " + instance);

        return this;
    }

    function checkItem(playerId, itemId)
    {
        foreach(item in _itemList)
        {
            if(itemId == item.itemId)
            {
                return item;
            }
        }

        return null;
    }

    function getItems() { return _itemList; }

    function getCurrency() { return _currency; }
}