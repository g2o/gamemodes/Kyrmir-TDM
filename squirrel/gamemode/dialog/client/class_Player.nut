
class Dialog.Player 
{
    _partnerId = -1;
    _currentData = null;

    constructor()
    {
        _partnerId = -1;
        _currentData = null;
    }

//:public
    function server_beginConversation(packet)
    {
        local instance = packet.readString(), typeId = packet.readInt8();

        switch(typeId)
        {          
            case DialogType.None: 
                _currentData = null;
            break;
            case DialogType.Buy: 
            {
                _currentData = {};

                _currentData.currencyId <- packet.readInt16();
                _currentData.list <- [];

                local packetSize = packet.readInt8();

                for(local i = 0; i < packetSize; i++)
                {
                    _currentData.list.push({ itemId = packet.readInt16(), price = packet.readInt32() });
                }
            } 
            break; 
            case DialogType.List: 
            {
                local packetSize = packet.readInt8();
                    _currentData = [];

                for(local i = 0; i < packetSize; i++)
                {
                    _currentData.push({ typeId = packet.readInt8(), instance = packet.readString() });
                }
            } 
            break;  
            case DialogType.End: 
                _currentData = null;
            break;                  
        }                 
        
        callEvent("Dialog.onInitialData", instance, typeId, _currentData);
    }

    function server_selectOption(packet)
    {
        local typeId = packet.readInt8();
        
        switch(typeId)
        {          
            case DialogType.None: 
            {
                if(packet.readBool())
                {
                    local packetSize = packet.readInt8();
                        _currentData = [];

                    for(local i = 0; i < packetSize; i++)
                    {
                        _currentData.push({ typeId = packet.readInt8(), instance = packet.readString() });
                    }
                }
                else 
                    _currentData = null;
            } 
            break;
            case DialogType.Buy: 
            {
                _currentData = {};

                _currentData.currencyId <- packet.readInt16();
                _currentData.list <- [];

                local packetSize = packet.readInt8();

                for(local i = 0; i < packetSize; i++)
                {
                    _currentData.list.push({ itemId = packet.readInt16(), price = packet.readInt32() });
                }
            } 
            break; 
            case DialogType.List: 
            {
                local packetSize = packet.readInt8();
                    _currentData = [];

                for(local i = 0; i < packetSize; i++)
                {
                    _currentData.push({ typeId = packet.readInt8(), instance = packet.readString() });
                }
                
            } 
            break;  
        }

        callEvent("Dialog.onUpdateData", typeId, _currentData);
    }

    function beginConversation(npcId)
    {
        if(_partnerId != -1) return;
        
        local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.BeginConversation);
            packet.writeInt16(getNpcByLocalId(_partnerId = npcId).getId());
        packet.send(RELIABLE);
    }

    function finishConversation()
    {
        if(_partnerId == -1) return;

        Packet(PacketIDs.Dialog, Dialog_PacketIDs.FinishConversation).send(RELIABLE);
        
        _partnerId = -1; 
        _currentData = null;
    }
    
    function selectOption(instance)
    {
        if(_partnerId == -1) return;

        local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.SelectOption);
            packet.writeString(instance);
        packet.send(RELIABLE);
    }

    function buyItem(itemId, amount)
    {
        if(_partnerId == -1) return;

        local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.BuyItem);
        
            packet.writeInt16(itemId);
            packet.writeInt32(amount);

        packet.send(RELIABLE);
    }

    function getData() { return _currentData; }

    function getPartnerId() { return _partnerId; }
}
