
class Admin
{
    static _templateList = [];

    static _checkList = [ 
        [  
            [ "Multiplayer//modules//focusbox.dll", null ], // No module
            [ "Multiplayer//modules//focusbox.dll", "3564b7a47fc099a69ea567eae99adbc6" ], // NEWEST
        ],
        [  
            [ "system//Shw32.dll", "7afd134e7926e0b3e715844a0429ee22" ], // Steam
            [ "system//Shw32.dll", "becb4cb40468fc7b062365480ed6437b" ], // Vanilla PL
        ],
        [ 
            [ "system//Vdfs32g.dll", "7e73dd55d0580a4535b7b06ef33d72f9" ], // Steam
            [ "system//Vdfs32g.dll", "72a644c64f4a99f004c5bf18a5c03f09" ], // System pack 1.8
            [ "system//Vdfs32g.dll", "d8b9bee42f61ce6d4476e14ea3151c2f" ], // System pack 1.9
            [ "system//Vdfs32g.dll", "a6c182a15fb91484b4585471a1484ef5" ], // Vanilla PL
        ],
        [ 
            [ "Data//SystemPack.vdf", "a31c3315257860e41b219f8f6bb2aebc" ], // Steam
            [ "Data//SystemPack.vdf", "5b19e835d74ad90e312eb123437a7864" ], // System pack 1.8-GOTHIC-FIX-TEAM
            [ "Data//SystemPack.vdf", "bb655ea6106bfde2487f673ac943e50b" ], // System pack 1.8-PL
            [ "Data//SystemPack.vdf", "086ac33c444b2de05365aa15a8ecc4cb" ], // System pack 1.8-DE
            [ "Data//SystemPack.vdf", "8f0b1c3b62dca662b8513dd359a424f7" ], // System pack 1.8-EN
            [ "Data//SystemPack.vdf", "7eeafe38bfd247c15e64e797e18a03e5" ], // System pack 1.8-RU
            [ "Data//SystemPack.vdf", "a4754c1b67df543ff273c954735d96a4" ], // System pack 1.8-IT
            [ "Data//SystemPack.vdf", null ], // Vanilla
        ]
    ];

//:protected
    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt16();

        for(local i = 0; i < packetSize; i++)
        {            
            _templateList.push(packet.readString());
        }
    }

//:public
    static function init()
    {
        foreach(check in _checkList)
        {
            local alter = false;

            foreach(alternative in check)
            {
                if(fileMd5(alternative[0]) == alternative[1])
                {
                    alter = true;
                    break;
                }
            }

            if(alter == false)
            {
                Packet(PacketIDs.Admin, Admin_PacketIDs.Union).send(RELIABLE);
                break;
            }
        }
    }

    static function spawn(instance)
    {
        local packet = Packet(PacketIDs.Admin, Admin_PacketIDs.Spawn);
            packet.writeString(instance);
        packet.send(RELIABLE);
    }

    static function unspawn()
    {
        Packet(PacketIDs.Admin, Admin_PacketIDs.Unspawn).send(RELIABLE);
    }

    static function getTemplates() { return _templateList; }

    static function key(keyId)
    {
        if(isInFlyMode() && chatInputIsOpen() == false)
        {
            switch(keyId)
            {
                case KEY_K: 
                        local angle = getPlayerAngle(heroId) / 180 * PI, position = getPlayerPosition(heroId);
                    setPlayerPosition(heroId, position.x + sin(angle) * 450, position.y, position.z + cos(angle) * 450); 
                break;
                case KEY_F8:
                        local angle = getPlayerAngle(heroId) / 180 * PI, position = getPlayerPosition(heroId);
                    setPlayerPosition(heroId, position.x + sin(angle) * 200, position.y + 600, position.z + cos(angle) * 200); 
                break;
            }
        }
    }

    static function packetRead(packet)
    {
        server_packetToList(packet);
    } 
}

getAdmin <- @() Admin;

// Admin

getMonsterTemplates <- @() Admin.getTemplates(); 