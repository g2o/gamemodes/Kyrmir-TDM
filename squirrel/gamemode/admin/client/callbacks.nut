
// Register packet callback
PacketReceive.add(PacketIDs.Admin, Admin);

addEventHandler("onInit", Admin.init.bindenv(Admin));

addEventHandler("onKey", Admin.key.bindenv(Admin));