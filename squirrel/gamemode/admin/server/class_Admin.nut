
class Admin
{
//:protected
    static function _listToPacket()
    {
        local templateList = getMonsterTemplates();

        local packet = Packet(PacketIDs.Admin);
            packet.writeInt16(templateList.len());

        foreach(index, template in templateList)
        {
            packet.writeString(index);
        }

        return packet;
    }

    static function client_spawn(playerId, packet)
    {
        local identifier = packet.readString(), templateList = getMonsterTemplates();

        if(identifier in templateList)
        {
            local position = getPlayerPosition(playerId), 
                angle = getPlayerAngle(playerId) / 180 * PI;
            
            print(format(".addPoint(%d, %d)\n", position.x, position.z));

            spawnBot(templateList[identifier], position.x + sin(angle) * 500, position.y + 100, position.z + cos(angle) * 500, getPlayerWorld(playerId));
        }
    }

    static function client_unspawn(playerId)
    {
        local pPosition = getPlayerPosition(playerId),
            botList = getBots();

        foreach(bot in botList)
        {
            local bPosition = bot.getPosition();

            if(getDistance2d(pPosition.x, pPosition.z, bPosition.x, bPosition.z) <= 300)
            {
                bot.destroy();
            }    
        }
    }

    static function client_union(playerId)
    {
        print(format("Player %s(%d) is using incompatible scripts!", getPlayerName(playerId), playerId));     
            kick(playerId, "UNION OR INCOMPATIBILITY DETECTED, VISIT OUR DISCORD TO GET MORE INFORMATIONS!");
    }
    
//:public
    static function exit()
    {
        local templateList = getMonsterTemplates();

        foreach(index, template in templateList)
        {
            local fStream = file("file_db/spawner/" + index + ".nut", "w+");

            if(fStream)
            {
                local botList = getBots();

                foreach(bot in botList)
                {
                    if(bot.getTemplate() == template)
                    {
                            local position = bot.getPosition();
                                                
                        fStream.write("spawnBot("+ bot.getInstance() + ", " + position.x + ", " + position.y + ", " + position.z + ", '" + bot.getWorld() + "');");
                        fStream.write("\n");
                    }
                }

                fStream.close();
            }
        }
    }

    static function playerJoin(playerId)
    {
        _listToPacket().send(playerId, RELIABLE);
    }

    static function playerUseCheat(playerId, cheatType)
    {
        if(cheatType == AC_SPEED_HACK) 
        {
            print(format("Player %s(%d) is using AC_SPEED_HACK!", getPlayerName(playerId), playerId));
                kick(playerId, "AC_SPEED_HACK");
        }

        sendServerMessageToAll(255, 80, 0, format("%s got kicked by anty-cheat", getPlayerName(playerId)));
    }

    static function packetRead(playerId, packet)
    {
        local packetType = packet.readInt8();

        if(getPlayerPersmission(playerId) >= PermissionLevel.Administrator) 
        {
            switch(packetType)
            {
                case Admin_PacketIDs.Spawn: client_spawn(playerId, packet); break;
                case Admin_PacketIDs.Unspawn: client_unspawn(playerId); break;
                case Admin_PacketIDs.Union: client_union(playerId); break;
            }   
        }
        else 
        {
            switch(packetType)
            {
                case Admin_PacketIDs.Union: client_union(playerId); break;
            }  
        }
    } 
}

getAdmin <- @() Admin;