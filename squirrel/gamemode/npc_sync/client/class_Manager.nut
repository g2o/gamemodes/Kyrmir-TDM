
class Npc.Manager 
{
    static _syncList = {};

//:public
    static function npcList(packet)
    {        
        local packetSize = packet.readInt16();
        
        for(local i = 0; i < packetSize; i++)
        {
            local flags = packet.readInt8();
            local id = packet.readInt16();
            
            local name = packet.readString();
            local instance = packet.readString();
            
            _syncList[id] <- Npc.Instance(id, name, instance);

            local npc = _syncList[id];

            npc.setGroup(packet.readInt16());

            local health = packet.readInt32();

            npc.setMaxHealth(health);
            npc.setHealth(health);

            npc.setCollision(packet.readBool());

            npc.setSkillWeapon(WEAPON_1H, packet.readInt8());
            npc.setSkillWeapon(WEAPON_2H, packet.readInt8());
            npc.setSkillWeapon(WEAPON_BOW, packet.readInt8());
            npc.setSkillWeapon(WEAPON_CBOW, packet.readInt8());

            npc.setColor(packet.readInt8(), packet.readInt8(), packet.readInt8());
            
            if(instance == "PC_HERO")
            {
                local bodyModel = packet.readString(), bodyTxt = packet.readInt16(),
                    headModel = packet.readString(), headTxt = packet.readInt16();

                npc.setVisual(bodyModel, bodyTxt, headModel, headTxt);     
            }

            local armor = (flags & 1) == 1;
            local helmet = (flags & 2) == 2;
            local melee = (flags & 4) == 4;
            local ranged = (flags & 8) == 8;

            if(armor) npc.equipArmor(packet.readInt16());
            if(helmet) npc.equipHelmet(packet.readInt16());
            if(melee) npc.equipMeleeWeapon(packet.readInt16());
            if(ranged) npc.equipRangedWeapon(packet.readInt16());
        }	
    }

    static function createNpc(packet)
    {
        local flags = packet.readInt8();
        local id = packet.readInt16();
        
        local name = packet.readString();
        local instance = packet.readString();
        
        _syncList[id] <- Npc.Instance(id, name, instance);
        
        local npc = _syncList[id];

        npc.setGroup(packet.readInt16());

        local health = packet.readInt32();

        npc.setMaxHealth(health);
        npc.setHealth(health);

        npc.setCollision(packet.readBool());

        npc.setSkillWeapon(WEAPON_1H, packet.readInt8());
        npc.setSkillWeapon(WEAPON_2H, packet.readInt8());
        npc.setSkillWeapon(WEAPON_BOW, packet.readInt8());
        npc.setSkillWeapon(WEAPON_CBOW, packet.readInt8());

        npc.setColor(packet.readInt8(), packet.readInt8(), packet.readInt8());
        
        if(instance == "PC_HERO")
        {
            local bodyModel = packet.readString(), bodyTxt = packet.readInt16(),
                headModel = packet.readString(), headTxt = packet.readInt16();

            npc.setVisual(bodyModel, bodyTxt, headModel, headTxt);    
        }

        local armor = (flags & 1) == 1;
        local helmet = (flags & 2) == 2;
        local melee = (flags & 4) == 4;
        local ranged = (flags & 8) == 8;

        if(armor) npc.equipArmor(packet.readInt16());
        if(helmet) npc.equipHelmet(packet.readInt16());
        if(melee) npc.equipMeleeWeapon(packet.readInt16());
        if(ranged) npc.equipRangedWeapon(packet.readInt16());
    }

    static function destroyNpc(packet)
    {
        local npc = getNpcById(packet.readInt16());
        
        if(npc != null) 
        {
                npc.unspawn();
            delete _syncList[npc.getId()]
        }
    }

    static function streamer(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.turnStream(!npc.isStreaming());
    }

    static function addToStream(packet)
	{
        local npc = getNpcById(packet.readInt16());

        if(npc)
        {
            local flags = packet.readInt16();

            local animation = (flags & 1) == 1;
            local target = (flags & 2) == 2;
            local attackMode = (flags & 4) == 4;
            local follow = (flags & 8) == 8;
            
            npc.setHealth(packet.readInt32());
            npc.setWeaponMode(packet.readInt8());
            npc.setActiveSpell(packet.readInt16());
        
            npc.setAngle(packet.readInt16());
            npc.setPosition(packet.readFloat(), packet.readFloat(), packet.readFloat());
            
            if(animation) npc.playAni(packet.readString());
            if(target) npc.setTarget(packet.readInt16());
            
            if(attackMode)
            {
                local attackTime = packet.readInt16();

                switch(npc.getWeaponMode())
                {
                    case WEAPONMODE_FIST:
                        npc.attackFist(attackTime); 
                    break;
                    case WEAPONMODE_1HS: 
                    case WEAPONMODE_2HS:
                        npc.attackMelee(packet.readInt8(), attackTime);
                    break;
                    case WEAPONMODE_BOW:
                    case WEAPONMODE_CBOW:
                        npc.attackRanged(attackTime);
                    break;
                    case WEAPONMODE_MAG:
                        npc.attackMagic(attackTime);
                    break;
                }
            }		

            if(follow) npc.follow(packet.readInt16());

            npc.turnStream(packet.readBool());
            npc.spawn();
        }
	}

    static function removeFromStream(packet)
    {
        local npc = getNpcById(packet.readInt16());
        
        if(npc != null) 
        {
            npc.unspawn();
        }
    }

    static function name(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setName(packet.readString());
    }

    static function instance(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setInstance(packet.readString());
    }

    static function group(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setGroup(packet.readInt16());
    }

    static function color(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setColor(packet.readInt8(), packet.readInt8(), packet.readInt8());
    }

    static function visual(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setVisual(packet.readString(), packet.readInt16(), packet.readString(), packet.readInt16());      
    }

    static function health(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setHealth(packet.readInt32());   
    }

    static function maxHealth(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setMaxHealth(packet.readInt32());   
    }

    static function position(packet)
    {        
        local npc = getNpcById(packet.readInt16());
        if(npc != null) 
            npc.setPosition(packet.readFloat(), packet.readFloat(), packet.readFloat());   
    }

    static function server_positionUpdate(packet)
    {
        local npcNumber = packet.readInt16();

        for(local i = 0; i < npcNumber; i++)
        {
            local npc = getNpcById(packet.readInt16());
            if(npc) npc.setPosition(packet.readFloat(), packet.readFloat(), packet.readFloat());
        }
    }

    static function angle(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.setAngle(packet.readInt16());   
    }

    static function target(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.setTarget(packet.readInt16());  
    }

    static function attackFist(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.attackFist(packet.readInt16());  
    }

    static function attackMelee(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.attackMelee(packet.readInt8(), packet.readInt16());  
    }

    static function attackRanged(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.attackRanged(packet.readInt16());  
    }

    static function attackMagic(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.attackMagic(packet.readInt16());  
    }

    static function stopAttack(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.stopAttack();  
    }

    static function follow(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.follow(packet.readInt16());     
    }

    static function parade(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.parade(packet.readInt8());     
    }
    
    static function activeSpell(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.setActiveSpell(packet.readInt16());         
    }

    static function equipItem(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.equipItem(packet.readInt8(), packet.readInt16());  
    }

    static function unequipItem(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.unequipItem(packet.readInt16());  
    }

    static function playAni(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.playAni(packet.readString());  
    }

    static function stopAni(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.stopAni();  
    }

    static function weaponMode(packet)
    {
        local npc = getNpcById(packet.readInt16());
        if(npc != null) npc.setWeaponMode(packet.readInt8());   
    }

	static function getNpcById(id) { return id in _syncList ? _syncList[id] : null; }	

    static function getNpcByLocalId(localId)
    {
        foreach(npc in _syncList)
        {
            if(localId == npc.getLocalId())
                return npc;
        }

        return null;
    }

    static function hit(killerId, targetId, context = -1) 
    { 
        if(heroId == killerId)
        {
            if(targetId >= getMaxSlots())
            {
                local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.HIT_C);
 
                    packet.writeInt16(killerId);
                    packet.writeInt16(getNpcByLocalId(targetId).getId());
                    packet.writeInt16(context);

                packet.send(UNRELIABLE);
            }
        }
        else if(heroId == targetId)
        {
            if(killerId >= getMaxSlots())
            {
                local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.HIT_C);
        
                    packet.writeInt16(getNpcByLocalId(killerId).getId());
                    packet.writeInt16(targetId);          

                packet.send(UNRELIABLE);
            }
        }
    }

    static function positionUpdate()
    {
		local dataList = [];

		foreach(npc in _syncList)
        {
        	if(npc.isPositionChanged() == false) continue; 
            
            local localId = npc.getLocalId(), position = getPlayerPosition(localId);
            
            if(npc.isStreaming()) 
            {
                dataList.push( [ npc.getId(), position.x, position.y, position.z, getPlayerAngle(localId) ] );
            }
        }

        local dataSize = dataList.len();

		if(dataSize > 0)
		{
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.POSITION_C);
			    packet.writeInt16(dataSize);

            foreach(data in dataList)
            {
                packet.writeInt16(data[0]);

                packet.writeInt32(data[1]);
                packet.writeInt32(data[2]);
                packet.writeInt32(data[3]);
                
                packet.writeInt16(data[4]);
            }
			
            packet.send(UNRELIABLE_SEQUENCED);
		} 
    }

    static function statusUpdate()
    {
		local dataList = [];

		foreach(npc in _syncList)
        {
        	if(npc.getTarget() != heroId) continue;

            local flags = 0;

            if(npc.isFollowing()) flags = flags | 1;
            if(npc.isObstacle()) flags = flags | 2; 

            if(npc.getLastBitFlag() != flags)
            {
                dataList.push( [ npc.getId(), flags ] );
                    npc.setLastBitFlag(flags);
            }
        }

        local dataSize = dataList.len();

		if(dataSize > 0)
		{
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.FOLLOW);
			    packet.writeInt16(dataSize);   

            foreach(data in dataList)
            {
                packet.writeInt16(data[0]);
                packet.writeInt8(data[1]);
            }
			
			packet.send(UNRELIABLE_SEQUENCED);
        }
    }

	static function followTarget()
	{
		foreach(npc in _syncList)
		{
			npc.followTarget();
		}
	}	

    static function refreshNames()
    {
        foreach(npc in _syncList)
        {
            npc.refreshName();
        }
    }

    static function changeWorld(world)
    {
		foreach(npc in _syncList)
		{
            npc.unspawn();		
        }
    }

    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case LIST_PACKET_NPC_IDS.NPC_LIST: npcList(packet); break;	
            case LIST_PACKET_NPC_IDS.CREATE_NPC: createNpc(packet); break;	
            case LIST_PACKET_NPC_IDS.DESTROY_NPC: destroyNpc(packet); break;
            case LIST_PACKET_NPC_IDS.STREAMER: streamer(packet); break;
            case LIST_PACKET_NPC_IDS.ADD_TO_STREAM: addToStream(packet); break;
            case LIST_PACKET_NPC_IDS.REMOVE_FROM_STREAM: removeFromStream(packet); break;
            case LIST_PACKET_NPC_IDS.NAME: name(packet); break;
            case LIST_PACKET_NPC_IDS.INSTANCE: instance(packet); break;
            case LIST_PACKET_NPC_IDS.GROUP: group(packet); break;
            case LIST_PACKET_NPC_IDS.COLOR: color(packet); break;
            case LIST_PACKET_NPC_IDS.VISUAL: visual(packet); break;
            case LIST_PACKET_NPC_IDS.HEALTH: health(packet); break;
            case LIST_PACKET_NPC_IDS.MAX_HEALTH: maxHealth(packet); break;
            case LIST_PACKET_NPC_IDS.POSITION_S: position(packet); break;
            case LIST_PACKET_NPC_IDS.POSITION_C: server_positionUpdate(packet); break;
            case LIST_PACKET_NPC_IDS.ANGLE_S: angle(packet); break;
            case LIST_PACKET_NPC_IDS.TARGET: target(packet); break;
            case LIST_PACKET_NPC_IDS.ATTACK_FIST: attackFist(packet); break;
            case LIST_PACKET_NPC_IDS.ATTACK_MELEE: attackMelee(packet); break;
            case LIST_PACKET_NPC_IDS.ATTACK_RANGED: attackRanged(packet); break;
            case LIST_PACKET_NPC_IDS.ATTACK_MAGIC: attackMagic(packet); break;
            case LIST_PACKET_NPC_IDS.STOP_ATTACK: stopAttack(packet); break;
            case LIST_PACKET_NPC_IDS.FOLLOW: follow(packet); break;
            case LIST_PACKET_NPC_IDS.PARADE: parade(packet); break;
            case LIST_PACKET_NPC_IDS.ACTIVE_SPELL: activeSpell(packet); break;
            case LIST_PACKET_NPC_IDS.EQUIP_ITEM: equipItem(packet); break;
            case LIST_PACKET_NPC_IDS.UNEQUIP_ITEM: unequipItem(packet); break;
            case LIST_PACKET_NPC_IDS.PLAY_ANI: playAni(packet); break;
            case LIST_PACKET_NPC_IDS.STOP_ANI: stopAni(packet); break;
            case LIST_PACKET_NPC_IDS.WEAPON_MODE: weaponMode(packet); break;
        }
    }
}

getNpcManager <- @() Npc.Manager;

// Npc
getNpc <- @(globalId) Npc.Manager.getNpcById(globalId);
getNpcByLocalId <- @(localId) Npc.Manager.getNpcByLocalId(localId);

refreshNpcNames <- @() Npc.Manager.refreshNames();