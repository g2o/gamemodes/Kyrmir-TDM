/*
    x1, z1 = skrajny lewy dolny kraniec mapy
    x2, z2 = skrajny prawy g�rny kraniec mapy 

    cell_size =  wielkosc kom�rki synchronizacyjnej
    
    getNpcManager().addGrid(Npc.Grid(world, x1, z1, x2, z2, cell_size));
*/

getNpcManager().addGrid(Npc.Grid("ARENA.ZEN", -25000, -25000, 25000, 25000, 2500));
getNpcManager().addGrid(Npc.Grid("COLONY.ZEN", -80000, -80000, 80000, 80000, 2500));
getNpcManager().addGrid(Npc.Grid("ADDONWORLD.ZEN", -80000, -80000, 80000, 80000, 2500));