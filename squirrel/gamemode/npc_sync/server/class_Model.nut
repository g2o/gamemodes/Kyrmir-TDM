
class Npc.Model
{
    static _npcCounter = { freeIds = [], counter = getMaxSlots() };

    _id = -1;

    _currentCell = null;

    _streamerId = -1;
    _streamList = null;

    _name = "null";
    _instance = "PC_HERO";

    _groupId = -1;

    _color = null;
    _visual = null;

    _health = 10;
    _maxHealth = 10;

    _immortal = false;
    _collision = true;

    _world = "NEWWORLD\\NEWWORLD.ZEN";
    _virtualWorld = 0;

    _position = null;
    _angle = 0;

    _targetId = -1;

    _attackMode = false;
    _attackTime = 0;

    _attackPattern = 0;

    _obstacle = false;

    _follow = false;
    _followDistance = 150;

    _weaponSkill = null;
    _activeSpellId = -1;

    _meleeWeaponId = -1;
    _rangedWeaponId = -1;
    _armorId = -1;
    _helmetId = -1;

    _animation = null;
    _weaponMode = 0;

    constructor(name, instance)
    {
        _id = assignId();
        _currentCell = null;

        _streamerId = -1;
        _streamList = [];

        _name = name;
        _instance = instance;
        
        _groupId = -1;

        _color = { r = 255, g = 255, b = 255 };
        _visual = instance != "PC_HERO" ? null : { bodyModel = "Hum_Body_Naked0", bodyTxt = 9, headModel = "Hum_Head_Pony", headTxt = 18 };

        _health = 10;
        _maxHealth = 10;

        _immortal = false;
        _collision = true;

        _world = "NEWWORLD\\NEWWORLD.ZEN";
        _virtualWorld = 0;

        _position = { x = 0, y = 0, z = 0 };
        _angle = 0;

        _targetId = -1;

        _attackMode = false;
        _attackTime = 0;

        _attackPattern = 0;
        
        _obstacle = false;

        _follow = false;
        _followDistance = 150;

        _weaponSkill = {};
        _activeSpellId = -1;

        _meleeWeaponId = -1;
        _rangedWeaponId = -1;
        _armorId = -1;
        _helmetId = -1;

        _animation = null;
        _weaponMode = 0;
    }

//:protected
    static function assignId()
	{
        local id = -1;

        if(_npcCounter.freeIds.len() != 0)
        {
            id = _npcCounter.freeIds[0];
            _npcCounter.freeIds.remove(0);
        }
		else id = _npcCounter.counter++;

        return id;
	}

    static function takeId(id)
    {
        _npcCounter.freeIds.push(id);
    }  

//:public 
    function destroyNpc()
    {
        takeId(_id);
    }

    function getId() { return _id; }
    
    function setName(name) 
    {
        if(_name != name)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.NAME);

                packet.writeInt16(_id);    
                packet.writeString(_name = name);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getName() { return _name; }

    function setInstance(instance) 
    {
        if(_instance != instance)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.INSTANCE);

                packet.writeInt16(_id);    
                packet.writeString(_instance = instance);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getInstance() { return _instance; }

    function setGroup(groupId)
    {
        if(_groupId != groupId)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.GROUP);

                packet.writeInt16(_id);     
                packet.writeInt16(_groupId = groupId);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getGroup() { return _groupId; }

    function setColor(r, g, b) 
    {
        if(_color.r != r && _color.g != g && _color.b != b) 
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.COLOR);

                packet.writeInt16(_id);    
                packet.writeInt8(_color.r = r);
                packet.writeInt8(_color.g = g);
                packet.writeInt8(_color.b = b);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getColor() { return _color; }

    function setVisual(bodyModel, bodyTxt, headModel, headTxt) 
    {
        if(_visual.bodyModel != bodyModel || _visual.bodyTxt != bodyTxt 
            || _visual.headModel != headModel || _visual.headTxt != headTxt)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.VISUAL);

                packet.writeInt16(_id);    
                packet.writeString(_visual.bodyModel = bodyModel);
                packet.writeInt16(_visual.bodyTxt = bodyTxt);
                packet.writeString(_visual.headModel = headModel);
                packet.writeInt16(_visual.headTxt = headTxt);
           
            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getVisual() { return _visual; }

    function setHealth(health) 
    {
        if(_health != health)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.HEALTH);
                packet.writeInt16(_id);    

            if(health <= 0)
            {
                _attackMode = false;
                _attackTime = 0;

                _attackPattern = 0;
                
                _targetId = -1;
                _animation = null;               

                _obstacle = false;
                
                _follow = false;
                _followDistance = 150;

                _weaponMode = 0;

                packet.writeInt32(_health = 0);
                packet.sendToArray(_streamList, RELIABLE_ORDERED);
            }
            else 
            {
                if(health > _maxHealth)
                    _health = _maxHealth;
                else 
                    _health = health;

                packet.writeInt32(_health);
                packet.sendToArray(_streamList, UNRELIABLE_SEQUENCED);
            }
        }

        return this;
    }

    function getHealth() { return _health; }

    function setMaxHealth(maxHealth) 
    {
        if(_maxHealth != maxHealth)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.MAX_HEALTH);

                packet.writeInt16(_id);    
                packet.writeInt32(_maxHealth = maxHealth);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getMaxHealth() { return _maxHealth; }

    function isAlive() { return _health > 0; }

    function setImmortal(immortal)
    {
        _immortal = immortal;
            return this;
    }

    function isImmortal() { return _immortal; }

    function setCollision(collision) // This func should be use only before npc creation...
    {
        _collision = collision;
            return this;
    }

    function isCollisionEnabled() { return _collision; } 

    function setWorld(world) // This func should be use only before npc creation...
    {
        _world = world;
            return this;
    }

    function getWorld() { return _world; }

    function setVirtualWorld(virtualWorld) // This func should be use only before npc creation...
    {
        _virtualWorld = virtualWorld;
            return this;
    }

    function getVirtualWorld() { return _virtualWorld; }

    function setPosition(x, y, z)
    {
        if(_position.x != x || _position.y != y || _position.z != z)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.POSITION_S);

                packet.writeInt16(_id);    
                packet.writeFloat(_position.x = x);
                packet.writeFloat(_position.y = y);
                packet.writeFloat(_position.z = z);
        
            packet.sendToArray(_streamList, RELIABLE);
        }

        return this;
    }

    function getPosition() { return _position; }

    function setAngle(angle)
    {
        if(_angle != angle)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ANGLE_S);

                packet.writeInt16(_id);    
                packet.writeInt16(_angle = angle);

            packet.sendToArray(_streamList, RELIABLE);
        }

        return this;
    }

    function getAngle() { return _angle; }

    function setTarget(targetId)
    {
        if(_targetId != targetId)
        {
            if(targetId == -1) 
            { 
                _follow = false;
                _followDistance = 150;

                _attackMode = false;
            }

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.TARGET);

                packet.writeInt16(_id);           
                packet.writeInt16(_targetId = targetId);

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }

    function getTarget() { return _targetId; }

    function follow(distance)
    {
        if(_targetId != -1)
        {
            _animation = null;

            _attackMode = false;
            _attackTime = 0;
            _attackPattern = 0;

            _follow = true;
            _followDistance = distance;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.FOLLOW);

                packet.writeInt16(_id);           
                packet.writeInt16(distance);

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }  

    function isObstacle() { return _obstacle; }
    
    function isFollowing() { return _follow; }

    function parade(paradeType = 0)
    {
        if(_attackMode)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.PARADE);

                packet.writeInt16(_id);           
                packet.writeInt8(paradeType);     

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }  

    function attackFist(attackTime = 2000) 
    {
        if(_weaponMode == WEAPONMODE_FIST && _targetId != -1)
        {
            _animation = null;

            _attackMode = true;
            _attackTime = attackTime;

            _attackPattern = ATTACK_FRONT;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ATTACK_FIST);

                packet.writeInt16(_id); 
                packet.writeInt16(_attackTime);    

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }

    function isAttackingWithFist() { return _attackMode && (_weaponMode == WEAPONMODE_FIST); }

    function attackMelee(pattern, attackTime = 2000)
	{	
        if((_weaponMode == WEAPONMODE_1HS || _weaponMode == WEAPONMODE_2HS || _weaponMode == WEAPONMODE_RAPIER) 
            && _targetId != -1)
        {
            _animation = null;
            
            _attackMode = true;
            _attackTime = attackTime;
            
            _attackPattern = pattern;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ATTACK_MELEE);

                packet.writeInt16(_id);    
                packet.writeInt8(_attackPattern);    
                packet.writeInt16(_attackTime);    

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
	}

    function isAttackingWithMelee() { return _attackMode && (_weaponMode == WEAPONMODE_1HS || _weaponMode == WEAPONMODE_2HS || _weaponMode == WEAPONMODE_RAPIER); }

    function attackRanged(attackTime = 2000) 
    {
        if((_weaponMode == WEAPONMODE_BOW || _weaponMode == WEAPONMODE_CBOW) && _targetId != -1)
        {
            _animation = null;

            _attackMode = true;
            _attackTime = attackTime;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ATTACK_RANGED);

                packet.writeInt16(_id);
                packet.writeInt16(_attackTime);   

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }
    
    function isAttackingWithRanged() { return _attackMode && (_weaponMode == WEAPONMODE_BOW || _weaponMode == WEAPONMODE_CBOW); }

    function attackMagic(attackTime = 2000) 
    {
        if(_weaponMode == WEAPONMODE_MAG && _targetId != -1)
        {
            _animation = null;

            _attackMode = true;
            _attackTime = attackTime;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ATTACK_MAGIC);
            
                packet.writeInt16(_id);   
                packet.writeInt16(_attackTime);    

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }

    function isAttackingWithMagic() { return _attackMode && _weaponMode == WEAPONMODE_MAG; }

    function isInAttackMode() { return _attackMode; }

    function stopAttack() 
    {
        if(_weaponMode != WEAPONMODE_NONE) 
        {
            _attackMode = false;
            _attackTime = 0;

            _attackPattern = 0;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.STOP_ATTACK);
                packet.writeInt16(_id);    
            packet.sendToArray(_streamList, RELIABLE_ORDERED);      
        }
    }

    function setSkillWeapon(skillType, skill) 
    {
        if(getSkillWeapon(skillType) != skill)
        {            
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.WEAPON_SKILL);
                
                packet.writeInt16(_id);    
                packet.writeInt8(skillType);
                packet.writeInt16(skill);

            _weaponSkill[skillType] <- skill;

            packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        }

        return this;
    }

    function getSkillWeapon(skillType) { return skillType in _weaponSkill ? _weaponSkill[skillType] : 0; }

    function setActiveSpell(spellId)
    {
        if(_activeSpellId != spellId)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ACTIVE_SPELL);

                packet.writeInt16(_id);               
                packet.writeInt16(_activeSpellId = spellId);

            packet.sendToWorld(_world, _virtualWorld, RELIABLE_ORDERED);
        }
    }

    function getActiveSpell() { return _activeSpellId; }

//:protected
    function equipItem(itemType, itemId)
    {
        local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.EQUIP_ITEM);
        
            packet.writeInt16(_id);       
            packet.writeInt8(itemType);
            packet.writeInt16(itemId);

        packet.sendToWorld(_world, _virtualWorld, RELIABLE);
    }

//:public
    function equipMeleeWeapon(meleeWeaponId)
    {
        if(_meleeWeaponId != meleeWeaponId)
        {
            equipItem(NPC_ITEM_IDS.MELEE_WEAPON, _meleeWeaponId = meleeWeaponId);
        }

        return this;
    }

    function getMeleeWeapon() { return _meleeWeaponId; }

    function equipRangedWeapon(rangedWeaponId)
    {
        if(_rangedWeaponId != rangedWeaponId)
        {
            equipItem(NPC_ITEM_IDS.RANGED_WEAPON, _rangedWeaponId = rangedWeaponId);
        }

        return this;
    }

    function getRangedWeapon() { return _rangedWeaponId; }

    function equipArmor(armorId)
    {
        if(_armorId != armorId)
        {
            equipItem(NPC_ITEM_IDS.ARMOR, _armorId = armorId);
        }

        return this;
    }

    function getArmor() { return _armorId; }

    function equipHelmet(helmetId)
    {
        if(_helmetId != helmetId)
        {
            equipItem(NPC_ITEM_IDS.HELMET, _helmetId = helmetId);
        }

        return this;
    }

    function getHelmet() { return _helmetId; }

    function unequipItem(itemId)
    {
        switch(itemId)
        {
            case _meleeWeaponId: _meleeWeaponId = -1; break;
            case _rangedWeaponId: _rangedWeaponId = -1;  break;
            case _armorId: _armorId = -1;  break;
            case _helmetId: _helmetId = -1;  break;
        }

        local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.UNEQUIP_ITEM);

            packet.writeInt16(_id);   
            packet.writeInt16(itemId);

        packet.sendToWorld(_world, _virtualWorld, RELIABLE);
        
        return this;
    }

    function playAni(animation)
    {
        if(_animation != animation)
        {
            _follow = false;
            _followDistance = 150;  

            _attackMode = false;
            _attackTime = 0;

            _attackPattern = 0;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.PLAY_ANI);

                packet.writeInt16(_id);    
                packet.writeString(_animation = animation);

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }

        return this;
    }

    function stopAni() 
    {
        if(_animation != null)
        {            
            _animation = null;

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.STOP_ANI);
                packet.writeInt16(_id);    
            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }
    }

    function getLastAni() { return _animation; }
    
    function setWeaponMode(weaponMode)
    {
        if(_weaponMode != weaponMode)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.WEAPON_MODE);

                packet.writeInt16(_id);                 
                packet.writeInt8(_weaponMode = weaponMode); 

            packet.sendToArray(_streamList, RELIABLE_ORDERED);
        }

        return this;
    }

    function getWeaponMode() { return _weaponMode; }
}