
class Npc.GridCell 
{
    _column = -1;
    _row = -1;

    _visitors = null;
    _playerVisitors = null;

    constructor(column, row)
    {
        _column = column;
        _row = row;
        
        _visitors = null;
        _playerVisitors = null;
    }

//:public
    function addVisitor(object)
    {
        if(_visitors == null) _visitors = [];    
            _visitors.push(object);
    }

    function removeVisitor(object)
    {
        local id = _visitors.find(object);
        
        if(id != null) 
        {
            _visitors.remove(id);
            
            if(_visitors.len() == 0) 
                _visitors = null;
        }
    }

    function getVisitors() { return _visitors; }

    function addPlayerVisitor(object)
    {
        if(_playerVisitors == null) _playerVisitors = [];    
            _playerVisitors.push(object);
    }

    function removePlayerVisitor(object)
    {
        local id = _playerVisitors.find(object);
        
        if(id != null) 
        {
            _playerVisitors.remove(id);
            
            if(_playerVisitors.len() == 0) 
                _playerVisitors = null;
        }
    }

    function getPlayerVisitors() { return _playerVisitors; }

    function isCellInRange(column, row) 
    {
        return abs(column - _column) <= 1 && abs(row - _row) <= 1;
    }

    /*function isCellInRange(column, row)
    {
        if((column >= (_column - 1) && column <= (_column + 1))
            && (row >= (_row - 1) && row <= (_row + 1)))
        {
            return true;
        }

        return false;
    }*/

    function getColumn()
    {
        return _column;
    }

    function getRow()
    {
        return _row;
    }
}