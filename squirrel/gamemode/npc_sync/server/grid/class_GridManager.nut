
class Npc.GridManager 
{
    static _WorldGrid = {};

//:public
    static function addGrid(grid)
    {
        _WorldGrid[grid.getWorld()] <- grid;
    }

    static function getGrid(world)
    {
        if(_WorldGrid.rawin(world))
            return _WorldGrid[world];
        
        return null;
    }
    
    static function syncGrid()
    {
        foreach(id, object in _syncList)
        {
            local currentCell = object.getCurrentCell();

            if(object instanceof Npc.Instance)
            {
                if(object.isSpawned())
                {
                    local position = object.getPosition();
                    local grid = getGrid(object.getWorld());

                    if(grid)
                    {
                        local nearestCell = grid.getNearestCell(position.x, position.z);

                        if(currentCell != nearestCell)
                        {
                            object.setCurrentCell(nearestCell);

                            if(currentCell != null) currentCell.removeVisitor(object);
                            if(nearestCell != null) nearestCell.addVisitor(object);

                            callEvent("Npc.cellUpdate", object, currentCell, nearestCell);
                        }
                    }
                }
            }
            else 
            {
                if(isPlayerSpawned(id))
                {
                    local position = getPlayerCameraPosition(id);
                    local grid = getGrid(getPlayerWorld(id));

                    if(grid)
                    {
                        local nearestCell = grid.getNearestCell(position.x, position.z);

                        if(currentCell != nearestCell)
                        {                     
                            object.setCurrentCell(nearestCell);
                            
                            if(currentCell != null) currentCell.removePlayerVisitor(object);
                            if(nearestCell != null) nearestCell.addPlayerVisitor(object);

                            callEvent("Npc.cellUpdate", object, currentCell, nearestCell);
                        }
                    }
                }
            }
        }
    }
}