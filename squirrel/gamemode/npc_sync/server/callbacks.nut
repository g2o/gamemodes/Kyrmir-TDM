
// Register packet callback
PacketReceive.add(PacketIDs.Npc, getNpcManager());

addEventHandler("onInit", Npc.Manager.init.bindenv(Npc.Manager));

addEventHandler("onPlayerJoin", Npc.Manager.playerJoin.bindenv(Npc.Manager));

addEventHandler("onPlayerDisconnect", function(playerId, reason)
{
    getNpcManager().playerDisconnect(playerId);
});

addEventHandler("onPlayerChangeWorld", Npc.Manager.playerChangeWorld.bindenv(Npc.Manager));

addEventHandler("Npc.cellUpdate", Npc.Manager.updateCell.bindenv(Npc.Manager));

setTimer(Npc.Manager.syncGrid.bindenv(Npc.Manager), 500, 0);