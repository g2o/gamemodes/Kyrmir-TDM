
class Npc.Player 
{
    _id = -1;
    _currentCell = null;

    constructor(id)
    {
        _id = id;
        _currentCell = null;
    }

//:public
    function getId() { return _id; }

    function setCurrentCell(currentCell)
    {
        _currentCell = currentCell;
    }

    function getCurrentCell() { return _currentCell; }
}