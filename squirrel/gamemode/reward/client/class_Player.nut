
class Reward.Player 
{
    _monsterStreak = 0;
    _playerStreak = 0;

    constructor()
    {
        _monsterStreak = 0;
        _playerStreak = 0;
    }

//:public
    function setMonsterStreak(streak)
    {
        if(streak != _monsterStreak)
        {
            callEvent("Reward.onHeroMonsterStreak", _monsterStreak = streak);
        }

        return this;
    }

    function getMonsterStreak() { return _monsterStreak; }

    function setPlayerStreak(streak)
    {
        if(streak != _playerStreak)
        {            
            callEvent("Reward.onHeroPlayerStreak", _playerStreak = streak);
        }

        return this;
    }

    function getPlayerStreak() { return _playerStreak; }
}