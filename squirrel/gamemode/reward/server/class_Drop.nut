
class Reward.Drop 
{
    _itemInstance = null;
    _itemId = -1;

    _chance = 0;

    _amountMin = 0;
    _amountMax = 0;

    _requirementFunc = null;

    constructor(itemInstance, amountMin, amountMax, chance = -1)
    {
        _itemInstance = itemInstance;
        _itemId = getItemTemplateIdByInstance(itemInstance);

        _chance = chance;

        _amountMin = amountMin;
        _amountMax = amountMax;

        _requirementFunc = null;
    }

//:public
    function getItemInstance() { return _itemInstance; }
    
    function getItemId() { return _itemId; }

    function getChance() { return _chance; }

    function getAmountMin() { return _amountMin; }

    function getAmountMax() { return _amountMax; }
    
    function bindRequirementFunc(func)
    {
        _requirementFunc = func;
            return this;
    }

    function checkRequirement(playerId)
    {
        if(_requirementFunc != null)
            return _requirementFunc(playerId);

        return true;
    }
}