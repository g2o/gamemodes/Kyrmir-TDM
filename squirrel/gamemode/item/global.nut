
enum ItemType
{
    WeaponMelee,
    WeaponRanged,
    Helmet,
    Belt,
    Armor, 
    Ring,
    Amulet,
    Rune,
    Scroll,
    Potion,
    Food,
    LootBox,
    Munition,
    Other
}

enum ItemRequirement
{
    Level,
    MagicLevel,
    Strength,
    Dexterity,
    Mana,
    Health
}

enum ItemInteraction 
{
    None,
    Equippable,
    QuickSlot, 
    Usable
}

enum ItemEffect
{
    None,
    HealthRecovery,
    HealthPercentRecovery,
    ManaRecovery,
    ManaPercentRecovery,
    Custom
}