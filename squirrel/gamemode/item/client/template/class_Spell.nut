
class Item.Spell extends Item.Weapon
{
    _healthUsage = 0;
    _manaUsage = 0;

    _instant = false;

    constructor(id, instance, visualId, typeId)
    {
        _healthUsage = 0;
        _manaUsage = 0;

        _instant = false;

        base.constructor(id, instance, visualId, typeId);
            _weaponMode = WEAPONMODE_MAG;
    }

//:public
    function setInstant(instant)
    {
        _instant = instant;
            return this;
    }

    function isInstant() { return _instant; }

    function setHealthUsage(healthUsage)
    {
        _healthUsage = healthUsage;
    }

    function getHealthUsage() { return _healthUsage; }

    function setManaUsage(manaUsage)
    {
        _manaUsage = manaUsage;
    }

    function getManaUsage() { return _manaUsage; }
}