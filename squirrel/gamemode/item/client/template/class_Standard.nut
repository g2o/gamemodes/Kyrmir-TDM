
class Item.Standard
{
    _id = -1;

    _instance = null;
    _visualId = -1;

    _typeId = -1;

    _bounded = false;
    _interactionId = 0;

    constructor(id, instance, visualId, typeId)
    {
        _id = id;

        _instance = instance;
        _visualId = visualId;

        _typeId = typeId;

        _bounded = false;
        _interactionId = 0;
    }

//:public
    function getId() { return _id; }

    function getInstance() { return _instance; }

    function getVisual() { return Items.name(_visualId); }
   
    function getVisualId() { return _visualId; }

    function getType() { return _typeId; }

    function setBound(bounded) 
    {
        _bounded = bounded;
    }

    function isBounded() { return _bounded; }

    function setInteractionType(interaction)
    {
        _interactionId = interaction;
    }

    function getInteractionType() { return _interactionId; }

    function isUsable() { return _interactionId == ItemInteraction.Usable; }

    function isEquippable() 
    { 
        return _interactionId == ItemInteraction.Equippable || _interactionId == ItemInteraction.QuickSlot; 
    }

    function isQuickSlotPossible() 
    { 
        return _interactionId == ItemInteraction.Usable || _interactionId == ItemInteraction.QuickSlot;
    }

    function isSpell() { return _typeId == ItemType.Rune || _typeId == ItemType.Scroll; }
}