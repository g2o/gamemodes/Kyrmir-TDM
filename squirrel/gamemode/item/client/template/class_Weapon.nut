
class Item.Weapon extends Item.Gear
{
    _weaponMode = -1;
   
    _damage = 0;
    _damageType = -1;

    constructor(id, instance, visualId, typeId)
    {
        _weaponMode = -1;

        _damage = 0;
        _damageType = -1;

        base.constructor(id, instance, visualId, typeId);
    }

//:public
    function setWeaponMode(weaponMode)
    {
        _weaponMode = weaponMode;
    }

    function getWeaponMode() { return _weaponMode; }

    function setDamage(damageType, damage)
    {
        _damage = damage;
        _damageType = damageType;
    }

    function getDamage() { return _damage; }

    function getDamageType() { return _damageType; }
}

class Item.WeaponRanged extends Item.Weapon
{
    _munition = null;

    constructor(id, instance, visualId, typeId)
    {
        base.constructor(id, instance, visualId, typeId);
    }

//:public    
    function setMunition(item)
    {
        _munition = item;
    }

    function getMunition() { return _munition; }
}