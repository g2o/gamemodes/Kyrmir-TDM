
class Item.Manager 
{
    static _templateList = {};

//:protected 
    static function _readRequirement(template, packet)
    {
        template.setRequirement(ItemRequirement.Level, packet.readInt16());
        template.setRequirement(ItemRequirement.MagicLevel, packet.readInt16());
        template.setRequirement(ItemRequirement.Strength, packet.readInt16());
        template.setRequirement(ItemRequirement.Dexterity, packet.readInt16());   
        template.setRequirement(ItemRequirement.Mana, packet.readInt16());   
        template.setRequirement(ItemRequirement.Health, packet.readInt16());  
    }

    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt16();

        for(local i = 0; i < packetSize; i++)
        {            
            local id = packet.readInt16();

            local instance = packet.readString();
            local visualId = packet.readInt16();
            local typeId = packet.readInt8();
            local bounded = packet.readBool();
            local interactionType = packet.readInt8();

            local template = null;

            switch(typeId)
            {
                case ItemType.WeaponMelee:
                {
                    template = Item.Weapon(id, instance, visualId, typeId);

                        _readRequirement(template, packet);

                    template.setWeaponMode(packet.readInt8())
                    template.setDamage(packet.readInt8(), packet.readInt16());
                } 
                break;
                case ItemType.WeaponRanged: 
                {
                    template = Item.WeaponRanged(id, instance, visualId, typeId);

                        _readRequirement(template, packet);

                    template.setWeaponMode(packet.readInt8())
                    template.setDamage(packet.readInt8(), packet.readInt16());
                    template.setMunition(getTemplateById(packet.readInt16()));
                } 
                break;
                case ItemType.Armor:
                case ItemType.Helmet: 
                {
                    template = Item.Armor(id, instance, visualId, typeId);
                        
                        _readRequirement(template, packet);
                    template.setProtection(DAMAGE_BLUNT, packet.readInt16());
                    template.setProtection(DAMAGE_EDGE, packet.readInt16());
                    template.setProtection(DAMAGE_POINT, packet.readInt16());
                    template.setProtection(DAMAGE_FIRE, packet.readInt16());
                    template.setProtection(DAMAGE_MAGIC, packet.readInt16());  
                } 
                break;
                case ItemType.Rune:
                case ItemType.Scroll:
                {
                    template = Item.Spell(id, instance, visualId, typeId);
       
                        _readRequirement(template, packet);

                    template.setInstant(packet.readBool());
                    template.setDamage(packet.readInt8(), packet.readInt16());
                    template.setHealthUsage(packet.readInt16());
                    template.setManaUsage(packet.readInt16());
                } 
                break;                
                case ItemType.Potion:
                case ItemType.Food:
                case ItemType.LootBox: 
                {
                    template = Item.Usable(id, instance, visualId, typeId);

                    template.setEffect(ItemEffect.HealthRecovery, packet.readInt16());
                    template.setEffect(ItemEffect.HealthPercentRecovery, packet.readInt16());
                    template.setEffect(ItemEffect.ManaRecovery, packet.readInt16());
                    template.setEffect(ItemEffect.ManaPercentRecovery, packet.readInt16());

                    local customEffect = packet.readString();

                    if(customEffect != "null")
                        template.setEffect(ItemEffect.Custom, customEffect);
                } 
                break;       
                case ItemType.Other:
                case ItemType.Munition: 
                    template = Item.Standard(id, instance, visualId, typeId);
                break;      
            }
            
            template.setBound(bounded);
            template.setInteractionType(interactionType);

            _addTemplate(template);
        }
    }

    static function _addTemplate(template)
    {
        local visual = Items.id(template.getVisual());

        if(visual != null)
        {
            _templateList[template.getId()] <- template;
        } 
    } 
    
//:public
    static function registerTranscription(transcription)
    {   
        _descriptionList[transcription.getInstance()] <- transcription;
    }

    static function getTemplateByInstance(instance) 
    { 
        foreach(template in _templateList)
        {
            if(template.getInstance() == instance)
            {
                return template;
            }
        }

        return null;
    }

    static function getTemplateById(itemId)
    {
        return itemId in _templateList ? _templateList[itemId] : null; 
    }

    static function getIdByInstance(instance) 
    { 
        foreach(template in _templateList)
        {
            if(template.getInstance() == instance)
            {
                return template.getId();
            }
        }     
        
        return -1; 
    }

    static function getTemplates() { return _templateList; }
    
    static function packetRead(packet)
    {
        server_packetToList(packet);
    } 
}

getItemManager <- @() Item.Manager;

// Item
getItemTemplate <- @(instance) Item.Manager.getTemplateByInstance(instance);
getItemTemplateById <- @(itemId) Item.Manager.getTemplateById(itemId);
getItemTemplateIdByInstance <- @(instance) Item.Manager.getIdByInstance(instance);
