
// Food, Potions, Lootboxes
class Item.Usable extends Item.Standard
{
    _effectList = null;

    constructor(instance, visualId, typeId)
    {
            _effectList = {};
        base.constructor(instance, visualId, typeId);
    }

//:public
    function setEffect(effecType, effect)
    {
        _effectList[effecType] <- effect;
            return this;
    }

    function getEffect(effecType) { return effecType in _effectList ? _effectList[effecType] : 0; }

    function getEffects() { return _effectList; }
}