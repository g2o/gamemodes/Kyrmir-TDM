
class Item.Spell extends Item.Weapon
{
    _healthUsage = 0;
    _manaUsage = 0;

    _effectFunc = null;
    _instant = false;

    constructor(instance, visualId, typeId)
    {
        _healthUsage = 0;
        _manaUsage = 0;

        _effectFunc = null;
        _instant = false;

        base.constructor(instance, visualId, typeId);
            _weaponMode = WEAPONMODE_MAG;
    }

//:public
    function bindEffectFunc(func)
    {
        _effectFunc = func;
            return this;
    }

    function callEffectFunc(target, killer)
    {
        if(_effectFunc != null)
            return _effectFunc(target, killer);

        return null;
    }

    function setInstant(instant)
    {
        _instant = instant;
            return this;
    }

    function isInstant() { return _instant; }

    function setHealthUsage(healthUsage)
    {
        _healthUsage = healthUsage;
            return this;
    }

    function getHealthUsage() { return _healthUsage; }

    function setManaUsage(manaUsage)
    {
        _manaUsage = manaUsage;
            return this;
    }

    function getManaUsage() { return _manaUsage; }
}