
class Item.Gear extends Item.Standard
{
    _requirement = null;
    _overlayId = -1;

    constructor(instance, visualId, typeId)
    {
        _requirement = {};
        _overlayId = -1;

        base.constructor(instance, visualId, typeId);
    }

    function setRequirement(requirementType, requirement)
    {
        _requirement[requirementType] <- requirement;
            return this;
    }
    
    function getRequirement(requirementType) { return requirementType in _requirement ? _requirement[requirementType] : 0; }

    function canBeEquippedByPlayer(playerId)
    {
        local rLevel = getRequirement(ItemRequirement.Level);
        local rMagicLevel = getRequirement(ItemRequirement.MagicLevel);
        local rStrength = getRequirement(ItemRequirement.Strength);
        local rDexterity = getRequirement(ItemRequirement.Dexterity);   
        local rMana = getRequirement(ItemRequirement.Mana);   
        local rHealth = getRequirement(ItemRequirement.Health);   

        if(rLevel > getPlayerLevel(playerId)) return false;
        if(rMagicLevel > getPlayerMagicLevel(playerId)) return false;
        if(rStrength > getPlayerStrength(playerId)) return false;
        if(rDexterity > getPlayerDexterity(playerId)) return false;
        if(rMana > getPlayerMaxMana(playerId)) return false;
        if(rHealth > getPlayerMaxHealth(playerId)) return false;

        return true;
    }

    function setOverlay(overlayId)
    {
        _overlayId = overlayId;
            return this;
    }

    function getOverlay() { return _overlayId; }
}