
class PacketReceive
{
	static function add(packetType, object)
	{
		_packets[packetType] <- object;
	}
	
	static function remove(packetType)
	{
		if(_packets.rawin(packetType)) delete _packets[packetType];
	}
	
	static function packetRead(packet)
	{
		local packetId = packet.readInt8();
		
		if(_packets.rawin(packetId))
		{
			_packets[packetId].packetRead(packet);
		}
		else 
			print("PacketRead: Not existing packet type: " + packetId + "!");
	}
	
	static _packets = {};
}