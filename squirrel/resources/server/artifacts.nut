
registerArtifact(Artifact.Instance("ITMI_FOCUS_YELLOW")
    .bindReceivingFunc(function(playerId, amount)
    {
        setPlayerStrength_Bonus(playerId, getPlayerStrength_Bonus(playerId) + 10 * amount);
    })
    .bindLossingFunc(function(playerId, amount)
    {
        setPlayerStrength_Bonus(playerId, getPlayerStrength_Bonus(playerId) - 10 * amount);
    })
);

registerArtifact(Artifact.Instance("ITMI_FOCUS_RED")
    .bindReceivingFunc(function(playerId, amount)
    {
        setPlayerMaxHealth_Bonus(playerId, getPlayerMaxHealth_Bonus(playerId) + 50 * amount);
    })
    .bindLossingFunc(function(playerId, amount)
    {
        setPlayerMaxHealth_Bonus(playerId, getPlayerMaxHealth_Bonus(playerId) - 50 * amount);
    })
);

registerArtifact(Artifact.Instance("ITMI_FOCUS_BLUE")
    .bindReceivingFunc(function(playerId, amount)
    {
        setPlayerMaxMana_Bonus(playerId, getPlayerMaxMana_Bonus(playerId) + 20 * amount);
    })
    .bindLossingFunc(function(playerId, amount)
    {
        setPlayerMaxMana_Bonus(playerId, getPlayerMaxMana_Bonus(playerId) - 20 * amount);
    })
);

registerArtifact(Artifact.Instance("ITMI_FOCUS_GREEN")
    .bindReceivingFunc(function(playerId, amount)
    {
        setPlayerDexterity_Bonus(playerId, getPlayerDexterity_Bonus(playerId) + 10 * amount);
    })
    .bindLossingFunc(function(playerId, amount)
    {
        setPlayerDexterity_Bonus(playerId, getPlayerDexterity_Bonus(playerId) - 10 * amount);
    })
);

registerArtifact(Artifact.Instance("ITMI_FOCUS_GRAY")
    .bindReceivingFunc(function(playerId, amount)
    {
        //
    })
    .bindLossingFunc(function(playerId, amount)
    {
        //
    })
);

registerArtifact(Artifact.Instance("ITMI_FOCUS_VIOLET")
    .bindReceivingFunc(function(playerId, amount)
    {
        //
    })
    .bindLossingFunc(function(playerId, amount)
    {
        //
    })
);