
addItemTemplate(
    Item.Usable("ITPO_HEALTH_00", Items.id("ITPO_PERM_DEX"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.HealthRecovery, 150)
        .setBound(true)
);
 
addItemTemplate(
    Item.Usable("ITPO_HEALTH_01", Items.id("ITPO_HEALTH_01"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.HealthRecovery, 50)
);
 
addItemTemplate(
    Item.Usable("ITPO_HEALTH_02", Items.id("ITPO_HEALTH_02"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.HealthRecovery, 75)
);

addItemTemplate(
    Item.Usable("ITPO_HEALTH_03", Items.id("ITPO_HEALTH_03"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.HealthRecovery, 100)
);

addItemTemplate(
    Item.Usable("ITPO_HEALTH_04", Items.id("ITPO_HEALTH_03"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.HealthPercentRecovery, 100)
        .setBound(true)
);
 
addItemTemplate(
    Item.Usable("ITPO_MANA_01", Items.id("ITPO_MANA_01"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.ManaRecovery, 50)
);

addItemTemplate(
    Item.Usable("ITPO_MANA_02", Items.id("ITPO_MANA_02"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.ManaRecovery, 75)
);

addItemTemplate(
    Item.Usable("ITPO_MANA_03", Items.id("ITPO_MANA_03"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.ManaRecovery, 100)
);

// Potion effects
addEffectTemplate(Effect.Template("ITPO_SPEED_EFFECT", 30)
    .bindBeginingFunc(function(playerId) 
    {
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addItemTemplate(
    Item.Usable("ITPO_SPEED", Items.id("ITPO_SPEED"), ItemType.Potion)
        .setInteractionType(ItemInteraction.Usable)
        .setEffect(ItemEffect.Custom, "ITPO_SPEED_EFFECT")
);