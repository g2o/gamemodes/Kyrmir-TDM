
// SKELETONMAGE AI:
// skeletonmage.nut

const DAMAGE_OPEN = 170;
const DAMAGE_CLOSE = 160;

const ATTACK_OPEN_SPEED = 1700;
const ATTACK_CLOSE_SPEED = 2600;

enum AI_STATE
{
    NONE = -1,
    WARNING = 0, // WARNING_STATE
    FIGHT_OPEN = 1, // SKIP_WARNING_STATE
    FIGHT_CLOSE = 2,
}

local warningState = null,
    fightCloseState = null,
    fightOpenState = null;

warningState = function(bot)
{
    local warnTime = bot.getWarnTime();

    if(warnTime == -1) 
    {
        local template = bot.getTemplate();

        bot.setWarnTime(getTickCount() + template.getWarnTime());
        bot.setWeaponMode(WEAPONMODE_MAG);

        bot.playAni("T_WARN");
    }
    else 
    {
        if(warnTime > getTickCount())
        {
            if(bot.getDistanceToEnemy() <= bot.getTemplate().getHitDistance())
            {
                bot.setFight(AI_STATE.FIGHT_CLOSE);
                    fightCloseState(bot);
            }
            else
            {
                bot.setFight(AI_STATE.FIGHT_OPEN);
                    fightOpenState(bot);          
            }  
        }
    }
};

fightCloseState = function(bot)
{
    local template = bot.getTemplate();

    local enemyDistance = bot.getDistanceToEnemy();
    local hitDistance = template.getHitDistance();

    if(enemyDistance <= hitDistance)
    {
        bot.setActiveSpell(Items.id("ITRU_MASSDEATH"));
        bot.setDamage(DAMAGE_BLUNT, DAMAGE_CLOSE);

        if(bot.isAttackingWithMagic() == false)
        {
            bot.setWeaponMode(WEAPONMODE_MAG);
            bot.attackMagic(ATTACK_CLOSE_SPEED);
        }
    } 
    else 
    {
        bot.setFight(AI_STATE.FIGHT_OPEN);
            fightOpenState(bot);
    }
};

fightOpenState = function(bot)
{
    if(bot.isObstacle())
    {
        if(bot.getEnemyNumber() > 1)
        {
            bot.setActiveSpell(Items.id("ITRU_FIRESTORM"));
        }
    }
    else 
    {
        local template = bot.getTemplate();

        if(bot.getDistanceToEnemy() > template.getHitDistance())
        {
            bot.setActiveSpell(Items.id("ITRU_BELIARSRAGE"));
            bot.setDamage(DAMAGE_BLUNT, DAMAGE_OPEN);

            if(bot.isAttackingWithMagic() == false)
            {
                bot.setWeaponMode(WEAPONMODE_MAG);
                bot.attackMagic(ATTACK_OPEN_SPEED);
            }
        } 
        else 
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightCloseState(bot);
        }
    }
};

SKELETONMAGE_AI <- function(bot)
{
    local state = bot.getFight();
    if(state == -1) bot.setFight(AI_STATE.WARNING);
    
    switch(state)
    {
        case AI_STATE.WARNING: 
            warningState(bot); 
        break;
        case AI_STATE.FIGHT_CLOSE: 
            fightCloseState(bot); 
        break;
        case AI_STATE.FIGHT_OPEN: 
            fightOpenState(bot); 
        break;
    }
};