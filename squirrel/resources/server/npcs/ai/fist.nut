
// STANDARD ANIMAL AI:
// wolf.nut
// scavenger.nut
// warg.nut
// waran.nut
// snapper.nut
// shadowbeast.nut
// razor.nut
// orcbiter.nut
// molerat.nut
// minecrawlerwarrior.nut
// minecrawler.nut
// lurker.nut

enum AI_STATE 
{
    NONE = -1,
    WARNING = 0, // WARNING_STATE
    FOLLOW = 1, // SKIP_WARNING_STATE
    FOLLOW_OBSTACLE_L = 2,
    FOLLOW_OBSTACLE_R = 3,
    FIGHT_CLOSE = 4,
}

local warningState = null,
    followState = null,
    obstacleStateL = null,
    obstacleStateR = null,
    fightState = null;

local function isLeft(bot)
{
    local bPosition = bot.getPosition(), tPosition = getPlayerPosition(bot.getTarget()),
        angle = getVectorAngle(bPosition.x, bPosition.z, tPosition.x, tPosition.z);
    
    return (((bPosition.x + sin(angle) * 5000) - bPosition.x) * (tPosition.z - bPosition.z) - ((bPosition.z + cos(angle) * 5000) - bPosition.z) * (tPosition.x - bPosition.x)) > 0;
}

warningState = function(bot)
{
    local warnTime = bot.getWarnTime();

    if(warnTime == -1) 
    {
        bot.setWarnTime(getTickCount() + bot.getTemplate().getWarnTime());
        bot.setWeaponMode(WEAPONMODE_FIST);

        bot.playAni("T_WARN");
    }
    else 
    {
        if(warnTime <= getTickCount() || bot.getDistanceToEnemy() < 600)
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else if(bot.getDistanceToEnemy() < bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }
};

followState = function(bot)
{   
    if(bot.isObstacle())
    {        
        if(isLeft(bot))
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_L); 
                obstacleStateL(bot);
        }
        else 
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_R);
                obstacleStateR(bot);
        }
    }
    else 
    {
        local hitDistance = bot.getTemplate().getHitDistance();

        if(bot.getDistanceToEnemy() > hitDistance)
        {
            if(bot.isFollowing() == false) 
                bot.follow(hitDistance);
        }
        else 
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }
};

obstacleStateL = function(bot)
{    
    if(bot.isObstacle())
    {
        if(isLeft(bot))
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_R);
                obstacleStateR(bot);
        }
        else 
            bot.playAni("T_FISTRUNSTRAFEL");        
    } 
    else 
    {
        if(bot.getDistanceToEnemy() > bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }    
};

obstacleStateR = function(bot)
{
    if(bot.isObstacle())
    {
        if(isLeft(bot) == false)
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_L);
                obstacleStateL(bot);
        }
        else 
            bot.playAni("T_FISTRUNSTRAFER");
    }
    else 
    {
        if(bot.getDistanceToEnemy() > bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }    
};

fightState = function(bot)
{
    bot.setWeaponMode(WEAPONMODE_FIST);

    local enemyDistance = bot.getDistanceToEnemy();

    local template = bot.getTemplate();
    local hitDistance = template.getHitDistance();

    if(enemyDistance <= hitDistance)
    {    
        if(bot.isInAttackMode() == false)
            bot.attackFist(template.getAttackSpeed());
        else 
        {
            if(enemyDistance <= (hitDistance * 0.65))
                bot.parade(PARADE.JUMPB);
            else 
            {
                //if(bot.getTimeFromLastHit() < 1500) 
                    //bot.parade(PARADE.BLOCK_0);
            }
        }
    } 
    else 
    {
        bot.setFight(AI_STATE.FOLLOW);
            followState(bot);
    }
};

STANDARD_ANIMAL_AI <- function(bot)
{
    local state = bot.getFight();
    if(state == -1) bot.setFight(AI_STATE.WARNING);

    switch(state)
    {
        case AI_STATE.WARNING: 
            warningState(bot); 
        break;
        case AI_STATE.FOLLOW: 
            followState(bot); 
        break;
        case AI_STATE.FOLLOW_OBSTACLE_R: 
            obstacleStateR(bot); 
        break;
        case AI_STATE.FOLLOW_OBSTACLE_L: 
            obstacleStateL(bot); 
        break;
        case AI_STATE.FIGHT_CLOSE: 
            fightState(bot); 
        break;
    }
};