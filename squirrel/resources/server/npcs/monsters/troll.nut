
local TROLL = Bot.Template("TROLL", "KYRMIR_TROLL"); 

TROLL.setLevel(0);
TROLL.setMagicLevel(6);

TROLL.setHealth(6200);
TROLL.setDamage(DAMAGE_EDGE, 230);
TROLL.setRespawnTime(600)

TROLL.setProtection(DAMAGE_EDGE, 0);
TROLL.setProtection(DAMAGE_BLUNT, 500);
TROLL.setProtection(DAMAGE_FIRE, 0);
TROLL.setProtection(DAMAGE_MAGIC, 25);
TROLL.setProtection(DAMAGE_POINT, 30);

// Fight system
TROLL.setWarnTime(6);
TROLL.setHitDistance(340);
TROLL.setChaseDistance(3600);
TROLL.setDetectionDistance(2100);
TROLL.setAttackSpeed(2100);

TROLL.setInitiativeFunction(STONEGOLEM_AI);

TROLL.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 3))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_TROLL", TROLL);

// Drop
registerMonsterReward("KYRMIR_TROLL", Reward.Content(1500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2000, 2000))
);

// Spawnlist
spawnBot(TROLL, -2982, 1379, 1493, "ADDONWORLD.ZEN");
spawnBot(TROLL, -21282, -3745, -21420, "ADDONWORLD.ZEN");
spawnBot(TROLL, -10322, -3137, -20155, "ADDONWORLD.ZEN");
