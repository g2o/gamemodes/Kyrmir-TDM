
local BLATTCRAWLER = Bot.Template("BLATTCRAWLER", "KYRMIR_BLATTCRAWLER"); 

BLATTCRAWLER.setLevel(0);
BLATTCRAWLER.setMagicLevel(0);

BLATTCRAWLER.setHealth(170);
BLATTCRAWLER.setRespawnTime(210);

BLATTCRAWLER.setDamage(DAMAGE_EDGE, 130);

BLATTCRAWLER.setProtection(DAMAGE_EDGE, 0);
BLATTCRAWLER.setProtection(DAMAGE_BLUNT, 500);
BLATTCRAWLER.setProtection(DAMAGE_FIRE, 0);
BLATTCRAWLER.setProtection(DAMAGE_MAGIC, 20);
BLATTCRAWLER.setProtection(DAMAGE_POINT, 30);

// Fight system
BLATTCRAWLER.setWarnTime(6);
BLATTCRAWLER.setHitDistance(230);
BLATTCRAWLER.setChaseDistance(1800);
BLATTCRAWLER.setDetectionDistance(1200);
BLATTCRAWLER.setAttackSpeed(1800);

BLATTCRAWLER.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLATTCRAWLER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 300)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }
           
        bot.playAni("S_FISTWALKL");		
    }
}
, 2);

registerMonsterTemplate("KYRMIR_BLATTCRAWLER", BLATTCRAWLER);

// Drop
registerMonsterReward("KYRMIR_BLATTCRAWLER", Reward.Content(250)
    .addDrop(Reward.Drop("ITMI_GOLD", 130, 130))
);

// Spawnlist
spawnBot(BLATTCRAWLER, -9753, -940, -3216, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -9870, -928, -3322, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -9312, -955, -4730, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -11507, -625, -2576, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -11675, -646, -2607, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -17774, -1023, -5966, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -19613, -1537, -7415, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -18183, -1938, -7238, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -17731, -2711, -8127, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -19317, -3324, -7978, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -11989, -2239, -9427, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -12301, -2253, -9371, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -11163, -2222, -10286, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -9184, -1878, -10912, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -9133, -1780, -11959, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -9583, -1794, -12604, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -7990, -1359, -8483, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -6641, -1256, -8345, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -5155, -1099, -6425, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -5730, -1128, -7464, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -4960, -1079, -7177, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 4538, -1463, -5627, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 4921, -1474, -5513, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 6010, -1837, -7085, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 7417, -2037, -6626, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 8016, -2328, -4866, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 6734, -1779, -8641, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 6441, -1785, -8702, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 7419, -2080, -10504, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -4086, -949, -12750, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -4488, -942, -10847, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -5726, -1043, -9819, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -6287, -1772, -13433, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -7218, -1805, -13024, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -6461, -1429, -12160, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -6840, -1351, -9776, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 6352, -1814, -6665, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, 8996, -2722, -8562, "ADDONWORLD.ZEN");
spawnBot(BLATTCRAWLER, -4671, 50, -3488, "ADDONWORLD.ZEN");
