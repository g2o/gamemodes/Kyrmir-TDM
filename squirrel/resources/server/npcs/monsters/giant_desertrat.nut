
local GIANT_DESERTRAT = Bot.Template("GIANT_DESERTRAT", "KYRMIR_GIANT_DESERTRAT"); 

GIANT_DESERTRAT.setLevel(0);
GIANT_DESERTRAT.setMagicLevel(0);

GIANT_DESERTRAT.setHealth(65);
GIANT_DESERTRAT.setRespawnTime(80);

GIANT_DESERTRAT.setDamage(DAMAGE_EDGE, 35);

GIANT_DESERTRAT.setProtection(DAMAGE_EDGE, 0);
GIANT_DESERTRAT.setProtection(DAMAGE_BLUNT, 500);
GIANT_DESERTRAT.setProtection(DAMAGE_FIRE, 0);
GIANT_DESERTRAT.setProtection(DAMAGE_MAGIC, 10);
GIANT_DESERTRAT.setProtection(DAMAGE_POINT, 20);

// Fight system
GIANT_DESERTRAT.setWarnTime(6);
GIANT_DESERTRAT.setHitDistance(180);
GIANT_DESERTRAT.setChaseDistance(1800);
GIANT_DESERTRAT.setDetectionDistance(1200);
GIANT_DESERTRAT.setAttackSpeed(1800);

GIANT_DESERTRAT.setInitiativeFunction(STANDARD_ANIMAL_AI);

GIANT_DESERTRAT.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_GIANT_DESERTRAT", GIANT_DESERTRAT);

// Drop
registerMonsterReward("KYRMIR_GIANT_DESERTRAT", Reward.Content(75)
    .addDrop(Reward.Drop("ITMI_GOLD", 45, 45))
);

// Spawnlist
