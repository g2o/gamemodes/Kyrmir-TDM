
local BLOODHOUND = Bot.Template("BLOODHOUND", "KYRMIR_BLOODHOUND"); 

BLOODHOUND.setLevel(0);
BLOODHOUND.setMagicLevel(0);

BLOODHOUND.setHealth(200);
BLOODHOUND.setRespawnTime(275)

BLOODHOUND.setDamage(DAMAGE_EDGE, 140);

BLOODHOUND.setProtection(DAMAGE_EDGE, 0);
BLOODHOUND.setProtection(DAMAGE_BLUNT, 500);
BLOODHOUND.setProtection(DAMAGE_FIRE, 0);
BLOODHOUND.setProtection(DAMAGE_MAGIC, 20);
BLOODHOUND.setProtection(DAMAGE_POINT, 30);

// Fight system
BLOODHOUND.setWarnTime(6);
BLOODHOUND.setHitDistance(200);
BLOODHOUND.setChaseDistance(1800);
BLOODHOUND.setDetectionDistance(1200);
BLOODHOUND.setAttackSpeed(1800);

BLOODHOUND.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLOODHOUND.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 6);

registerMonsterTemplate("KYRMIR_BLOODHOUND", BLOODHOUND);

// Drop
registerMonsterReward("KYRMIR_BLOODHOUND", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
spawnBot(BLOODHOUND, -20404, -320, -2129, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -15601, -3411, 22141, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -15845, -3591, 24420, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -14150, -3611, 23381, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -2445, -3741, 22922, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -939, -3809, 23605, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, 947, -3636, 22502, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, 0, -3690, 22143, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12442, -3949, 23114, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -10920, -4123, 30615, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12419, -3956, 24706, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12268, -4006, 26912, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -11065, -4159, 30004, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -10828, -4174, 29553, "ADDONWORLD.ZEN");

spawnBot(BLOODHOUND, -16851.1, 160.078, 19452.6, "COLONY.ZEN");
spawnBot(BLOODHOUND, -44469.9, 3508.89, -1960.33, "COLONY.ZEN");
spawnBot(BLOODHOUND, -40302.8, 3615.98, -11211.8, "COLONY.ZEN");
spawnBot(BLOODHOUND, -894, 332.573, -13479, "COLONY.ZEN");
spawnBot(BLOODHOUND, 5515, 5342.22, -17569.8, "COLONY.ZEN");
spawnBot(BLOODHOUND, 6538, 4820.53, -16735.1, "COLONY.ZEN");
spawnBot(BLOODHOUND, 9334, 5228.72, -18842.4, "COLONY.ZEN");
spawnBot(BLOODHOUND, 7999, 5923.72, -20267.3, "COLONY.ZEN");
spawnBot(BLOODHOUND, 7855, 5941.42, -20106.4, "COLONY.ZEN");
spawnBot(BLOODHOUND, 9232, 5633.94, -20311, "COLONY.ZEN");
spawnBot(BLOODHOUND, 18871, 1099.99, -27668.5, "COLONY.ZEN");
spawnBot(BLOODHOUND, 20868.1, 5262.92, -24614.2, "COLONY.ZEN");
spawnBot(BLOODHOUND, 18220, 6435.7, -26058.9, "COLONY.ZEN");
spawnBot(BLOODHOUND, 16677, 6385.71, -26929.2, "COLONY.ZEN");
spawnBot(BLOODHOUND, 15149, 6414.25, -27432.5, "COLONY.ZEN");
spawnBot(BLOODHOUND, 13649, 6727.59, -26759.2, "COLONY.ZEN");
spawnBot(BLOODHOUND, 12859, 6891.16, -26238.3, "COLONY.ZEN");



