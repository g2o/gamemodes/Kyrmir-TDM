
local SWAMPRAT = Bot.Template("SWAMPRAT", "KYRMIR_SWAMPRAT"); 

SWAMPRAT.setLevel(0);
SWAMPRAT.setMagicLevel(0);

SWAMPRAT.setHealth(50);
SWAMPRAT.setRespawnTime(60);

SWAMPRAT.setDamage(DAMAGE_EDGE, 25);

SWAMPRAT.setProtection(DAMAGE_EDGE, 0);
SWAMPRAT.setProtection(DAMAGE_BLUNT, 500);
SWAMPRAT.setProtection(DAMAGE_FIRE, 0);
SWAMPRAT.setProtection(DAMAGE_MAGIC, 10);
SWAMPRAT.setProtection(DAMAGE_POINT, 20);

// Fight system
SWAMPRAT.setWarnTime(6);
SWAMPRAT.setHitDistance(180);
SWAMPRAT.setChaseDistance(1800);
SWAMPRAT.setDetectionDistance(1200);
SWAMPRAT.setAttackSpeed(1800);

SWAMPRAT.setInitiativeFunction(STANDARD_ANIMAL_AI);

SWAMPRAT.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_SWAMPRAT", SWAMPRAT);

// Drop
registerMonsterReward("KYRMIR_SWAMPRAT", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(SWAMPRAT, 23343, -5228, 10086, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 23583, -5213, 10372, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 21923, -5190, 10660, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 22389, -5254, 9728, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 19787, -5245, 8331, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 20889, -5167, 6977, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 22028, -5112, 5663, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 22217, -5113, 5314, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 21765, -5209, 3878, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 22659, -5108, 3270, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24113, -5131, 3398, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24457, -5343, 1867, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 23257, -5107, 2921, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 25188, -5110, 4357, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24188, -5110, 5211, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 25367, -5012, 5332, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 23752, -5185, 6341, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 23851, -5211, 7936, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24148, -5251, 8791, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24932, -5152, -2510, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 22951, -5162, -1198, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 24560, -4626, -8225, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 23946, -4533, -8416, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 21255, -5162, 1046, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 20174, -5271, 6757, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 20497, -5343, 9547, "ADDONWORLD.ZEN");
spawnBot(SWAMPRAT, 21483, -5267, 11951, "ADDONWORLD.ZEN");
