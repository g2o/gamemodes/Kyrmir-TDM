
local SWAMPGOLEM = Bot.Template("SWAMPGOLEM", "KYRMIR_SWAMPGOLEM"); 

SWAMPGOLEM.setLevel(0);
SWAMPGOLEM.setMagicLevel(6);

SWAMPGOLEM.setHealth(800);
SWAMPGOLEM.setDamage(DAMAGE_EDGE, 150);
SWAMPGOLEM.setRespawnTime(500);

SWAMPGOLEM.setProtection(DAMAGE_EDGE, 0);
SWAMPGOLEM.setProtection(DAMAGE_BLUNT, 500);
SWAMPGOLEM.setProtection(DAMAGE_FIRE, 0);
SWAMPGOLEM.setProtection(DAMAGE_MAGIC, 20);
SWAMPGOLEM.setProtection(DAMAGE_POINT, 25);

// Fight system
SWAMPGOLEM.setWarnTime(6);
SWAMPGOLEM.setHitDistance(240);
SWAMPGOLEM.setChaseDistance(3600);
SWAMPGOLEM.setDetectionDistance(1200);
SWAMPGOLEM.setAttackSpeed(3100);

SWAMPGOLEM.setInitiativeFunction(STONEGOLEM_AI);

SWAMPGOLEM.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_SWAMPGOLEM", SWAMPGOLEM);

// Drop
registerMonsterReward("KYRMIR_SWAMPGOLEM", Reward.Content(1000)
    .addDrop(Reward.Drop("ITMI_GOLD", 1000, 1000))
);

// Spawnlist
spawnBot(SWAMPGOLEM, -11273, -4326, -27609, "ADDONWORLD.ZEN");
