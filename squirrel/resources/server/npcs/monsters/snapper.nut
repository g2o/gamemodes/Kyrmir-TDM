
local SNAPPER = Bot.Template("SNAPPER", "KYRMIR_SNAPPER"); 

SNAPPER.setLevel(0);
SNAPPER.setMagicLevel(0);

SNAPPER.setHealth(125);
SNAPPER.setRespawnTime(185);

SNAPPER.setDamage(DAMAGE_EDGE, 120);

SNAPPER.setProtection(DAMAGE_EDGE, 0);
SNAPPER.setProtection(DAMAGE_BLUNT, 500);
SNAPPER.setProtection(DAMAGE_FIRE, 0);
SNAPPER.setProtection(DAMAGE_MAGIC, 20);
SNAPPER.setProtection(DAMAGE_POINT, 30);

// Fight system
SNAPPER.setWarnTime(6);
SNAPPER.setHitDistance(150);
SNAPPER.setChaseDistance(1800);
SNAPPER.setDetectionDistance(1200);
SNAPPER.setAttackSpeed(2100);

SNAPPER.setInitiativeFunction(STANDARD_ANIMAL_AI);

SNAPPER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_SNAPPER", SNAPPER);

// Drop
registerMonsterReward("KYRMIR_SNAPPER", Reward.Content(200)
    .addDrop(Reward.Drop("ITMI_GOLD", 100, 100))
);

// Spawnlist
spawnBot(SNAPPER, -12303, -3695, 18867, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -12455, -3677, 18815, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -14167, -3868, 21226, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7888, -3159, 22752, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7495, -3264, 23444, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7494, -3231, 23696, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -5987, -2832, 23464, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -4964, -2863, 23513, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -4483, -3010, 23418, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9630, -3203, -3211, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9996, -3231, -3744, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9901, -3230, -3885, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 7152, -2048, -4507, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 8151, -2000, -8500, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 6147, -1757, -8942, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -13328, -2570, -15330, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -13832, -2563, -15874, "ADDONWORLD.ZEN");

spawnBot(SNAPPER, -20327.9, 198.028, 22655.8, "COLONY.ZEN");
spawnBot(SNAPPER, -20121, 228, 22506, "COLONY.ZEN");
spawnBot(SNAPPER, -20659, 181, 22132, "COLONY.ZEN");
spawnBot(SNAPPER, -20351, 193, 22523, "COLONY.ZEN");
spawnBot(SNAPPER, -12640.5, 915.951, 22059.3, "COLONY.ZEN");
spawnBot(SNAPPER, -13787, 1474.85, 23798.5, "COLONY.ZEN");
spawnBot(SNAPPER, -11984.9, 4942.51, 30604.9, "COLONY.ZEN");
spawnBot(SNAPPER, 17390, -972, 9994, "COLONY.ZEN");
spawnBot(SNAPPER, 17467, -1027, 10070, "COLONY.ZEN");
spawnBot(SNAPPER, 17737, -1145, 10090, "COLONY.ZEN");
spawnBot(SNAPPER, -13553, 1437.43, 23792, "COLONY.ZEN");
spawnBot(SNAPPER, -14325, 2414.47, 27461.8, "COLONY.ZEN");
spawnBot(SNAPPER, -14621.9, 2464.06, 27668.6, "COLONY.ZEN");
spawnBot(SNAPPER, -13126, 3915.71, 29587.1, "COLONY.ZEN");
spawnBot(SNAPPER, -7758, 5542, 33790, "COLONY.ZEN");
spawnBot(SNAPPER, -5674, 4917, 31993, "COLONY.ZEN");
spawnBot(SNAPPER, -7379, 4794, 31289, "COLONY.ZEN");
spawnBot(SNAPPER, -7487, 4809, 30791, "COLONY.ZEN");
spawnBot(SNAPPER, -7923, 4783, 30885, "COLONY.ZEN");
spawnBot(SNAPPER, -9277, 4852, 29825, "COLONY.ZEN");
spawnBot(SNAPPER, -8283, 4905, 31869, "COLONY.ZEN");
spawnBot(SNAPPER, -15805, -1286, -3689, "COLONY.ZEN");
spawnBot(SNAPPER, -14573, -1107, -4219, "COLONY.ZEN");
spawnBot(SNAPPER, -15541, -1061, -5695, "COLONY.ZEN");
spawnBot(SNAPPER, -14436, -1090, -4340, "COLONY.ZEN");
spawnBot(SNAPPER, -14942, -1035, -5544, "COLONY.ZEN");
spawnBot(SNAPPER, -16015, -1545, -1856, "COLONY.ZEN");
spawnBot(SNAPPER, 19108, 7, 20025, "COLONY.ZEN");
spawnBot(SNAPPER, 19283, 9, 20782, "COLONY.ZEN");
spawnBot(SNAPPER, 19092, -53, 21119, "COLONY.ZEN");
spawnBot(SNAPPER, 19138, -21, 20845, "COLONY.ZEN");
spawnBot(SNAPPER, 18652, -69, 22439, "COLONY.ZEN");
spawnBot(SNAPPER, 21233, 7, 21383, "COLONY.ZEN");
spawnBot(SNAPPER, 22691.1, 253.184, 26057.7, "COLONY.ZEN");
spawnBot(SNAPPER, 22818, 247.59, 25965.9, "COLONY.ZEN");
spawnBot(SNAPPER, 22485.1, 249.689, 26107.9, "COLONY.ZEN");
spawnBot(SNAPPER, 23792, 307, 26761, "COLONY.ZEN");
spawnBot(SNAPPER, 24210, 326, 26673, "COLONY.ZEN");
spawnBot(SNAPPER, 23564, 289.677, 26014.6, "COLONY.ZEN");
spawnBot(SNAPPER, 22292, 259, 26120, "COLONY.ZEN");
spawnBot(SNAPPER, 24178, 317, 26870, "COLONY.ZEN");
spawnBot(SNAPPER, 18167, -1707, 18480, "COLONY.ZEN");
spawnBot(SNAPPER, 15792, -1849, 19976, "COLONY.ZEN");
spawnBot(SNAPPER, 37521, 920.218, -24482.3, "COLONY.ZEN");
spawnBot(SNAPPER, 35469.8, 1070.88, -24565.7, "COLONY.ZEN");
spawnBot(SNAPPER, -36626.1, 1522.9, 26960.1, "COLONY.ZEN");
spawnBot(SNAPPER, -35518, 1423.83, 28449.6, "COLONY.ZEN");
spawnBot(SNAPPER, -35930.2, 1563.23, 28309.6, "COLONY.ZEN");
spawnBot(SNAPPER, -36034.5, 1475.42, 27387.6, "COLONY.ZEN");
spawnBot(SNAPPER, -57263, 4977, -2645, "COLONY.ZEN");
spawnBot(SNAPPER, -56675, 4855, -1750, "COLONY.ZEN");
spawnBot(SNAPPER, -57297, 4759, -2185, "COLONY.ZEN");