
local ORCELITE_ROAM = Bot.Template("ORCELITE_ROAM", "KYRMIR_ORCELITE_ROAM"); 

ORCELITE_ROAM.setLevel(0);
ORCELITE_ROAM.setMagicLevel(0);

ORCELITE_ROAM.setHealth(1300);
ORCELITE_ROAM.setRespawnTime(300);

ORCELITE_ROAM.setMeleeWeapon(Items.id("ITMW_2H_ORCSWORD_02"));
ORCELITE_ROAM.setSkillWeapon(WEAPON_2H, 100);

ORCELITE_ROAM.setDamage(DAMAGE_EDGE, 140);

ORCELITE_ROAM.setProtection(DAMAGE_EDGE, 0);
ORCELITE_ROAM.setProtection(DAMAGE_BLUNT, 500);
ORCELITE_ROAM.setProtection(DAMAGE_FIRE, 0);
ORCELITE_ROAM.setProtection(DAMAGE_MAGIC, 0);
ORCELITE_ROAM.setProtection(DAMAGE_POINT, 0);

// Fight system
ORCELITE_ROAM.setWarnTime(6);
ORCELITE_ROAM.setHitDistance(240);
ORCELITE_ROAM.setChaseDistance(3600);
ORCELITE_ROAM.setDetectionDistance(1300);
ORCELITE_ROAM.setAttackSpeed(360);

ORCELITE_ROAM.setInitiativeFunction(TWO_HAND_AI);

ORCELITE_ROAM.setRoutineFunction(function(bot)
{
{
    if(bot.getLastAni() == "S_WALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 1))
        {
            case 0: bot.playAni("T_STAND_2_EAT"); break;	
            case 1: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_WALKL");			
    }
}
});

registerMonsterTemplate("KYRMIR_ORCELITE_ROAM", ORCELITE_ROAM);

// Drop
registerMonsterReward("KYRMIR_ORCELITE_ROAM", Reward.Content(450)
    .addDrop(Reward.Drop("ITMI_GOLD", 250, 250))
);

// Spawnlist
spawnBot(ORCELITE_ROAM, -1822, -840, -4338, "ADDONWORLD.ZEN");
spawnBot(ORCELITE_ROAM, 150, -891, -5154, "ADDONWORLD.ZEN");
spawnBot(ORCELITE_ROAM, 41, -977, -8331, "ADDONWORLD.ZEN");
spawnBot(ORCELITE_ROAM, 4866, -963, 982, "ADDONWORLD.ZEN");
spawnBot(ORCELITE_ROAM, 5906, -971, 934, "ADDONWORLD.ZEN");
spawnBot(ORCELITE_ROAM, 2016, -887, -3585, "ADDONWORLD.ZEN");
