
local WARAN = Bot.Template("WARAN", "KYRMIR_WARAN"); 

WARAN.setLevel(0);
WARAN.setMagicLevel(0);

WARAN.setHealth(90);
WARAN.setRespawnTime(110);

WARAN.setDamage(DAMAGE_EDGE, 65);

WARAN.setProtection(DAMAGE_EDGE, 0);
WARAN.setProtection(DAMAGE_BLUNT, 500);
WARAN.setProtection(DAMAGE_FIRE, 0);
WARAN.setProtection(DAMAGE_MAGIC, 10);
WARAN.setProtection(DAMAGE_POINT, 20);

// Fight system
WARAN.setWarnTime(6);
WARAN.setHitDistance(260);
WARAN.setChaseDistance(1800);
WARAN.setDetectionDistance(1200);
WARAN.setAttackSpeed(1800);

WARAN.setInitiativeFunction(STANDARD_ANIMAL_AI);

WARAN.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WARAN", WARAN);

// Drop
registerMonsterReward("KYRMIR_WARAN", Reward.Content(130)
    .addDrop(Reward.Drop("ITMI_GOLD", 70, 70))
);

// Spawnlist
spawnBot(WARAN, -29033, -4350, 8995, "ADDONWORLD.ZEN");
spawnBot(WARAN, -28226, -4942, 8024, "ADDONWORLD.ZEN");
spawnBot(WARAN, -27292, -4144, 9644, "ADDONWORLD.ZEN");
spawnBot(WARAN, -26389, -3859, 8602, "ADDONWORLD.ZEN");
spawnBot(WARAN, -25667, -3873, 9175, "ADDONWORLD.ZEN");
spawnBot(WARAN, -25402, -3559, 7908, "ADDONWORLD.ZEN");
spawnBot(WARAN, -24502, -3082, 7074, "ADDONWORLD.ZEN");
spawnBot(WARAN, -35203, -1849, 7491, "ADDONWORLD.ZEN");
spawnBot(WARAN, -32379, -1945, 5560, "ADDONWORLD.ZEN");
spawnBot(WARAN, -35415, -1862, 8187, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37070, -1927, 9665, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37505, -1965, 10576, "ADDONWORLD.ZEN");
spawnBot(WARAN, 29349, -3482, 20299, "ADDONWORLD.ZEN");
spawnBot(WARAN, 27456, -3333, 16510, "ADDONWORLD.ZEN");
spawnBot(WARAN, 28461, -3357, 17593, "ADDONWORLD.ZEN");
spawnBot(WARAN, -36981, -1943, 11567, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37561, -1955, 12496, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37032, -1932, 11959, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37916, -1994, 12548, "ADDONWORLD.ZEN");
spawnBot(WARAN, -38499.7, -1889.77, 9180.22, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37834, -1963, 9508, "ADDONWORLD.ZEN");
spawnBot(WARAN, -36598, -1887, 8744, "ADDONWORLD.ZEN");
spawnBot(WARAN, -36056, -1855, 7026, "ADDONWORLD.ZEN");
spawnBot(WARAN, -34535, -1786, 6450, "ADDONWORLD.ZEN");
spawnBot(WARAN, -35551, -1795, 5915, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37435, -1912, 6471, "ADDONWORLD.ZEN");

spawnBot(WARAN, 8998, 486.189, -11149.6, "COLONY.ZEN");
spawnBot(WARAN, 10506.6, -1268.2, 10826.6, "COLONY.ZEN");
spawnBot(WARAN, 9026, -1397.63, 11359.1, "COLONY.ZEN");
spawnBot(WARAN, 9309, -1269.14, 9291.07, "COLONY.ZEN");
spawnBot(WARAN, 7544, -1463.06, 10894.1, "COLONY.ZEN");
spawnBot(WARAN, -30142.8, 166.943, 780.402, "COLONY.ZEN");
spawnBot(WARAN, -39977.2, 492.569, 1533.94, "COLONY.ZEN");
spawnBot(WARAN, -38846.9, 473.704, 1506, "COLONY.ZEN");
spawnBot(WARAN, -28334.1, 777.902, -4337.31, "COLONY.ZEN");
spawnBot(WARAN, -28100.3, 748.532, -4207.65, "COLONY.ZEN");
spawnBot(WARAN, -28847.5, 846.441, -4707.03, "COLONY.ZEN");
spawnBot(WARAN, -29124.4, 875.274, -4734.93, "COLONY.ZEN");
spawnBot(WARAN, -28624.2, -619.22, 10348.6, "COLONY.ZEN");
spawnBot(WARAN, -26528, -341.721, 9662.68, "COLONY.ZEN");
spawnBot(WARAN, -27492.1, -483.044, 11983.3, "COLONY.ZEN");
spawnBot(WARAN, -18369, -1010.91, -5027, "COLONY.ZEN");
spawnBot(WARAN, -18461.2, -972.266, -3753.45, "COLONY.ZEN");
spawnBot(WARAN, 15189, -785.812, 15706.7, "COLONY.ZEN");
spawnBot(WARAN, 15970, -760.168, 15869.8, "COLONY.ZEN");
spawnBot(WARAN, 16902, -847.108, 15475.8, "COLONY.ZEN");
spawnBot(WARAN, 16699, -546.754, 16807.8, "COLONY.ZEN");
spawnBot(WARAN, 17610, -247.974, 18002.1, "COLONY.ZEN");
spawnBot(WARAN, 19304, -1095.37, 16277.7, "COLONY.ZEN");
spawnBot(WARAN, 21591, 1486.76, -18718.5, "COLONY.ZEN");
spawnBot(WARAN, 21878, 1561.59, -18311.5, "COLONY.ZEN");
spawnBot(WARAN, 22615, 1716.17, -18174.8, "COLONY.ZEN");
spawnBot(WARAN, 26470, 2747.63, -20130.5, "COLONY.ZEN");
spawnBot(WARAN, 26791, 2774.58, -20363.7, "COLONY.ZEN");
spawnBot(WARAN, 25236, 3729.91, -22448.8, "COLONY.ZEN");
spawnBot(WARAN, 24462, 3990.91, -23038.2, "COLONY.ZEN");
spawnBot(WARAN, 29319, -145.312, -6257.11, "COLONY.ZEN");
spawnBot(WARAN, 33394, -3601.87, 13591.8, "COLONY.ZEN");
spawnBot(WARAN, 34662, -3683.28, 13060.6, "COLONY.ZEN");
spawnBot(WARAN, 31557, -3499.75, 14504.6, "COLONY.ZEN");
spawnBot(WARAN, 31002, -3606.63, 16303.6, "COLONY.ZEN");
spawnBot(WARAN, -29514.9, 900.112, -4340.21, "COLONY.ZEN");
spawnBot(WARAN, -29153.1, 801.219, -3416.98, "COLONY.ZEN");