
local KEILER = Bot.Template("KEILER", "KYRMIR_KEILER"); 

KEILER.setLevel(0);
KEILER.setMagicLevel(0);

KEILER.setHealth(110);
KEILER.setRespawnTime(130);

KEILER.setDamage(DAMAGE_EDGE, 90);

KEILER.setProtection(DAMAGE_EDGE, 0);
KEILER.setProtection(DAMAGE_BLUNT, 500);
KEILER.setProtection(DAMAGE_FIRE, 0);
KEILER.setProtection(DAMAGE_MAGIC, 15);
KEILER.setProtection(DAMAGE_POINT, 25);

// Fight system
KEILER.setWarnTime(6);
KEILER.setHitDistance(200);
KEILER.setChaseDistance(1800);
KEILER.setDetectionDistance(1200);
KEILER.setAttackSpeed(1800);

KEILER.setInitiativeFunction(STANDARD_ANIMAL_AI);

KEILER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_KEILER", KEILER);

// Drop
registerMonsterReward("KYRMIR_KEILER", Reward.Content(180)
    .addDrop(Reward.Drop("ITMI_GOLD", 80, 80))
);

// Spawnlist
spawnBot(KEILER, -21109, -1066, 3275, "ADDONWORLD.ZEN");
spawnBot(KEILER, -19356, -871, 2611, "ADDONWORLD.ZEN");
spawnBot(KEILER, -18980, -863, 2460, "ADDONWORLD.ZEN");
spawnBot(KEILER, -15395, -433, -6282, "ADDONWORLD.ZEN");
spawnBot(KEILER, -15428, -435, -6235, "ADDONWORLD.ZEN");
spawnBot(KEILER, -14072, -401, -5774, "ADDONWORLD.ZEN");
spawnBot(KEILER, -13069, -609, -5188, "ADDONWORLD.ZEN");
spawnBot(KEILER, -20963, -3836, -7332, "ADDONWORLD.ZEN");
spawnBot(KEILER, -20496, -4181, -8310, "ADDONWORLD.ZEN");
spawnBot(KEILER, -22232, -3567, -6841, "ADDONWORLD.ZEN");
spawnBot(KEILER, -17766, -4503, -9105, "ADDONWORLD.ZEN");
spawnBot(KEILER, -17980, -4481, -8631, "ADDONWORLD.ZEN");
