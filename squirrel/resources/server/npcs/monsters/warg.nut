
local WARG = Bot.Template("WARG", "KYRMIR_WARG"); 

WARG.setLevel(0);
WARG.setMagicLevel(0);

WARG.setHealth(175);
WARG.setRespawnTime(210);

WARG.setDamage(DAMAGE_EDGE, 130);

WARG.setProtection(DAMAGE_EDGE, 0);
WARG.setProtection(DAMAGE_BLUNT, 500);
WARG.setProtection(DAMAGE_FIRE, 0);
WARG.setProtection(DAMAGE_MAGIC, 20);
WARG.setProtection(DAMAGE_POINT, 30);

// Fight system
WARG.setWarnTime(6);
WARG.setHitDistance(240);
WARG.setChaseDistance(1800);
WARG.setDetectionDistance(1200);
WARG.setAttackSpeed(2100);

WARG.setInitiativeFunction(STANDARD_ANIMAL_AI);

WARG.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WARG", WARG);

// Drop
registerMonsterReward("KYRMIR_WARG", Reward.Content(270)
    .addDrop(Reward.Drop("ITMI_GOLD", 120, 120))
);

// Spawnlist
spawnBot(WARG, -20012, -2974, -14263, "ADDONWORLD.ZEN");
spawnBot(WARG, -19540, -3017, -14038, "ADDONWORLD.ZEN");
spawnBot(WARG, -19563, -3008, -14149, "ADDONWORLD.ZEN");
spawnBot(WARG, -20064, -3258, -16109, "ADDONWORLD.ZEN");
spawnBot(WARG, 3290, -1970, -13508, "ADDONWORLD.ZEN");
spawnBot(WARG, 4020, -1939, -13722, "ADDONWORLD.ZEN");
spawnBot(WARG, 5363, -1931, -12892, "ADDONWORLD.ZEN");
spawnBot(WARG, 6201, -2032, -11471, "ADDONWORLD.ZEN");
spawnBot(WARG, 5714, -1794, -10481, "ADDONWORLD.ZEN");
spawnBot(WARG, 4282, -1441, -11267, "ADDONWORLD.ZEN");
spawnBot(WARG, -2467, -1652, -14220, "ADDONWORLD.ZEN");
spawnBot(WARG, -4056, -974, -13071, "ADDONWORLD.ZEN");
spawnBot(WARG, -3057, -694, -12638, "ADDONWORLD.ZEN");
spawnBot(WARG, -2451, -801, -13542, "ADDONWORLD.ZEN");
spawnBot(WARG, -822, -796, -13495, "ADDONWORLD.ZEN");
spawnBot(WARG, -10725, -3321, -13321, "ADDONWORLD.ZEN");
spawnBot(WARG, -11817, -3356, -13560, "ADDONWORLD.ZEN");
spawnBot(WARG, -10730, -3257, -15025, "ADDONWORLD.ZEN");
spawnBot(WARG, -11905, -3546, -15293, "ADDONWORLD.ZEN");
spawnBot(WARG, -12002, -3671, -16696, "ADDONWORLD.ZEN");
spawnBot(WARG, -13857, -2722, -20697, "ADDONWORLD.ZEN");
spawnBot(WARG, -14142, -2792, -20319, "ADDONWORLD.ZEN");
spawnBot(WARG, -13515, -3273, -22097, "ADDONWORLD.ZEN");
spawnBot(WARG, 4169, -1312, -9530, "ADDONWORLD.ZEN");
spawnBot(WARG, 3547, -1363, -7727, "ADDONWORLD.ZEN");
spawnBot(WARG, -17282, -2745, -10903, "ADDONWORLD.ZEN");

spawnBot(WARG, 1027, 2789.4, 21040.9, "COLONY.ZEN");
spawnBot(WARG, 1003, 2767.59, 21162.8, "COLONY.ZEN");
spawnBot(WARG, -12202, 150.21, -8963.49, "COLONY.ZEN");
spawnBot(WARG, 6765.08, 219.453, -13255.9, "COLONY.ZEN");
spawnBot(WARG, -21750, -377.556, -10238.9, "COLONY.ZEN");
spawnBot(WARG, -21818, -379.965, -10195, "COLONY.ZEN");
spawnBot(WARG, -632, 233.04, -15289.2, "COLONY.ZEN");
spawnBot(WARG, -778, 239.016, -15410.7, "COLONY.ZEN");
spawnBot(WARG, -1420, 330.048, -15380, "COLONY.ZEN");
spawnBot(WARG, -1853, 383.177, -15554, "COLONY.ZEN");
spawnBot(WARG, -1158, 251.262, -14621, "COLONY.ZEN");
spawnBot(WARG, -3440, 691.684, -15774.8, "COLONY.ZEN");
spawnBot(WARG, -4799, 1222.41, -16954.7, "COLONY.ZEN");
spawnBot(WARG, -4510, 2221.49, -19929, "COLONY.ZEN");
spawnBot(WARG, -6874, 1829.75, -21740, "COLONY.ZEN");
spawnBot(WARG, -1547, 3163.03, -18869.7, "COLONY.ZEN");
spawnBot(WARG, 996, 5378, -18531.7, "COLONY.ZEN");
spawnBot(WARG, 872, 5389.66, -18571, "COLONY.ZEN");
spawnBot(WARG, -1237, 6635.37, -21137, "COLONY.ZEN");
spawnBot(WARG, 2609, 7047.45, -19351.7, "COLONY.ZEN");
spawnBot(WARG, 5280, 7251.01, -18466, "COLONY.ZEN");
spawnBot(WARG, 5081.77, 8237.19, -21361.8, "COLONY.ZEN");
spawnBot(WARG, 4053.59, 8283.12, -21690.1, "COLONY.ZEN");
spawnBot(WARG, 4418, 8250.89, -20440, "COLONY.ZEN");
spawnBot(WARG, 4985, 8198.92, -20021.7, "COLONY.ZEN");
spawnBot(WARG, 3839.48, 8256.17, -19690.1, "COLONY.ZEN");
spawnBot(WARG, 2669, 8281.42, -20402, "COLONY.ZEN");
spawnBot(WARG, 3359, 8302.05, -21208, "COLONY.ZEN");
spawnBot(WARG, 3512.33, 8313.91, -20104.2, "COLONY.ZEN");
spawnBot(WARG, -12949, 521.659, -11009, "COLONY.ZEN");
spawnBot(WARG, -13015, 523.645, -10977, "COLONY.ZEN");
spawnBot(WARG, -12762, 512.285, -10996, "COLONY.ZEN");
spawnBot(WARG, -16647, 1054.04, -13654.3, "COLONY.ZEN");
spawnBot(WARG, -18405.5, 1084.92, -13269.9, "COLONY.ZEN");
spawnBot(WARG, -18000, 987.656, -15052, "COLONY.ZEN");
spawnBot(WARG, -22535.8, 1386.33, -14050.6, "COLONY.ZEN");
spawnBot(WARG, -10943, 1950.05, -18166, "COLONY.ZEN");
spawnBot(WARG, -8030, 1764.99, -15988, "COLONY.ZEN");
spawnBot(WARG, -7792.85, 1819.38, -16276.9, "COLONY.ZEN");
spawnBot(WARG, -4983, 2005.11, -19043.8, "COLONY.ZEN");
spawnBot(WARG, -3628.21, 2300.39, -20102.6, "COLONY.ZEN");
spawnBot(WARG, -11381.7, 2040.39, -34242.3, "COLONY.ZEN");
spawnBot(WARG, -12838, 2040.39, -34789.4, "COLONY.ZEN");
spawnBot(WARG, -13063, 2021.28, -34512.4, "COLONY.ZEN");
spawnBot(WARG, -20708, 2489.17, -20003, "COLONY.ZEN");
spawnBot(WARG, -21644.3, 2506.17, -20208, "COLONY.ZEN");
spawnBot(WARG, -22503, 2571.87, -20611, "COLONY.ZEN");
spawnBot(WARG, -23622, 2760.52, -21451, "COLONY.ZEN");
spawnBot(WARG, -24686.1, 2765.47, -19137.4, "COLONY.ZEN");
spawnBot(WARG, -24841, 2846.7, -18097, "COLONY.ZEN");
spawnBot(WARG, -25114, 3154.67, -23793, "COLONY.ZEN");
spawnBot(WARG, -24342, 3060.78, -23333, "COLONY.ZEN");
spawnBot(WARG, -27446, 3292.15, -24567, "COLONY.ZEN");
spawnBot(WARG, -26556, 2689.03, -26688, "COLONY.ZEN");
spawnBot(WARG, -27214, 2683.61, -27280, "COLONY.ZEN");
spawnBot(WARG, 22041, -625.24, 6917.78, "COLONY.ZEN");
spawnBot(WARG, 21529, -652.887, 4759.48, "COLONY.ZEN");
spawnBot(WARG, 21408, -665.277, 4749.41, "COLONY.ZEN");
spawnBot(WARG, -60971.3, 5315.21, -1985.89, "COLONY.ZEN");
spawnBot(WARG, -60371.9, 5296.6, -2013.05, "COLONY.ZEN");
spawnBot(WARG, -60596.6, 5229.48, -305.93, "COLONY.ZEN");