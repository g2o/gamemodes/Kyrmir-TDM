
local HARPIE = Bot.Template("HARPIE", "KYRMIR_HARPIE"); 

HARPIE.setLevel(0);
HARPIE.setMagicLevel(0);

HARPIE.setHealth(170);
HARPIE.setRespawnTime(185);

HARPIE.setDamage(DAMAGE_EDGE, 140);

HARPIE.setProtection(DAMAGE_EDGE, 0);
HARPIE.setProtection(DAMAGE_BLUNT, 500);
HARPIE.setProtection(DAMAGE_FIRE, 0);
HARPIE.setProtection(DAMAGE_MAGIC, 15);
HARPIE.setProtection(DAMAGE_FIRE, 0);

// Fight system
HARPIE.setWarnTime(6);
HARPIE.setHitDistance(190);
HARPIE.setChaseDistance(1800);
HARPIE.setDetectionDistance(1200);
HARPIE.setAttackSpeed(2100);

HARPIE.setInitiativeFunction(STANDARD_ANIMAL_AI);

HARPIE.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 3))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 1: bot.playAni("R_ROAM3"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_HARPIE", HARPIE);

// Drop
registerMonsterReward("KYRMIR_HARPIE", Reward.Content(200)
    .addDrop(Reward.Drop("ITMI_GOLD", 100, 100))
);

// Spawnlist
spawnBot(HARPIE, -7688, -4074, 26895, "ADDONWORLD.ZEN");
spawnBot(HARPIE, -8039, -4154, 27859, "ADDONWORLD.ZEN");
spawnBot(HARPIE, 2999, 38, -2995, "ADDONWORLD.ZEN");

spawnBot(HARPIE, 22208, 8439.92, -36141.9, "COLONY.ZEN");
spawnBot(HARPIE, 9913, 7444.77, -27352.9, "COLONY.ZEN");
spawnBot(HARPIE, 17603, 5981.75, -30472.4, "COLONY.ZEN");
spawnBot(HARPIE, 16957, 6023.64, -30580.4, "COLONY.ZEN");
spawnBot(HARPIE, 17803, 5787.65, -32085.3, "COLONY.ZEN");
spawnBot(HARPIE, 18474, 5334.7, -33744.7, "COLONY.ZEN");
spawnBot(HARPIE, 17557, 5290.54, -34218, "COLONY.ZEN");
spawnBot(HARPIE, 18168, 6026.48, -35725.9, "COLONY.ZEN");
spawnBot(HARPIE, 17929, 6072.46, -36073.3, "COLONY.ZEN");
spawnBot(HARPIE, 18978, 6995.65, -36072.9, "COLONY.ZEN");
spawnBot(HARPIE, 19014, 7003.33, -36198.9, "COLONY.ZEN");
spawnBot(HARPIE, 18852, 6975.68, -36295.7, "COLONY.ZEN");
spawnBot(HARPIE, 20896, 7922.03, -35856, "COLONY.ZEN");
spawnBot(HARPIE, 20385, 7893.37, -36019.7, "COLONY.ZEN");
spawnBot(HARPIE, 20588, 7747.39, -35131.6, "COLONY.ZEN");
spawnBot(HARPIE, 20929, 7731.04, -34397.4, "COLONY.ZEN");
spawnBot(HARPIE, 19343, 7992.71, -38599.3, "COLONY.ZEN");
spawnBot(HARPIE, 21133, 7992.71, -39052.4, "COLONY.ZEN");
spawnBot(HARPIE, 21687, 7992.66, -38012.4, "COLONY.ZEN");
spawnBot(HARPIE, 22186, 7992.66, -39230.4, "COLONY.ZEN");
spawnBot(HARPIE, 17900, 6515.91, -37288.6, "COLONY.ZEN");
spawnBot(HARPIE, 47184, -3582.76, -30374, "COLONY.ZEN");
spawnBot(HARPIE, 48583.3, -3700.2, -29464, "COLONY.ZEN");
