
local RAZOR = Bot.Template("RAZOR", "KYRMIR_RAZOR"); 

RAZOR.setLevel(0);
RAZOR.setMagicLevel(0);

RAZOR.setHealth(200);
RAZOR.setRespawnTime(270);

RAZOR.setDamage(DAMAGE_EDGE, 140);

RAZOR.setProtection(DAMAGE_EDGE, 0);
RAZOR.setProtection(DAMAGE_BLUNT, 500);
RAZOR.setProtection(DAMAGE_FIRE, 0);
RAZOR.setProtection(DAMAGE_MAGIC, 25);
RAZOR.setProtection(DAMAGE_POINT, 35);

// Fight system
RAZOR.setWarnTime(6);
RAZOR.setHitDistance(150);
RAZOR.setChaseDistance(1800);
RAZOR.setDetectionDistance(1200);
RAZOR.setAttackSpeed(1800);

RAZOR.setInitiativeFunction(STANDARD_ANIMAL_AI);

RAZOR.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_RAZOR", RAZOR);

// Drop
registerMonsterReward("KYRMIR_RAZOR", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
spawnBot(RAZOR, 542.024, 2803.18, 20849.8, "COLONY.ZEN");
spawnBot(RAZOR, 625.874, 2820.48, 20752.2, "COLONY.ZEN");
spawnBot(RAZOR, -10747.9, 5643.23, 31862.5, "COLONY.ZEN");
spawnBot(RAZOR, -6543.94, 2127.74, -28269.6, "COLONY.ZEN");
spawnBot(RAZOR, -6465.93, 2139.68, -28210, "COLONY.ZEN");
spawnBot(RAZOR, -5817.86, 2167.12, -27377.1, "COLONY.ZEN");
spawnBot(RAZOR, 539.023, 2824.12, 21981.5, "COLONY.ZEN");
spawnBot(RAZOR, 397.98, 2791.81, 22023.9, "COLONY.ZEN");
spawnBot(RAZOR, 437.048, 2787.71, 20992.1, "COLONY.ZEN");
spawnBot(RAZOR, 306.981, 2772.45, 22027.9, "COLONY.ZEN");
spawnBot(RAZOR, -6508, 4868.74, 30688, "COLONY.ZEN");
spawnBot(RAZOR, -6986.18, 4835.86, 30400.1, "COLONY.ZEN");
spawnBot(RAZOR, 1249.12, 5889.23, -31829.4, "COLONY.ZEN");
spawnBot(RAZOR, 1727, 5915.35, -31864, "COLONY.ZEN");
spawnBot(RAZOR, 487.353, 5739.22, -26591.1, "COLONY.ZEN");
spawnBot(RAZOR, 337, 5640.78, -25645, "COLONY.ZEN");
spawnBot(RAZOR, -214.013, 5200.83, -26014, "COLONY.ZEN");
spawnBot(RAZOR, 811.598, 5722.19, -25812.4, "COLONY.ZEN");
spawnBot(RAZOR, -12846, 2180.49, -21158.7, "COLONY.ZEN");
spawnBot(RAZOR, -11180, 2723.28, -19451, "COLONY.ZEN");
spawnBot(RAZOR, -10189.4, 3109.53, -20484.3, "COLONY.ZEN");
spawnBot(RAZOR, -13509, 885.138, -25909, "COLONY.ZEN");
spawnBot(RAZOR, -13984, 697.848, -27247.9, "COLONY.ZEN");
spawnBot(RAZOR, -14802, 837.954, -27004, "COLONY.ZEN");
spawnBot(RAZOR, -14922, 846.222, -26769, "COLONY.ZEN");
spawnBot(RAZOR, -8217, 1644.19, -27252, "COLONY.ZEN");
spawnBot(RAZOR, 23333, 273.175, 26239.7, "COLONY.ZEN");
spawnBot(RAZOR, 23063.1, 258.968, 26163.7, "COLONY.ZEN");
spawnBot(RAZOR, 36731.4, 275.159, -24771.2, "COLONY.ZEN");
spawnBot(RAZOR, 36520.9, 271.495, -24751.1, "COLONY.ZEN");
spawnBot(RAZOR, 36087, 317.987, -24908.8, "COLONY.ZEN");
