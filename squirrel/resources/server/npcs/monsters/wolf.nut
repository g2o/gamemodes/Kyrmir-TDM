
local WOLF = Bot.Template("WOLF", "KYRMIR_WOLF"); 

WOLF.setLevel(0);
WOLF.setMagicLevel(0);

WOLF.setHealth(75);
WOLF.setRespawnTime(100);

WOLF.setDamage(DAMAGE_EDGE, 50);

WOLF.setProtection(DAMAGE_EDGE, 0);
WOLF.setProtection(DAMAGE_BLUNT, 500);
WOLF.setProtection(DAMAGE_FIRE, 0);
WOLF.setProtection(DAMAGE_MAGIC, 10);
WOLF.setProtection(DAMAGE_POINT, 20);

// Fight system
WOLF.setWarnTime(6);
WOLF.setHitDistance(200);
WOLF.setChaseDistance(1800);
WOLF.setDetectionDistance(1200);
WOLF.setAttackSpeed(1800);

WOLF.setInitiativeFunction(STANDARD_ANIMAL_AI);

WOLF.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WOLF", WOLF);

// Drop
registerMonsterReward("KYRMIR_WOLF", Reward.Content(100)
    .addDrop(Reward.Drop("ITMI_GOLD", 50, 50))
);

// Spawnlist
spawnBot(WOLF, -23594, -879, 15464, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23279, -953, 15742, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22013, -1230, 16571, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22885, -1074, 17563, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23093, -1063, 17824, "ADDONWORLD.ZEN");
spawnBot(WOLF, -24774, -318, 10054, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23840, -246, 9514, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22723, -269, 8993, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23033, -2462, 5567, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22895, -2292, 5215, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23081, -2935, 6813, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16833, -830, 1802, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16744, -684, 1621, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16663, -712, 1706, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16237, -772, 2049, "ADDONWORLD.ZEN");
spawnBot(WOLF, -19529, -2216, 17946, "ADDONWORLD.ZEN");
spawnBot(WOLF, -20003, -2223, 18078, "ADDONWORLD.ZEN");
spawnBot(WOLF, -19547, -2205, 18517, "ADDONWORLD.ZEN");
spawnBot(WOLF, -21480, -3718, -11990, "ADDONWORLD.ZEN");
spawnBot(WOLF, -21263, -3758, -11905, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22567, -3980, -10267, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22581, -4244, -9119, "ADDONWORLD.ZEN");

spawnBot(WOLF, 1332, -730, -10763, "COLONY.ZEN");
spawnBot(WOLF, -2952, -431, -11533, "COLONY.ZEN");
spawnBot(WOLF, -18489, -173, 4736, "COLONY.ZEN");
spawnBot(WOLF, -20555, -886, -7226, "COLONY.ZEN");
spawnBot(WOLF, -20263, -981, -6832, "COLONY.ZEN");
spawnBot(WOLF, -20373, -993, -7507, "COLONY.ZEN");
spawnBot(WOLF, 2090, -691, -10481, "COLONY.ZEN");
spawnBot(WOLF, 12157, 225, -1183, "COLONY.ZEN");
spawnBot(WOLF, 14097, 278, -48, "COLONY.ZEN");
spawnBot(WOLF, 11927, 334, 656, "COLONY.ZEN");
spawnBot(WOLF, 11296, 288, 1334, "COLONY.ZEN");
spawnBot(WOLF, 13891, 176, 3576, "COLONY.ZEN");
spawnBot(WOLF, 13691.3, 137.454, 5778.94, "COLONY.ZEN");
spawnBot(WOLF, 12242, -451, 8105, "COLONY.ZEN");
spawnBot(WOLF, 11005, -811, 7988, "COLONY.ZEN");
spawnBot(WOLF, 15463, -13, 7393, "COLONY.ZEN");
spawnBot(WOLF, 15936, -24, 7728, "COLONY.ZEN");
spawnBot(WOLF, 20357, -606, 2213, "COLONY.ZEN");
spawnBot(WOLF, 19769, -1039, 3806, "COLONY.ZEN");
spawnBot(WOLF, 21643, -453, 2563, "COLONY.ZEN");
spawnBot(WOLF, 20928, -561, 2928, "COLONY.ZEN");
spawnBot(WOLF, -9789, -746, 10775, "COLONY.ZEN");
spawnBot(WOLF, -13772, -719, 15630, "COLONY.ZEN");
spawnBot(WOLF, -14153, -734, 16452, "COLONY.ZEN");
spawnBot(WOLF, -14593, -779, 16446, "COLONY.ZEN");
spawnBot(WOLF, -16322, 169, 18472, "COLONY.ZEN");
spawnBot(WOLF, -17433, 104, 20022, "COLONY.ZEN");
spawnBot(WOLF, -18437, -866, 17099, "COLONY.ZEN");
spawnBot(WOLF, -20119, -16, 19310, "COLONY.ZEN");
spawnBot(WOLF, -24935, 625, -6259, "COLONY.ZEN");
spawnBot(WOLF, -22823, 516, -4287, "COLONY.ZEN");
spawnBot(WOLF, -21809, 572, -4572, "COLONY.ZEN");
spawnBot(WOLF, -20720, 579, -3413, "COLONY.ZEN");
spawnBot(WOLF, -19928, -8, -576, "COLONY.ZEN");
spawnBot(WOLF, -23752, -254, 2050, "COLONY.ZEN");
spawnBot(WOLF, -23632, -287, 2840, "COLONY.ZEN");
spawnBot(WOLF, -24422, -290, 1948, "COLONY.ZEN");
spawnBot(WOLF, -27495, -153, 2285, "COLONY.ZEN");
spawnBot(WOLF, -18470, -90, 4334, "COLONY.ZEN");
spawnBot(WOLF, 3736, 711, -13862, "COLONY.ZEN");
spawnBot(WOLF, 22528, -585, 11755, "COLONY.ZEN");
spawnBot(WOLF, 23581, -379, 12014, "COLONY.ZEN");
spawnBot(WOLF, 24026, -455, 9407, "COLONY.ZEN");
spawnBot(WOLF, 24069, -449, 9462, "COLONY.ZEN");
spawnBot(WOLF, 24000, -521, 8838, "COLONY.ZEN");
spawnBot(WOLF, 24408, -179, 6834, "COLONY.ZEN");
spawnBot(WOLF, 24224, -183, 6507, "COLONY.ZEN");
spawnBot(WOLF, 26141, 30, 5648, "COLONY.ZEN");
spawnBot(WOLF, 26204, -132, 6721, "COLONY.ZEN");
spawnBot(WOLF, 30057.3, -27.23, 5314.16, "COLONY.ZEN");
spawnBot(WOLF, 29854.2, -40.0968, 5637.73, "COLONY.ZEN");
spawnBot(WOLF, 39354, -204, 5002, "COLONY.ZEN");
spawnBot(WOLF, 39465, -198, 4464, "COLONY.ZEN");
spawnBot(WOLF, 39393, -184, 4545, "COLONY.ZEN");
spawnBot(WOLF, 39950.7, -42.9518, 1541.82, "COLONY.ZEN");
spawnBot(WOLF, 40103.3, -6.82261, 2157, "COLONY.ZEN");
spawnBot(WOLF, 18524.6, 114.794, -818.917, "COLONY.ZEN");
spawnBot(WOLF, 17347, 275, -1654, "COLONY.ZEN");
spawnBot(WOLF, 17216, 120, -1391, "COLONY.ZEN");
spawnBot(WOLF, 19233, 1105, -5270, "COLONY.ZEN");
spawnBot(WOLF, 16817, 1123, -8020, "COLONY.ZEN");
spawnBot(WOLF, 17960, 1198, -5616, "COLONY.ZEN");
spawnBot(WOLF, 16180, 1008, -5297, "COLONY.ZEN");
spawnBot(WOLF, 14368, 756, -7488, "COLONY.ZEN");
spawnBot(WOLF, 15600, 999, -8278, "COLONY.ZEN");
spawnBot(WOLF, 13114, 1038, -15141, "COLONY.ZEN");
spawnBot(WOLF, 17696, 1966, -11739, "COLONY.ZEN");
spawnBot(WOLF, 17565, 1969, -11709, "COLONY.ZEN");
spawnBot(WOLF, 19500, 1406, -15859, "COLONY.ZEN");
spawnBot(WOLF, 24769, 540.301, -6689.97, "COLONY.ZEN");
spawnBot(WOLF, 24586, 591, -7670, "COLONY.ZEN");
spawnBot(WOLF, 23174, 681, -6160, "COLONY.ZEN");
spawnBot(WOLF, 21730, 1090, -7001, "COLONY.ZEN");
spawnBot(WOLF, 25309, 3919, -23958, "COLONY.ZEN");
spawnBot(WOLF, 25231, 3948, -24029, "COLONY.ZEN");
spawnBot(WOLF, -43031, 3403, -2105, "COLONY.ZEN");
spawnBot(WOLF, -42828, 3335, -1703, "COLONY.ZEN");
spawnBot(WOLF, -43488, 3430, -1506, "COLONY.ZEN");
spawnBot(WOLF, -58915, 5821, 7607, "COLONY.ZEN");
spawnBot(WOLF, -58911, 5857, 6754, "COLONY.ZEN");
spawnBot(WOLF, -57611, 5931, 6523, "COLONY.ZEN");
spawnBot(WOLF, -58444, 5906, 7336, "COLONY.ZEN");
spawnBot(WOLF, -58464, 5834, 7944, "COLONY.ZEN");
spawnBot(WOLF, -57448, 5709, 8350, "COLONY.ZEN");
spawnBot(WOLF, -56935, 5611, 8385, "COLONY.ZEN");
spawnBot(WOLF, -57466, 5600, 8949, "COLONY.ZEN");
spawnBot(WOLF, -43185, 3416, -1902, "COLONY.ZEN");
spawnBot(WOLF, -43532, 3427, -2090, "COLONY.ZEN");
spawnBot(WOLF, -43560, 3427, -2163, "COLONY.ZEN");
