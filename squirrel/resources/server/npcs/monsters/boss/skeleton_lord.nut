
local SKELETON_LORD = Bot.Template("SKELETON_LORD", "KYRMIR_BOSS_GREEN"); 

SKELETON_LORD.setLevel(0);
SKELETON_LORD.setMagicLevel(6);

SKELETON_LORD.setHealth(30000);
SKELETON_LORD.setDamage(DAMAGE_EDGE, 180);
SKELETON_LORD.setRespawnTime(1800)

SKELETON_LORD.setProtection(DAMAGE_EDGE, 0);
SKELETON_LORD.setProtection(DAMAGE_BLUNT, 500);
SKELETON_LORD.setProtection(DAMAGE_FIRE, 0);
SKELETON_LORD.setProtection(DAMAGE_MAGIC, 0);
SKELETON_LORD.setProtection(DAMAGE_FIRE, 0);

// Fight system
SKELETON_LORD.setWarnTime(6);
SKELETON_LORD.setHitDistance(240);
SKELETON_LORD.setChaseDistance(3600);
SKELETON_LORD.setDetectionDistance(1200);
SKELETON_LORD.setAttackSpeed(2100);

SKELETON_LORD.setInitiativeFunction(TWO_HAND_AI);

SKELETON_LORD.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_GREEN", SKELETON_LORD);

// Drop
registerMonsterReward("KYRMIR_BOSS_GREEN", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITRW_BOW_11", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(SKELETON_LORD, 28324.1, -2495.47, 6856.95, "COLONY.ZEN"));
registerBoss(spawnBot(SKELETON_LORD, -2246, -3185, 31006, "ADDONWORLD.ZEN"));
