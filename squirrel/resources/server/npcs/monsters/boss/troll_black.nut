
local TROLL_BLACK = Bot.Template("TROLL_BLACK", "KYRMIR_BOSS_BLACK"); 

TROLL_BLACK.setLevel(0);
TROLL_BLACK.setMagicLevel(6);

TROLL_BLACK.setHealth(30000);
TROLL_BLACK.setDamage(DAMAGE_EDGE, 270);
TROLL_BLACK.setRespawnTime(1800)

TROLL_BLACK.setProtection(DAMAGE_EDGE, 0);
TROLL_BLACK.setProtection(DAMAGE_BLUNT, 500);
TROLL_BLACK.setProtection(DAMAGE_FIRE, 0);
TROLL_BLACK.setProtection(DAMAGE_MAGIC, 0);
TROLL_BLACK.setProtection(DAMAGE_FIRE, 0);

// Fight system
TROLL_BLACK.setWarnTime(6);
TROLL_BLACK.setHitDistance(340);
TROLL_BLACK.setChaseDistance(3600);
TROLL_BLACK.setDetectionDistance(2100);
TROLL_BLACK.setAttackSpeed(2100);

TROLL_BLACK.setInitiativeFunction(STONEGOLEM_AI);

TROLL_BLACK.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 3))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_BLACK", TROLL_BLACK);

// Drop
registerMonsterReward("KYRMIR_BOSS_BLACK", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITAR_ONB_10", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "SWAMP_CAMP";
        })    
    )
    .addDrop(Reward.Drop("ITAR_SO_10", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "OLD_CAMP";
        })   
    )
    .addDrop(Reward.Drop("ITAR_NO_10", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "NEW_CAMP";
        })    
    )
    .addDrop(Reward.Drop("ITAR_PIR_10", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "PIRATE_CAMP";
        })
    )  
    .addDrop(Reward.Drop("ITAR_BDT_10", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "BANDIT_CAMP";
        })
    )  
);

// Spawnlist
registerBoss(spawnBot(TROLL_BLACK, -18159.6, 3705.86, 34323.9, "COLONY.ZEN"));
registerBoss(spawnBot(TROLL_BLACK, -51, -965, -6023, "ADDONWORLD.ZEN"));