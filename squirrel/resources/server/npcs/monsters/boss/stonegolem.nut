
local STONEGOLEM = Bot.Template("GOLEM_BLUE", "KYRMIR_BOSS_BLUE"); 

STONEGOLEM.setLevel(0);
STONEGOLEM.setMagicLevel(6);

STONEGOLEM.setHealth(30000);
STONEGOLEM.setDamage(DAMAGE_EDGE, 180);
STONEGOLEM.setRespawnTime(1800);

STONEGOLEM.setProtection(DAMAGE_EDGE, 0);
STONEGOLEM.setProtection(DAMAGE_BLUNT, 500);
STONEGOLEM.setProtection(DAMAGE_FIRE, 0);
STONEGOLEM.setProtection(DAMAGE_MAGIC, 0);
STONEGOLEM.setProtection(DAMAGE_FIRE, 0);

// Fight system
STONEGOLEM.setWarnTime(6);
STONEGOLEM.setHitDistance(240);
STONEGOLEM.setChaseDistance(3600);
STONEGOLEM.setDetectionDistance(1200);
STONEGOLEM.setAttackSpeed(3100);

STONEGOLEM.setInitiativeFunction(STONEGOLEM_AI);

STONEGOLEM.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_BLUE", STONEGOLEM);

// Drop
registerMonsterReward("KYRMIR_BOSS_BLUE", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITMW_2H_11", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(STONEGOLEM, -23296.2, 257.578, 28408, "COLONY.ZEN"));
registerBoss(spawnBot(STONEGOLEM, -12845, -4290, -29967, "ADDONWORLD.ZEN"));
