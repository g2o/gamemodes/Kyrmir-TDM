
local FIREWARAN = Bot.Template("MINECRAWLERWARRIOR_PINK", "KYRMIR_MINIBOSS_VIOLET"); 

FIREWARAN.setLevel(0);
FIREWARAN.setMagicLevel(6);

FIREWARAN.setHealth(2000);
FIREWARAN.setDamage(DAMAGE_EDGE, 150);
FIREWARAN.setRespawnTime(900)

FIREWARAN.setProtection(DAMAGE_EDGE, 0);
FIREWARAN.setProtection(DAMAGE_BLUNT, 500);
FIREWARAN.setProtection(DAMAGE_FIRE, 0);
FIREWARAN.setProtection(DAMAGE_MAGIC, 0);
FIREWARAN.setProtection(DAMAGE_FIRE, 0);

// Fight system
FIREWARAN.setWarnTime(6);
FIREWARAN.setHitDistance(200);
FIREWARAN.setChaseDistance(3600);
FIREWARAN.setDetectionDistance(1200);
FIREWARAN.setAttackSpeed(2100);
FIREWARAN.setInitiativeFunction(STANDARD_ANIMAL_AI);

FIREWARAN.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

FIREWARAN.setRespawnRequirement(function() 
{
    local artifact = getArtifactByInstance("ITMI_FOCUS_VIOLET");
    
    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterTemplate("KYRMIR_MINIBOSS_VIOLET", FIREWARAN);

// Drop
registerMonsterReward("KYRMIR_MINIBOSS_VIOLET", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_VIOLET", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(FIREWARAN, -33215, 2791, -18072, "COLONY.ZEN"));
registerBoss(spawnBot(FIREWARAN, 7612, -3588, 19709, "ADDONWORLD.ZEN"));