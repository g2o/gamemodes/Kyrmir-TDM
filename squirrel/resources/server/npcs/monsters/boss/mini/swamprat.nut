
local SWAMPRAT = Bot.Template("RAZOR_GREY", "KYRMIR_MINIBOSS_GRAY"); 

SWAMPRAT.setLevel(0);
SWAMPRAT.setMagicLevel(6);

SWAMPRAT.setHealth(2000);
SWAMPRAT.setDamage(DAMAGE_EDGE, 150);
SWAMPRAT.setRespawnTime(900)

SWAMPRAT.setProtection(DAMAGE_EDGE, 0);
SWAMPRAT.setProtection(DAMAGE_BLUNT, 500);
SWAMPRAT.setProtection(DAMAGE_FIRE, 0);
SWAMPRAT.setProtection(DAMAGE_MAGIC, 0);
SWAMPRAT.setProtection(DAMAGE_FIRE, 0);

// Fight system
SWAMPRAT.setWarnTime(6);
SWAMPRAT.setHitDistance(200);
SWAMPRAT.setChaseDistance(3600);
SWAMPRAT.setDetectionDistance(1200);
SWAMPRAT.setAttackSpeed(2100);
SWAMPRAT.setInitiativeFunction(STANDARD_ANIMAL_AI);

SWAMPRAT.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

SWAMPRAT.setRespawnRequirement(function() {
    local artifact = getArtifactByInstance("ITMI_FOCUS_GRAY");
    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterReward("KYRMIR_MINIBOSS_GRAY", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_GRAY", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(SWAMPRAT, 36555, 248, -24938, "COLONY.ZEN"));
registerBoss(spawnBot(SWAMPRAT, -20826, -328, -2607, "ADDONWORLD.ZEN"));
