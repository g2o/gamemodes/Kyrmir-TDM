
local DRAGONSNAPPER = Bot.Template("SNAPPER_YELLOW", "KYRMIR_MINIBOSS_YELLOW"); 

DRAGONSNAPPER.setLevel(0);
DRAGONSNAPPER.setMagicLevel(6);

DRAGONSNAPPER.setHealth(2000);
DRAGONSNAPPER.setDamage(DAMAGE_EDGE, 150);
DRAGONSNAPPER.setRespawnTime(900)

DRAGONSNAPPER.setProtection(DAMAGE_EDGE, 0);
DRAGONSNAPPER.setProtection(DAMAGE_BLUNT, 500);
DRAGONSNAPPER.setProtection(DAMAGE_FIRE, 0);
DRAGONSNAPPER.setProtection(DAMAGE_MAGIC, 0);
DRAGONSNAPPER.setProtection(DAMAGE_FIRE, 0);

// Fight system
DRAGONSNAPPER.setWarnTime(6);
DRAGONSNAPPER.setHitDistance(200);
DRAGONSNAPPER.setChaseDistance(3600);
DRAGONSNAPPER.setDetectionDistance(1200);
DRAGONSNAPPER.setAttackSpeed(2100);
DRAGONSNAPPER.setInitiativeFunction(STANDARD_ANIMAL_AI);

DRAGONSNAPPER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

DRAGONSNAPPER.setRespawnRequirement(function() 
{
    local artifact = getArtifactByInstance("ITMI_FOCUS_YELLOW");

    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterTemplate("KYRMIR_MINIBOSS_YELLOW", DRAGONSNAPPER);

// Drop
registerMonsterReward("KYRMIR_MINIBOSS_YELLOW", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_YELLOW", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(DRAGONSNAPPER, 23096, 262, 26551, "COLONY.ZEN"));
registerBoss(spawnBot(DRAGONSNAPPER, 6872, -4474, -8123, "ADDONWORLD.ZEN"));
