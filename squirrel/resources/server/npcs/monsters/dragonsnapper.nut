
local DRAGONSNAPPER = Bot.Template("DRAGONSNAPPER", "KYRMIR_DRAGONSNAPPER"); 

DRAGONSNAPPER.setLevel(0);
DRAGONSNAPPER.setMagicLevel(0);

DRAGONSNAPPER.setHealth(250);
DRAGONSNAPPER.setRespawnTime(260);

DRAGONSNAPPER.setDamage(DAMAGE_EDGE, 150);

DRAGONSNAPPER.setProtection(DAMAGE_EDGE, 0);
DRAGONSNAPPER.setProtection(DAMAGE_BLUNT, 500);
DRAGONSNAPPER.setProtection(DAMAGE_FIRE, 0);
DRAGONSNAPPER.setProtection(DAMAGE_MAGIC, 30);
DRAGONSNAPPER.setProtection(DAMAGE_POINT, 35);

// Fight system
DRAGONSNAPPER.setWarnTime(6);
DRAGONSNAPPER.setHitDistance(150);
DRAGONSNAPPER.setChaseDistance(1800);
DRAGONSNAPPER.setDetectionDistance(1200);
DRAGONSNAPPER.setAttackSpeed(2100);

DRAGONSNAPPER.setInitiativeFunction(STANDARD_ANIMAL_AI);

DRAGONSNAPPER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_DRAGONSNAPPER", DRAGONSNAPPER);

// Drop
registerMonsterReward("KYRMIR_DRAGONSNAPPER", Reward.Content(440)
    .addDrop(Reward.Drop("ITMI_GOLD", 220, 220))
);

// Spawnlist
spawnBot(DRAGONSNAPPER, -22547, -2899, -4573, "ADDONWORLD.ZEN");
spawnBot(DRAGONSNAPPER, -23140, -2890, -4502, "ADDONWORLD.ZEN");
spawnBot(DRAGONSNAPPER, -22192, -2636, -3118, "ADDONWORLD.ZEN");
spawnBot(DRAGONSNAPPER, -13554, -8301, -17887, "ADDONWORLD.ZEN");
spawnBot(DRAGONSNAPPER, -10687, -5205, -18851, "ADDONWORLD.ZEN");
spawnBot(DRAGONSNAPPER, -11272, -5191, -18772, "ADDONWORLD.ZEN");
