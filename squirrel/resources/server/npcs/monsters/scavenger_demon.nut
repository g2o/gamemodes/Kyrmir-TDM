
local SCAVENGER_DEMON = Bot.Template("SCAVENGER_DEMON", "KYRMIR_SCAVENGER_DEMON"); 

SCAVENGER_DEMON.setLevel(0);
SCAVENGER_DEMON.setMagicLevel(0);

SCAVENGER_DEMON.setHealth(50);
SCAVENGER_DEMON.setRespawnTime(60);

SCAVENGER_DEMON.setDamage(DAMAGE_EDGE, 25);

SCAVENGER_DEMON.setProtection(DAMAGE_EDGE, 0);
SCAVENGER_DEMON.setProtection(DAMAGE_BLUNT, 500);
SCAVENGER_DEMON.setProtection(DAMAGE_FIRE, 0);
SCAVENGER_DEMON.setProtection(DAMAGE_MAGIC, 10);
SCAVENGER_DEMON.setProtection(DAMAGE_POINT, 20);

// Fight system
SCAVENGER_DEMON.setWarnTime(6);
SCAVENGER_DEMON.setHitDistance(210);
SCAVENGER_DEMON.setChaseDistance(1800);
SCAVENGER_DEMON.setDetectionDistance(1200);
SCAVENGER_DEMON.setAttackSpeed(1800);

SCAVENGER_DEMON.setInitiativeFunction(STANDARD_ANIMAL_AI);

SCAVENGER_DEMON.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_SCAVENGER_DEMON", SCAVENGER_DEMON);

// Drop
registerMonsterReward("KYRMIR_SCAVENGER_DEMON", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(SCAVENGER_DEMON, 23656, -5160, 5474, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 24677, -4995, 6978, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 23221, -5134, 5970, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 21586, -5147, 4414, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 22075, -5106, 3905, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 20345, -5176, 3310, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 19906, -5082, 3096, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 19898, -5251, 1440, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 20873, -5152, 6009, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 21953, -5085, 7002, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 22546, -5143, 6674, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 19326, -5206, 8577, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 18368, -5089, 8452, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 19087, -5115, 9285, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 18848, -5120, 9996, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 24095, -5090, 5255, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 25431, -5048, 4338, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 22188, -5073, 5212, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 22056, -4120, -11078, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 20740, -4145, -11468, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 17080, -4899, -9565, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 15561, -5071, -9023, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 13746, -5151, -9406, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 11248, -4383, -6390, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 13990, -5051, -4837, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 16261, -4998, -951, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 25597, -4952, 5175, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 25004, -4932, 7540, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 26190, -4849, 6452, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 21938, -5067, 6124, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 21821, -5223, 3516, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER_DEMON, 22796, -5079, -353, "ADDONWORLD.ZEN");
