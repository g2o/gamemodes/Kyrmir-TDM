
local NPC_ARMOR = createNpc("NPC_ARMOR", "PC_HERO", "COLONY.ZEN");

NPC_ARMOR.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Pony", 20);
NPC_ARMOR.equipArmor(Items.id("SLD_ARMOR_G1_H"));

NPC_ARMOR.playAni("S_HGUARD");

NPC_ARMOR.setPosition(-46929, 1788, 13081);
NPC_ARMOR.setAngle(32);
NPC_ARMOR.setCollision(false);

NPC_ARMOR.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARMOR_TRADE = Dialog.TradeOption("NPC_ARMOR_INITIAL", "ITMI_GOLD")
    .addItem("ITAR_NO_01", 200)
    .addItem("ITAR_NO_02", 400)
    .addItem("ITAR_NO_03", 800)
    .addItem("ITAR_NO_04", 1600)
    .addItem("ITAR_NO_05", 3200)
    .addItem("ITAR_NO_06", 6400)
    .addItem("ITAR_NO_07", 800)
    .addItem("ITAR_NO_08", 2400);

registerDialogNpc(NPC_ARMOR.getId(), function(playerId) {
    return NPC_ARMOR_TRADE;
});
