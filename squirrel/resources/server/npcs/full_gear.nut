
local gearArray = 
[
    [ getItemTemplateIdByInstance("ITMW_1H_11"), 1 ],
    [ getItemTemplateIdByInstance("ITMW_2H_10"), 1 ],
    [ getItemTemplateIdByInstance("ITMW_RAPIER_01"), 1 ],
    [ getItemTemplateIdByInstance("ITRW_BOW_10"), 1 ],
    [ getItemTemplateIdByInstance("ITRW_CROSSBOW_10"), 1 ],
    [ getItemTemplateIdByInstance("ITRW_BOW_ARROW"), 1000 ],
    [ getItemTemplateIdByInstance("ITRW_CROSSBOW_BOLT"), 1000 ],
    [ getItemTemplateIdByInstance("ITAR_NO_06"), 1 ],
    [ getItemTemplateIdByInstance("ITAR_SO_06"), 1 ],
    [ getItemTemplateIdByInstance("ITAR_ONB_06"), 1 ],
    [ getItemTemplateIdByInstance("ITPO_HEALTH_04"), 1000 ],
    // CLAN ARMORS
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_CR"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_SI"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_NG"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_SG"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_SKG"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_KM"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_OM"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_TW"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_D"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_ZZ"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_E"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_NG"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_BW"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_BZN"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_ARMOR_BA"), 1 ],
    // CLAN HELMETS
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_NG"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_BZN"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_BA"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_E"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_CR"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_ZZ"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_D"), 1 ],
    [ getItemTemplateIdByInstance("KYRMIR_HELMET_SG"), 1 ],
];

function give_gear(playerId)
{
    if(getPlayerInstance(playerId) != "PC_HERO") 
    {
        setPlayerInstance(playerId, "PC_HERO");
    }
    
    setPlayerLevel(playerId, 10)
    setPlayerLearnPoints(playerId, 0);

    setPlayerExperience(playerId, 0);
    setPlayerExperienceNextLevel(playerId, -1);

    setPlayerMaxHealth(playerId, 300);
    setPlayerHealth(playerId, 300);   

    setPlayerStrength(playerId, 100);
    setPlayerDexterity(playerId,  100);

    setPlayerSkillWeapon(playerId, WEAPON_1H, 100);
    setPlayerSkillWeapon(playerId, WEAPON_2H, 100);
    setPlayerSkillWeapon(playerId, WEAPON_BOW, 100);
    setPlayerSkillWeapon(playerId, WEAPON_CBOW, 100);
    
    giveItemMultiple(playerId, gearArray);
}

function take_gear(playerId)
{
    foreach(item in gearArray)
    {
        removeItem(playerId, item[0], item[1]);
    }
}
