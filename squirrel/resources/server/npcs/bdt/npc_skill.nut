
local NPC_SKILL = createNpc("NPC_SKILL", "PC_HERO", "ADDONWORLD.ZEN");

NPC_SKILL.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Thief", 2);
NPC_SKILL.equipArmor(Items.id("ITAR_RAVEN_ADDON"));
NPC_SKILL.equipMeleeWeapon(Items.id("ITMW_2H_SPECIAL_04"));

NPC_SKILL.playAni("S_HGUARD");

NPC_SKILL.setPosition(31545, -4462, 9975);
NPC_SKILL.setAngle(265);
NPC_SKILL.setCollision(false);

NPC_SKILL.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_SKILL_LIST = Dialog.List();

local NPC_SKILL_LIST_LEARN_1H = Dialog.List();
local NPC_SKILL_LIST_LEARN_2H = Dialog.List();
local NPC_SKILL_LIST_LEARN_BOW = Dialog.List();
local NPC_SKILL_LIST_LEARN_CBOW = Dialog.List();
local NPC_SKILL_LIST_LEARN_STRENGTH = Dialog.List();
local NPC_SKILL_LIST_LEARN_DEXTERITY = Dialog.List();
local NPC_SKILL_LIST_LEARN_MANA = Dialog.List();
local NPC_SKILL_LIST_LEARN_MAGIC = Dialog.List();

/********************* LIST 1H *********************/

NPC_SKILL_LIST_LEARN_1H
.addOption(Dialog.Option("NPC_SKILL_1H_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerSkillWeapon(playerId, WEAPON_1H) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerSkillWeapon(playerId, WEAPON_1H, getPlayerSkillWeapon(playerId, WEAPON_1H) + 5);
    })
)
.addOption(Dialog.Option("NPC_SKILL_1H_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_SKILL_1H_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerSkillWeapon(playerId, WEAPON_1H) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerSkillWeapon(playerId, WEAPON_1H, getPlayerSkillWeapon(playerId, WEAPON_1H) + 10);
    })
)
.addOption(Dialog.Option("NPC_SKILL_1H_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_SKILL_1H_END"));

/********************* LIST 2H *********************/

NPC_SKILL_LIST_LEARN_2H
.addOption(Dialog.Option("NPC_SKILL_2H_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerSkillWeapon(playerId, WEAPON_2H) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerSkillWeapon(playerId, WEAPON_2H, getPlayerSkillWeapon(playerId, WEAPON_2H) + 5);
    })
)
.addOption(Dialog.Option("NPC_SKILL_2H_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_SKILL_2H_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerSkillWeapon(playerId, WEAPON_2H) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerSkillWeapon(playerId, WEAPON_2H, getPlayerSkillWeapon(playerId, WEAPON_2H) + 10);
    })
)
.addOption(Dialog.Option("NPC_SKILL_2H_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_SKILL_2H_END"));

/********************* LIST BOW *********************/

NPC_SKILL_LIST_LEARN_BOW
.addOption(Dialog.Option("NPC_SKILL_BOW_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerSkillWeapon(playerId, WEAPON_BOW) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerSkillWeapon(playerId, WEAPON_BOW, getPlayerSkillWeapon(playerId, WEAPON_BOW) + 5);
    })
)
.addOption(Dialog.Option("NPC_SKILL_BOW_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_SKILL_BOW_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerSkillWeapon(playerId, WEAPON_BOW) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerSkillWeapon(playerId, WEAPON_BOW, getPlayerSkillWeapon(playerId, WEAPON_BOW) + 10);
    })
)
.addOption(Dialog.Option("NPC_SKILL_BOW_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_SKILL_BOW_END"));

/********************* LIST CBOW *********************/

NPC_SKILL_LIST_LEARN_CBOW
.addOption(Dialog.Option("NPC_SKILL_CBOW_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerSkillWeapon(playerId, WEAPON_CBOW) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerSkillWeapon(playerId, WEAPON_CBOW, getPlayerSkillWeapon(playerId, WEAPON_CBOW) + 5);
    })
)
.addOption(Dialog.Option("NPC_SKILL_CBOW_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_SKILL_CBOW_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerSkillWeapon(playerId, WEAPON_CBOW) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerSkillWeapon(playerId, WEAPON_CBOW, getPlayerSkillWeapon(playerId, WEAPON_CBOW) + 10);
    })
)
.addOption(Dialog.Option("NPC_SKILL_CBOW_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_SKILL_CBOW_END"));

/********************* LIST STRENGTH *********************/

NPC_SKILL_LIST_LEARN_STRENGTH
.addOption(Dialog.Option("NPC_STRENGTH_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerStrength_Core(playerId) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerStrength(playerId, getPlayerStrength_Core(playerId) + 5);
    })
)
.addOption(Dialog.Option("NPC_STRENGTH_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_STRENGTH_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerStrength_Core(playerId) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerStrength(playerId, getPlayerStrength_Core(playerId) + 10);
    })
)
.addOption(Dialog.Option("NPC_STRENGTH_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_STRENGTH_END"));

/********************* LIST DEXTERITY *********************/

NPC_SKILL_LIST_LEARN_DEXTERITY
.addOption(Dialog.Option("NPC_DEXTERITY_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerDexterity_Core(playerId) + 5) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        setPlayerDexterity(playerId, getPlayerDexterity_Core(playerId) + 5);
    })
)
.addOption(Dialog.Option("NPC_DEXTERITY_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_DEXTERITY_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerDexterity_Core(playerId) + 10) <= 100;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerDexterity(playerId, getPlayerDexterity_Core(playerId) + 10);
    })
)
.addOption(Dialog.Option("NPC_DEXTERITY_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_DEXTERITY_END"));

NPC_SKILL_LIST_LEARN_MANA
.addOption(Dialog.Option("NPC_MANA_FIVE_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 5 && (getPlayerMaxMana_Core(playerId) + 5) <= 150; 
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 5);
        
        setPlayerMaxMana(playerId, getPlayerMaxMana_Core(playerId) + 5);
        setPlayerMana(playerId, getPlayerMaxMana(playerId)); 
    })
)
.addOption(Dialog.Option("NPC_MANA_FIVE_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 5;
    })
)
.addOption(Dialog.Option("NPC_MANA_TEN_POINT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) >= 10 && (getPlayerMaxMana_Core(playerId) + 10) <= 150;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);

        setPlayerMaxMana(playerId, getPlayerMaxMana_Core(playerId) + 10);
        setPlayerMana(playerId, getPlayerMaxMana(playerId));
    })
)
.addOption(Dialog.Option("NPC_MANA_TEN_POINT_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_MANA_END"));

NPC_SKILL_LIST_LEARN_MAGIC
.addOption(Dialog.Option("NPC_MAGIC_ONE_LEVEL")
    .bindRequirementFunc(function(playerId) {
        return getPlayerMagicLevel(playerId) < 6 && getPlayerLearnPoints(playerId) >= 10;
    })
    .bindEffectFunc(function(playerId) {
        setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - 10);
        setPlayerMagicLevel(playerId, getPlayerMagicLevel(playerId) + 1);
    })
)
.addOption(Dialog.Option("NPC_MAGIC_ONE_LEVEL_CANT")
    .bindRequirementFunc(function(playerId) {
        return getPlayerLearnPoints(playerId) < 10;
    })
)
.addOption(Dialog.EndOption("NPC_MAGIC_END"));


/********************* ALL LISTS REGISTER *********************/

NPC_SKILL_LIST
.addOption(Dialog.ListOption("NPC_SKILL_1H_INITIAL", NPC_SKILL_LIST_LEARN_1H)
    .bindRequirementFunc(function(playerId) {
        return getPlayerSkillWeapon(playerId, WEAPON_1H) < 100;
    })
)
.addOption(Dialog.ListOption("NPC_SKILL_2H_INITIAL", NPC_SKILL_LIST_LEARN_2H)
    .bindRequirementFunc(function(playerId) {
        return getPlayerSkillWeapon(playerId, WEAPON_2H) < 100;
    })
)
.addOption(Dialog.ListOption("NPC_SKILL_BOW_INITIAL", NPC_SKILL_LIST_LEARN_BOW)
    .bindRequirementFunc(function(playerId) {
        return getPlayerSkillWeapon(playerId, WEAPON_BOW) < 100;
    })
)
.addOption(Dialog.ListOption("NPC_SKILL_CBOW_INITIAL", NPC_SKILL_LIST_LEARN_CBOW)
    .bindRequirementFunc(function(playerId) {
        return getPlayerSkillWeapon(playerId, WEAPON_CBOW) < 100;
    })
)
.addOption(Dialog.ListOption("NPC_STRENGTH_INITIAL", NPC_SKILL_LIST_LEARN_STRENGTH))
.addOption(Dialog.ListOption("NPC_DEXTERITY_INITIAL", NPC_SKILL_LIST_LEARN_DEXTERITY))
.addOption(Dialog.ListOption("NPC_MANA_INITIAL", NPC_SKILL_LIST_LEARN_MANA))
.addOption(Dialog.ListOption("NPC_MAGIC_INITIAL", NPC_SKILL_LIST_LEARN_MAGIC))
.addOption(Dialog.EndOption("NPC_SKILL_TRAINING_GROUND")
    .bindRequirementFunc(function(playerId) {
        return isRoundStarted() == false;
    })
    .bindEffectFunc(function(playerId) {
            give_gear(playerId);
        setPlayerWorld(playerId, "ARENA.ZEN");
        setPlayerPosition(playerId, 3050, -890, 1015);
    })
)
.addOption(Dialog.EndOption("NPC_SKILL_END"));

registerDialogNpc(NPC_SKILL.getId(), function(playerId) {
    return Dialog.ListOption("NPC_SKILL_INITIAL", NPC_SKILL_LIST);
});