
local NPC_ARMOR = createNpc("NPC_ARMOR", "PC_HERO", "ADDONWORLD.ZEN");

NPC_ARMOR.setVisual("Hum_Body_Naked0", 3, "Hum_Head_Fighter", 4);
NPC_ARMOR.equipArmor(Items.id("GRD_ARMOR_G1_H"));

NPC_ARMOR.playAni("S_HGUARD");

NPC_ARMOR.setPosition(31349, -4462, 9778);
NPC_ARMOR.setAngle(355);
NPC_ARMOR.setCollision(false);

NPC_ARMOR.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARMOR_TRADE = Dialog.TradeOption("NPC_ARMOR_INITIAL", "ITMI_GOLD")
    .addItem("ITAR_BDT_01", 200)
    .addItem("ITAR_BDT_02", 400)
    .addItem("ITAR_BDT_03", 800)
    .addItem("ITAR_BDT_04", 1600)
    .addItem("ITAR_BDT_05", 3200)
    .addItem("ITAR_BDT_06", 6400)
    .addItem("ITAR_BDT_07", 800)
    .addItem("ITAR_BDT_08", 2400);

registerDialogNpc(NPC_ARMOR.getId(), function(playerId) {
    return NPC_ARMOR_TRADE;
});
