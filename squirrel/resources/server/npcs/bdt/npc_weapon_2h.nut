
local NPC_WEAPON_2H = createNpc("NPC_WEAPON_2H", "PC_HERO", "ADDONWORLD.ZEN");

NPC_WEAPON_2H.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Pony", 3);
NPC_WEAPON_2H.equipArmor(Items.id("GRD_ARMOR_G1_M"));
NPC_WEAPON_2H.equipMeleeWeapon(Items.id("ITMW_ZWEIHAENDER4"));

NPC_WEAPON_2H.playAni("S_HGUARD");

NPC_WEAPON_2H.setPosition(30448, -4462, 9214);
NPC_WEAPON_2H.setAngle(25);
NPC_WEAPON_2H.setCollision(false);

NPC_WEAPON_2H.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_WEAPON_2H_TRADE = Dialog.TradeOption("NPC_WEAPON_2H_INITIAL", "ITMI_GOLD")
    .addItem("ITMW_2H_01", 100)
    .addItem("ITMW_2H_02", 200)
    .addItem("ITMW_2H_03", 300)
    .addItem("ITMW_2H_04", 400)
    .addItem("ITMW_2H_05", 500)
    .addItem("ITMW_2H_06", 600)
    .addItem("ITMW_2H_07", 700)
    .addItem("ITMW_2H_08", 800)
    .addItem("ITMW_2H_09", 900)
    .addItem("ITMW_2H_10", 1000);

registerDialogNpc(NPC_WEAPON_2H.getId(), function(playerId) {
    return NPC_WEAPON_2H_TRADE;
});