
local NPC_BOW = createNpc("NPC_BOW", "PC_HERO", "ADDONWORLD.ZEN");

NPC_BOW.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Pony", 64);
NPC_BOW.equipArmor(Items.id("ITAR_BDT_M"));
NPC_BOW.equipRangedWeapon(Items.id("ITRW_BOW_L_01"));

NPC_BOW.playAni("S_HGUARD");

NPC_BOW.setPosition(30650, -4462, 9229);
NPC_BOW.setAngle(354);
NPC_BOW.setCollision(false);

NPC_BOW.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_BOW_TRADE = Dialog.TradeOption("NPC_BOW_INITIAL", "ITMI_GOLD")
    .addItem("ITRW_BOW_ARROW", 1)
    .addItem("ITRW_BOW_01", 200)
    .addItem("ITRW_BOW_02", 400)
    .addItem("ITRW_BOW_03", 600)
    .addItem("ITRW_BOW_04", 800)
    .addItem("ITRW_BOW_05", 1000)
    .addItem("ITRW_BOW_06", 1200)
    .addItem("ITRW_BOW_07", 1400)
    .addItem("ITRW_BOW_08", 1600)
    .addItem("ITRW_BOW_09", 1800)
    .addItem("ITRW_BOW_10", 2000);

registerDialogNpc(NPC_BOW.getId(), function(playerId) {
    return NPC_BOW_TRADE;
});