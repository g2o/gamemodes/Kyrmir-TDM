
local NPC_ARMOR = createNpc("NPC_ARMOR", "PC_HERO", "ADDONWORLD.ZEN");

NPC_ARMOR.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Thief", 73);
NPC_ARMOR.equipArmor(Items.id("ITAR_PIR_H"));

NPC_ARMOR.playAni("S_HGUARD");

NPC_ARMOR.setPosition(-35982, -1873, 19949);
NPC_ARMOR.setAngle(129);
NPC_ARMOR.setCollision(false);

NPC_ARMOR.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARMOR_TRADE = Dialog.TradeOption("NPC_ARMOR_INITIAL", "ITMI_GOLD")
    .addItem("ITAR_PIR_01", 200)
    .addItem("ITAR_PIR_02", 400)
    .addItem("ITAR_PIR_03", 800)
    .addItem("ITAR_PIR_04", 1600)
    .addItem("ITAR_PIR_05", 3200)
    .addItem("ITAR_PIR_06", 6400)
    .addItem("ITAR_PIR_07", 800)
    .addItem("ITAR_PIR_08", 2400);

registerDialogNpc(NPC_ARMOR.getId(), function(playerId) {
    return NPC_ARMOR_TRADE;
});
