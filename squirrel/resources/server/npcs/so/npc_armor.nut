
local NPC_ARMOR = createNpc("NPC_ARMOR", "PC_HERO", "COLONY.ZEN");

NPC_ARMOR.setVisual("Hum_Body_Naked0", 3, "Hum_Head_Fighter", 4);
NPC_ARMOR.equipArmor(Items.id("GRD_ARMOR_G1_H"));

NPC_ARMOR.playAni("S_HGUARD");

NPC_ARMOR.setPosition(844, 247, -580);
NPC_ARMOR.setAngle(273);

NPC_ARMOR.setCollision(false);
NPC_ARMOR.setImmortal(true);

NPC_ARMOR.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARMOR_TRADE = Dialog.TradeOption("NPC_ARMOR_INITIAL", "ITMI_GOLD")
    .addItem("ITAR_SO_01", 200)
    .addItem("ITAR_SO_02", 400)
    .addItem("ITAR_SO_03", 800)
    .addItem("ITAR_SO_04", 1600)
    .addItem("ITAR_SO_05", 3200)
    .addItem("ITAR_SO_06", 6400)
    .addItem("ITAR_SO_07", 800)
    .addItem("ITAR_SO_08", 2400);

registerDialogNpc(NPC_ARMOR.getId(), function(playerId) {
    return NPC_ARMOR_TRADE;
});
