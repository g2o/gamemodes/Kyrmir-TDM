
local NPC_WEAPON_1H = createNpc("NPC_WEAPON_1H", "PC_HERO", "COLONY.ZEN");

NPC_WEAPON_1H.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Bald", 1);
NPC_WEAPON_1H.equipArmor(Items.id("GRD_ARMOR_G1_L"));
NPC_WEAPON_1H.equipMeleeWeapon(Items.id("ITMW_SCHWERT5"));

NPC_WEAPON_1H.playAni("S_HGUARD");

NPC_WEAPON_1H.setPosition(831, 247, 706);
NPC_WEAPON_1H.setAngle(273);

NPC_WEAPON_1H.setCollision(false);
NPC_WEAPON_1H.setImmortal(true);

NPC_WEAPON_1H.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_WEAPON_1H_TRADE = Dialog.TradeOption("NPC_WEAPON_1H_INITIAL", "ITMI_GOLD")
    .addItem("ITMW_1H_02", 100)
    .addItem("ITMW_1H_03", 200)
    .addItem("ITMW_1H_04", 300)
    .addItem("ITMW_1H_05", 400)
    .addItem("ITMW_1H_06", 500)
    .addItem("ITMW_1H_07", 600)
    .addItem("ITMW_1H_08", 700)
    .addItem("ITMW_1H_09", 800)
    .addItem("ITMW_1H_10", 900)
    .addItem("ITMW_1H_11", 1000);

registerDialogNpc(NPC_WEAPON_1H.getId(), function(playerId) {
    return NPC_WEAPON_1H_TRADE;
});