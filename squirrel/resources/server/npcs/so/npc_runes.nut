
local NPC_RUNE = createNpc("NPC_RUNE", "PC_HERO", "COLONY.ZEN");

NPC_RUNE.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Bald", 6);
NPC_RUNE.equipArmor(Items.id("KDF_ARMOR_G1_L"));

NPC_RUNE.playAni("S_HGUARD");

NPC_RUNE.setPosition(840, 247, -103);
NPC_RUNE.setAngle(273);

NPC_RUNE.setCollision(false);
NPC_RUNE.setImmortal(true);

NPC_RUNE.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_RUNE_LIST = Dialog.List();

registerDialogNpc(NPC_RUNE.getId(), function(playerId) {
    return Dialog.ListOption("NPC_RUNE_INITIAL", NPC_RUNE_LIST);
});

NPC_RUNE_LIST
.addOption(Dialog.Option("NPC_RUNE_INITIAL_0")
    .bindRequirementFunc(function(playerId) {
        return getPlayerHealth(playerId) != getPlayerMaxHealth(playerId) || getPlayerMana(playerId) != getPlayerMaxMana(playerId);
    })
    .bindEffectFunc(function(playerId) {
        setPlayerHealth(playerId, getPlayerMaxHealth(playerId));
        setPlayerMana(playerId, getPlayerMaxMana(playerId));
    })
)
.addOption(Dialog.TradeOption("NPC_RUNE_INITIAL_1", "ITMI_GOLD")
    .addItem("ITRU_FIREBOLT", 150)
    .addItem("ITRU_ICEBOLT", 400)
    .addItem("ITRU_ZAP", 800)
    .addItem("ITRU_ICELANCE", 1200)
    .addItem("ITRU_HEAL_TARGET", 1500)
    .addItem("ITRU_INSTANTFIREBALL", 2000)
    .addItem("ITSC_MASSDEATH", 5000)
)
.addOption(Dialog.EndOption("NPC_RUNE_END"));