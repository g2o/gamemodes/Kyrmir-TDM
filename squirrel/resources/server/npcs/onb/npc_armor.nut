
local NPC_ARMOR = createNpc("NPC_ARMOR", "PC_HERO", "COLONY.ZEN");

NPC_ARMOR.setVisual("Hum_Body_Naked0", 3, "Hum_Head_Bald", 11);
NPC_ARMOR.equipArmor(Items.id("TPL_ARMOR_G1_H"));

NPC_ARMOR.playAni("S_HGUARD");

NPC_ARMOR.setPosition(48596, -3872, -2946);
NPC_ARMOR.setAngle(135);
NPC_ARMOR.setCollision(false);

NPC_ARMOR.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARMOR_TRADE = Dialog.TradeOption("NPC_ARMOR_INITIAL", "ITMI_GOLD")
    .addItem("ITAR_ONB_01", 200)
    .addItem("ITAR_ONB_02", 400)
    .addItem("ITAR_ONB_03", 800)
    .addItem("ITAR_ONB_04", 1600)
    .addItem("ITAR_ONB_05", 3200)
    .addItem("ITAR_ONB_06", 6400)
    .addItem("ITAR_ONB_07", 800)
    .addItem("ITAR_ONB_08", 2400);

registerDialogNpc(NPC_ARMOR.getId(), function(playerId) {
    return NPC_ARMOR_TRADE;
});
