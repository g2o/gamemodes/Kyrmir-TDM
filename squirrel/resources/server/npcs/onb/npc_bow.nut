
local NPC_BOW = createNpc("NPC_BOW", "PC_HERO", "COLONY.ZEN");

NPC_BOW.setVisual("Hum_Body_Naked0", 0, "Hum_Head_Bald", 19);
NPC_BOW.equipArmor(Items.id("NOV_ARMOR_G1_H"));
NPC_BOW.equipRangedWeapon(Items.id("ITRW_BOW_L_01"));

NPC_BOW.playAni("S_HGUARD");

NPC_BOW.setPosition(47603, -3870, -3931);
NPC_BOW.setAngle(135);
NPC_BOW.setCollision(false);

NPC_BOW.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_BOW_TRADE = Dialog.TradeOption("NPC_BOW_INITIAL", "ITMI_GOLD")
    .addItem("ITRW_BOW_ARROW", 1)
    .addItem("ITRW_BOW_01", 200)
    .addItem("ITRW_BOW_02", 400)
    .addItem("ITRW_BOW_03", 600)
    .addItem("ITRW_BOW_04", 800)
    .addItem("ITRW_BOW_05", 1000)
    .addItem("ITRW_BOW_06", 1200)
    .addItem("ITRW_BOW_07", 1400)
    .addItem("ITRW_BOW_08", 1600)
    .addItem("ITRW_BOW_09", 1800)
    .addItem("ITRW_BOW_10", 2000);

registerDialogNpc(NPC_BOW.getId(), function(playerId) {
    return NPC_BOW_TRADE;
});