
// Potion effects
addEffectTemplate(Effect.Template("ITPO_SPEED_EFFECT", 30)
    .bindBeginingFunc(function(playerId) 
    {
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("HOME_GUARD_SPEED_EFFECT", -1)
    .bindBeginingFunc(function(playerId) 
    {
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("OLD_CAMP_SPEED_EFFECT", 8)
    .bindBeginingFunc(function(playerId) 
    {
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("NEW_CAMP_SPEED_EFFECT", 12)
    .bindBeginingFunc(function(playerId) 
    {
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("SWAMP_CAMP_SPEED_EFFECT", 26)
    .bindBeginingFunc(function(playerId) 
    {   
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("PIRATE_CAMP_SPEED_EFFECT", 20)
    .bindBeginingFunc(function(playerId) 
    {   
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);

addEffectTemplate(Effect.Template("BANDIT_CAMP_SPEED_EFFECT", 14)
    .bindBeginingFunc(function(playerId) 
    {   
        applyPlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
    .bindFinishFunc(function(playerId)
    {
        removePlayerOverlay(playerId, Mds.id("HUMANS_SPRINT.MDS"));
    })
);
