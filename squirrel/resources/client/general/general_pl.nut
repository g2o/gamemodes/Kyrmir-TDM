
/********************* OTHERS *********************/

registerTranslation("INTERACTION_KEY_0", LANG.PL, "Kliknij aby wej�� w interakcje");
registerTranslation("USE_ONLY_DURING_S_RUN", LANG.PL, "St�j w miejscu by u�y� mikstury");
registerTranslation("CHECK_YOUR_MANA", LANG.PL, "Nie masz wystarczaj�cej ilo�ci many!");

/********************* MESSAGES *********************/

registerTranslation("POP-UP_MSG_RENDER", LANG.PL, "Mo�esz zmieni� zasi�g widzenia raz na 15 sekund...");
registerTranslation("POP-UP_MSG_EXIT", LANG.PL, "%d sekund do wyj�cia...");
registerTranslation("POP-UP_MSG_GUILD", LANG.PL, "Nie mo�esz zmienia� gildii, gdy osiagna�� poziom 3 lub wy�ej!");
registerTranslation("POP-UP_MSG_DEBUG_COMBINATION" LANG.PL, "Odczekaj par� sekund nim znowu u�yjesz tej kombinacji");
registerTranslation("POP-UP_MSG_VOICECALL" LANG.PL, "Odczekaj par� sekund nim znowu u�yjesz komendy g�osowej");
registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.PL, "Osi�gn��e� %d poziom!");
registerTranslation("POP-UP_MSG_EXP-GAIN", LANG.PL, "Otrzyma�e� %d punkty/�w do�wiadczenia!");
registerTranslation("POP-UP_MSG_ITEM-GAIN", LANG.PL, "Otrzyma�e� przedmiot ;%s; x %d!");

registerTranslation("SERVER_MESSAGE_JOIN", LANG.PL, "Gracz %s do��czy� do gry!");
registerTranslation("SERVER_MESSAGE_LEFT", LANG.PL, "Gracz %s opu�ci� gr�!");

/********************* CHATS *********************/

registerTranslation("GLOBAL_CHAT", LANG.PL, "Global");
registerTranslation("GUILD_CHAT", LANG.PL, "Frakcja");
registerTranslation("ADMIN_CHAT", LANG.PL, "Moderacja");

/********************* CHAT MESSAGES *********************/ 

registerTranslation("CHAT_ERROR", LANG.PL, "Odczekaj przed wys�aniem kolejnej wiadomo�ci!");
registerTranslation("CHAT_NO_PERMISSION", LANG.PL, "Nie masz uprawnie� by u�y� tej komendy!");
registerTranslation("CHAT_MUTED", LANG.PL, "Zosta�e� wyciszony!");

/********************* DIALOG VIEW *********************/

registerTranslation("DIALOG_VIEW_AVAILABLE", LANG.PL, "Dost�pne");
registerTranslation("DIALOG_VIEW_TYPE_TITLE", LANG.PL, "Typ broni");
registerTranslation("DIALOG_VIEW_REQUIREMENT_TITLE", LANG.PL, "Wymagania przedmiotu");
registerTranslation("DIALOG_VIEW_PROTECTION_TITLE", LANG.PL, "Ochrona przed");
registerTranslation("DIALOG_VIEW_DESCRIPTION_TITLE", LANG.PL, "Opis");
registerTranslation("DIALOG_VIEW_ITEM_PRICE", LANG.PL, "za sztuk�");
registerTranslation("DIALOG_VIEW_MANA_USAGE", LANG.PL, "Koszt many");
registerTranslation("DIALOG_VIEW_COST", LANG.PL, "sztuk z�ota");
registerTranslation("DIALOG_VIEW_COST_NO_DATA", LANG.PL, "Brak danych");
registerTranslation("DIALOG_VIEW_PLACEHOLDER", LANG.PL, "Ilo��");
registerTranslation("DIALOG_VIEW_END", LANG.PL, "Zako�cz handel");
registerTranslation("DIALOG_VIEW_BUY", LANG.PL, "Kup");
registerTranslation("DIALOG_VIEW_BUY_SUCCES", LANG.PL, "Zakup zako�czony sukcesem...");
registerTranslation("DIALOG_VIEW_BUY_FAULT", LANG.PL, "Poczekaj 5 sekund przed nast�pnym u�yciem...");
registerTranslation("DIALOG_VIEW_BUY_FAULT_AMOUNT", LANG.PL, "Liczba przedmiot�w znajduje si� poza skal�...");
registerTranslation("DIALOG_VIEW_CANNOT_BUY", LANG.PL, "Nie masz wystarczaj�cej liczby zasob�w aby kupi� ten przedmiot...");
registerTranslation("DIALOG_VIEW_EFFECT_TITLE", LANG.PL, "Efekt");

/********************* MAP VIEW *********************/

registerTranslation("MAP_VIEW_RESPAWN", LANG.PL, "W ka�dej chwili");

/********************* EGUIPMENT VIEW *********************/

registerTranslation("EQUIPMENT_VIEW_TYPE_TILE", LANG.PL, "Typ broni");
registerTranslation("EQUIPMENT_VIEW_REQUIREMENT_TITLE", LANG.PL, "Wymagania przedmiotu");
registerTranslation("EQUIPMENT_VIEW_PROTECTION_TITLE", LANG.PL, "Ochrona przed");
registerTranslation("EQUIPMENT_VIEW_DAMAGE_TITLE", LANG.PL, "Obra�enia przeciw");
registerTranslation("EQUIPMENT_VIEW_MANA_USAGE", LANG.PL, "Koszt many");
registerTranslation("EQUIPMENT_VIEW_DESCRIPTION_TITLE", LANG.PL, "Opis");
registerTranslation("EQUIPMENT_VIEW_EQUIP_FAULT", LANG.PL, "Nie mo�esz u�y� tego przedmiotu...");
registerTranslation("EQUIPMENT_VIEW_INFO", LANG.PL, "By przypisa� item do slotu kliknij:\n0, 1, 2, 3, 4, 5, 6, 7, 8, 9\nBy wyrzuci� zaznaczony item u�yj - Q!\nBy u�y�/za�o�y� zaznaczony item\nkliknij - E lub LEWY MYSZY!");
registerTranslation("EQUIPMENT_VIEW_DROP_INPUT", LANG.PL, "Wprowad� ilo��");
registerTranslation("EQUIPMENT_VIEW_DROP_BUTTON", LANG.PL, "Wyrzu�");
registerTranslation("EQUIPMENT_VIEW_DROP_EQUIPED", LANG.PL, "Przedmiot jest u�ywany!");
registerTranslation("EQUIPMENT_VIEW_DROP_AMOUNT", LANG.PL, "Posiadasz za ma�� ilo��!");
registerTranslation("EQUIPMENT_VIEW_DROP_BOUNDED", LANG.PL, "Ten przedmiot jest powi�zany z twoj� postaci�!");
registerTranslation("EQUIPMENT_VIEW_EFFECT_TITLE", LANG.PL, "Efekt");

/********************* LOGIN VIEW *********************/

registerTranslation("LOGIN_VIEW_TITLE", LANG.PL, "Witaj na serwerze Kyrmir TDM");
registerTranslation("LOGIN_VIEW_SUBTITLE", LANG.PL, "Wprowad� swoj� nazw� u�ytkownika i has�o.");
registerTranslation("LOGIN_VIEW_USERNAME", LANG.PL, "Nazwa u�ytkownika");
registerTranslation("LOGIN_VIEW_PASSWORD", LANG.PL, "Has�o");
registerTranslation("LOGIN_VIEW_BTN_LOGIN", LANG.PL, "Zaloguj");
registerTranslation("LOGIN_VIEW_BTN_REGISTER", LANG.PL, "Rejestracja");
registerTranslation("LOGIN_VIEW_LANG_SELECT" LANG.PL, "Wybierz j�zyk");
registerTranslation("LOGIN_VIEW_MSG_USERNAME", LANG.PL, "Nazwa u�ytkownika jest b��dna!");
registerTranslation("LOGIN_VIEW_MSG_EMPTY_FIELD", LANG.PL, "Pozostawi�e� puste pole!");
registerTranslation("LOGIN_VIEW_MSG_FAULT", LANG.PL, "Wprowadzone dane s� b��dne!");
registerTranslation("LOGIN_VIEW_MSG_NOT_EXIST", LANG.PL, "Takie konto nie istnieje!");

/********************* REGISTER VIEW *********************/

registerTranslation("REGISTER_VIEW_TITLE", LANG.PL, "Witaj na serwerze Kyrmir TDM");
registerTranslation("REGISTER_VIEW_SUBTITLE", LANG.PL, "Wprowad� swoj� nazw� u�ytkownika i has�o.");
registerTranslation("REGISTER_VIEW_INFO_0", LANG.PL, "Nasze zasady bezpiecze�stwa wymagaj� by twoje has�o zawiera�o");
registerTranslation("REGISTER_VIEW_INFO_1", LANG.PL, "conajmniej 6 znak�w, conajmniej 1 cyfr�, conajmniej jedn� du�� liter�!");
registerTranslation("REGISTER_VIEW_INFO_2", LANG.PL, "Pami�taj te� o u�ywaniu unikalnych hase�!");
registerTranslation("REGISTER_VIEW_USERNAME", LANG.PL, "Nazwa u�ytkownika");
registerTranslation("REGISTER_VIEW_PASSWORD", LANG.PL, "Has�o");
registerTranslation("REGISTER_VIEW_PASSWORD_REPEAT", LANG.PL, "Powt�rz has�o");
registerTranslation("REGISTER_VIEW_BTN_REGISTER", LANG.PL, "Zarejestruj");
registerTranslation("REGISTER_VIEW_BTN_BACK", LANG.PL, "Powr�t");
registerTranslation("REGISTER_VIEW_MSG_PASSWORDS_MATCH", LANG.PL, "Has�a do siebie nie pasuj�!");
registerTranslation("REGISTER_VIEW_MSG_PASSWORD", LANG.PL, "Has�o nie spe�nia wymog�w bezpiecze�stwa!");
registerTranslation("REGISTER_VIEW_MSG_USERNAME", LANG.PL, "Nazwa u�ytkownika nie spe�nia wymog�w!");
registerTranslation("REGISTER_VIEW_MSG_EMPTY_FIELD", LANG.PL, "Pozostawi�e� puste pole!");
registerTranslation("REGISTER_VIEW_MSG_SUCCES", LANG.PL, "Pomy�lnie zarejestrowano konto! Zaloguj si�!");
registerTranslation("REGISTER_VIEW_MSG_FAULT", LANG.PL, "Nie mo�na zarejestrowa� takiego konta!");
registerTranslation("REGISTER_VIEW_MSG_DATA_USED", LANG.PL, "Takie konto ju� istnieje!");

/********************* VISUAL VIEW *********************/

registerTranslation("VISUAL_VIEW_TITLE", LANG.PL, "Wybierz wygl�d");
registerTranslation("VISUAL_VIEW_FACES_DESCRIPTION", LANG.PL, "Do poruszania si� po li�cie twarzy,\nmo�esz u�y� scrolla myszki!");
registerTranslation("VISUAL_VIEW_HEAD", LANG.PL, "Zmie� model g�owy");
registerTranslation("VISUAL_VIEW_TORSO", LANG.PL, "Zmie� tors");
registerTranslation("VISUAL_VIEW_SEX", LANG.PL, "Zmie� p�e�");
registerTranslation("VISUAL_VIEW_SAVE_VISUAL", LANG.PL, "Zapisz");
registerTranslation("VISUAL_VIEW_RESET_VISUAL", LANG.PL, "Zresetuj");
registerTranslation("VISUAL_VIEW_CAMERA", LANG.PL, "Zmie� widok");

/********************* HELP VIEW *********************/

registerTranslation("GUILD_VIEW_TITLE", LANG.PL, "Wybierz sw�j ob�z!");
registerTranslation("GUILD_VIEW_BTN_CAMP", LANG.PL, "Aktywni gracze");

/********************* MAIN VIEW *********************/

registerTranslation("MAIN_VIEW_OPTIONS", LANG.PL, "Ustawienia");
registerTranslation("MAIN_VIEW_VISUAL", LANG.PL, "Zmiana wygl�du");
registerTranslation("MAIN_VIEW_HELP", LANG.PL, "Pomoc");
registerTranslation("MAIN_VIEW_GUILD", LANG.PL, "Zmie� frakcj�");
registerTranslation("MAIN_VIEW_EXIT", LANG.PL, "Wyj�cie");
registerTranslation("MAIN_VIEW_LANG_SELECT" LANG.PL, "Wybierz j�zyk");

/********************* ANIMATIONS VIEW *********************/

registerTranslation("VIEW_ANIMATION_TITLE", LANG.PL, "Lista animacji");

/********************* OPTIONS VIEW *********************/

registerTranslation("OPTIONS_VIEW_RESOLUTION", LANG.PL, "Rozdzielczo��");
registerTranslation("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM", LANG.PL, "Wy��cz d�wi�k");
registerTranslation("OPTIONS_VIEW_SOUND_VOLUME", LANG.PL, "G�o�no�� d�wi�k�w");
registerTranslation("OPTIONS_VIEW_MUSIC_VOLUME", LANG.PL, "G�o�no�� muzyki");
registerTranslation("OPTIONS_VIEW_CHAT_LINES", LANG.PL, "Linie czatu");
registerTranslation("OPTIONS_VIEW_SAVE_LOGIN", LANG.PL, "Zapisuj dane logowania");
registerTranslation("OPTIONS_VIEW_WEAPON_TRAIL", LANG.PL, "Smuga broni");
registerTranslation("OPTIONS_VIEW_YES", LANG.PL, "Tak");
registerTranslation("OPTIONS_VIEW_NO", LANG.PL, "Nie");

/********************* HELP VIEW *********************/

registerTranslation("HELP_VIEW_TITLE", LANG.PL, "Komendy i skr�ty klawiszowe");
registerTranslation("HELP_VIEW_RENDER", LANG.PL, "F1 F2 F3 F4 - Zmiana zasi�gu renderowania");
registerTranslation("HELP_VIEW_PLAYERLIST", LANG.PL, "F5 - Lista graczy");
registerTranslation("HELP_VIEW_ANIMLIST", LANG.PL, "F6 - Lista animacji");
registerTranslation("HELP_VIEW_HIDE_UI", LANG.PL, "F12 - Ukryj UI");
registerTranslation("HELP_VIEW_STATS", LANG.PL, "B - Statystyki postaci");
registerTranslation("HELP_VIEW_TALK", LANG.PL, "CTRL - Rozmowa z NPC");
registerTranslation("HELP_VIEW_SKIP", LANG.PL, "Spacja - Pomi� dialogi w rozmowie z NPC");
registerTranslation("HELP_VIEW_SCROLL", LANG.PL, "Mouse Scroll - Przewijanie asortymentu kupca lub opcji dialogowych");
registerTranslation("HELP_VIEW_DEBUG", LANG.PL, "CTRL + ALT + F8 - Je�li nie mo�esz sk�d� wyj��, to u�yj tej kombinacji");
registerTranslation("HELP_VIEW_MAP", LANG.PL, "M - Poka� map�");
registerTranslation("HELP_VIEW_CHAT_CHANGE", LANG.PL, "CTRL + X - Prze��cz zak�adk� czatu");
registerTranslation("HELP_VIEW_CHAT_OPEN", LANG.PL, "T - Czat");
registerTranslation("HELP_VIEW_QUICKSLOT", LANG.PL, "Quicksloty - klawisze: 1-2-3-4-5-6-7-8-9-0");
registerTranslation("HELP_VIEW_VOICE_COMMAND_0", LANG.PL, "Komendy g�osowe: (Mo�esz u�y� co 6 sekund)");
registerTranslation("HELP_VIEW_VOICE_COMMAND_1", LANG.PL, "Num 9 - Cholera, biegnij!");
registerTranslation("HELP_VIEW_VOICE_COMMAND_2", LANG.PL, "Num 8 - Przesta� ucieka�, tch�rzu");
registerTranslation("HELP_VIEW_VOICE_COMMAND_3", LANG.PL, "Num 7 - Alarm");
registerTranslation("HELP_VIEW_VOICE_COMMAND_4", LANG.PL, "Num 6 - Pomocy");
registerTranslation("HELP_VIEW_VOICE_COMMAND_5", LANG.PL, "Num 5 - Zniewaga");
registerTranslation("HELP_VIEW_VOICE_COMMAND_6", LANG.PL, "Num 4 - Wr�g nie �yje");
registerTranslation("HELP_VIEW_COMMANDS", LANG.PL, "Komendy czatu:");
registerTranslation("HELP_VIEW_COMMANDS_PM", LANG.PL, "Prywatna wiadomo�� - /pw player_id text przyk�ad: /pw 5 hello");

/********************* STATS VIEW *********************/

registerTranslation("STATS_VIEW_TITLE", LANG.PL, "Statystyki gracza");
registerTranslation("STATS_VIEW_CHARACTER", LANG.PL, "Posta�");
registerTranslation("STATS_VIEW_ATRIBUTES", LANG.PL, "Atrybuty");
registerTranslation("STATS_VIEW_PROTECTION", LANG.PL, "Ochrona postaci");
registerTranslation("STATS_VIEW_SKILLS", LANG.PL, "Umiej�tno�ci");
registerTranslation("STATS_VIEW_GUILD", LANG.PL, "Frakcja");

/********************* STATUS VIEW *********************/

registerTranslation("STATUS_VIEW_TARGET", LANG.PL, "Cel rundy");
registerTranslation("STATUS_VIEW_BEGIN", LANG.PL, "Runda wystartuje za");
registerTranslation("STATUS_VIEW_NO_ROUND", LANG.PL, "Runda nie trwa");
registerTranslation("STATUS_VIEW_BONUS_EMPTY", LANG.PL, "Bonus exp");
registerTranslation("STATUS_VIEW_ROUND_BONUS", LANG.PL, "Bonus exp: %.2fx");
registerTranslation("STATUS_VIEW_KILL", LANG.PL, "%s zosta� zabity przez %s(%d)!");
registerTranslation("STATUS_VIEW_STREAK", LANG.PL, "Gracz %s osi�gn�� seri� %d zab�jstw, zabijaj�c gracza %s!");
registerTranslation("STATUS_VIEW_LEVEL_THREE", LANG.PL, "Gracz %s osi�gn�� 3 poziom!");
registerTranslation("STATUS_VIEW_BOSS_KILL", LANG.PL, "%s zosta� zabity przez %s!");
registerTranslation("STATUS_VIEW_BOSS_RESPAWN", LANG.PL, "Boss %s pojawi� si� na mapie!");
registerTranslation("STATUS_VIEW_KILLS", LANG.PL, "Zab�jstwa");
registerTranslation("STATUS_VIEW_DEATHS", LANG.PL, "�mierci");
registerTranslation("STATUS_VIEW_ASSISTS", LANG.PL, "Asysty");
registerTranslation("STATUS_VIEW_ENTER_CAMP", LANG.PL, "Ulecz si� za darmo u sprzedawcy run!");

/********************* PLAYERLIST VIEW *********************/

registerTranslation("PLAYER_LIST_VIEW_TITLE", LANG.PL, "Lista Graczy");
registerTranslation("PLAYER_LIST_VIEW_ONLINE", LANG.PL, "Gracze Online");
registerTranslation("PLAYER_LIST_VIEW_MEMBERS", LANG.PL, "Gracze w dru�ynie");
registerTranslation("PLAYER_LIST_VIEW_ID", LANG.PL, "Id");
registerTranslation("PLAYER_LIST_VIEW_NICKNAME", LANG.PL, "Nazwa");
registerTranslation("PLAYER_LIST_VIEW_PING", LANG.PL, "Ping");
registerTranslation("PLAYER_LIST_VIEW_KILLS", LANG.PL, "Zab�jstwa");
registerTranslation("PLAYER_LIST_VIEW_DEATHS", LANG.PL, "�mierci");
registerTranslation("PLAYER_LIST_VIEW_ASSISTS", LANG.PL, "Asysty");

/********************* RANKLIST VIEW *********************/

registerTranslation("RANK_LIST_VIEW_NUMBER", LANG.PL, "Lp");
registerTranslation("RANK_LIST_VIEW_TITLE", LANG.PL, "Lista Graczy");
registerTranslation("RANK_LIST_VIEW_NICKNAME", LANG.PL, "Nazwa");
registerTranslation("RANK_LIST_VIEW_KILLS", LANG.PL, "Zab�jstwa");
registerTranslation("RANK_LIST_VIEW_DEATHS", LANG.PL, "�mierci");
registerTranslation("RANK_LIST_VIEW_ASSISTS", LANG.PL, "Asysty");
registerTranslation("RANK_LIST_VIEW_KDA", LANG.PL, "Points");
registerTranslation("RANK_LIST_VIEW_DMG", LANG.PL, "Damage");

/********************* STATS *********************/

registerTranslation("LEVEL", LANG.PL, "Poziom");
registerTranslation("EXPERIENCE", LANG.PL, "Do�wiadczenie");
registerTranslation("EXPERIENCE_NEXT_LEVEL", LANG.PL, "Nast�pny poziom");
registerTranslation("LEARN_POINTS", LANG.PL, "Punkty nauki");
registerTranslation("STRENGTH", LANG.PL, "Si�a");
registerTranslation("DEXTERITY", LANG.PL, "Zr�czno��");
registerTranslation("MANA", LANG.PL, "Mana");
registerTranslation("HEALTH", LANG.PL, "Zdrowie");
registerTranslation("MAGIC_LEVEL", LANG.PL, "Kr�g magii");
registerTranslation("GUILD_NAME", LANG.PL, "Nazwa frakcji");

/********************* MULTIUSE *********************/

registerTranslation("REQUIREMENT_TYPE_0", LANG.PL, "Poziom");
registerTranslation("REQUIREMENT_TYPE_1", LANG.PL, "Kr�g magii");
registerTranslation("REQUIREMENT_TYPE_2", LANG.PL, "Si�a");
registerTranslation("REQUIREMENT_TYPE_3", LANG.PL, "Zr�czno��");
registerTranslation("REQUIREMENT_TYPE_4", LANG.PL, "Punkty many");
registerTranslation("REQUIREMENT_TYPE_5", LANG.PL, "Punkty �ycia");

registerTranslation("DAMAGE_TYPE_2", LANG.PL, "Obra�enia obuchowe");
registerTranslation("DAMAGE_TYPE_4", LANG.PL, "Obra�enia sieczne");
registerTranslation("DAMAGE_TYPE_64", LANG.PL, "Obra�enia od �uk�w/kusz");
registerTranslation("DAMAGE_TYPE_8", LANG.PL, "Obra�enia od ognia");
registerTranslation("DAMAGE_TYPE_32", LANG.PL, "Obra�enia magiczne");

registerTranslation("WEAPON_MODE_3", LANG.PL, "Bro� Jednor�czna");
registerTranslation("WEAPON_MODE_4", LANG.PL, "Bro� Dwur�czna");
registerTranslation("WEAPON_MODE_5", LANG.PL, "�uk");
registerTranslation("WEAPON_MODE_6", LANG.PL, "Kusza");
registerTranslation("WEAPON_MODE_7", LANG.PL, "Magia");
registerTranslation("WEAPON_SKILL_0", LANG.PL, "Bro� Jednor�czna");
registerTranslation("WEAPON_SKILL_1", LANG.PL, "Bro� Dwur�czna");
registerTranslation("WEAPON_SKILL_2", LANG.PL, "�uki");
registerTranslation("WEAPON_SKILL_3", LANG.PL, "Kusze");

registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthRecovery, LANG.PL, "Przywraca punkty zdrowia");
registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthPercentRecovery, LANG.PL, "Przywraca punkty zdrowia (procent)");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaRecovery, LANG.PL, "Przywraca punkty many");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaPercentRecovery, LANG.PL, "Przywraca punkty many (procent)");

/********************* ANIMS *********************/

registerTranslation("T_STAND_2_SIT", LANG.PL, "Siedzi");
registerTranslation("T_STAND_2_SLEEPGROUND", LANG.PL, "�pi");
registerTranslation("T_STAND_2_PEE", LANG.PL, "Sika");
registerTranslation("S_PRAY", LANG.PL, "Modli si�");
registerTranslation("S_DEAD", LANG.PL, "Martwy");
registerTranslation("S_LGUARD", LANG.PL, "Pilnuje 1");
registerTranslation("S_HGUARD", LANG.PL, "Pilnuje 2");
registerTranslation("T_SEARCH", LANG.PL, "Rozgl�da si�");
registerTranslation("T_PLUNDER", LANG.PL, "Pl�druje");
registerTranslation("S_WASH", LANG.PL, "Myje si�");
registerTranslation("R_SCRATCHEGG", LANG.PL, "Drapie si�");
registerTranslation("S_FIRE_VICTIM", LANG.PL, "Taniec");
registerTranslation("T_1HSINSPECT", LANG.PL, "Sprawd� bro�");
registerTranslation("T_GREETNOV", LANG.PL, "Uk�on");
registerTranslation("T_1HSFREE", LANG.PL, "Trenuj");
registerTranslation("T_WATCHFIGHT_OHNO", LANG.PL, "Dopinguj");
registerTranslation("T_YES", LANG.PL, "Tak");
registerTranslation("T_NO", LANG.PL, "Nie");
registerTranslation("R_SCRATCHHEAD", LANG.PL, "Drapie si� 2");
registerTranslation("T_BORINGKICK", LANG.PL, "Kopnij");

/********************* GUILDS *********************/

registerTranslation("OLD_CAMP", LANG.PL, "Stary Ob�z");
registerTranslation("SWAMP_CAMP", LANG.PL, "Ob�z na Bagnie");
registerTranslation("NEW_CAMP", LANG.PL, "Nowy Ob�z");
registerTranslation("PIRATE_CAMP", LANG.PL, "Ob�z Pirat�w");
registerTranslation("BANDIT_CAMP", LANG.PL, "Ob�z Bandyt�w");
