
/********************* OTHERS *********************/

registerTranslation("INTERACTION_KEY_0", LANG.DE, "Zum Interagieren klicken");
registerTranslation("USE_ONLY_DURING_S_RUN", LANG.DE, "Steh still, um den Trank zu benutzen");
registerTranslation("CHECK_YOUR_MANA", LANG.DE, "Du hast nicht genug Mana!");

/********************* MESSAGES *********************/

registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.DE, "Du hast Level %d erreicht");
registerTranslation("POP-UP_MSG_RENDER", LANG.DE, "Du kannst deinen Rendering-Bereich 1 Mal pro 15 Sekunden �ndern...");
registerTranslation("POP-UP_MSG_DEBUG_COMBINATION" LANG.DE, "Warten Sie ein paar Sekunden, bevor Sie diese Kombination erneut verwenden");
registerTranslation("POP-UP_MSG_VOICECALL" LANG.DE, "Warten Sie ein paar Sekunden, bevor Sie den Sprachbefehl erneut verwenden");
registerTranslation("POP-UP_MSG_EXIT", LANG.DE, "%d Sekunden bis zum Ende");
registerTranslation("POP-UP_MSG_GUILD", LANG.DE, "Du kannst die Gilde nicht mehr wechseln, wenn du Stufe 3 oder h�her erreicht hast!");
registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.DE, "Du hast Level %d erreicht");
registerTranslation("POP-UP_MSG_EXP-GAIN", LANG.DE, "Du hast %d Erfahrungspunkte erhalten!");
registerTranslation("POP-UP_MSG_ITEM-GAIN", LANG.DE, "Du hast einen Gegenstand erhalten ;%s; x %d!");
registerTranslation("SERVER_MESSAGE_JOIN", LANG.DE, "Spieler %s ist dem Spiel beigetreten!");
registerTranslation("SERVER_MESSAGE_LEFT", LANG.DE, "Spieler %s hat das Spiel verlassen!");

/********************* CHATS *********************/

registerTranslation("GLOBAL_CHAT", LANG.DE, "Global");
registerTranslation("GUILD_CHAT", LANG.DE, "Fraktion");
registerTranslation("ADMIN_CHAT", LANG.DE, "Admin");

/********************* CHAT MESSAGES *********************/ 

registerTranslation("CHAT_ERROR", LANG.DE, "Warten, bevor eine weitere Nachricht gesendet wird!");
registerTranslation("CHAT_NO_PERMISSION", LANG.DE, "Sie haben keine Berechtigung, diesen Befehl zu verwenden!");
registerTranslation("CHAT_MUTED", LANG.DE, "Du bist stumm!");

/********************* DIALOG VIEW  *********************/

registerTranslation("DIALOG_VIEW_AVAILABLE", LANG.DE, "Verf�gbar");
registerTranslation("DIALOG_VIEW_TYPE_TITLE", LANG.DE, "Waffentyp");
registerTranslation("DIALOG_VIEW_REQUIREMENT_TITLE", LANG.DE, "Anforderungen f�r den Gegenstand");
registerTranslation("DIALOG_VIEW_PROTECTION_TITLE", LANG.DE, "Schutz gegen");
registerTranslation("DIALOG_VIEW_DESCRIPTION_TITLE", LANG.DE, "Desc");
registerTranslation("DIALOG_VIEW_ITEM_PRICE", LANG.DE, "st�ckkosten");
registerTranslation("DIALOG_VIEW_MANA_USAGE", LANG.DE, "Mana Kosten");
registerTranslation("DIALOG_VIEW_COST", LANG.DE, "gold");
registerTranslation("DIALOG_VIEW_COST_NO_DATA", LANG.DE, "Keine Daten");
registerTranslation("DIALOG_VIEW_PLACEHOLDER", LANG.DE, "Anzahl");
registerTranslation("DIALOG_VIEW_END", LANG.DE, "Handel beenden");
registerTranslation("DIALOG_VIEW_BUY", LANG.DE, "Kaufen");
registerTranslation("DIALOG_VIEW_BUY_SUCCES", LANG.DE, "Dein Handel wurde erfolgreich abgeschlossen...");
registerTranslation("DIALOG_VIEW_BUY_FAULT", LANG.DE, "5 Sekunden bis zur n�chsten Verwendung warten...");
registerTranslation("DIALOG_VIEW_BUY_FAULT_AMOUNT", LANG.DE, "Der Gegenstand ist au�erhalb von der Skala...");
registerTranslation("DIALOG_VIEW_CANNOT_BUY", LANG.DE, "Du hast nicht gen�gend Gold, um diesen Gegenstand zu kaufen...");
registerTranslation("DIALOG_VIEW_EFFECT_TITLE", LANG.DE, "Effekt");

/********************* MAP VIEW *********************/

registerTranslation("MAP_VIEW_RESPAWN", LANG.DE, "Jeder moment");

/********************* EGUIPMENT VIEW *********************/

registerTranslation("EQUIPMENT_VIEW_TYPE_TILE", LANG.DE, "Waffentyp");
registerTranslation("EQUIPMENT_VIEW_REQUIREMENT_TITLE", LANG.DE, "Anforderungen f�r den Gegenstand");
registerTranslation("EQUIPMENT_VIEW_PROTECTION_TITLE", LANG.DE, "Schutz gegen");
registerTranslation("EQUIPMENT_VIEW_DAMAGE_TITLE", LANG.DE, "Schaden gegen");
registerTranslation("EQUIPMENT_VIEW_MANA_USAGE" LANG.DE, "Mana Kosten");
registerTranslation("EQUIPMENT_VIEW_DESCRIPTION_TITLE", LANG.DE, "Beschreibung");
registerTranslation("EQUIPMENT_VIEW_EQUIP_FAULT", LANG.DE, "Du kannst diesen Gegenstand nicht benutzen...");
registerTranslation("EQUIPMENT_VIEW_INFO", LANG.DE, "Um einen Gegenstand einem Steckplatz\nzuzuweisen, klicken Sie auf: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9\nUm den ausgew�hlten Gegenstand fallen zu lassen,\nverwenden Sie - Q!\nUm den ausgew�hlten Gegenstand zu verwenden/ersetzen,\nklicken Sie - E oder die LINKE MAUSTASTE!");
registerTranslation("EQUIPMENT_VIEW_DROP_INPUT", LANG.DE, "Betrag eingeben");
registerTranslation("EQUIPMENT_VIEW_DROP_BUTTON", LANG.DE, "Abladen");
registerTranslation("EQUIPMENT_VIEW_DROP_EQUIPED", LANG.DE, "Der Gegenstand ist gebraucht!");
registerTranslation("EQUIPMENT_VIEW_DROP_AMOUNT", LANG.DE, "Nicht genug Gegenst�nde!");
registerTranslation("EQUIPMENT_VIEW_DROP_BOUNDED", LANG.DE, "Dieser Gegenstand ist an deinen Charakter gebunden!");
registerTranslation("EQUIPMENT_VIEW_EFFECT_TITLE", LANG.DE, "Effekt");

/********************* LOGIN VIEW *********************/

registerTranslation("LOGIN_VIEW_TITLE", LANG.DE, "Willkommen bei Kyrmir TDM");
registerTranslation("LOGIN_VIEW_SUBTITLE", LANG.DE, "Gebe deinen Benutzernamen und dein Passwort ein.");
registerTranslation("LOGIN_VIEW_USERNAME", LANG.DE, "Benutzername");
registerTranslation("LOGIN_VIEW_PASSWORD", LANG.DE, "Passwort");
registerTranslation("LOGIN_VIEW_BTN_LOGIN", LANG.DE, "Login");
registerTranslation("LOGIN_VIEW_BTN_REGISTER", LANG.DE, "Registrierung");
registerTranslation("LOGIN_VIEW_LANG_SELECT" LANG.DE, "W�hlen Sie Ihre Sprache");
registerTranslation("LOGIN_VIEW_MSG_USERNAME", LANG.DE, "Der Benutzername ist falsch!");
registerTranslation("LOGIN_VIEW_MSG_EMPTY_FIELD", LANG.DE, "Du hast das Feld leer gelassen!");
registerTranslation("LOGIN_VIEW_MSG_FAULT", LANG.DE, "Die angegebenen Daten sind falsch!");
registerTranslation("LOGIN_VIEW_MSG_NOT_EXIST", LANG.DE, "Dieses Konto Existiert nicht!");

/********************* REGISTER VIEW *********************/

registerTranslation("REGISTER_VIEW_TITLE", LANG.DE, "Willkommen bei Kyrmir TDM");
registerTranslation("REGISTER_VIEW_SUBTITLE", LANG.DE, "Gebe deinen Benutzernamen und dein Passwort ein.");
registerTranslation("REGISTER_VIEW_INFO_0", LANG.DE, "Unsere Sicherheitsrichtlinien verlangen, dass Ihr Passwort Folgendes enth�lt");
registerTranslation("REGISTER_VIEW_INFO_1", LANG.DE, "mindestens 6 Zeichen, mindestens 1 Zahl, mindestens ein Gro�buchstabe!");
registerTranslation("REGISTER_VIEW_INFO_2", LANG.DE, "Denken Sie auch daran, eindeutige Passw�rter zu verwenden!");
registerTranslation("REGISTER_VIEW_USERNAME", LANG.DE, "Benutzername");
registerTranslation("REGISTER_VIEW_PASSWORD", LANG.DE, "Passwort");
registerTranslation("REGISTER_VIEW_PASSWORD_REPEAT", LANG.DE, "Passwort wiederholen");
registerTranslation("REGISTER_VIEW_BTN_REGISTER", LANG.DE, "Registrieren");
registerTranslation("REGISTER_VIEW_BTN_BACK", LANG.DE, "Zur�ck");
registerTranslation("REGISTER_VIEW_MSG_PASSWORDS_MATCH", LANG.DE, "Die Passw�rter stimmen nicht �berein!");
registerTranslation("REGISTER_VIEW_MSG_PASSWORD", LANG.DE, "Das Passwort entspricht nicht den Anforderungen!");
registerTranslation("REGISTER_VIEW_MSG_USERNAME", LANG.DE, "Der Benutzername entspricht nicht den Anforderungen!");
registerTranslation("REGISTER_VIEW_MSG_EMPTY_FIELD", LANG.DE, "Du hast das Feld leer gelassen!");
registerTranslation("REGISTER_VIEW_MSG_SUCCES", LANG.DE, "Du hast dich erfolgreich registriert! Du kannst dich jetzt anmelden!");
registerTranslation("REGISTER_VIEW_MSG_FAULT", LANG.DE, "Konto kann nicht registriert werden");
registerTranslation("REGISTER_VIEW_MSG_DATA_USED", LANG.DE, "Dieser Account existiert bereits!");
registerTranslation("VISUAL_VIEW_TITLE", LANG.DE, "Passe deinen Charakter an");
registerTranslation("VISUAL_VIEW_FACES_DESCRIPTION", LANG.DE, "Du kannst mit der Maus durch\ndie Gesichtsliste navigieren!");
registerTranslation("VISUAL_VIEW_HEAD", LANG.DE, "Kopfmodell ausw�hlen");
registerTranslation("VISUAL_VIEW_TORSO", LANG.DE, "K�rpertextur ausw�hlen");
registerTranslation("VISUAL_VIEW_SEX", LANG.DE, "Geschlecht ausw�hlen");
registerTranslation("VISUAL_VIEW_SAVE_VISUAL", LANG.DE, "Speichern");
registerTranslation("VISUAL_VIEW_RESET_VISUAL", LANG.DE, "Zur�cksetzen");
registerTranslation("VISUAL_VIEW_CAMERA", LANG.DE, "Kamera wechseln");
registerTranslation("GUILD_VIEW_TITLE", LANG.DE, "W�hle dein Lager!");
registerTranslation("GUILD_VIEW_BTN_CAMP", LANG.DE, "Aktive Spieler");

/********************* MAIN VIEW *********************/

registerTranslation("MAIN_VIEW_OPTIONS", LANG.DE, "Optionen");
registerTranslation("MAIN_VIEW_VISUAL", LANG.DE, "Aussehen ver�ndern");
registerTranslation("MAIN_VIEW_HELP", LANG.DE, "Hilfe");
registerTranslation("MAIN_VIEW_GUILD", LANG.DE, "Fraktionswechsel");
registerTranslation("MAIN_VIEW_EXIT", LANG.DE, "Beenden");
registerTranslation("MAIN_VIEW_LANG_SELECT" LANG.DE, "W�hlen Sie Ihre Sprache");

/********************* ANIMATIONS VIEW *********************/

registerTranslation("VIEW_ANIMATION_TITLE", LANG.DE, "Animationsliste");

/********************* OPTIONS VIEW *********************/

registerTranslation("OPTIONS_VIEW_RESOLUTION", LANG.DE, "Bildschirmaufl�sung");
registerTranslation("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM", LANG.DE, "Ton ausschalten");
registerTranslation("OPTIONS_VIEW_SOUND_VOLUME", LANG.DE, "Lautst�rke");
registerTranslation("OPTIONS_VIEW_MUSIC_VOLUME", LANG.DE, "Musik Lautst�rke");
registerTranslation("OPTIONS_VIEW_CHAT_LINES", LANG.DE, "Chat-Leitungen");
registerTranslation("OPTIONS_VIEW_SAVE_LOGIN", LANG.DE, "Login-Daten speichern");
registerTranslation("OPTIONS_VIEW_WEAPON_TRAIL", LANG.DE, "Waffenspuren");
registerTranslation("OPTIONS_VIEW_YES", LANG.DE, "Ja");
registerTranslation("OPTIONS_VIEW_NO", LANG.DE, "Nein");
registerTranslation("HELP_VIEW_TITLE", LANG.DE, "Befehle und Tastaturk�rzel");
registerTranslation("HELP_VIEW_RENDER", LANG.DE, "F1 F2 F3 F4 - �ndert den Rendering-Bereich");
registerTranslation("HELP_VIEW_PLAYERLIST", LANG.DE, "F5 - Spielerliste");
registerTranslation("HELP_VIEW_ANIMLIST", LANG.DE, "F6 - Liste an Animationen");
registerTranslation("HELP_VIEW_HIDE_UI", LANG.DE, "F12 - UI ausblenden");
registerTranslation("HELP_VIEW_STATS", LANG.DE, "B - Charakterstatistiken");
registerTranslation("HELP_VIEW_TALK", LANG.DE, "CTRL - Mit NPC sprechen");
registerTranslation("HELP_VIEW_SKIP", LANG.DE, "Space - Gespr�ch mit NPC �berspringen");
registerTranslation("HELP_VIEW_SCROLL", LANG.DE, "Mouse Scroll - Durch das Sortiment des H�ndlers oder die Dialogoptionen schauen");
registerTranslation("HELP_VIEW_DEBUG", LANG.DE, "CTRL + ALT + F8 - Wenn du wo nicht mehr rauskommst, verwende diese Tasten-Kombination");
registerTranslation("HELP_VIEW_MAP", LANG.DE, "M - Karte anzeigen");
registerTranslation("HELP_VIEW_CHAT_CHANGE", LANG.DE, "CTRL + X - Wechsel die Chat-Registerkarte");
registerTranslation("HELP_VIEW_CHAT_OPEN", LANG.DE, "T - Chat");
registerTranslation("HELP_VIEW_QUICKSLOT", LANG.DE, "Quickslots - keys: 1-2-3-4-5-6-7-8-9-0");
registerTranslation("HELP_VIEW_VOICE_COMMAND_0", LANG.DE, "Sprachsteuerung: (Du kannst sie alle 6 Sekunden verwenden)");
registerTranslation("HELP_VIEW_VOICE_COMMAND_1", LANG.DE, "Num 9 - Verdammt, lauf!");
registerTranslation("HELP_VIEW_VOICE_COMMAND_2", LANG.DE, "Num 8 - H�r auf zu rennen, Feigling");
registerTranslation("HELP_VIEW_VOICE_COMMAND_3", LANG.DE, "Num 7 - Alarm");
registerTranslation("HELP_VIEW_VOICE_COMMAND_4", LANG.DE, "Num 6 - Hilfe");
registerTranslation("HELP_VIEW_VOICE_COMMAND_5", LANG.DE, "Num 5 - Beleidigung");
registerTranslation("HELP_VIEW_VOICE_COMMAND_6", LANG.DE, "Num 4 - Feind tot");
registerTranslation("HELP_VIEW_COMMANDS", LANG.DE, "Chat Befehle");
registerTranslation("HELP_VIEW_COMMANDS_PM", LANG.DE, "Private Nachricht - /pw player_id text Beispiel: /pw 5 hallo");
registerTranslation("STATS_VIEW_TITLE", LANG.DE, "Spieler Statistiken");
registerTranslation("STATS_VIEW_CHARACTER", LANG.DE, "Charakter");
registerTranslation("STATS_VIEW_ATRIBUTES", LANG.DE, "Attribute");
registerTranslation("STATS_VIEW_PROTECTION", LANG.DE, "Charakterschutz");
registerTranslation("STATS_VIEW_SKILLS", LANG.DE, "Fertigkeiten");
registerTranslation("STATS_VIEW_GUILD", LANG.DE, "Fraktion");

registerTranslation("STATUS_VIEW_TARGET", LANG.DE, "Runden Ziel");
registerTranslation("STATUS_VIEW_BEGIN", LANG.DE, "Die Runde beginnt in");
registerTranslation("STATUS_VIEW_NO_ROUND", LANG.DE, "Es gibt keine runde");
registerTranslation("STATUS_VIEW_BONUS_EMPTY", LANG.DE, "Bonus exp");
registerTranslation("STATUS_VIEW_ROUND_BONUS", LANG.DE, "Bonus exp: %.2fx");
registerTranslation("STATUS_VIEW_KILL", LANG.DE, "%s wurde get�tet von %s(%d)!");
registerTranslation("STATUS_VIEW_STREAK", LANG.DE, "Spieler %s hat %d Spieler get�tet, indem er Spieler %s get�tet hat!");
registerTranslation("STATUS_VIEW_LEVEL_THREE", LANG.DE, "Spieler %s hat Level 3 erreicht!");
registerTranslation("STATUS_VIEW_BOSS_KILL", LANG.DE, "%s wurde get�tet von %s!");
registerTranslation("STATUS_VIEW_BOSS_RESPAWN", LANG.DE, "Boss %s ist auf der Karte erschienen!");
registerTranslation("STATUS_VIEW_KILLS", LANG.DE, "Kills");
registerTranslation("STATUS_VIEW_DEATHS", LANG.DE, "Deaths");
registerTranslation("STATUS_VIEW_ASSISTS", LANG.DE, "Assists");
registerTranslation("STATUS_VIEW_ENTER_CAMP", LANG.DE, "Heile dich kostenlos beim Runenh�ndler!");

/********************* PLAYERLIST VIEW *********************/

registerTranslation("PLAYER_LIST_VIEW_TITLE", LANG.DE, "Spielerliste");
registerTranslation("PLAYER_LIST_VIEW_ONLINE", LANG.DE, "Spieler Online");
registerTranslation("PLAYER_LIST_VIEW_MEMBERS", LANG.DE, "Spieler im Team");
registerTranslation("PLAYER_LIST_VIEW_ID", LANG.DE, "Id");
registerTranslation("PLAYER_LIST_VIEW_NICKNAME", LANG.DE, "Nickname");
registerTranslation("PLAYER_LIST_VIEW_PING", LANG.DE, "Ping");
registerTranslation("PLAYER_LIST_VIEW_KILLS", LANG.DE, "Kills");
registerTranslation("PLAYER_LIST_VIEW_DEATHS", LANG.DE, "Deaths");
registerTranslation("PLAYER_LIST_VIEW_ASSISTS", LANG.DE, "Assists");

/********************* RANKLIST VIEW *********************/

registerTranslation("RANK_LIST_VIEW_NUMBER", LANG.DE, "No");
registerTranslation("RANK_LIST_VIEW_TITLE", LANG.DE, "Rangliste");
registerTranslation("RANK_LIST_VIEW_NICKNAME", LANG.DE, "Nickname");
registerTranslation("RANK_LIST_VIEW_KILLS", LANG.DE, "Kills");
registerTranslation("RANK_LIST_VIEW_DEATHS", LANG.DE, "Deaths");
registerTranslation("RANK_LIST_VIEW_ASSISTS", LANG.DE, "Assists");
registerTranslation("RANK_LIST_VIEW_KDA", LANG.DE, "Score");
registerTranslation("RANK_LIST_VIEW_DMG", LANG.DE, "Damage");

/********************* STATS *********************/

registerTranslation("LEVEL", LANG.DE, "Level");
registerTranslation("EXPERIENCE", LANG.DE, "Erfahrung");
registerTranslation("EXPERIENCE_NEXT_LEVEL", LANG.DE, "N�chstes Level");
registerTranslation("LEARN_POINTS", LANG.DE, "Lernpunkte");
registerTranslation("STRENGTH", LANG.DE, "St�rke");
registerTranslation("DEXTERITY", LANG.DE, "Geschick");
registerTranslation("MANA", LANG.DE, "Mana");
registerTranslation("HEALTH", LANG.DE, "Lebenspunkte");
registerTranslation("MAGIC_LEVEL", LANG.DE, "Magiekreis");
registerTranslation("GUILD_NAME", LANG.DE, "Fraktionsname");

/********************* MULTIUSE *********************/

registerTranslation("REQUIREMENT_TYPE_0", LANG.DE, "Level");
registerTranslation("REQUIREMENT_TYPE_1", LANG.DE, "Magiekreis");
registerTranslation("REQUIREMENT_TYPE_2", LANG.DE, "St�rke");
registerTranslation("REQUIREMENT_TYPE_3", LANG.DE, "Geschick");
registerTranslation("REQUIREMENT_TYPE_4", LANG.DE, "Mana");
registerTranslation("REQUIREMENT_TYPE_5", LANG.DE, "Lebenspunkte");

registerTranslation("DAMAGE_TYPE_2", LANG.DE, "Stumpfer Schaden");
registerTranslation("DAMAGE_TYPE_4", LANG.DE, "Waffen Schaden");
registerTranslation("DAMAGE_TYPE_64", LANG.DE, "Pfeil Schaden");
registerTranslation("DAMAGE_TYPE_8", LANG.DE, "Feuer Schaden");
registerTranslation("DAMAGE_TYPE_32", LANG.DE, "Magie Schaden");

registerTranslation("WEAPON_MODE_3", LANG.DE, "Einhandwaffen");
registerTranslation("WEAPON_MODE_4", LANG.DE, "Zweihandwaffen");
registerTranslation("WEAPON_MODE_5", LANG.DE, "B�gen");
registerTranslation("WEAPON_MODE_6", LANG.DE, "Armbr�ste");
registerTranslation("WEAPON_MODE_7", LANG.DE, "Magie");

registerTranslation("WEAPON_SKILL_0", LANG.DE, "Einhandwaffen");
registerTranslation("WEAPON_SKILL_1", LANG.DE, "Zweihandwaffen");
registerTranslation("WEAPON_SKILL_2", LANG.DE, "B�gen");
registerTranslation("WEAPON_SKILL_3", LANG.DE, "Armbr�ste");

registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthRecovery, LANG.DE, "Stellt Lebenspunkte wieder her");
registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthPercentRecovery, LANG.DE, "Stellt Lebenspunkte wieder her (Prozent)");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaRecovery, LANG.DE, "Stellt Mana wieder her");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaPercentRecovery, LANG.DE, "Stellt Mana wieder her (Prozent)");

/********************* ANIMS *********************/

registerTranslation("T_STAND_2_SIT", LANG.DE, "Sitzen");
registerTranslation("T_STAND_2_SLEEPGROUND", LANG.DE, "Schlafen");
registerTranslation("T_STAND_2_PEE", LANG.DE, "Pee");
registerTranslation("S_PRAY", LANG.DE, "Beten");
registerTranslation("S_DEAD", LANG.DE, "Tod");
registerTranslation("S_LGUARD", LANG.DE, "Wache 1");
registerTranslation("S_HGUARD", LANG.DE, "Wache 2");
registerTranslation("T_SEARCH", LANG.DE, "Umschauen");
registerTranslation("T_PLUNDER", LANG.DE, "Pl�ndern");
registerTranslation("S_WASH", LANG.DE, "Waschen");
registerTranslation("R_SCRATCHEGG", LANG.DE, "Sackkratzen");
registerTranslation("S_FIRE_VICTIM", LANG.DE, "Tanzen");
registerTranslation("T_1HSINSPECT", LANG.DE, "Waffe pr�fen");
registerTranslation("T_GREETNOV", LANG.DE, "Verbeugen");
registerTranslation("T_1HSFREE", LANG.DE, "Schwertausbildung");
registerTranslation("T_WATCHFIGHT_OHNO", LANG.DE, "Anfeuern");
registerTranslation("T_YES", LANG.DE, "Ja");
registerTranslation("T_NO", LANG.DE, "Nein");
registerTranslation("R_SCRATCHHEAD", LANG.DE, "Kratzen");
registerTranslation("T_BORINGKICK", LANG.DE, "Kicken");

/********************* GUILDS *********************/

registerTranslation("OLD_CAMP", LANG.DE, "Altes Lager");
registerTranslation("SWAMP_CAMP", LANG.DE, "Sumpf Lager");
registerTranslation("NEW_CAMP", LANG.DE, "Neues Lager");
registerTranslation("PIRATE_CAMP", LANG.DE, "Piratenlager");
registerTranslation("BANDIT_CAMP", LANG.DE, "Banditenlager");