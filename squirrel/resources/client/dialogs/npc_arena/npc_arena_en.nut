
registerTranslation("NPC_ARENA_INITIAL_0", LANG.EN, "Hello?");
registerTranslation("NPC_ARENA_INITIAL_1", LANG.EN, "Welcome...");

registerTranslation("NPC_ARENA_BACK_TO_WORLD_TITLE", LANG.EN, "I want to get back to the main map...");

registerTranslation("NPC_ARENA_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_ARENA_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_ARENA_END_1", LANG.EN, "See you later...");