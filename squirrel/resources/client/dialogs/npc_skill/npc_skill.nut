
registerDialog(Dialog.Transcription("NPC_SKILL_INITIAL")
    .addDialog("NPC_SKILL_INITIAL_0", true, 2500)
    .addDialog("NPC_SKILL_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_END")
    .setTitle("NPC_SKILL_END_TITLE")
    .addDialog("NPC_SKILL_END_0" , true, 2500)
    .addDialog("NPC_SKILL_END_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_TRAINING_GROUND")
    .setTitle("NPC_SKILL_TRAINING_GROUND_TITLE")
);

/********************* LIST 1H *********************/

registerDialog(Dialog.Transcription("NPC_SKILL_1H_INITIAL")
    .setTitle("NPC_SKILL_1H_INITIAL_TITLE")
    .addDialog("NPC_SKILL_1H_INITIAL_0", true, 2500)
    .addDialog("NPC_SKILL_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_1H_FIVE_POINT")
    .setTitle("NPC_SKILL_1H_FIVE_POINT_TITLE")
    .addDialog("NPC_SKILL_1H_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_1H_FIVE_POINT_CANT")
    .setTitle("NPC_SKILL_1H_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_1H_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_1H_TEN_POINT")
    .setTitle("NPC_SKILL_1H_TEN_POINT_TITLE")
    .addDialog("NPC_SKILL_1H_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_1H_TEN_POINT_CANT")
    .setTitle("NPC_SKILL_1H_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_1H_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_1H_END")
    .setTitle("NPC_SKILL_1H_END_TITLE")
    .addDialog("NPC_SKILL_1H_END_0" , true, 2500)
    .addDialog("NPC_SKILL_1H_END_1", false, 2500)
);

/********************* LIST 2H *********************/

registerDialog(Dialog.Transcription("NPC_SKILL_2H_INITIAL")
    .setTitle("NPC_SKILL_2H_INITIAL_TITLE")
    .addDialog("NPC_SKILL_2H_INITIAL_0", true, 2500)
    .addDialog("NPC_SKILL_2H_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_2H_FIVE_POINT")
    .setTitle("NPC_SKILL_2H_FIVE_POINT_TITLE")
    .addDialog("NPC_SKILL_2H_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_2H_FIVE_POINT_CANT")
    .setTitle("NPC_SKILL_2H_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_2H_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_2H_TEN_POINT")
    .setTitle("NPC_SKILL_2H_TEN_POINT_TITLE")
    .addDialog("NPC_SKILL_2H_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_2H_TEN_POINT_CANT")
    .setTitle("NPC_SKILL_2H_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_2H_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_2H_END")
    .setTitle("NPC_SKILL_2H_END_TITLE")
    .addDialog("NPC_SKILL_2H_END_0" , true, 2500)
    .addDialog("NPC_SKILL_2H_END_1", false, 2500)
);

/********************* LIST BOW *********************/

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_INITIAL")
    .setTitle("NPC_SKILL_BOW_INITIAL_TITLE")
    .addDialog("NPC_SKILL_BOW_INITIAL_0", true, 2500)
    .addDialog("NPC_SKILL_BOW_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_FIVE_POINT")
    .setTitle("NPC_SKILL_BOW_FIVE_POINT_TITLE")
    .addDialog("NPC_SKILL_BOW_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_FIVE_POINT_CANT")
    .setTitle("NPC_SKILL_BOW_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_BOW_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_TEN_POINT")
    .setTitle("NPC_SKILL_BOW_TEN_POINT_TITLE")
    .addDialog("NPC_SKILL_BOW_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_TEN_POINT_CANT")
    .setTitle("NPC_SKILL_BOW_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_BOW_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_BOW_END")
    .setTitle("NPC_SKILL_BOW_END_TITLE")
    .addDialog("NPC_SKILL_BOW_END_0" , true, 2500)
    .addDialog("NPC_SKILL_BOW_END_1", false, 2500)
);

/********************* LIST CBOW *********************/

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_INITIAL")
    .setTitle("NPC_SKILL_CBOW_INITIAL_TITLE")
    .addDialog("NPC_SKILL_CBOW_INITIAL_0", true, 2500)
    .addDialog("NPC_SKILL_CBOW_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_FIVE_POINT")
    .setTitle("NPC_SKILL_CBOW_FIVE_POINT_TITLE")
    .addDialog("NPC_SKILL_CBOW_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_FIVE_POINT_CANT")
    .setTitle("NPC_SKILL_CBOW_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_CBOW_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_TEN_POINT")
    .setTitle("NPC_SKILL_CBOW_TEN_POINT_TITLE")
    .addDialog("NPC_SKILL_CBOW_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_TEN_POINT_CANT")
    .setTitle("NPC_SKILL_CBOW_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_SKILL_CBOW_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_SKILL_CBOW_END")
    .setTitle("NPC_SKILL_CBOW_END_TITLE")
    .addDialog("NPC_SKILL_CBOW_END_0" , true, 2500)
    .addDialog("NPC_SKILL_CBOW_END_1", false, 2500)
);

/********************* LIST STRENGTH *********************/

registerDialog(Dialog.Transcription("NPC_STRENGTH_INITIAL")
    .setTitle("NPC_STRENGTH_INITIAL_TITLE")
    .addDialog("NPC_STRENGTH_INITIAL_0", true, 2500)
    .addDialog("NPC_STRENGTH_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_STRENGTH_FIVE_POINT")
    .setTitle("NPC_STRENGTH_FIVE_POINT_TITLE")
    .addDialog("NPC_STRENGTH_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_STRENGTH_FIVE_POINT_CANT")
    .setTitle("NPC_STRENGTH_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_STRENGTH_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_STRENGTH_TEN_POINT")
    .setTitle("NPC_STRENGTH_TEN_POINT_TITLE")
    .addDialog("NPC_STRENGTH_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_STRENGTH_TEN_POINT_CANT")
    .setTitle("NPC_STRENGTH_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_STRENGTH_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_STRENGTH_END")
    .setTitle("NPC_STRENGTH_END_TITLE")
    .addDialog("NPC_STRENGTH_END_0" , true, 2500)
    .addDialog("NPC_STRENGTH_END_1", false, 2500)
);

/********************* LIST DEXTERITY *********************/

registerDialog(Dialog.Transcription("NPC_DEXTERITY_INITIAL")
    .setTitle("NPC_DEXTERITY_INITIAL_TITLE")
    .addDialog("NPC_DEXTERITY_INITIAL_0", true, 2500)
    .addDialog("NPC_DEXTERITY_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_DEXTERITY_FIVE_POINT")
    .setTitle("NPC_DEXTERITY_FIVE_POINT_TITLE")
    .addDialog("NPC_DEXTERITY_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_DEXTERITY_FIVE_POINT_CANT")
    .setTitle("NPC_DEXTERITY_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_DEXTERITY_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_DEXTERITY_TEN_POINT")
    .setTitle("NPC_DEXTERITY_TEN_POINT_TITLE")
    .addDialog("NPC_DEXTERITY_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_DEXTERITY_TEN_POINT_CANT")
    .setTitle("NPC_DEXTERITY_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_DEXTERITY_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_DEXTERITY_END")
    .setTitle("NPC_DEXTERITY_END_TITLE")
    .addDialog("NPC_DEXTERITY_END_0" , true, 2500)
    .addDialog("NPC_DEXTERITY_END_1", false, 2500)
);

/********************* LIST MANA *********************/

registerDialog(Dialog.Transcription("NPC_MANA_INITIAL")
    .setTitle("NPC_MANA_INITIAL_TITLE")
    .addDialog("NPC_MANA_INITIAL_0" , true, 2500)
    .addDialog("NPC_MANA_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MANA_FIVE_POINT")
    .setTitle("NPC_MANA_FIVE_POINT_TITLE")
    .addDialog("NPC_MANA_FIVE_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MANA_FIVE_POINT_CANT")
    .setTitle("NPC_MANA_FIVE_POINT_CANT_TITLE")
    .addDialog("NPC_MANA_FIVE_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MANA_TEN_POINT")
    .setTitle("NPC_MANA_TEN_POINT_TITLE")
    .addDialog("NPC_MANA_TEN_POINT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MANA_TEN_POINT_CANT")
    .setTitle("NPC_MANA_TEN_POINT_CANT_TITLE")
    .addDialog("NPC_MANA_TEN_POINT_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MANA_END")
    .setTitle("NPC_MANA_END_TITLE")
    .addDialog("NPC_MANA_END_0" , true, 2500)
    .addDialog("NPC_MANA_END_1", false, 2500)
);

/********************* LIST MAGIC CIRCLE *********************/

registerDialog(Dialog.Transcription("NPC_MAGIC_INITIAL")
    .setTitle("NPC_MAGIC_INITIAL_TITLE")
    .addDialog("NPC_MAGIC_INITIAL_0" , true, 2500)
    .addDialog("NPC_MAGIC_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MAGIC_ONE_LEVEL")
    .setTitle("NPC_MAGIC_ONE_LEVEL_TITLE")
    .addDialog("NPC_MAGIC_ONE_LEVEL_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MAGIC_ONE_LEVEL_CANT")
    .setTitle("NPC_MAGIC_ONE_LEVEL_CANT_TITLE")
    .addDialog("NPC_MAGIC_ONE_LEVEL_CANT_0", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_MAGIC_END")
    .setTitle("NPC_MAGIC_END_TITLE")
    .addDialog("NPC_MAGIC_END_0" , true, 2500)
    .addDialog("NPC_MAGIC_END_1", false, 2500)
);