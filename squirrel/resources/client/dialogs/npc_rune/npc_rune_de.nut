
registerTranslation("NPC_RUNE_INITIAL_0", LANG.DE, "Hallo?");
registerTranslation("NPC_RUNE_INITIAL_1", LANG.DE, "Hallo...");

registerTranslation("NPC_RUNE_INITIAL_0_TITLE", LANG.DE, "Heile mich...");
registerTranslation("NPC_RUNE_INITIAL_0_0", LANG.DE, "Heile mich...");
registerTranslation("NPC_RUNE_INITIAL_0_1", LANG.DE, "Im Namen der G�tter, du bist geheilt!");

registerTranslation("NPC_RUNE_INITIAL_1_TITLE", LANG.DE, "Zeig mir, deine Runen...");
registerTranslation("NPC_RUNE_INITIAL_1_0", LANG.DE, "Zeig mir, deine Runen...");
registerTranslation("NPC_RUNE_INITIAL_1_1", LANG.DE, "F�hl dich frei alle zu kaufen...");

registerTranslation("NPC_RUNE_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_RUNE_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_RUNE_END_1", LANG.DE, "Bis zum n�chsten Mal...");