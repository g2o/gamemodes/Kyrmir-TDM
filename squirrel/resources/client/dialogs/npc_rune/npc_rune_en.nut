
registerTranslation("NPC_RUNE_INITIAL_0", LANG.EN, "Hey?");
registerTranslation("NPC_RUNE_INITIAL_1", LANG.EN, "Hello...");

registerTranslation("NPC_RUNE_INITIAL_0_TITLE", LANG.EN, "Heal me...");
registerTranslation("NPC_RUNE_INITIAL_0_0", LANG.EN, "Heal me...");
registerTranslation("NPC_RUNE_INITIAL_0_1", LANG.EN, "In the name of Gods, you are healed!");

registerTranslation("NPC_RUNE_INITIAL_1_TITLE", LANG.EN, "Show me what you have...");
registerTranslation("NPC_RUNE_INITIAL_1_0", LANG.EN, "Show me what you have...");
registerTranslation("NPC_RUNE_INITIAL_1_1", LANG.EN, "Feel free to buy...");

registerTranslation("NPC_RUNE_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_RUNE_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_RUNE_END_1", LANG.EN, "See you later...");