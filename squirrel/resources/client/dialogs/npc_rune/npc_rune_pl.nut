
registerTranslation("NPC_RUNE_INITIAL_0", LANG.PL, "Hej?");
registerTranslation("NPC_RUNE_INITIAL_1", LANG.PL, "Witaj...");

registerTranslation("NPC_RUNE_INITIAL_0_TITLE", LANG.PL, "Ulecz mnie...");
registerTranslation("NPC_RUNE_INITIAL_0_0", LANG.PL, "Ulecz mnie...");
registerTranslation("NPC_RUNE_INITIAL_0_1", LANG.PL, "W imi� Bog�w, zostajesz uleczony!");

registerTranslation("NPC_RUNE_INITIAL_1_TITLE", LANG.PL, "Poka� mi swoje towary...");
registerTranslation("NPC_RUNE_INITIAL_1_0", LANG.PL, "Poka� mi swoje towary...");
registerTranslation("NPC_RUNE_INITIAL_1_1", LANG.PL, "Oczywi�cie...");

registerTranslation("NPC_RUNE_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_RUNE_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_RUNE_END_1", LANG.PL, "Do zobaczenia...");