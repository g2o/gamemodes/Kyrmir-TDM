
/********************* MONSTERS *********************/

registerTranslation("KYRMIR_BLOODFLY", LANG.RU, "�������� ����");
registerTranslation("KYRMIR_BLOODHOUND", LANG.RU, "������");
registerTranslation("KYRMIR_GOBBO_BLACK", LANG.RU, "������ ������");
registerTranslation("KYRMIR_GOBBO_GREEN", LANG.RU, "������");
registerTranslation("KYRMIR_GOBBO_WARRIOR", LANG.RU, "������-����");
registerTranslation("KYRMIR_HARPIE", LANG.RU, "������");
registerTranslation("KYRMIR_LURKER", LANG.RU, "������");
registerTranslation("KYRMIR_MINECRAWLER", LANG.RU, "�������");
registerTranslation("KYRMIR_MINECRAWLERWARRIOR", LANG.RU, "�������-����");
registerTranslation("KYRMIR_MOLERAT", LANG.RU, "���������");
registerTranslation("KYRMIR_ORCBITER", LANG.RU, "�����");
registerTranslation("KYRMIR_ORCSHAMAN_SIT", LANG.RU, "���-�����");
registerTranslation("KYRMIR_ORCWARRIOR_ROAM", LANG.RU, "���-����");
registerTranslation("KYRMIR_RAZOR", LANG.RU, "���������");
registerTranslation("KYRMIR_SCAVENGER", LANG.RU, "���������");
registerTranslation("KYRMIR_SHADOWBEAST", LANG.RU, "��������");
registerTranslation("KYRMIR_SKELETON", LANG.RU, "������-����");
registerTranslation("KYRMIR_SNAPPER", LANG.RU, "�������");
registerTranslation("KYRMIR_SWAMPSHARK", LANG.RU, "�������� �����");
registerTranslation("KYRMIR_UNDEADORCWARRIOR", LANG.RU, "���-������");
registerTranslation("KYRMIR_WARAN", LANG.RU, "�������");
registerTranslation("KYRMIR_WARG", LANG.RU, "����");
registerTranslation("KYRMIR_WOLF", LANG.RU, "����");
registerTranslation("KYRMIR_BLATTCRAWLER", LANG.RU, "�������");
registerTranslation("KYRMIR_DRAGONSNAPPER", LANG.RU, "�������� �������");
registerTranslation("KYRMIR_GIANT_DESERTRAT", LANG.RU, "��������� �����");
registerTranslation("KYRMIR_GOBBO_SKELETON", LANG.RU, "������ �������");
registerTranslation("KYRMIR_GOBBO_WARRIOR_VISIR", LANG.RU, "������-����");
registerTranslation("KYRMIR_KEILER", LANG.RU, "�����");
registerTranslation("KYRMIR_MAYAZOMBIE03", LANG.RU, "������� �����");
registerTranslation("KYRMIR_ORCELITE_ROAM", LANG.RU, "������� ���");
registerTranslation("KYRMIR_SCAVENGER_DEMON", LANG.RU, "������� ���������");
registerTranslation("KYRMIR_SHADOWBEAST_ADDON_FIRE", LANG.RU, "�������� ������");
registerTranslation("KYRMIR_STONEGUARDIAN", LANG.RU, "�������� �����");
registerTranslation("KYRMIR_SWAMPDRONE", LANG.RU, "�������� ����");
registerTranslation("KYRMIR_SWAMPGOLEM", LANG.RU, "�������� �����");
registerTranslation("KYRMIR_SWAMPRAT", LANG.RU, "�������� �����");
registerTranslation("KYRMIR_TROLL", LANG.RU, "������");
registerTranslation("KYRMIR_ZOMBIE_ADDON_KNECHT", LANG.RU, "����� �����");
registerTranslation("KYRMIR_BOSS_BLACK", LANG.RU, "������ ������");
registerTranslation("KYRMIR_BOSS_GREEN", LANG.RU, "������� ������� ����");
registerTranslation("KYRMIR_BOSS_WHITE", LANG.RU, "����� ��������");
registerTranslation("KYRMIR_BOSS_RED", LANG.RU, "������� �����");
registerTranslation("KYRMIR_BOSS_BLUE", LANG.RU, "����� �����");
registerTranslation("KYRMIR_BOSS_VIOLET", LANG.RU, "���������� ������-���");
registerTranslation("KYRMIR_BOSS_YELLOW", LANG.RU, "������ �����-����");
registerTranslation("KYRMIR_ALEXIA", LANG.RU, "�������");
registerTranslation("KYRMIR_MINIBOSS_GREEN", LANG.RU, "������� ������");
registerTranslation("KYRMIR_MINIBOSS_RED", LANG.RU, "������� ����");
registerTranslation("KYRMIR_MINIBOSS_YELLOW", LANG.RU, "������ c������");
registerTranslation("KYRMIR_MINIBOSS_GRAY", LANG.RU, "����� ���������");
registerTranslation("KYRMIR_MINIBOSS_VIOLET", LANG.RU, "���������� �������-����");
registerTranslation("KYRMIR_MINIBOSS_BLUE", LANG.RU, "����� ���������");

/********************* NPCS *********************/

registerTranslation("NPC_ARMOR", LANG.RU, "�������� ���������");
registerTranslation("NPC_BOW", LANG.RU, "�������� ������");
registerTranslation("NPC_CROSSBOW", LANG.RU, "�������� ����������");
registerTranslation("NPC_POTION", LANG.RU, "�������� �������");
registerTranslation("NPC_RUNE", LANG.RU, "�������� ������");
registerTranslation("NPC_SKILL", LANG.RU, "�������");
registerTranslation("NPC_WEAPON_1H", LANG.RU, "�������� ���������� �������");
registerTranslation("NPC_WEAPON_2H", LANG.RU, "�������� ��������� �������");
registerTranslation("NPC_ARENA", LANG.RU, "��������");
