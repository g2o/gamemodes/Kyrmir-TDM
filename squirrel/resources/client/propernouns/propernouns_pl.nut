
/********************* MONSTERS *********************/

registerTranslation("KYRMIR_BLOODFLY", LANG.PL, "Krwiopijca");
registerTranslation("KYRMIR_BLOODHOUND", LANG.PL, "Krwawy Ogar");
registerTranslation("KYRMIR_GOBBO_BLACK", LANG.PL, "Czarny Goblin");
registerTranslation("KYRMIR_GOBBO_GREEN", LANG.PL, "Goblin");
registerTranslation("KYRMIR_GOBBO_WARRIOR", LANG.PL, "Goblin Wojownik");
registerTranslation("KYRMIR_HARPIE", LANG.PL, "Harpia");
registerTranslation("KYRMIR_LURKER", LANG.PL, "Topielec");
registerTranslation("KYRMIR_MINECRAWLER", LANG.PL, "Pe�zacz");
registerTranslation("KYRMIR_MINECRAWLERWARRIOR", LANG.PL, "Pe�zacz Wojownik");
registerTranslation("KYRMIR_MOLERAT", LANG.PL, "Kretoszczur");
registerTranslation("KYRMIR_ORCBITER", LANG.PL, "K�sacz");
registerTranslation("KYRMIR_ORCSHAMAN_SIT", LANG.PL, "Ork Szaman");
registerTranslation("KYRMIR_ORCWARRIOR_ROAM", LANG.PL, "Ork Wojownik");
registerTranslation("KYRMIR_RAZOR", LANG.PL, "Brzytwa");
registerTranslation("KYRMIR_SCAVENGER", LANG.PL, "�cierwojad");
registerTranslation("KYRMIR_SHADOWBEAST", LANG.PL, "Cieniostw�r");
registerTranslation("KYRMIR_SKELETON", LANG.PL, "Szkielet Wojownik");
registerTranslation("KYRMIR_SNAPPER", LANG.PL, "Z�bacz");
registerTranslation("KYRMIR_SWAMPSHARK", LANG.PL, "W�� b�otny");
registerTranslation("KYRMIR_UNDEADORCWARRIOR", LANG.PL, "Nieumar�y Ork");
registerTranslation("KYRMIR_WARAN", LANG.PL, "Jaszczur");
registerTranslation("KYRMIR_WARG", LANG.PL, "Warg");
registerTranslation("KYRMIR_WOLF", LANG.PL, "Wilk");
registerTranslation("KYRMIR_BLATTCRAWLER", LANG.PL, "Polny Pe�zacz");
registerTranslation("KYRMIR_DRAGONSNAPPER", LANG.PL, "Smoczy Z�bacz");
registerTranslation("KYRMIR_GIANT_DESERTRAT", LANG.PL, "Pustynny Szczur");
registerTranslation("KYRMIR_GOBBO_SKELETON", LANG.PL, "Goblin Szkielet");
registerTranslation("KYRMIR_GOBBO_WARRIOR_VISIR", LANG.PL, "Goblin Wojownik");
registerTranslation("KYRMIR_KEILER", LANG.PL, "Dzik");
registerTranslation("KYRMIR_MAYAZOMBIE03", LANG.PL, "Pradawny Zombie");
registerTranslation("KYRMIR_ORCELITE_ROAM", LANG.PL, "Ork Elita");
registerTranslation("KYRMIR_SCAVENGER_DEMON", LANG.PL, "Preriowy �cierwojad");
registerTranslation("KYRMIR_SHADOWBEAST_ADDON_FIRE", LANG.PL, "Ognisty Demon");
registerTranslation("KYRMIR_STONEGUARDIAN", LANG.PL, "Kamienny Stra�nik");
registerTranslation("KYRMIR_SWAMPDRONE", LANG.PL, "Bagienny Trute�");
registerTranslation("KYRMIR_SWAMPGOLEM", LANG.PL, "Bagienny Golem");
registerTranslation("KYRMIR_SWAMPRAT", LANG.PL, "Bagienny Szczur");
registerTranslation("KYRMIR_TROLL", LANG.PL, "Troll");
registerTranslation("KYRMIR_ZOMBIE_ADDON_KNECHT", LANG.PL, "Zombie Stra�nik");
registerTranslation("KYRMIR_BOSS_BLACK", LANG.PL, "Czarny Troll");
registerTranslation("KYRMIR_BOSS_GREEN", LANG.PL, "Zielony Szkielet Paladyn");
registerTranslation("KYRMIR_BOSS_WHITE", LANG.PL, "Bia�y Cieniostw�r");
registerTranslation("KYRMIR_BOSS_RED", LANG.PL, "Czerwony Zombie");
registerTranslation("KYRMIR_BOSS_BLUE", LANG.PL, "Niebieski Golem");
registerTranslation("KYRMIR_BOSS_VIOLET", LANG.PL, "Fioletowy Szkielet Mag");
registerTranslation("KYRMIR_BOSS_YELLOW", LANG.PL, "��ty Ksi��� Demon�w");
registerTranslation("KYRMIR_ALEXIA", LANG.PL, "Alexia");
registerTranslation("KYRMIR_MINIBOSS_GREEN", LANG.PL, "Zielony Topielec");
registerTranslation("KYRMIR_MINIBOSS_RED", LANG.PL, "Czerwony Warg");
registerTranslation("KYRMIR_MINIBOSS_YELLOW", LANG.PL, "��ty Z�bacz");
registerTranslation("KYRMIR_MINIBOSS_GRAY", LANG.PL, "Szara Brzytwa");
registerTranslation("KYRMIR_MINIBOSS_VIOLET", LANG.PL, "Fioletowy Pe�zacz Wojownik");
registerTranslation("KYRMIR_MINIBOSS_BLUE", LANG.PL, "Niebieski �cierwojad");

/********************* NPCS *********************/

registerTranslation("NPC_ARMOR", LANG.PL, "Pancerze");
registerTranslation("NPC_BOW", LANG.PL, "�uki");
registerTranslation("NPC_CROSSBOW", LANG.PL, "Kusze");
registerTranslation("NPC_POTION", LANG.PL, "Mikstury");
registerTranslation("NPC_RUNE", LANG.PL, "Runy");
registerTranslation("NPC_SKILL", LANG.PL, "Nauczyciel");
registerTranslation("NPC_WEAPON_1H", LANG.PL, "Bro� jednor�czna");
registerTranslation("NPC_WEAPON_2H", LANG.PL, "Bro� dwur�czna");
registerTranslation("NPC_ARENA", LANG.PL, "Mistrz Areny");

