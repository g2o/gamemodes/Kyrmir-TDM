
/********************* ITAR *********************/

registerTranslation("ITAR_SO_01", LANG.RU, "����� ��������");
registerTranslation("ITAR_SO_02", LANG.RU, "˸���� o������ ��������");
registerTranslation("ITAR_SO_03", LANG.RU, "������� ����� ��������");
registerTranslation("ITAR_SO_04", LANG.RU, "˸���� ������� ���������");
registerTranslation("ITAR_SO_05", LANG.RU, "������� ����� ���������");
registerTranslation("ITAR_SO_06", LANG.RU, "������ ������� ���������");
registerTranslation("ITAR_SO_07", LANG.RU, "���� ���������� o���");
registerTranslation("ITAR_SO_08", LANG.RU, "��������� ����� ����");
registerTranslation("ITAR_SO_09", LANG.RU, "������������� �����");
registerTranslation("ITAR_SO_10", LANG.RU, "������������� �����");
registerTranslation("ITAR_NO_01", LANG.RU, "����� ����");
registerTranslation("ITAR_NO_02", LANG.RU, "˸���� ������� ����");
registerTranslation("ITAR_NO_03", LANG.RU, "������� ������� ����");
registerTranslation("ITAR_NO_04", LANG.RU, "˸���� ������� �������");
registerTranslation("ITAR_NO_05", LANG.RU, "������� ������� ��������");
registerTranslation("ITAR_NO_06", LANG.RU, "������� ������� ��������");
registerTranslation("ITAR_NO_07", LANG.RU, "���� ���������� ����");
registerTranslation("ITAR_NO_08", LANG.RU, "��������� ����� ����");
registerTranslation("ITAR_NO_09", LANG.RU, "������������� �����");
registerTranslation("ITAR_NO_10", LANG.RU, "������������� �����");
registerTranslation("ITAR_ONB_01", LANG.RU, "����������� ������� ����������");
registerTranslation("ITAR_ONB_02", LANG.RU, "������ ������� ����������");
registerTranslation("ITAR_ONB_03", LANG.RU, "������� ������� ����������");
registerTranslation("ITAR_ONB_04", LANG.RU, "������ ������� c�����");
registerTranslation("ITAR_ONB_05", LANG.RU, "������� ����� ���������");
registerTranslation("ITAR_ONB_06", LANG.RU, "������� ������� c�����");
registerTranslation("ITAR_ONB_07", LANG.RU, "��������� ����");
registerTranslation("ITAR_ONB_08", LANG.RU, "��������� ������ ����");
registerTranslation("ITAR_ONB_09", LANG.RU, "������������� �����");
registerTranslation("ITAR_ONB_10", LANG.RU, "������������� �����");
registerTranslation("ITAR_PIR_01" LANG.RU, "����� ������");
registerTranslation("ITAR_PIR_02" LANG.RU, "������ ������");
registerTranslation("ITAR_PIR_03" LANG.RU, "������ ��������� �������");
registerTranslation("ITAR_PIR_04" LANG.RU, "������� ��������� �������");
registerTranslation("ITAR_PIR_05" LANG.RU, "������� ��������� �������");
registerTranslation("ITAR_PIR_06" LANG.RU, "������� ���������� ������");
registerTranslation("ITAR_PIR_07" LANG.RU, "���� ���������� ����be");
registerTranslation("ITAR_PIR_08" LANG.RU, "��������� ����� ����");
registerTranslation("ITAR_PIR_09" LANG.RU, "������������� �����");
registerTranslation("ITAR_PIR_10" LANG.RU, "������������� �����");
registerTranslation("ITAR_BDT_01" LANG.RU, "����� ��������");
registerTranslation("ITAR_BDT_02" LANG.RU, "������� �������");
registerTranslation("ITAR_BDT_03" LANG.RU, "������ ������� ����������");
registerTranslation("ITAR_BDT_04" LANG.RU, "������� ������� ����������");
registerTranslation("ITAR_BDT_05" LANG.RU, "������� ������������ ������");
registerTranslation("ITAR_BDT_06" LANG.RU, "������� ����� ����������");
registerTranslation("ITAR_BDT_07" LANG.RU, "������ �������");
registerTranslation("ITAR_BDT_08" LANG.RU, "������ ����������");
registerTranslation("ITAR_BDT_09" LANG.RU, "������������� �����");
registerTranslation("ITAR_BDT_10" LANG.RU, "������������� �����");

/********************* ITMW *********************/

registerTranslation("ITMW_1H_01", LANG.RU, "������ ���");
registerTranslation("ITMW_1H_02", LANG.RU, "������");
registerTranslation("ITMW_1H_03", LANG.RU, "������ �������� ���");
registerTranslation("ITMW_1H_04", LANG.RU, "������ ������ � ������");
registerTranslation("ITMW_1H_05", LANG.RU, "������ ���");
registerTranslation("ITMW_1H_06", LANG.RU, "����������� �����");
registerTranslation("ITMW_1H_07", LANG.RU, "��� ��������");
registerTranslation("ITMW_1H_08", LANG.RU, "������� ���");
registerTranslation("ITMW_1H_09", LANG.RU, "���������� ������� ���");
registerTranslation("ITMW_1H_10", LANG.RU, "���������� ���������� ���");
registerTranslation("ITMW_1H_11", LANG.RU, "������ ������ '������ ��������'");
registerTranslation("ITMW_1H_12", LANG.RU, "������ �������");
registerTranslation("ITMW_2H_01", LANG.RU, "������ ��������� ���");
registerTranslation("ITMW_2H_02", LANG.RU, "����� �����");
registerTranslation("ITMW_2H_03", LANG.RU, "˸���� ��������� ���");
registerTranslation("ITMW_2H_04", LANG.RU, "��������� ��� ��������");
registerTranslation("ITMW_2H_05", LANG.RU, "���� ���");
registerTranslation("ITMW_2H_06", LANG.RU, "������ ��������� ���");
registerTranslation("ITMW_2H_07", LANG.RU, "����� ��������");
registerTranslation("ITMW_2H_08", LANG.RU, "������ �����");
registerTranslation("ITMW_2H_09", LANG.RU, "������� ����");
registerTranslation("ITMW_2H_10", LANG.RU, "������� ������ ������ '������ ��������'");
registerTranslation("ITMW_2H_11", LANG.RU, "��� �������");

/********************* ITRW *********************/

registerTranslation("ITRW_BOW_ARROW", LANG.RU, "������");
registerTranslation("ITRW_CROSSBOW_BOLT", LANG.RU, "����");
registerTranslation("ITRW_MAGICBOW_ARROW", LANG.RU, "M��������� ������");
registerTranslation("ITRW_MAGICCROSSBOW_BOLT", LANG.RU, "M��������� ����");
registerTranslation("ITRW_BOW_01", LANG.RU, "�������� ���");
registerTranslation("ITRW_BOW_02", LANG.RU, "������ ���");
registerTranslation("ITRW_BOW_03", LANG.RU, "��������� ���");
registerTranslation("ITRW_BOW_04", LANG.RU, "������� ���");
registerTranslation("ITRW_BOW_05", LANG.RU, "����������� ���");
registerTranslation("ITRW_BOW_06", LANG.RU, "�������� ���");
registerTranslation("ITRW_BOW_07", LANG.RU, "������� ���");
registerTranslation("ITRW_BOW_08", LANG.RU, "������� ���");
registerTranslation("ITRW_BOW_09", LANG.RU, "�������� ���");
registerTranslation("ITRW_BOW_10", LANG.RU, "������� ���");
registerTranslation("ITRW_BOW_11", LANG.RU, "���������� ���");
registerTranslation("ITRW_CROSSBOW_01", LANG.RU, "������ �������");
registerTranslation("ITRW_CROSSBOW_02", LANG.RU, "������ �������");
registerTranslation("ITRW_CROSSBOW_03", LANG.RU, "������� �������");
registerTranslation("ITRW_CROSSBOW_04", LANG.RU, "��������� �������");
registerTranslation("ITRW_CROSSBOW_05", LANG.RU, "������ �������");
registerTranslation("ITRW_CROSSBOW_06", LANG.RU, "��������� �������");
registerTranslation("ITRW_CROSSBOW_07", LANG.RU, "������� �������");
registerTranslation("ITRW_CROSSBOW_08", LANG.RU, "������ �������");
registerTranslation("ITRW_CROSSBOW_09", LANG.RU, "������� ���������");
registerTranslation("ITRW_CROSSBOW_10", LANG.RU, "������� �������� �� ��������");
registerTranslation("ITRW_CROSSBOW_11", LANG.RU, "���������� �������");

/********************* ITRU *********************/

registerTranslation("ITRU_FIREBOLT", LANG.RU, "�������� �����");
registerTranslation("ITRU_ICEBOLT", LANG.RU, "������� ������");
registerTranslation("ITRU_LIGHTHEAL", LANG.RU, "������� ����� �������");
registerTranslation("ITRU_ZAP", LANG.RU, "����� ������");
registerTranslation("ITRU_MEDIUMHEAL", LANG.RU, "������� ������� �������");
registerTranslation("ITRU_ICELANCE", LANG.RU, "������� �����");
registerTranslation("ITRU_HEAL_TARGET", LANG.RU, "��������� ������");
registerTranslation("ITRU_HEAL_TARGET_DESC", LANG.RU, "�������� ������� ������ (100 hp)");
registerTranslation("ITRU_FIRESTORM", LANG.RU, "����� �������� ����");
registerTranslation("ITRU_FULLHEAL", LANG.RU, "������� ������� �������");
registerTranslation("ITRU_INSTANTFIREBALL", LANG.RU, "�������� ���");
registerTranslation("ITRU_LIGHTNINGFLASH", LANG.RU, "������ ������");

/********************* ITSC *********************/

registerTranslation("ITSC_MASSDEATH", LANG.RU, "����� ������");

/********************* ITPO *********************/

registerTranslation("ITPO_HEALTH_00", LANG.RU, "����� �������");
registerTranslation("ITPO_HEALTH_01", LANG.RU, "����� ����� ���������");
registerTranslation("ITPO_HEALTH_02", LANG.RU, "������� ����� ���������");
registerTranslation("ITPO_HEALTH_03", LANG.RU, "������� ����� ���������");
registerTranslation("ITPO_HEALTH_04", LANG.RU, "������� ����� ���������");
registerTranslation("ITPO_MANA_01", LANG.RU, "����� ����� ����");
registerTranslation("ITPO_MANA_02", LANG.RU, "������� ����� ����");
registerTranslation("ITPO_MANA_03", LANG.RU, "������� ����� ����");
registerTranslation("ITPO_SPEED", LANG.RU, "����� ���������");
registerTranslation("ITPO_SPEED_EFFECT", LANG.RU, "������ � ������� 30 ������");

/********************* ITMI *********************/

registerTranslation("ITMI_GOLD", LANG.RU, "������");
registerTranslation("ITMI_GOLD_DESC", LANG.RU, "��� ������ ������� ������...");
registerTranslation("ITMI_FOCUS_RED", LANG.RU, "������� ������������ ������");
registerTranslation("ITMI_FOCUS_RED_DESC", LANG.RU, "�����: 50 �������� - �������� �� ����������, ����� �� ��������");
registerTranslation("ITMI_FOCUS_YELLOW", LANG.RU, "������ ������������ ������");
registerTranslation("ITMI_FOCUS_YELLOW_DESC", LANG.RU, "�����: 10 ���� - �������� �� ����������, ����� �� ��������");
registerTranslation("ITMI_FOCUS_GRAY", LANG.RU, "����� ������ �����������");
registerTranslation("ITMI_FOCUS_GRAY_DESC", LANG.RU, "����������� � ���������� - ��� ����������� ����������� Z - �������� �� ���������� ����� ������");
registerTranslation("ITMI_FOCUS_VIOLET", LANG.RU, "���������� ������ �����������");
registerTranslation("ITMI_FOCUS_VIOLET_DESC", LANG.RU, "����������� � ��������-����� - ��� ����������� ����������� Z - �������� �� ���������� ����� ������");
registerTranslation("ITMI_FOCUS_BLUE", LANG.RU, "����� ������������ ������");
registerTranslation("ITMI_FOCUS_BLUE_DESC", LANG.RU, "�����: 20 ���� - �������� �� ����������, ����� �� ��������");
registerTranslation("ITMI_FOCUS_GREEN", LANG.RU, "������� ������������ ������");
registerTranslation("ITMI_FOCUS_GREEN_DESC", LANG.RU, "�����: 10 �������� - �������� �� ����������, ����� �� ��������");
